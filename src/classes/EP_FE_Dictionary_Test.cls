@istest
/*********************************************************************************************
    @Author <Soumya Raj>
    @name <testDictionaryData>
    @CreateDate <>
    @Description  <   >  
    @Version <1.0>
*********************************************************************************************/
Private class EP_FE_Dictionary_Test{
/*********************************************************************************************
    @Author <Soumya Raj>
    @name <testDictionaryData>
    @CreateDate <>
    @Description  <   >  
    @Version <1.0>
*********************************************************************************************/    
static testMethod  void testDictionaryData(){
      Test.startTest();     
      EP_FE_BootstrapResponse resp =new EP_FE_BootstrapResponse(); 
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();
      req.requestURI = '/FE/V1/Dictionary/*';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      system.debug('Before doGet()');
      resp =EP_FE_Dictionary.doGet();
      Test.stopTest();     
    }
/*********************************************************************************************
    @Author <Soumya Raj>
    @name <testDictionary>
    @CreateDate <>
    @Description  <   >  
    @Version <1.0>
*********************************************************************************************/     
static testMethod  void testDictionary(){
      Test.startTest();   
      EP_FE_BootstrapResponse resp =new EP_FE_BootstrapResponse(); 
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();
      req.requestURI = '/FE/V1/Dictionary/*';  //Request URL
      req.addParameter('content','dictionary');
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      system.debug('Before doGet()');
      resp =EP_FE_Dictionary.doGet();
      Test.stopTest(); 
          
    }

}