/**
*  @Author <Accenture>
*  @Name <EP_ManageLoadPlanDocument>
*  @CreateDate <09/05/2018>
*  @Description <Class for Load Plan Document>
*  @Version <1.0>
*/
public with sharing class EP_ManageLoadPlanDocument{
	static map<string,List<csord__Order__c>> tripIdOrderMap;
	static list<EP_AttachmentNotificationService.attachmentWrapper> attachmentWrapperList;
	/**
    *  @author <Accenture>
    *  @name <generateAttachmentForOrderLoadPlan>
    *  @createDate <09/05/2018>
    *  @description <Method to generate Load Plan Attachment obj and insert it against approiate order records>
	*  @Parameter EP_ManageDocument.Documents
    *  @return NA
    */
	@TestVisible
	public static void generateAttachmentForOrderLoadPlan(EP_ManageDocument.Documents documents){
    EP_GeneralUtility.Log('Public','EP_ManageLoadPlanDocument','generateAttachmentForOrderLoadPlan');
		List<Attachment> attachments = new List<Attachment>();
		List<String> tripIds = getTripIds(documents);
		tripIdOrderMap = getOrdersAsscociatedWithTrip(tripIds);
		attachments = createAttachmentRecords(documents);
		if(!attachments.isEmpty()){
			EP_DocumentUtil.deleteAttachmentsByName(attachments);
			insertAttachments(attachments);
		}
	}
	/**
		*  @author <Accenture>
		*  @name <createAttachmentRecords>
		*  @createDate <09/05/2018>
		*  @description <Method to insert Load Plan Attachments>
		*  @Parameter EP_ManageDocument.Documents
		*  @return list<Attachment>
		*/
	private static list<Attachment> createAttachmentRecords(EP_ManageDocument.Documents documents){
		List<Attachment> attachmentList = new List<Attachment>();
		attachmentWrapperList	= new list <EP_AttachmentNotificationService.attachmentWrapper>();
		for(EP_ManageDocument.Document doc : documents.document) {
			LoadPlanWrapper loadPlanWrapper = EP_DocumentUtil.fillLoadPlanWrapper(doc.documentMetaData.metaDatas);
			string tripId = loadPlanWrapper.tripId;
			if(tripIdOrderMap.containsKey(tripId)){
				for(csord__Order__c orderId : tripIdOrderMap.get(tripId)){
					Attachment attachmentObject = EP_DocumentUtil.generateAttachment(doc);
					attachmentObject.ParentId = orderId.Id;
					attachmentList.add(attachmentObject);
					attachmentWrapperList.add(new EP_AttachmentNotificationService.attachmentWrapper(attachmentObject ,
												orderId.csord__Account__c, EP_Common_Constant.LOADPLAN_CLASS));
				}
      }else{
      	EP_ManageDocument.errDesMap.put(tripId,label.EP_No_Order_for_TripId + tripId);
				EP_ManageDocument.errCodeMap.put(tripId,label.EP_No_Order_for_TripId + tripId);
      }
		}
		return attachmentList;
	}
	/**
    *  @author <Accenture>
    *  @name <insertAttachments>
    *  @createDate <09/05/2018>
    *  @description <Method to insert Load Plan Attachments>
	  *  @Parameter List<Attachment>
    *  @return NA
    */
	@TestVisible
	private static void insertAttachments(List<Attachment> attachments){
        EP_GeneralUtility.Log('Public','EP_ManageLoadPlanDocument','insertAttachments');
		Database.SaveResult[] results = Database.insert(attachments, false);
		for(Integer i = 0; i < results.size(); i++){
			if(results[i].isSuccess()) {
				// Operation was successful, so get the ID of the record that was processed
				EP_ManageDocument.successRecIdMap.put(attachments[i].Name, results[i].getId());
				//Trigger email notification after successfull insert
				EP_AttachmentNotificationService.sendAttachmentNotification(attachmentWrapperList);
			}else{
				// Operation failed, so get all errors
				for(Database.Error error : results[i].getErrors()){
					EP_ManageDocument.errDesMap.put(attachments[i].Name, error.getMessage());
					EP_ManageDocument.errCodeMap.put(attachments[i].Name, String.valueOf(error.getStatusCode()));
				}
			}
		}
	}
	/**
    *  @author <Accenture>
    *  @name <getTripIds>
    *  @createDate <09/05/2018>
    *  @description <Method to get the all the Tripids from document obj>
	*  @Parameter EP_ManageDocument.Documents
    *  @return List<String>
    */
	@TestVisible
	private static List<String> getTripIds(EP_ManageDocument.Documents documents){
        EP_GeneralUtility.Log('Public','EP_ManageLoadPlanDocument','getTripIds');
		list<LoadPlanWrapper> loadPlanWrapperlist = new list<LoadPlanWrapper>();
		for(EP_ManageDocument.Document doc : documents.document) {
			loadPlanWrapperlist.add(EP_DocumentUtil.fillLoadPlanWrapper(doc.documentMetaData.metaDatas));
		}
		return getTripIds(loadPlanWrapperlist);
	}
	/**
    *  @author <Accenture>
    *  @name <getTripIds>
    *  @createDate <09/05/2018>
    *  @description <Method to get the all the Tripids from LoadPlanWrapper obj>
	*  @Parameter list<LoadPlanWrapper>
    *  @return List<String>
    */
	@TestVisible
	private static List<String> getTripIds(list<LoadPlanWrapper> loadPlanWrapperlist){
        EP_GeneralUtility.Log('Public','EP_ManageLoadPlanDocument','getTripIds');
		List<String> tripIdList =  new List<String>();
		for(LoadPlanWrapper obj : loadPlanWrapperlist){
			tripIdList.add(obj.tripId);
		}
		return tripIdList;
	}
	/**
    *  @author <Accenture>
    *  @name <getOrdersAsscociatedWithTrip>
    *  @createDate <09/05/2018>
    *  @description <Method to get orders assciated with tripIds>
	*  @Parameter List<String>
    *  @return map<string,List<csord__Order__c>>
    */
	@TestVisible
	private static map<string,List<csord__Order__c>> getOrdersAsscociatedWithTrip (List<String> tripIds){
        EP_GeneralUtility.Log('Public','EP_ManageLoadPlanDocument','getOrdersAsscociatedWithTrip');
		map<string,List<csord__Order__c>> tripIdOrderMap =  EP_OrderMapper.getOrdersAsscociatedWithTrip(tripIds);
		return tripIdOrderMap;
	}

	public with sharing class LoadPlanWrapper{
		public string documentFileName;
		public string tripId;
	}
}