/* 
   @Author 			Accenture
   @name 			EP_TankDipExportWS
   @CreateDate 		03/10/2017
   @Description		Web service class for tank dips export
   @Version 		1.0
*/ 
@RestResource(urlMapping='/v1/TankDips/*')
global class EP_TankDipExportWS {
    @HttpGet 
    global static void retrieveTankDips(){
        RestRequest request = RestContext.request;
        //RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        RestContext.response.addHeader(EP_Common_Constant.ContentType, EP_Common_Constant.applicationjson);
        //EP_IntegrationService service = new EP_IntegrationService();
        //string response = service.handleTankDipsGetRequest();
        EP_IntegrationService service = new EP_IntegrationService();
        string response =service.handleRequest(EP_Common_Constant.WINDMS_TO_SFDC_TANKS_EXPORT);
        RestContext.response.responseBody = Blob.valueOf(response);
    }
    
	//TODO : Need to cofirm the structure    
    @HttpPost
    global static void updateTankDips(){
        RestRequest request = RestContext.request;
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
        String requestBody = request.requestBody.toString();
        //EP_IntegrationService service = new EP_IntegrationService();
        //string response = service.handleTankDipsUpdate(requestBody);
        EP_IntegrationService service = new EP_IntegrationService();
        string response =service.handleRequest(EP_Common_Constant.WINDMS_TO_SFDC_TANKS_EXPORT,requestBody);
        RestContext.response.responseBody = Blob.valueOf(response);
    }
}