global class CutOffMatrixController {

    //private final sObject mysObject;

    private String recordId;
    public Account acc {get;set;}
    public Company__c company {get;set;}
    public EP_CutOffMatrixHelper cutoffHelper {get;set;}
    public static final EP_CutOffMatrixHelper staticcutoffHelper = new EP_CutOffMatrixHelper();
    public static List<DeliveryDay__c> deliveryDays {get;set;}
    public CutOffMatrix__c cutOffMatrix  {get;set;}
    public String cutoffError  {get;set;}
    private Boolean newMatrix = false;

    
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CutOffMatrixController(ApexPages.StandardController controller) { //
        recordId = ApexPages.currentPage().getParameters().get('id');

        cutoffHelper = new EP_CutOffMatrixHelper();
        cutoffHelper.initializeCutoffMatrixHelper(recordId);
        cutoffError = '';
    }

    public PageReference saveCutOffMatrix(){
        //myString = 'New Value';

        List<CutOffMatrix__c> cutOffList = [SELECT id FROM CutOffMatrix__c 
            WHERE Puma_Company__c = :cutoffHelper.cutOffMatrix.Puma_Company__c
            AND Site_Location__c = :cutoffHelper.cutOffMatrix.Site_Location__c
            AND Delivery_Type__c = :cutoffHelper.cutOffMatrix.Delivery_Type__c
            AND Id != :cutoffHelper.cutOffMatrix.Id];

        if(cutOffList.size() == 0){

            // always set the matrix name to resebmle delivery type set in it
            cutoffHelper.cutOffMatrix.name = cutoffHelper.cutOffMatrix.Delivery_Type__c + ' matrix';

            // if no cutoff matrix is set in the delivery days list, it means
            // new cutoff matrix needs to be created
            if (cutoffHelper.deliveryDayList.get(0).Cut_Off_Matrix__c == null) {
                // save new cutoff matrix
                insert cutoffHelper.cutOffMatrix;
                // update list of delivery days to hold that new cutoff matrix reference
                for (DeliveryDay__c dd : cutoffHelper.deliveryDayList) {
                    dd.Cut_Off_Matrix__c = cutoffHelper.cutOffMatrix.id;
                }
                // save delivery days
                insert cutoffHelper.deliveryDayList;

                return goBack();
            }

            update cutoffHelper.deliveryDayList;
            update cutoffHelper.cutOffMatrix;
            return goBack();
        } else {
            cutoffError = 'Cannot save - there is an existing Cut Off Matrix for the same configuration.';
            return null;
        }
    }

    public PageReference cloneCutOffMatrix(){
        return null;
    }

    public PageReference cancelCutOffMatrix(){
        return goBack();
    }

    public PageReference goBack()   {
        //PageReference pg = new PageReference('/' + cutoffHelper.cutOffMatrix.Puma_Company__c);
        PageReference pg = new PageReference('/' + ApexPages.currentPage().getParameters().get('retURL'));
        pg.setRedirect(true);
        return pg;
    }
}