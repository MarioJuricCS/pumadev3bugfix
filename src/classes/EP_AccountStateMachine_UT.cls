@isTest
public class EP_AccountStateMachine_UT
{   
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void getAccountState_test() {
        EP_AccountStateMachine localObj = new EP_AccountStateMachine();
        EP_AccountEvent oe = new EP_AccountEvent('01-ProspectTo01-Prospect');
        localObj.setAccountDomainObject(EP_TestDataUtility.getSellToASProspectDomainObject());
        localObj.accountEvent = oe;
        Test.startTest();
        EP_AccountState result = localObj.getAccountState();
        Test.stopTest();
        System.Assert(result <> null);
    }

    static testMethod void getAccountState_test2() {
        EP_AccountStateMachine localObj = new EP_AccountStateMachine();
        EP_AccountEvent oe = new EP_AccountEvent('01-ProspectTo01-Prospect');
        localObj.setAccountDomainObject(EP_TestDataUtility.getSellToASProspectDomainObject());
        localObj.accountEvent = oe;
        Test.startTest();
        EP_AccountState result = localObj.getAccountState(oe);
        Test.stopTest();
        System.Assert(result <> null);
    }
    
    static testMethod void setAccountDomainObject_test() {
        EP_AccountStateMachine localObj = new EP_AccountStateMachine();
        EP_AccountDomainObject accountDomainObject = EP_TestDataUtility.getSellToASProspectDomainObject();
        Test.startTest();
        localObj.setAccountDomainObject(accountDomainObject);
        Test.stopTest();
        System.AssertEquals(accountDomainObject,localObj.accountDomain);
    }
    static testMethod void getAccountState1_test() {
        EP_AccountDomainObject currentAccount =EP_TestDataUtility.getSellToASProspectDomainObject();
        EP_AccountEvent currentEvent = new EP_AccountEvent('01-ProspectTo01-Prospect');
        Test.startTest();
        EP_AccountState result = EP_AccountStateMachine.getAccountState(currentAccount,currentEvent);
        Test.stopTest();
        System.Assert(result <> null);
    }
    
    static testMethod void getAccountStateInstance_test() {
        EP_AccountDomainObject localaccountDomain = EP_TestDataUtility.getSellToASProspectDomainObject();
        EP_AccountEvent currentEvent = new EP_AccountEvent('01-ProspectTo01-Prospect');
        Test.startTest();
        EP_AccountState result = EP_AccountStateMachine.getAccountStateInstance(localaccountDomain,currentEvent);
        Test.stopTest();
        System.Assert(result <> null);
    }
    
	static testMethod void getAccountStateInstance_Exception_test() {
        EP_AccountDomainObject localaccountDomain = EP_TestDataUtility.getSellToASProspectDomainObject();
        EP_AccountEvent currentEvent = new EP_AccountEvent('ProspectToActive');
        Test.startTest();
        EP_AccountState result = EP_AccountStateMachine.getAccountStateInstance(localaccountDomain,currentEvent);
        Test.stopTest();
        System.Assert(result <> null);
    }
    
    static testMethod void getAccountStateClassName_test() {
        EP_AccountDomainObject accountDomain = EP_TestDataUtility.getSellToASProspectDomainObject();
        Test.startTest();
        String result = EP_AccountStateMachine.getAccountStateClassName(accountDomain);
        Test.stopTest();
        System.assert(EP_SellToASProspect.Class.getName().equalsIgnoreCase(result));
    }
    
    static testMethod void getAccountType_test() {
        EP_AccountDomainObject localAccountDomain = EP_TestDataUtility.getSellToASProspectDomainObject();
        Test.startTest();
        String result = EP_AccountStateMachine.getAccountType(localAccountDomain);
        Test.stopTest();
        System.AssertEquals('SellTo',result);
    }
    
	static testMethod void getAccountTypeShipTo_test() {
        EP_AccountDomainObject localAccountDomain = EP_TestDataUtility.getNonVMIShipToASAccountSetupDomainObject();
        Test.startTest();
        String result = EP_AccountStateMachine.getAccountType(localAccountDomain);
        Test.stopTest();
        System.AssertEquals('NonVMIShipTo',result);
    }
}