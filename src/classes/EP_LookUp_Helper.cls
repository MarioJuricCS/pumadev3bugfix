/****************************************************************
* @author       Accenture                                       *
* @name         EP_LookUp_Helper                                *
* @Created Date 26/12/2017                                      *
* @description  Class to show LookUp Data                       *
****************************************************************/
public with sharing class EP_LookUp_Helper {
    private EP_LookUp_Context ctx;
    private set<Id> accSet;
    private static final string searchFilter = ' and name like \'%';
    private static final string limitFilter = ' Limit 1000';
    private  static final string LIKE_STRING = '%\'';

/****************************************************************
* @author       Accenture                                       *
* @name         EP_LookUp_Helper                                *
* @description  Constructor                                     *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public EP_LookUp_Helper(EP_LookUp_Context ctx) {
        this.ctx = ctx;
        lookUpRecords();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         hideClearSearchButton                           *
* @description  method to hide clear search button              *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void hideClearSearchButton(){
        EP_GeneralUtility.Log('public','EP_LookUp_Helper','hideClearSearchButton');
        ctx.clearSearchButton = false;
        ctx.searchText = null;
        lookUpRecords();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         lookUpRecords                                   *
* @description  method to get data from system                  *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public void lookUpRecords(){
        EP_GeneralUtility.Log('public','EP_LookUp_Helper','lookUpRecords');
        setFilterIds();
        ctx.SobjectList = new List<Sobject>();
        ctx.queryData = EP_Look_Up__c.getValues(ctx.objectVariable);
        string queryString = ctx.queryData.EP_Fields__c + EP_Common_Constant.SPACE + ctx.queryData.EP_Where_Clause__c +': accSet';
        If(!String.isBlank(ctx.searchText)){
            ctx.clearSearchButton = true;
            queryString+= searchFilter +  String.escapeSingleQuotes(ctx.searchText) + LIKE_STRING;
        }
        queryString+= limitFilter;
        system.debug('queryString = '+queryString);
        ctx.SobjectList = Database.query(queryString);
    }

/****************************************************************
* @author       Accenture                                       *
* @name         setFilterIds                                    *
* @description  method to get shipTo from sellTo                *
* @param        NA                                              *
* @return       set<Id>                                         *
****************************************************************/
    private void setFilterIds(){
        EP_GeneralUtility.Log('private','EP_LookUp_Helper','setFilterIds');
        accSet = new set<Id>();
        accSet.add(ctx.IdVariable);
        if(ctx.objectVariable == system.label.EP_Contact){
            for(Account acc : EP_AccountMapper.getChildShiptoByStatus(ctx.IdVariable,EP_Common_Constant.STATUS_ACTIVE)){
                accSet.add(acc.id);
            }
        }
    }
}