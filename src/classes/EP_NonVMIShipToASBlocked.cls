/*
 *  @Author <Accenture>
 *  @Name <EP_NonVMIShipToASBlocked>
 *  @CreateDate <>
 *  @Description <VMI Ship To Account State for 06-Blocked Status>
 *  @NovaSuite Fix -- comments added
 *  @Version <1.0>
 */
 public with sharing class EP_NonVMIShipToASBlocked extends EP_AccountState{
    /***NOvasuite fix constructor removed**/ 
    /*
   @Author          Accenture
   @Name            setAccountDomainObject
   @Param           EP_AccountDomainObject      
   */
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBlocked','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }
    /*
   @Author          Accenture
   @Name            doOnEntry
   */
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBlocked','doOnEntry');
       
        EP_AccountService service = new EP_AccountService(this.account);
        //code Changes for #45362 Start
         service.doActionSendUpdateRequestToNavAndLomo();
        //code Changes for #45362 End
        
    }  
    /*
   @Author          Accenture
   @Name            doOnExit
   */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBlocked','doOnExit');
        
    }
    /*
   @Author          Accenture
   @Name            doTransition
   @Param           boolean       
   */
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBlocked','doTransition');
        return super.doTransition();
    }
   /*
   @Author          Accenture
   @Name            isInboundTransitionPossible
   @Param           boolean       
   */
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASBlocked','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}