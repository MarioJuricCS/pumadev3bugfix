/*
   @Author          CloudSense
   @Name            CheckInvoicesHandler
   @CreateDate      02/01/2017
   @Description     This class is responsible for fetching fresh invoices for CE from NAV
   @Version         1.1
 
*/

global class CheckInvoicesHandler implements CSPOFA.ExecutionHandler{
    
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         02/01/2017
    * @Description  Method to process the step and calculate invoices
    * @Param        list<SObject>
    * @return       NA
    */  
    public List<sObject> process(List<SObject> data)
    {
        List<sObject> result = new List<sObject>();
        //collect the data for all steps passed in, if needed
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        Map<Id,CSPOFA__Orchestration_Step__c> stepMap = new Map<Id,CSPOFA__Orchestration_Step__c>();
        List<Id> orderIdList = new List<Id>();
        Map<Id,Id> orderIdCrIdMap = new Map<Id,Id>();
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];
                                          
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            
            try{
                fetchCreditInvoice(step.CSPOFA__Orchestration_Process__r.Order__c,step.CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c);
            }Catch(Exception e){
              EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'CheckInvoicesHandler',apexPages.severity.ERROR);
            }
          
            /*step.CSPOFA__Status__c ='Complete';
            step.CSPOFA__Completed_Date__c=Date.today();
            step.CSPOFA__Message__c = 'Custom step succeeded';
            result.add(step);*/
        }

        for (CSPOFA__Orchestration_Step__c step : stepList) {

            //mark step Status, Completed Date
            step.CSPOFA__Status__c = 'Complete';
            step.CSPOFA__Completed_Date__c = Date.today();
            step.CSPOFA__Message__c = 'Custom step succeeded';
            result.add(step);
        }

        return result;
    }

    public void fetchCreditInvoice(Id orderId, Id AccountId) {
        csord__Order__c order = [SELECT id,EP_Available_Funds__c, EP_Overdue_Amount__c FROM csord__Order__c WHERE Id = :orderId LIMIT:EP_Common_Constant.ONE];
            
        List<Account> acc = [SELECT Id, EP_Account_Type__c, EP_Bill_To_Account__c, EP_Sell_To_Holding_Account__c, EP_Status__c, 
                              EP_Available_Funds_LCY__c, EP_Holding_Overdue_Balance_HCY__c,EP_Credit_Limit__c,EP_Holding_Overdue_Balance_LCY__c,EP_Credit_Limit_FCY__c
                              FROM Account
                              WHERE Id =: AccountId];
            if(!acc.isEmpty()){
                EP_AccountDomainObject accDomainObj = new EP_AccountDomainObject(acc[0], null);
                EP_AccountService accService = new EP_AccountService(accDomainObj);
                accService.setFinanceInfo();
                accService.setCreditInfo();
                EP_CreditHoldingHierarchyBalance creditHoldingClass = new EP_CreditHoldingHierarchyBalance();
                creditHoldingClass = EP_GeneralUtility.getCreditHoildingHierarchyBalance(accountId);
                //String returnValue = accDomainObj.localAccount.EP_Available_Funds_LCY__c + ',' + accDomainObj.localAccount.EP_Holding_Overdue_Balance_LCY__c + ',' + accDomainObj.localAccount.EP_Credit_Limit__c + ',' + creditHoldingClass.loadedOrdersAmount + ',' + creditHoldingClass.notInvoicedLinesAmount + ',' + creditHoldingClass.Exposure;
                if(order != null){
                    order.EP_Available_Funds__c =accDomainObj.localAccount.EP_Available_Funds_LCY__c;
                    order.EP_Overdue_Amount__c= accDomainObj.localAccount.EP_Holding_Overdue_Balance_LCY__c;
                    update order;
                }
          }
    }    
      
}