<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>EP_Update_Payment_company_code</fullName>
        <description>Payment company code (Unique field) should be populated with Payment code and company code value</description>
        <field>EP_Payment_Company_Code__c</field>
        <formula>if(NOT( ISBLANK(EP_Company__r.EP_Company_Code__c) ),EP_Payment_Term_Code__c + &apos;-&apos; + EP_Company__r.EP_Company_Code__c,EP_Payment_Term_Code__c)</formula>
        <name>EP Update Payment company code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EP Unique Payment code and company code</fullName>
        <actions>
            <name>EP_Update_Payment_company_code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EP_Payment_Term__c.EP_Payment_Term_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>L4 - 45352 On update or create of Payment term, Payment company code (Unique field) should be populated with Payment code and company code value</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
