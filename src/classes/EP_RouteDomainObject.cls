/* 
   @Author      Accenture
   @name        EP_AccountDomainObject
   @CreateDate  05/05/2017
   @Description This is the domain class for Route Object
   @Version     1.0
*/
 public class EP_RouteDomainObject{ 
    @testvisible Map<Id,EP_Route__c> oldRouteMap;
    @testvisible Map<Id,EP_Route__c> newRouteMap;
    @testvisible EP_RouteMapper routeMapper = new EP_RouteMapper ();
    @testvisible EP_RouteService service; 
    /**
    * @author       Accenture
    * @name         EP_RouteDomainObject
    * @date         05/05/2017
    * @description  Constructor for setting new Route and old Route objects which will be passed by the trigger
    * @param        List<EP_Route__c>,List<EP_Route__c>
    * @return       NA
    */  
    public EP_RouteDomainObject(Map<Id,EP_Route__c> oldRoutes,Map<Id,EP_Route__c> newRoutes){
        this.newRouteMap = newRoutes;
        this.oldRouteMap = oldRoutes;
        this.service = new EP_RouteService(this); 
    } 
    
     /**
    * @author       Accenture
    * @name         EP_RouteDomainObject
    * @date         05/05/2017
    * @description  Constructor for setting  old Route objects which will be passed by the trigger
    * @param        Map<Id,EP_Route__c> 
    * @return       NA
    */  
    public EP_RouteDomainObject(Map<Id,EP_Route__c> oldRoutes){
        this.oldRouteMap = oldRoutes;
        this.service = new EP_RouteService(this);
    } 
     
    /**
    * @author       Accenture
    * @name         getNewRoutes
    * @date         05/05/2017
    * @description  This method is used to get new route records
    * @param        NA
    * @return       Map<Id,EP_Route__c>
    */
    public Map<Id,EP_Route__c> getNewRoutes(){
        EP_GeneralUtility.Log('Public','EP_RouteDomainObject','getNewRoutes');
        return newRouteMap; 
    } 
    
    /**
    * @author       Accenture
    * @name         getOldRoutes
    * @date         05/05/2017
    * @description  This method is used to get old route records
    * @param        NA
    * @return      Map<Id,EP_Route__c>
    */
    public Map<Id,EP_Route__c> getOldRoutes(){
        EP_GeneralUtility.Log('Public','EP_RouteDomainObject','getOldRoutes');
        return oldRouteMap; 
    } 
    
         
    /**
    * @author       Accenture
    * @name         doActionBeforeUpdate
    * @date         05/05/2017
    * @description  This method is to be called thorugh trigger to execute all before update event logic/actions
    * @param        NA
    * @return       NA
    */
    public void doActionBeforeUpdate(){
        try{
            EP_GeneralUtility.Log('Public','EP_RouteDomainObject','doActionBeforeUpdate');
            this.service.doActionBeforeUpdate();
        }catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionBeforeUpdate' , 'EP_RouteDomainObject', ApexPages.Severity.ERROR);
            EP_Common_Util.addCommonErrorToAllRecord((List<Sobject>)newRouteMap.values(), ex.getMessage());

        }        
    }
    
    /**
    * @author       Accenture
    * @name         doActionBeforeDelete
    * @date         05/05/2017
    * @description  This method is used to perform some validation check before deleting a route
    * @param        NA
    * @return       void
    */
    public void doActionBeforeDelete(){
        try{
            EP_GeneralUtility.Log('Public','EP_RouteDomainObject','doActionBeforeDelete');
            this.service.doActionBeforeDelete();
        }catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionBeforeDelete' , 'EP_RouteDomainObject', ApexPages.Severity.ERROR);
            EP_Common_Util.addCommonErrorToAllRecord((List<Sobject>)oldRouteMap.values(), ex.getMessage());
        } 
    } 

    public boolean isRouteNameChanged(EP_Route__c route, EP_Route__c oldroute){
        return !(route.EP_Route_Name__c.equalsIgnoreCase(oldroute.EP_Route_Name__c));
    }
    
    
    public boolean isRoutecodeChanged(EP_Route__c route, EP_Route__c oldroute){
        return !(route.EP_Route_Code__c.equalsIgnoreCase(oldroute.EP_Route_Code__c));
    }
    
    public boolean isRouteStatusChanged(EP_Route__c route, EP_Route__c oldroute){
        return !(route.EP_Status__c.equalsIgnoreCase(oldroute.EP_Status__c));
    }
 }