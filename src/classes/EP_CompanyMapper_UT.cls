@isTest
public class EP_CompanyMapper_UT
{
    static testMethod void getRecordsByIds_test() {
        EP_CompanyMapper localObj = new EP_CompanyMapper();
        Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;
        Set<id> idSet = new Set<Id>();
        idSet.add(cmpny.id);
        Test.startTest();
        LIST<Company__c> result = localObj.getRecordsByIds(idSet);
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    static testMethod void getCompanyDetailsByCompanyCode_test() {
        EP_CompanyMapper localObj = new EP_CompanyMapper();
        Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;
        String compCode = 'AUN';
        Test.startTest();
        Company__c result = localObj.getCompanyDetailsByCompanyCode(compCode);
        Test.stopTest();
        System.AssertEquals(cmpny.id,result.id);
    }
    static testMethod void getExistingCompany_test() {
        EP_CompanyMapper localObj = new EP_CompanyMapper();
        Test.startTest();
        List<Company__c> cmpnyList = EP_TestDataUtility.createMultipleCompanies(1);
        insert cmpnyList;
        Company__c result = localObj.getExistingCompany();
        Test.stopTest();
        System.AssertEquals(cmpnyList[0].id,result.id);
    }
    //L4 45526 Start
    static testMethod void getCompanyDetailsByCompanyCodes_test() {
        EP_CompanyMapper localObj = new EP_CompanyMapper();
        Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;
        set<string> setCompCode = new set<string>();
        setCompCode.add('AUN');
        Test.startTest();
        map<string,Company__c> result = localObj.getCompanyDetailsByCompanyCodes(setCompCode);
        Test.stopTest();
        System.AssertEquals(true,!result.isEmpty());
    }
    //L4 45526 End
}