/*******************************************************************************
@Author: Sanchit Dua
Email: sanchit.dua@accenture.com
Description: Batch Process to do a Daily Delete from Integration Record object. 

Change history (key updates):
@author sanchit.dua
email sanchit.dua@accenture.com

********************************************************************************/
/* Description: This class will run daily to delete the Integration Records which are older than 30 days */
global class EP_PeriodicDataPurgeBatchClass implements Database.batchable<sObject>, Database.Stateful {
   
    private static final Integer LMT = Limits.getLimitQueryRows();
    private static EP_PeriodicDataPurge__c oCustomSetting; 
    private static final String ERROR_MESSAGE = 'Please initialize the Custom Setting named EP_PeriodicDataPurge__c';
    private static final String Query_Locator = 'SELECT ID FROM SOBJECTNAME WHERE (LastModifiedDate <: d OR CreatedDate <: d ) LIMIT : lmt';
    private static final String SOBJECTNAME = 'SOBJECTNAME';
    
    static {
        if( EP_PeriodicDataPurge__c.getOrgDefaults() <> NULL ){
            oCustomSetting = EP_PeriodicDataPurge__c.getOrgDefaults();      
        }
    } 
   
    
    /*Start Method: This will return filtered Integration Records based on the condition*/
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        /* if(oCustomSetting.EP_Number_of_days__c == NULL || oCustomSetting.EP_SObject_Name__c == NULL)
            throw new IntentionalException(ERROR_MESSAGE);
        */ 
        Date d = System.today();
        Integer numOfDays= Integer.valueOf(oCustomSetting.EP_Number_of_days__c);
        String objName = oCustomSetting.EP_SObject_Name__c;
        d = d - numOfDays;
        
        return Database.getQueryLocator(Query_Locator.replace(SOBJECTNAME, objName));
    }
    
    /*execute Method: This method includes the actual batch logic which is to do the mapping between ERP Customer and Accounts and then load them. */
    global void execute(Database.BatchableContext BC, List<sobject> scope) {
        database.delete(scope);
        
    }
    
    /*Finish Method: */
    global void finish(Database.BatchableContext BC) {
    
        //Enter finish logic
    } 
    
    /*
     * @author sanchit.dua
     */
    public without sharing class IntentionalException extends Exception {}
    
}