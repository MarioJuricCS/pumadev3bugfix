/**
  * @author       Accenture                                       
  * @name         EP_OrderConfirmationEvent_Subscription                           
  * @Created Date 17/01/2018                                      
  * @description  Quote Email Notification Class                   
  */
public with sharing class EP_OrderConfirmationEvent_Subscription extends EP_Event_Subscription{
	public EP_OrderConfirmationEvent_Subscription(){
		subscriptionEventObj.eventType = EP_Common_Constant.NOTIFICATION_CODE_ORDERCONFIRMATION;
	}
	
}