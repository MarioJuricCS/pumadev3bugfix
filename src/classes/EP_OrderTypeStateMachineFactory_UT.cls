@isTest
public class EP_OrderTypeStateMachineFactory_UT{

	static testMethod void getOrderStateMachine_test() {
		EP_OrderTypeStateMachineFactory localObj = new EP_OrderTypeStateMachineFactory();
		EP_OrderDomainObject currentOrder = EP_TestDataUtility.getOrderStateDraftDomainObjectPostiveScenario();
		Test.startTest();
		EP_OrderStateMachine objSM = localObj.getOrderStateMachine(currentOrder);
		Test.stopTest();
		Boolean result = objSM instanceof EP_NonVMINonConsignmentSM;
		system.assert(result);
	}

	static testMethod void getOrderStateMachineInstance_test() {
		EP_OrderTypeStateMachineFactory localObj = new EP_OrderTypeStateMachineFactory();
		EP_OrderDomainObject currentOrder = EP_TestDataUtility.getOrderStateDraftDomainObjectPostiveScenario();
		Test.startTest();
		EP_OrderStateMachine objSM = EP_OrderTypeStateMachineFactory.getOrderStateMachineInstance(currentOrder);
		Test.stopTest();
		Boolean result = objSM instanceof EP_NonVMINonConsignmentSM;
		system.assert(result);
	}

	static testMethod void getOrderStateMachine_withConstParam_test() {		
		EP_OrderDomainObject currentOrder = EP_TestDataUtility.getOrderStateDraftDomainObjectPostiveScenario();
		EP_OrderTypeStateMachineFactory localObj = new EP_OrderTypeStateMachineFactory(currentOrder);
		Test.startTest();
		EP_OrderStateMachine objSM = EP_OrderTypeStateMachineFactory.getOrderStateMachine();
		Test.stopTest();
		Boolean result = objSM instanceof EP_NonVMINonConsignmentSM;
		system.assert(result);
	}

	static testMethod void  getOrderStateMachineInstance_negative_test(){
		EP_OrderDomainObject currentOrder = new EP_OrderDomainObject(new csord__Order__c());
		//Test Class Fix Start
		try {
			EP_OrderTypeStateMachineFactory localObj = new EP_OrderTypeStateMachineFactory(null);
			Test.startTest();
			EP_OrderStateMachine objSM = EP_OrderTypeStateMachineFactory.getOrderStateMachine();
			Test.stopTest();
		}catch(exception ex) {
			system.debug('Catch the exception for State Machine Since we are passing order ref as null');
		}
		//Test Class Fix End
	}
}