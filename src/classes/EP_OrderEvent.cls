/*
 *  @Author <Aravindhan Ramalingam>
 *  @Name <EP_OrderEventObject>
 *  @CreateDate <04/02/2017>
 *  @Description <Wrapper class to store order events>
 *  @Version <1.0>
 */
 
 public class EP_OrderEvent {

    String eventName;
    String eventMessage;
    String gotoStatus;
    Boolean isStatusChanged;
    
    public EP_OrderEvent(String eventName){
        this.eventName = eventName;
    }
    
    public String getEventName(){
        EP_GeneralUtility.Log('Public','EP_OrderEvent','getEventName');
        return eventName;
    }

    public void setEventName(String value){
        EP_GeneralUtility.Log('Public','EP_OrderEvent','setEventName');
        eventName = value;
        
    }
    
    public String getEventMessage(){
        EP_GeneralUtility.Log('Public','EP_OrderEvent','getEventMessage');
        return eventMessage;
    }

    public void setEventMessage(String value){
        EP_GeneralUtility.Log('Public','EP_OrderEvent','setEventMessage');
        eventMessage = value;
        
    }
}