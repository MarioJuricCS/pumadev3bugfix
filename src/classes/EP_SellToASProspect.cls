/*
 *  @Author <Bhushan Adhikari>
 *  @Name <EP_SellToASProspect>
 *  @CreateDate <08/02/2017>
 *  @Description <Sell To Account State for Prospect Status>
 *  @Version <1.0>
 */
 public with sharing class EP_SellToASProspect extends EP_AccountState{
     
    public EP_SellToASProspect() {
        system.debug('****IN EP_SellToASProspect****');
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount)
    {
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_SellToASProspect','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_SellToASProspect','doOnExit');
        system.debug('In do Exit of Prospect State');
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_SellToASProspect','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_SellToASProspect','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}