/*
    @Author          Accenture
    @Name            EP_OrderImportController
    @CreateDate      
    @Description     This is a controller extension for RO Order Import VF page
    @Version         1.0
    @Reference       NA
*/
public with sharing class EP_OrderImportController {
	public EP_OrderImportContext ctx {get;set;}
	public EP_OrderImportHelper csvHelper{get;set;}
	public EP_File__c fileRecord;
    
    /* 
    	Constructor
    */
    public EP_OrderImportController(ApexPages.StandardController controller) {
    	EP_GeneralUtility.Log('private','EP_OrderImportController','EP_OrderImportController');
    	fileRecord = (EP_File__c) controller.getRecord();
    	ctx = new EP_OrderImportContext(fileRecord);
    	csvHelper = new EP_OrderImportHelper(ctx);    	
    }
   
   /*
    * @ Description: This method will parse the CSV file and populate the values in the local inner clas
    */
    public PageReference parseCSVFile() {
    	EP_GeneralUtility.Log('private','EP_OrderImportController','parseCSVFile');
    	try {
    		if(csvHelper.hasValidFile()) {
    			csvHelper.processCSVData();
	    		csvHelper.processImportRequest();
	    		csvHelper.initDataValidation();
    		}
    	} catch (exception exp){
    		ctx.CSVParseSuccess = false;
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,system.Label.EP_CSV_IMPORT_ERROR_MSG + EP_Common_Constant.COLON_WITH_SPACE + exp.getMessage()));
    		system.debug('**Exception** ' + exp.getMessage()+' ***' + exp.getStackTraceString());
    	}
   		return null;
    } 
}