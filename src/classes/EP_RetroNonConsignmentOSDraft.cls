/*   
     @Author Aravindhan Ramalingam
     @name <RetroNonConsignmentOSDraft.cls>     
     @Description <Retro Non Consignment Order State for draft status>   
     @Version <1.1> 
     */

     public class EP_RetroNonConsignmentOSDraft extends EP_OrderState {
        
        public EP_RetroNonConsignmentOSDraft(){
            
        }
        public override void setOrderDomainObject(EP_OrderDomainObject currentOrder)
        {
            super.setOrderDomainObject(currentOrder);
        }
        
        public override void doOnEntry(){
            EP_GeneralUtility.Log('Public','EP_RetroNonConsignmentOSDraft','doOnEntry');
            csord__Order__c  ord = order.getOrder();
            
            if(ord.EP_Loading_Date__c != NULL){
                ord.EP_Expected_Loading_Date__c = EP_PortalLibClass_R1.returnLocalDate(Date.valueOf(ord.EP_Loading_Date__c));
            }
            
        //Updates values in the retro order
        if(ord.EP_Expected_Delivery_Date__c != null){
         String strRoDeliveryDate = String.valueOf(ord.EP_Expected_Delivery_Date__c);
         ord.EP_Actual_Delivery_Date_Time__c = EP_GeneralUtility.convertDateTimeToDecimal(strRoDeliveryDate);
               ord.EP_Requested_Delivery_Date__c = Date.newInstance(ord.EP_Expected_Delivery_Date__c.Year(),ord.EP_Expected_Delivery_Date__c.Month(),ord.EP_Expected_Delivery_Date__c.Day()); // defect fix 44398 ,#anubhutiWork update Request Delivery Date with actual delivery date in case of Order Epoch = “Retrospective”. This would enable ROs also to appear in On-run report if they are getting assigned to a run at the time of order creation.
               ord.EP_Requested_Pickup_Time__c = String.valueOf(EP_GeneralUtility.convertSelectedHourToTime(order.availableSlots)).subString(0,8); //Defect fix 44398
               ord.EP_Expected_Delivery_Date__c = NULL;
           }       
           
           if(ord.EP_Order_Date__c != NULL){
            ord.EffectiveDate__c = ord.EP_Order_Date__c.date();
        } 
        
        //set thr order back in the order domainboject
        order.setOrder(ord);
    }
    
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_RetroNonConsignmentOSDraft','doOnExit');

    }
    
    
    public static String getTextValue()
    {
        return EP_OrderConstant.OrderState_Draft;
    }
    

}