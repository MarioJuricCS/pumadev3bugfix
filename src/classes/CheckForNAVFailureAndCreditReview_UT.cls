@isTest
private class CheckForNAVFailureAndCreditReview_UT {

    @testSetup
    public static void setupTestData() {
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name = 'Test';
        acc.EP_Status__c = '05-Active';
        acc.cscfga__Active__c = 'Yes';
        insert acc;
        
        System.debug(acc.EP_Active_Status__c);
        
        csord__Order__c ord = new csord__Order__c(Name='Test Order',
                                                  csord__Account__c = acc.Id,
                                                  AccountId__c = acc.Id,
                                                  EP_Sell_To__c = acc.Id,
                                                  csord__Status__c = 'Awaiting Credit Review',
                                                  csord__Status2__c = 'Awaiting Credit Review',
                                                  ep_integration_status__c = 'FAILURE',
                                                  csord__Identification__c = '9SADGA83JFOW24234R24');
        insert ord;
        
        Account billToAccount = EP_TestDataUtility.createBillToAccount();
        insert billToAccount;
        
        EP_Credit_Exception_Request__c creditException = new EP_Credit_Exception_Request__c();
        creditException.EP_Available_Funds__c = 20000;
        creditException.EP_Bill_To__c = billToAccount.Id;
        creditException.EP_Current_Order_Value__c = 12;
        creditException.EP_CS_Order__c = ord.Id;
        creditException.EP_Overdue_Invoices__c = false;
        creditException.EP_Reason__c = 'Reason';
        creditException.EP_Request_Date__c = Date.today();
        insert creditException;
        
        CSPOFA__Orchestration_Process_Template__c oProcessTemplate = new CSPOFA__Orchestration_Process_Template__c(Name='CSC Cancel Template Test');
        insert oProcessTemplate;
        
        CSPOFA__Orchestration_Process__c oProcess = new CSPOFA__Orchestration_Process__c(Name = 'Cancel Process', CSPOFA__Orchestration_Process_Template__c = oProcessTemplate.Id, Order__c = ord.Id);
        insert oProcess;

        CSPOFA__Orchestration_Step__c oStep = new CSPOFA__Orchestration_Step__c(Name = 'Cancel Test Step', CSPOFA__Orchestration_Process__c = oProcess.Id);
        insert oStep;
    }

    @isTest
	private static void processTest() {

        List<CSPOFA__Orchestration_Step__c> data = [SELECT Id, Name FROM CSPOFA__Orchestration_Step__c];
        
        List<sObject> result = new List<sObject>();
        
        CheckForNAVFailureAndCreditReview obj = new CheckForNAVFailureAndCreditReview();
        
        test.startTest();

        result = obj.process(data);

        test.stopTest();
        
        System.debug(result);
        
        csord__Order__c ord = [SELECT Id, ep_integration_status__c FROM csord__Order__c LIMIT 1];
        
        System.assertNotEquals(null, result, 'Invalid data');
        System.assertEquals('SYNC', ord.ep_integration_status__c, 'Invalid data');
	}
}