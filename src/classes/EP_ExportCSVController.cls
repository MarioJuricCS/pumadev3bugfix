/**
* @author <Spiros Markantonatos>
* @name <EP_ExportCSVController>
* @CreateDate <30/08/2016>
* @description <This class is a generic export file constuctor. It can be used to build CSV files dynamically based on 
				the "EP_Export_File_Type__mdt" and "EP_Export_File_Column_Mapping__mdt" customer metadata objects> 
* @version <1.0>
*/
public without sharing class EP_ExportCSVController {

	//Constants
	private static final String NEW_LINE = '\n';
	private static final String NULL_VALUE = 'null';
	private static final String CLASS_NAME = 'EP_ExportCSVController';
	private static final String GENERATE_CSV_METHOD = 'generateCSV';
	private static final String AUTO_NUMBER_TYPE = 'Auto-number';
	private static final String DATE_TYPE = 'Date';
	private static final String COLON_VALUE = ':';
	private static final String FROM_WITH_SPACE = ' FROM ';
	private static final String SPACE_VALUE = ' ';
	private static final String LIST_PARAMS = ':listParams';
	
	//Properties
	public String strFileTypeID {get;set;} // This property is used to set the ID of the metadata that holds the export file type
	public List<String> listParams {get;set;}
	private String queryString {get;set;}
	private String sObjectNameString {get;set;}
	private List<String> listFileColumnNames {get;set;}
	private List<String> listFileFieldAPINames {get;set;}
	private Map<String, String> mapExportFields {get;set;} // This map is used to link export file columns with the SOQL fields
	private Map<String, String> mapFieldFormattings {get;set;} // This map is used to store the formatting of the column, as defined in the file definition
	private Map<String, String> mapDefaultValues {get;set;} // This map is used to store the default value of the column
	private Map<String, String> mapColumnTypes {get;set;} // This map is used to store the type of the column (e.g. General, Auto-number, etc)

	/**
	* @author <Spiros Markantonatos>
	* @date <30/08/2016>
	* @name <EP_ExportCSVController>
	* @description <Constructor>
	* @param none
	* @return none
	*/
	/*public EP_ExportCSVController() {
	//Constructor
	EP_DebugLogger.printDebug('++++++listParams+++++++++'+listParams);
	}*/ 

	/**
	* @author <Spiros Markantonatos>
	* @date <30/08/2016>
	* @name <generateCSV>
	* @description <This method will generate dynamically the CSV file based on the records retrieved by the dynamic SOQL query>
	* @param none
	* @return String
	*/
	public String generateCSV() {
		Map<String, Integer> mapColumnIndex = new Map<String, Integer>(); // This map is used to maintain the row index as per parent reference
		String strBody = NULL;

		try {
			// 1. Read file definition metadata and build the query and file column header
			retrieveFileDefinitions();

			if (String.isNotBlank(queryString)){
				// 2. Retrieve the relevant data
				List<sObject> listSObjectRecords = Database.query(queryString);

				if (!listSObjectRecords.isEmpty()){
					// Reset the body variable
					strBody = EP_Common_Constant.SPACE;

					// Add the header values to the file
					for (String fileColumnNameString : listFileColumnNames){
						strBody += fileColumnNameString + EP_Common_Constant.COMMA;
					}
					// Remove the last comma and add new line
					strBody = completeFileLine(strBody);

					// Add the records as new lines
					String strValue;

					// Get the field defination of the object
					Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
					Schema.SObjectType objectType = gd.get(sObjectNameString); 
					Map<String, Schema.SobjectField> mapObjectFields = objectType.getDescribe().fields.getMap();

					for (sObject sObjectRecord: listSObjectRecords){
						// Iterate through the map of columns, as some columns might be left intentionally blank
						for (String fileColumnNameString : listFileColumnNames){
							strValue = NULL; // Reset the value variable

							if (mapExportFields.containsKey(fileColumnNameString)){
								String strFieldName = mapExportFields.get(fileColumnNameString);

								if (String.isNotBlank(strFieldName)){
									// Check for parent fields as they need to be retrieved separately (due to Salesforce limitation)
									if (strFieldName.contains(EP_Common_Constant.DOT)){
										strFieldName = strFieldName.replace(EP_Common_Constant.DOT, COLON_VALUE); // Salesforce does not split by 'dot'
										List<String> lParentFields = strFieldName.split(COLON_VALUE);

										SObject oParent = sObjectRecord.getSObject(lParentFields[0]);

										if (oParent != NULL){
											if (oParent.get(lParentFields[1]) != NULL){
												strValue = (String)oParent.get(lParentFields[1]);
											}
										}
									} else {
										if (sObjectRecord.get(strFieldName) != NULL){
											strValue = String.valueOf(sObjectRecord.get(strFieldName));

											// Convert date and date/time to local user values (only applicable for date values)
											if (mapObjectFields.containsKey(strFieldName)){
												if (mapObjectFields.get(strFieldName).getDescribe().getType() == Schema.DisplayType.Date){
													Date dateOfRecord = (Date)sObjectRecord.get(strFieldName);
													dateOfRecord = EP_PortalLibClass_R1.returnLocalDate(dateOfRecord);
													strValue = String.valueOf(dateOfRecord);
												}
												if (mapObjectFields.get(strFieldName).getDescribe().getType() == Schema.DisplayType.DateTime){
													DateTime dt = (DateTime)sObjectRecord.get(strFieldName);
													dt = EP_PortalLibClass_R1.returnLocalDateTime(dt);
													strValue = String.valueOf(dt);

													// If the type of the column is Date and the value is Date/Time
													// then convert back to date
													if (mapColumnTypes.containsKey(fileColumnNameString)){
														if (mapColumnTypes.get(fileColumnNameString) == DATE_TYPE){
															Date dDate = Date.newInstance(dt.Year(), dt.Month(), dt.Day());
															strValue = String.valueOf(dDate);
														}
													}
												}
											}
										}
									} // End parent field check

									// Populate auto-number (depending on the type of the column)
									if (mapColumnTypes.containsKey(fileColumnNameString) && String.isNotBlank(strValue)){
										if (mapColumnTypes.get(fileColumnNameString) == AUTO_NUMBER_TYPE){
											// Maintain the column index
											if (!mapColumnIndex.containsKey(strValue)){
												mapColumnIndex.put(strValue, 0);
											}
											// Increase the value by 1
											Integer intIndex = mapColumnIndex.get(strValue);
											intIndex++;
											mapColumnIndex.put(strValue, intIndex);

											// Overwrite the actual value with the auto-number
											strValue = String.valueOf(intIndex);
										}
									}
								}
							}

							strValue = strValue == NULL_VALUE || strValue == NULL ? EP_Common_Constant.SPACE : strValue;

							// Complete any value formatting
							if (mapFieldFormattings.containsKey(fileColumnNameString)){
								if (String.isNotBlank(strValue)){
									strValue = String.format(mapFieldFormattings.get(fileColumnNameString), new String[]{strValue});
								}
							}

							// Set default value if there is no value available
							if (!String.isNotBlank(strValue) && String.isNotBlank(fileColumnNameString)){
								if (mapDefaultValues.containsKey(fileColumnNameString)){
									strValue = mapDefaultValues.get(fileColumnNameString);
								}
							}

							if (!strValue.isNumeric())
							strValue = EP_Common_Constant.DOUBLE_QUOTES_STRING + strValue + EP_Common_Constant.DOUBLE_QUOTES_STRING;

							strBody += strValue + EP_Common_Constant.COMMA;

						} // End column FOR 

						// Remove the last comma and add new line
						strBody = completeFileLine(strBody);
					}
				}
			}
		} catch(exception ex) {
			Apexpages.addMessages(ex);
			EP_LoggingService.logHandledException(ex, 
			EP_Common_Constant.EPUMA, 
			CLASS_NAME, 
			GENERATE_CSV_METHOD, 
			ApexPages.Severity.ERROR);
		}

		return strBody;
	}
	
	/**
	* @author <Spiros Markantonatos>
	* @date <30/08/2016>
	* @name <completeFileLine>
	* @description <This method will remove the extra comma at the end of the line and add the new line >
	* @param String
	* @return String
	*/
	private String completeFileLine(String strBody) {
		// Remove the last comma
		strBody = strBody.removeEnd(EP_Common_Constant.COMMA);
		// Add new line
		strBody += NEW_LINE;

		return strBody;
	}

	/**
	* @author <Spiros Markantonatos>
	* @date <30/08/2016>
	* @name <retrieveFileDefinitions>
	* @description <This method will retrieve the file definitions for the custom metadata objects and generate the dynamic query >
	* @param none
	* @return void
	*/
	private void retrieveFileDefinitions() {

		if (!listParams.isEmpty()){
			// Retrieve the export file type reocrd
			List<EP_Export_File_Type__mdt> listExportFileTypes = [SELECT Id, MasterLabel, DeveloperName, 
			EP_Export_Order_By_Clause__c, EP_Export_Where_Clause__c, 
			EP_Object_API_Name__c 
			FROM EP_Export_File_Type__mdt
			WHERE Id = :strFileTypeID
			LIMIT 1];
			if (!listExportFileTypes.isEmpty()){   
				Integer nRows = EP_Common_Util.getQueryLimit();
				
				List<EP_Export_File_Column_Mapping__mdt> listExportFileColumns = 
				[SELECT Id, MasterLabel, EP_Export_File_Type_API_Name__c, 
				EP_Field_API_Name__c, EP_Column_Order__c, EP_Format__c,
				EP_Default_Value__c, EP_Value_Type__c
				FROM EP_Export_File_Column_Mapping__mdt
				WHERE EP_Export_File_Type_API_Name__c = :listExportFileTypes[0].DeveloperName
				ORDER BY EP_Column_Order__c ASC Limit:nRows];

				if (!listExportFileColumns.isEmpty()){
					listFileColumnNames = new List<String>();
					listFileFieldAPINames = new List<String>();

					Map<String, String> mapFieldAPINames = new Map<String, String>();
					mapExportFields = new Map<String, String>();
					mapFieldFormattings = new Map<String, String>();
					mapDefaultValues = new Map<String, String>();
					mapColumnTypes = new Map<String, String>();

					sObjectNameString = listExportFileTypes[0].EP_Object_API_Name__c;

					// Build the select statement
					queryString = EP_Common_Constant.SELECT_STRING;

					for (EP_Export_File_Column_Mapping__mdt exportFileColumnMapping : listExportFileColumns){
						// Create a map of the fields back to the column name. This is used when building the CSV to "skip" blank columns
						mapExportFields.put(exportFileColumnMapping.MasterLabel, exportFileColumnMapping.EP_Field_API_Name__c);

						// Store formatting of the field (if available)
						if (String.isNotBlank(exportFileColumnMapping.EP_Format__c)){
							mapFieldFormattings.put(exportFileColumnMapping.MasterLabel, exportFileColumnMapping.EP_Format__c);
						}

						// Store the default value of the column
						if(String.isNotBlank(exportFileColumnMapping.EP_Default_Value__c)){
							mapDefaultValues.put(exportFileColumnMapping.MasterLabel, exportFileColumnMapping.EP_Default_Value__c);
						}

						// Store the type of the column
						mapColumnTypes.put(exportFileColumnMapping.MasterLabel, exportFileColumnMapping.EP_Value_Type__c);

						listFileColumnNames.add(exportFileColumnMapping.MasterLabel);
						// Fields can bee added twice to the file
						listFileFieldAPINames.add(exportFileColumnMapping.EP_Field_API_Name__c);
						// However they cannot be added twice to the query as SFDC is firing an error
						// Add an extra check to ensure that the same field is not added twice to the query
						if (!mapFieldAPINames.containsKey(exportFileColumnMapping.EP_Field_API_Name__c)){
							// The field API name could be blank. In this case it should be ignored
							if (String.isNotBlank(exportFileColumnMapping.EP_Field_API_Name__c)){
								queryString += SPACE_VALUE  + exportFileColumnMapping.EP_Field_API_Name__c + EP_Common_Constant.COMMA;
								mapFieldAPINames.put(exportFileColumnMapping.EP_Field_API_Name__c, NULL);
							} 
						}
					}

					// Remove the last comma
					queryString = queryString.removeEnd(EP_Common_Constant.COMMA);

					// Build the FROM clause
					queryString += FROM_WITH_SPACE + listExportFileTypes[0].EP_Object_API_Name__c + EP_Common_Constant.SPACE;

					// Build the WHERE clause
					queryString += listExportFileTypes[0].EP_Export_Where_Clause__c + EP_Common_Constant.SPACE + EP_Common_Constant.IN_STRING + LIST_PARAMS + EP_Common_Constant.SPACE; 

					// Build the ORDER BY clause
					queryString += listExportFileTypes[0].EP_Export_Order_By_Clause__c;
				}
			}
		}
		EP_DebugLogger.printDebug('QUERY: ' + queryString);
	}
}