@isTest
private class EP_CustomerEditRedirectController_UT {
    static Account acc;
    static EP_Tank__c tank;
    static Id rectypeId;
    private static final string RECTYPESTR = 'RecordType';
    private static final String ID_VAR = 'Id';

    
    static void dataSetup(){
        acc = EP_TestDataUtility.getshipto();
        tank = EP_TestDataUtility.createTestEP_Tank(acc);
        system.debug('tank = '+tank);
        rectypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.TANK_OBJ,EP_Common_Constant.RETAIL_SITE_TANK);
    }   

    static testMethod void redirectTank_test(){
        dataSetup();
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        system.debug('tank = '+tank);
        ApexPages.currentPage().getParameters().put(ID_VAR,tank.Id);
        ApexPages.currentPage().getParameters().put(RECTYPESTR,rectypeId);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(tank));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectTank();
        Test.stopTest();
        system.assertEquals(true,pageRedirect!=null);
    }

    static testMethod void redirectTankNegative_test(){
        dataSetup();
        tank.EP_Tank_Status__c = EP_Common_Constant.TANK_DECOMISSIONED_STATUS;
        update tank;
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        system.debug('tank = '+tank);
        ApexPages.currentPage().getParameters().put(ID_VAR,tank.Id);
        ApexPages.currentPage().getParameters().put(RECTYPESTR,rectypeId);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(tank));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectTank();
        Test.stopTest();
        system.assertEquals(false,pageRedirect!=null);
    }
    
    static testMethod void redirectTankNegative1_test(){
        dataSetup();
        //L4# 78534 - Test Class Fix - Start
        Account sellTo = [select Id from Account where Id =: acc.ParentId limit 1];
        sellTo.EP_Status__c = EP_AccountConstant.DEACTIVATE;
        update sellTo;
        //acc.parentId = ac.id;
        //update acc;
        //L4# 78534 - Test Class Fix - End
        EP_CS_Validation_Rule_Settings__c obj = new EP_CS_Validation_Rule_Settings__c();
        obj.EP_Skip_Tank_Rules__c = false;
        insert obj;
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        system.debug('tank = '+tank);
        ApexPages.currentPage().getParameters().put(ID_VAR,tank.Id);
        ApexPages.currentPage().getParameters().put(RECTYPESTR,rectypeId);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(tank));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectTank();
        Test.stopTest();
        system.assertEquals(false,pageRedirect!=null);
    }
    
    static testMethod void redirectTankNegativeCatch_test(){
        dataSetup();
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        system.debug('tank = '+tank);
        //ApexPages.currentPage().getParameters().put(ID_VAR,tank.Id);
        //ApexPages.currentPage().getParameters().put(RECTYPESTR,rectypeId);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(tank));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectTank();
        Test.stopTest();
        system.assertEquals(false,pageRedirect!=null);
    }
    // Changes made for CUSTOMER MODIFICATION L4 Start
    static testMethod void redirectAccountNegative1_test(){
        dataSetup();
        system.debug('EP_Status__c = '+acc.EP_Status__c);
        acc.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update acc;
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        system.debug('tank = '+tank);
        ApexPages.currentPage().getParameters().put(ID_VAR,acc.Id);
        ApexPages.currentPage().getParameters().put(RECTYPESTR,rectypeId);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(acc));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectAccount();
        Test.stopTest();
        system.assertEquals(false,pageRedirect!=null);
    }
    // Changes made for CUSTOMER MODIFICATION L4 End
    static testMethod void redirectAccountPositive_test(){
        Account account = EP_TestDataUtility.getSellTo();
        account.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update account;
        rectypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.SELL_TO);
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        ApexPages.currentPage().getParameters().put(ID_VAR,account.Id);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(account));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectAccount();
        Test.stopTest();
        system.assertEquals(true,pageRedirect!=null);
    }

    static testMethod void redirectAccountNegative2_test(){
        Account account = EP_TestDataUtility.getShipTo();
        account.EP_Status__c = EP_Common_Constant.STATUS_REJECTED;
        update account;

        rectypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.SELL_TO);
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        ApexPages.currentPage().getParameters().put(ID_VAR,account.Id);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(account));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectAccount();
        Test.stopTest();
        system.assertEquals(false,pageRedirect!=null);
    }

    static testMethod void redirectAccountNegative3_test(){
        Account account = EP_TestDataUtility.getShipTo();
        rectypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.SELL_TO);
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        ApexPages.currentPage().getParameters().put(ID_VAR,account.Id);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(account));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectAccount();
        Test.stopTest();
        system.assertEquals(true,pageRedirect!=null);
    }

    static testMethod void redirectAccountNegative4_test(){
        Account account = EP_TestDataUtility.getSellTo();
        account.EP_Is_Dummy__c = true;
        update account;
        rectypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.SELL_TO);
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        ApexPages.currentPage().getParameters().put(ID_VAR,account.Id);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(account));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectAccount();
        Test.stopTest();
        system.assertEquals(true,pageRedirect!=null);
    }

    static testMethod void redirectAccountNegativeCatch_test(){
        Account account = EP_TestDataUtility.getSellTo();
        EP_Bank_Account__c bankAccount = EP_TestDataUtility.createBankAccount(account.id);
        insert bankAccount;
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        //ApexPages.currentPage().getParameters().put(ID_VAR,bankAccount.Id);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(bankAccount));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectAccount();
        Test.stopTest();
        system.assertEquals(false,pageRedirect!=null);
    }
	/*
    static testMethod void redirectBankAccountPositive_test(){
        Account account = EP_TestDataUtility.getSellTo();
        account.EP_Status__c = EP_Common_Constant.STATUS_REJECTED;
        update account;
        EP_Bank_Account__c bankAccount = EP_TestDataUtility.createBankAccount(account.id);
        insert bankAccount;
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        ApexPages.currentPage().getParameters().put(ID_VAR,bankAccount.Id);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(bankAccount));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectBankAccount();
        Test.stopTest();
        system.assertEquals(true,pageRedirect!=null);
    }

    static testMethod void redirectBankAccountNegative_test(){
        Account account = EP_TestDataUtility.getSellTo();
        EP_Bank_Account__c bankAccount = EP_TestDataUtility.createBankAccount(account.id);
        insert bankAccount;
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        ApexPages.currentPage().getParameters().put(ID_VAR,bankAccount.Id);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(bankAccount));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectBankAccount();
        Test.stopTest();
        system.assertEquals(true,pageRedirect!=null);
    }

    static testMethod void redirectBankAccountNegativeCatch_test(){
        Account account = EP_TestDataUtility.getSellTo();
        EP_Bank_Account__c bankAccount = EP_TestDataUtility.createBankAccount(account.id);
        insert bankAccount;
        Test.setCurrentPage(Page.EP_RedirectTankEdit);
        //ApexPages.currentPage().getParameters().put(ID_VAR,bankAccount.Id);
        EP_CustomerEditRedirectController localObj = new EP_CustomerEditRedirectController(new ApexPages.StandardController(bankAccount));
        Test.startTest();
        pageReference pageRedirect = localObj.redirectBankAccount();
        Test.stopTest();
        system.assertEquals(false,pageRedirect!=null);
    }
	*/
}