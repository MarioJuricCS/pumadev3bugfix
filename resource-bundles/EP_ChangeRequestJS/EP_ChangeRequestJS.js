	function clearLookup(orignalVal,lookupId, hiddnCheckBoxId,imgID){
		var hiddnfld = lookupId.replace("newValInput", "newValuehidden");
		var hiddnRef = lookupId.replace("newValInput", "newRefValuehidden");
		document.getElementById(hiddnfld).value = '';
		document.getElementById(lookupId).value = '';
		document.getElementById(hiddnRef).value = '';
		if(orignalVal != '' ){
			document.getElementById(imgID).style.display = "";
			document.getElementById(hiddnCheckBoxId).checked = true;
		}else{
			document.getElementById(imgID).style.display = "none";
			document.getElementById(hiddnCheckBoxId).checked = false;
		}
		showHideSubmitButton();
	}
	function confirmSubmit(submitMessage) {
			var r = confirm(submitMessage);
			return r;
	}
	function showDiv(){
		document.getElementById('pnlLoading').style.display = '';
	}
	function hideDiv(){
		document.getElementById('pnlLoading').style.display = 'none';
	}
	function copyNewValue(fieldType , newValHiddenFldID, newValueFIeld, oldValue, comID){
		var newValueFIeldId = newValueFIeld.id;
		if(fieldType == 'REFERENCE' && newValueFIeld.value !=undefined){
			newValueFIeldId = comID.replace("newVal", "newValInput_lkid");
			
			//document.getElementById(comID.replace("newVal", "newRefValuehidden")).value = newValueFIeld.value;
			
			document.getElementById(comID.replace("newVal", "newValuehidden")).value;
			var outputField =  document.getElementById(comID.replace("newVal", "newRefValuehidden"))
		}
		var newCHeck = document.getElementById(newValueFIeldId);
		if(newValueFIeld.value !=undefined){
			document.getElementById(newValHiddenFldID).value = newCHeck.value;
		}   
		var fieldVal = document.getElementById(newValHiddenFldID).value ;
		if(fieldType == 'REFERENCE'){
			var opRefId = comID.replace("newVal", "newOrigRefValuehidden");
			var opId  = comID.replace("newVal", "orgValInput");
			var outputField =  document.getElementById(opId);
			
			var childs = $(outputField).children().length;
			
			if(childs > 0){
				document.getElementById(opRefId).value = $(outputField).first().text();
				
			}
			if(oldValue != '' && oldValue.length == 18){
				oldValue = oldValue.substring(0, 15);
			}
			if(fieldVal == '000000000000000'){
				fieldVal = ''; 
			}
			
			document.getElementById(comID.replace("newVal", "newValuehidden")).value = newValueFIeld.value;
			document.getElementById(comID.replace("newVal", "newRefValuehidden")).value =fieldVal;
			//fieldVal = newValueFIeld.value;
			//document.getElementById(opRefId).value = oldValue;
		}
		else if(fieldType == 'DOUBLE' && fieldVal != ''){
			if(fieldVal != '' && fieldVal.indexOf(",") != -1){
				fieldVal = fieldVal.replace(/,/g , "");//fieldVal.replace(",","");    
			}
			fieldVal = parseFloat(fieldVal);
			if(oldValue != '' && oldValue.indexOf(",") != -1){
				oldValue = oldValue.replace(/,/g , "");//oldValue.replace(",","");    
				oldValue = parseFloat(oldValue);
			}
			document.getElementById(newValHiddenFldID).value=fieldVal;
		}
		else if(fieldType == 'BOOLEAN'){
			fieldVal = newValueFIeld.checked.toString();
			document.getElementById(newValHiddenFldID).value=fieldVal;
		}
		var checkBoxFIeld = document.getElementById(comID.replace("newVal", "changeCHeck"));
		var imageFIeldld = document.getElementById(comID.replace("newVal", "imgID"));
	   
		if(oldValue != fieldVal){
			checkBoxFIeld.checked = true;
			imageFIeldld.style.display = "";
		}else{
			checkBoxFIeld.checked = false;  
			imageFIeldld.style.display = "none";
		}
		showHideSubmitButton();
	}
	function showChange(oldVal, newVAl , checkId, imgID){
		if(oldVal != newVAl){
			document.getElementById(checkId).checked = true;
			document.getElementById(imgID).style.display = "";
		}else{
			document.getElementById(checkId).checked = false;
			document.getElementById(imgID).style.display = "none";
		}
	}
	function showHideSubmitButton(){
		var showsubmit = false;
		$('#btnsect').hide();
		$('.changeTrack').each(function() {
			if($(this).is(':checked')){
				showsubmit = true;
				document.getElementById(this.id.replace("changeCHeck","imgID")).style.display = "";
			}
		});
		if(showsubmit){
			$('#btnsect').show();
		}
	}   
