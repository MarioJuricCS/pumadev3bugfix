@isTest
private class CreateBasketAndOfferController_UT
{
    
    @isTest
    static void createBasket_test() {

        Account acc = createAccount();      
        
        cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c();
        productDefinition.Name = 'Puma Energy Order';
        productDefinition.cscfga__Description__c = 'Puma Energy Order';
        insert productDefinition;
         
        cscfga__Product_Configuration__c testProductConfiguration = new cscfga__Product_Configuration__c();
        testProductConfiguration.Name = 'Puma Energy Order';
        testProductConfiguration.cscfga__Product_Definition__c = productDefinition.id;
        insert testProductConfiguration;
        
        csoe__Non_Commercial_Schema__c nonCommercialSchema = new csoe__Non_Commercial_Schema__c(csoe__Description__c = 'Test',csoe__Schema__c = 'csoe__Schema__c');
        insert nonCommercialSchema;
        
        csoe__Non_Commercial_Product_Association__c productAssociation = new csoe__Non_Commercial_Product_Association__c(csoe__Commercial_Product_Definition__c =testProductConfiguration.cscfga__Product_Definition__c,csoe__Non_Commercial_Schema__c = nonCommercialSchema.Id);
        insert productAssociation;
        
        CS_ORDER_SETTINGS__c orderSettings = new CS_ORDER_SETTINGS__c(Parent_Order_Name__c = testProductConfiguration.Name,OfferTemplateId__c = '07M5B000003CdUY');
        insert orderSettings;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        CreateBasketAndOfferControllerNew controller = new CreateBasketAndOfferControllerNew(sc);
        controller.createBasket();

        String basketName = 'Order for ' + acc.Name;
        cscfga__Product_Basket__c createdBasket = [SELECT ID, name, csbb__Account__c from cscfga__Product_Basket__c limit 1];

        System.assertEquals(basketName, createdBasket.name);
        System.assertEquals(acc.id, createdBasket.csbb__Account__c);

        CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
        String iFrame = '<iframe id="mle-iframe" src="'+csOrderSetting.MLE_Org_URL__c+'?batchSize='+csOrderSetting.MLE_BatchSize__c+'&id=' + createdBasket.id + '&productDefinitionId=' 
        +csOrderSetting.OrderLineItemProductDefinitionLineId__c +'&showHeader='+csOrderSetting.MLE_ShowHeader__c+'&sideBar='+csOrderSetting.MLE_SideBar_Selected__c+'&embedded='
        +csOrderSetting.MLE_Embedded__c+'&cssoverride='+csOrderSetting.CSS_Resource_Path__c+'&scriptplugin='+csOrderSetting.Javascript_Resource_Path__c
        +'" width="'+csOrderSetting.MLE_IFRAME_WIDTH__c+'" height="'+csOrderSetting.MLE_HEIGHT__c+'" frameBorder="'+csOrderSetting.MLE_FrameBorder__c+'"></iframe>';
        System.assertEquals(0, controller.attributes.size());
        
    }
    
    
    private static Account createAccount(){        
        Account acc = new Account();        
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccountyy';
        acc.EP_Status__c = '06-Blocked';
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        insert acc;
        return acc;
    }    

    
    @isTest
    static void editBasket_test() {
    
        Account acc = createAccount();
        cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c();
        productDefinition.Name = 'Puma Energy Order';
        productDefinition.cscfga__Description__c = 'Puma Energy Order';
        insert productDefinition;
         
        cscfga__Product_Configuration__c testProductConfiguration = new cscfga__Product_Configuration__c();
        testProductConfiguration.Name = 'Puma Energy Order';
        testProductConfiguration.cscfga__Product_Definition__c = productDefinition.id;
        insert testProductConfiguration;
        
        csoe__Non_Commercial_Schema__c nonCommercialSchema = new csoe__Non_Commercial_Schema__c(csoe__Description__c = 'Test',csoe__Schema__c = 'csoe__Schema__c');
        insert nonCommercialSchema;
        
        csoe__Non_Commercial_Product_Association__c productAssociation = new csoe__Non_Commercial_Product_Association__c(csoe__Commercial_Product_Definition__c =testProductConfiguration.cscfga__Product_Definition__c,csoe__Non_Commercial_Schema__c = nonCommercialSchema.Id);
        insert productAssociation;
        
        CS_ORDER_SETTINGS__c orderSettings = new CS_ORDER_SETTINGS__c(Parent_Order_Name__c = testProductConfiguration.Name,OfferTemplateId__c = '07M5B000003CdUY');
        insert orderSettings;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);
        CreateBasketAndOfferControllerNew controller = new CreateBasketAndOfferControllerNew(sc);

        controller.connectConfigurations();
        controller.editBasket();

        String basketName = 'Order for ' + acc.Name;
        cscfga__Product_Basket__c createdBasket = [SELECT ID, name, csbb__Account__c from cscfga__Product_Basket__c limit 1];

        System.assertEquals(basketName, createdBasket.name);
        System.assertEquals(acc.id, createdBasket.csbb__Account__c);

    }
    
}