/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToBlockedToBlocked>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from 06-Blocked to 06-Blocked>
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToBlockedToBlocked extends EP_AccountStateTransition {

    public EP_ASTNonVMIShipToBlockedToBlocked () {
        finalState = EP_AccountConstant.BLOCKED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBlockedToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBlockedToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBlockedToBlocked','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToBlockedToBlocked','doOnExit');

    }
}