public with sharing class EP_ASTSellToBasicDataToRejected extends EP_AccountStateTransition {
    public EP_ASTSellToBasicDataToRejected() {
        finalState = EP_AccountConstant.REJECTED;   
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataToRejected','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataToRejected','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToBasicDataToRejected','isGuardCondition');
        return true; 
    }
    
}