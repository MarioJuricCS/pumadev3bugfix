public with sharing class EP_CRM_SelectCustomReportController extends EP_CRM_UserAuthorization {
    public PageReference redirectToReport() {
        String reportType = ApexPages.currentPage().getParameters().get('reportName');
        if(reportType == EP_CRM_Constants.PIPELINE_REPORT) {
            return Page.PipelineReportPage;
        }
        return null;
    }
    
    public PageReference redirectToAdoptionReport() {
        String reportType = ApexPages.currentPage().getParameters().get('reportName');
        if(reportType == EP_CRM_Constants.ADOPTION_REPORT) {
            return Page.AdoptionRateReport;
        }
        return null;
    }      
}