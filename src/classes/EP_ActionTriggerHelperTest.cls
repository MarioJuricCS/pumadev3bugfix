/**
 * @author <Sandeep Kumar>
 * @name <EP_ActionTriggerHelperTest>
 * @createDate <06/11/2016>
 * @description
 * @version <1.0>
 */
@isTest
public class EP_ActionTriggerHelperTest {
    
     //Code changes for #45436
    /*********************************************************************************************
    *@Description : This method valids restrictActionOwnerChange method of EP_ActionTriggerHelper class
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    @testSetup static void init() {
        list<user> users = new list<user>();
        profile objProfile = [select id from profile where name= 'EP_Business Support Manager' limit 1];
        user objUser = EP_TestDataUtility.createUser(objProfile.id);
        users.add(objUser);
        user objUser1 =EP_TestDataUtility.createUser(objProfile.id);
        users.add(objUser1);
        
        database.insert(users);
        /*
        account acc = EP_TestDataUtility.getSellToPositiveScenario();
        acc.EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';
        acc.EP_Synced_NAV__c = true;
        acc.EP_Synced_PE__c = true;
        acc.EP_Synced_WINDMS__c = true;
        acc.EP_Status__c = '02-Basic Data Setup';
        update acc; */
        Account accShipTo = EP_TestDataUtility.getShipToPositiveScenario();
        accShipTo.EP_Synced_NAV__c = true;
        accShipTo.EP_Synced_PE__c = true;
        accShipTo.EP_Synced_WINDMS__c = true;
        accShipTo.EP_Status__c = '02-Basic Data Setup';
        update accShipTo; 

        List<account> sellToAcc = [select id,EP_Indcative_Total_Purchase_Value_Yr_USD__c, EP_Synced_NAV__c,EP_Synced_PE__c,EP_Synced_WINDMS__c,EP_Status__c,EP_Puma_Company__c From Account Where Id =: accShipTo.ParentId];
        system.debug('accShipTo%%' +accShipTo);

        if(!sellToAcc.isEmpty()){  
          sellToAcc[0].EP_Indcative_Total_Purchase_Value_Yr_USD__c = '1000';      
          sellToAcc[0].EP_Synced_NAV__c = true;
          sellToAcc[0].EP_Synced_PE__c = true;
          sellToAcc[0].EP_Synced_WINDMS__c = true;
          sellToAcc[0].EP_Status__c = '02-Basic Data Setup';          
        }
        update sellToAcc;
         
        EP_TestDataUtility.createUserCompany(objUser.id,sellToAcc[0].EP_Puma_Company__c);
        EP_TestDataUtility.createUserCompany(objUser1.id,sellToAcc[0].EP_Puma_Company__c);
    }
    
    
    private static EP_Action__c createAction() {
        Account account = [select Id from account limit 1];
        user objUser = [select Id from user where Profile.Name = 'EP_Business Support Manager' limit 1];
        EP_Action__c action = new EP_Action__c();
        action.RecordTypeId = Schema.SObjectType.EP_Action__c.getRecordTypeInfosByName().get('BSM/GM Review').getRecordTypeId();
        action.EP_Account__c = account.id;
        action.OwnerId = objUser.Id;
        database.insert( action);
        return action;
    }
    /*private static testMethod void testRestrictActionOwnerChange_POSITIVE(){ 
        EP_Action__c action  = createAction();
        user objUser1 = [select Id from user where Id !=: action.OwnerId and Profile.Name = 'EP_Business Support Manager' limit 1];
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = false;    
        Test.startTest();
            action.OwnerId = objUser1.Id;
            //database.update(action);
        Test.stopTest(); 
        EP_Action__c action1 = [select OwnerId from  EP_Action__c where Id=:action.Id limit 1];
        System.AssertEquals(action1.OwnerId,objUser1.Id);
    }*/
    
    private static testMethod void testRestrictActionOwnerChange_Negative(){ 
        list<user> users = new list<user>();
        EP_Action__c action  = createAction();
        profile objProfile1 = [select id from profile where name= 'EP_CSC' limit 1];
        user objUser1 =EP_TestDataUtility.createUser(objProfile1.id);
        users.add(objUser1);
        database.insert(users);   
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = false;    
        try {
            Test.startTest();
                action.OwnerId = objUser1.Id;
                database.update(action);
            Test.stopTest(); 
        } catch (Exception exp) {
            system.assertEquals(true, exp.getMessage().contains(Label.EP_Insufficient_Permissions));
        }
    }
    
    private static testMethod void updateAccountStatus_UT(){ 
        Account acc = [select Id from Account limit 1];
        
        Test.startTest();   
             EP_ActionTriggerHelper.updateAccountStatus(new set<id>{acc.id}); 
        Test.stopTest();
        
        Account acc1 = [select Id,EP_Status__c from Account where id =:acc.Id limit 1];
        system.assertEquals(acc1.EP_Status__c,EP_Common_Constant.STATUS_SET_UP);
    }
    
    /*private  static testMethod void assignQueueChangeActions_UT() {
        Id chngRqstTypeAction = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACT_OBJ,EP_Common_Constant.ACT_CR_RT);
        EP_Action__c action  = createAction();
        action.RecordTypeId = chngRqstTypeAction;
        action.EP_Action_Name__c = 'BSM/GM Review';
        action.EP_Region__c = 'Australian Capital Territory';
        action.EP_Country__c = 'AU';
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

       system.runAs(thisUser){
            Test.startTest();   
                 EP_ActionTriggerHelper.assignQueueChangeActions(new list<EP_Action__c>{action}); 
            Test.stopTest();
       }
       system.assertNotEquals(null,action.EP_System_Queue__c);
    }*/

    public static testMethod void assignQueueAndRecordType_test(){
      String REVIEWQUEUE = 'EP_Review_Queue';
      string QUEUE_STR = 'Queue';
      List<EP_Action__c> actionList = createActionRecord('BSM/GM Review');
      Test.startTest();
        database.insert(actionList);
      Test.stopTest();
      EP_Action__c action = [select id,OwnerId,RecordTypeId From EP_Action__c Limit 1];
      //Assert Review queue is been assigend to the action record
      list<Group> lstqueue = [select Id From Group Where DeveloperName =: REVIEWQUEUE And Type =: QUEUE_STR Limit 1];      
      system.assertEquals(lstqueue[0].id,action.OwnerId);
    }

    public static testMethod void DeactivateAllPriceBookEntries_test(){
      String REVIEWQUEUE = 'EP_Review_Queue';
      string QUEUE_STR = 'Queue';
      Account sellToAccount = EP_TestDataUtility.getSellToASProspectDomainObject().localAccount;
      EP_Product_Option__c productlist = [select id,Price_List__c from EP_Product_Option__c where  sell_To__c =: sellToAccount.id];
      List<EP_Action__c> actionList = createActionRecord('Price Book Review');
      for(EP_Action__c action : actionList){
        action.EP_Product_List__c = productlist.Price_List__c;
      }
      Test.startTest();
        database.insert(actionList);
      Test.stopTest();
      List<PricebookEntry> pbeList = [select id,IsActive From PricebookEntry where Pricebook2Id =: productlist.Price_List__c];
      //Assert Review queue is been assigend to the action record
      for(PricebookEntry pbe: pbeList) {
        system.assertEquals(false,pbe.IsActive);
      }     
    }

    public static testMethod void ActivateAllPriceBookEntries_test(){
      Account sellToAccount = EP_TestDataUtility.getSellToASProspectDomainObject().localAccount;
      EP_Product_Option__c productlist = [select id,Price_List__c from EP_Product_Option__c where  sell_To__c =: sellToAccount.id];
      List<EP_Action__c> actionList = createActionRecord('Price Book Review');
      for(EP_Action__c action : actionList){
        action.EP_Product_List__c = productlist.Price_List__c;
      }
      database.insert(actionList);
      User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
      insert new EP_CS_Validation_Rule_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), EP_Skip_Action_Rules__c=true);      
      Test.startTest();
          EP_Action__c pricebookAction = [select id,EP_Status__c,EP_Action_Name__c,EP_Product_List__c from EP_Action__c where EP_Action_Name__c = 'Price Book Review' Limit 1];
          system.debug('pricebookAction##'+pricebookAction);
          //pricebookAction.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
          pricebookAction.OwnerId = thisUser.id;
          database.update(pricebookAction);

          pricebookAction.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
          database.update(pricebookAction);
          EP_ActionTriggerHelper.ActivateAllPriceBookEntries(new set<Id>{productlist.Price_List__c});
      Test.stopTest();
      List<PricebookEntry> pbeList = [select id,IsActive From PricebookEntry where Pricebook2Id =: productlist.Price_List__c];
      //Assert Review queue is been assigend to the action record
      for(PricebookEntry pbe: pbeList) {
        //system.assertEquals(true,pbe.IsActive);
      }     
    }

    public static testMethod void isBSMGMRequired_test(){
      String REVIEWQUEUE = 'EP_Review_Queue';
      string QUEUE_STR = 'Queue';
      List<EP_Action__c> actionUpdateList = new List<EP_Action__c>();
      User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
      insert new EP_CS_Validation_Rule_Settings__c(SetupOwnerId=UserInfo.getOrganizationId(), EP_Skip_Action_Rules__c=true);
      Test.startTest();
        for(EP_Action__c actn : [select id,OwnerId,RecordTypeId,EP_Status__c,EP_Action_Name__c,EP_Account__c From EP_Action__c]){
          actn.OwnerId = thisUser.id;
          actn.EP_Status__c = EP_Common_Constant.ACT_COMPLETED_STATUS;
          actionUpdateList.add(actn);
          system.debug('$$REC'+actn.RecordTypeId +'$$NAme '+actn.EP_Action_Name__c + '$$Acc ' + actn.EP_Account__c);          
        }
        database.update(actionUpdateList);
      Test.stopTest();
    }

    private static List<EP_Action__c> createActionRecord(string actionName){
      Account sellToAccount = EP_TestDataUtility.getSellToASProspectDomainObject().localAccount;
      List<EP_Action__c> actionList =  new List<EP_Action__c>();
      EP_Action__c action = new EP_Action__c();
      action.EP_Account__c = sellToAccount.id;
      action.EP_Record_Type_Name__c = actionName;
      action.EP_Action_Name__c = actionName;
      action.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
      actionList.add(action);
      return actionList;
    }
}