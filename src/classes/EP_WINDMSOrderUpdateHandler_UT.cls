@isTest
public class EP_WINDMSOrderUpdateHandler_UT
{
    public static final String ACC_NAME = 'Account1';
     
    
    static testMethod void processRequest_test() {
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'Teopter', Email='OperationsTank@testorg.com',
        EmailEncodingKey='UTF-8', LastName='priceentr', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='OperationsTank@testorg.com');
        System.runAs(u) {
        
      
        EP_WINDMSOrderUpdateHandler localObj = new EP_WINDMSOrderUpdateHandler();
        EP_WINDMSOrderUpdateStub stub = EP_TestDataUtility.createWINDMSOrderUpdateStub();
        
        
        list<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = stub.MSG.Payload.Any0.OrderStatuses.orderStatus;
        //orderWrapperList.get(0).sfOrder = OrderObject;
        orderWrapperList.get(0).sfOrder.EP_WinDMS_Status__c = 'Loaded';
        orderWrapperList.get(0).orderStatusWinDms = 'Loaded';
        
        String jsonBody = JSON.serialize(stub);
        
        Test.startTest();
            String result = localObj.processRequest(jsonBody);
        Test.stopTest();
        
        System.AssertEquals(true,result!=null);
        }
    }
    static testMethod void processRequest_negativeTest() {
        String jsonBody = '';
        
        Test.startTest();
            EP_WINDMSOrderUpdateHandler localObj = new EP_WINDMSOrderUpdateHandler();
            String result = localObj.processRequest(jsonBody);
        Test.stopTest();
        
        System.AssertEquals(true,result!=null);
    }
    static testMethod void JSONParse_test() {
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'Teopter', Email='OperationsTank@testorg.com',
        EmailEncodingKey='UTF-8', LastName='priceentr', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='OperationsTank@testorg.com');
        System.runAs(u) {
    
        EP_WINDMSOrderUpdateStub stub = EP_TestDataUtility.createWINDMSOrderUpdateStub();
        list<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = stub.MSG.Payload.Any0.OrderStatuses.orderStatus;
        orderWrapperList.get(0).sfOrder = new csord__Order__c();
        String jsonBody = JSON.serialize(stub);
        system.debug('**jsonBody**' + jsonBody);
        Test.startTest();
            EP_WINDMSOrderUpdateStub deserializeStub = (EP_WINDMSOrderUpdateStub )  System.JSON.deserialize(jsonBody, EP_WINDMSOrderUpdateStub.class);
        Test.stopTest();
        system.debug('**deserializeStub**' + deserializeStub);
        System.AssertEquals(true,deserializeStub!=null);
        }
    }
    
    static testMethod void processOrderForUpdate_test() {
    
      Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'Teopter', Email='OperationsTank@testorg.com',
        EmailEncodingKey='UTF-8', LastName='priceentr', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='OperationsTank@testorg.com');
        System.runAs(u) {
    
        List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        
        orderWrapperList.get(0).errorDescription = '';
        
        Test.startTest();
            EP_WINDMSOrderUpdateHandler.processOrderForUpdate(orderWrapperList);
        Test.stopTest();
        csord__Order__c ordObj = [SELECT Id, EP_Order_Comments__c,EP_SeqId__c FROM csord__Order__c WHERE Id =:orderWrapperList[0].sfOrder.id ];
        System.AssertEquals(orderWrapperList.get(0).sfOrder.EP_SeqId__c,ordObj.EP_SeqId__c);
     }   
    }
    static testMethod void processOrderForUpdate_NegativeScenariotest() {
        List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
       
        orderWrapperList[0].sfOrder.EP_SeqId__c = 'SEQ-123';
        orderWrapperList[0].errorDescription = 'Error';
    
        Test.startTest();
            EP_WINDMSOrderUpdateHandler.processOrderForUpdate(orderWrapperList);
        Test.stopTest();
        
        csord__Order__c ordObj = [SELECT Id, EP_Order_Comments__c,EP_SeqId__c FROM csord__Order__c WHERE Id =:orderWrapperList[0].sfOrder.id ];
        System.AssertNotEquals(orderWrapperList.get(0).sfOrder.EP_SeqId__c,ordObj.EP_SeqId__c);
        
    } 
    
    static testMethod void updateOrders_test() {
        List<csord__Order__c> orderList = new List<csord__Order__c>{EP_TestDataUtility.getSalesOrder()};
        orderList[0].EP_Order_Comments__c = 'TEXT';
        
        Test.startTest();
            EP_WINDMSOrderUpdateHandler.updateOrders(orderList);
        Test.stopTest();
        
        csord__Order__c ordObj = [SELECT Id, EP_Order_Comments__c FROM csord__Order__c WHERE Id =:orderList[0].id ];
        System.AssertEquals('TEXT', ordObj.EP_Order_Comments__c);
    }
    static testMethod void updateOrders_NegativeTest() {
        List<csord__Order__c> orderList = new List<csord__Order__c>{EP_TestDataUtility.getSalesOrder()};
        orderList[0].EP_Order_Comments__c = 'TEXT';
        orderList[0].Name = '012345678901234567890123456789012345678901234567890123456789012345678901234567890';
        
        Test.startTest();
            EP_WINDMSOrderUpdateHandler.updateOrders(orderList);
        Test.stopTest();
        
        csord__Order__c ordObj = [SELECT Id, EP_Order_Comments__c FROM csord__Order__c WHERE Id =:orderList[0].id ];
        System.AssertNotEquals('TEXT', ordObj.EP_Order_Comments__c);
    }
    static testMethod void processUpsertErrors_test() {
        List<csord__Order__c> orderList = new List<csord__Order__c>{EP_TestDataUtility.getSalesOrder()};
        orderList[0].Name = '012345678901234567890123456789012345678901234567890123456789012345678901234567890';
        List<Database.Error> errorList = new List<Database.Error>();
        Database.UpsertResult[] srList = Database.upsert(orderList, false);
        errorList.add(srList[0].getErrors()[0]);

        Test.startTest();
            EP_WINDMSOrderUpdateHandler.processUpsertErrors(errorList,'seqId');
        Test.stopTest();
        system.AssertEquals(true,EP_WINDMSOrderUpdateHandler.ackResponseList.size() > 0);
    }
    static testMethod void createResponse_test() {
        EP_WINDMSOrderUpdateHandler localObj = new EP_WINDMSOrderUpdateHandler ();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        EP_WINDMSOrderUpdateStub.OrderWrapper ordWrp = orderWrapperList[0];
        String seqId = ordWrp.seqId;
        String errorCode = ordWrp.errorCode;
        String errorDescription = ordWrp.errorDescription;
        Test.startTest();
        EP_WINDMSOrderUpdateHandler.createResponse(seqId,errorCode,errorDescription);
        Test.stopTest();
        System.AssertEquals(true,seqId!=null);
        System.AssertEquals(true,errorCode!=null);
        System.AssertEquals(true,errorDescription!=null);
        
    }
  
}
