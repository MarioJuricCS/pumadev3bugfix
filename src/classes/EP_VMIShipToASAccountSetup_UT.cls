@isTest
public class EP_VMIShipToASAccountSetup_UT
{

    static final string EVENT_NAME = '04-AccountSet-upTo04-AccountSet-up';
    static final string INVALID_EVENT_NAME = '08-ProspectTo04-Account Set-up';
    /*  
       @description: method to intialise data
    */
    @testSetup static void init() {
      List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
      List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    }
    
    static testMethod void setAccountDomainObject_test() {
        EP_VMIShipToASAccountSetup localObj = new EP_VMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        Test.startTest();
        localObj.setAccountDomainObject(obj);
        Test.stopTest();
        system.assertEquals(obj.getAccount().Id,localObj.account.getAccount().Id);
    }
    //Delegates to other methods. Adding dummy assert 
    static testMethod void doOnEntry_test() {
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        EP_VMIShipToASAccountSetup localObj = new EP_VMIShipToASAccountSetup();
        Account newAccount = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectPositiveScenario().getAccount();
        Account sellToAccount = [select id,EP_Status__c from Account Where Id =: newAccount.ParentId Limit 1];
        sellToAccount.EP_Status__c = EP_AccountConstant.ACTIVE;
        update sellToAccount;
        Account oldAccount = newAccount.clone();
        oldAccount.EP_Status__c = EP_AccountConstant.BASICDATASETUP;
        EP_AccountDomainObject obj  = new EP_AccountDomainObject(newAccount, oldAccount);
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnEntry();
        Test.stopTest();
        //System.assertEquals(true, true); 
         System.AssertEquals(EP_AccountConstant.BASICDATASETUP,oldAccount.EP_Status__c);
        
    }
     
    //Method has no implementation, hence adding dummy assert 
    static testMethod void doOnExit_test() {
        EP_VMIShipToASAccountSetup localObj = new EP_VMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        localObj.doOnExit();
        Test.stopTest();
        System.assertEquals(true, true); 
    }
    static testMethod void doTransition_PositiveScenariotest() {
        EP_VMIShipToASAccountSetup localObj = new EP_VMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent(EVENT_NAME);
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.doTransition();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void doTransition_NegativeScenariotest() {
        EP_VMIShipToASAccountSetup localObj = new EP_VMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectNegativeScenario();
        EP_AccountEvent oe = new EP_AccountEvent(INVALID_EVENT_NAME);
        localObj.setAccountContext(obj,oe);
         List<Exception> excList = new List<Exception>();
        Test.startTest();
        try{
            Boolean result = localObj.doTransition();
            }
        catch(Exception e){
            excList.add(e);
        }
        Test.stopTest();
        System.Assert(excList.size()>0);
    }
    
    /* isInboundTransitionPossible does not have negative scenerio because it does not have implementation. It always returns true */   
    static testMethod void isInboundTransitionPossible_PositiveScenariotest() {
        EP_VMIShipToASAccountSetup localObj = new EP_VMIShipToASAccountSetup();
        EP_AccountDomainObject obj = EP_TestDataUtility.getVMIShipToASAccountSetupDomainObjectPositiveScenario();
        EP_AccountEvent oe = new EP_AccountEvent('01-ProspectTo01-Prospect');
        localObj.setAccountContext(obj,oe);
        Test.startTest();
        Boolean result = localObj.isInboundTransitionPossible();
        Test.stopTest();
        System.AssertEquals(true,result);
    }

}