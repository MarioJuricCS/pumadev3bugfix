/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class EP_CompanyTriggerTest {
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';
    @testSetup
    static void dataSetup(){
        EP_Country__c country = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
        Database.insert(country,false);
        Company__c comp = EP_TestDataUtility.createCompany(EP_Common_Constant.EPUMA);
        comp.EP_BSM_GM_Review_Required__c = false;
        comp.EP_Disallow_DD_for_PP_customers__c = false; 
        database.insert(comp);
        EP_Region__c region = EP_TestDataUtility.createCountryRegion( REGION_NAME, country.Id);  
        Database.insert(region,false);
        
        EP_Payment_Method__c paymentMethod = EP_TestDataUtility.createPaymentMethod(EP_Common_Constant.DIRECT_DEBIT
                                                                                        ,EP_Common_Constant.PM_DIRECT_DEBIT_CODE);
        insert paymentMethod; 
        
        Account  sellToAccount =  EP_TestDataUtility.createSellToAccountWithPickupContry(NULL, NULL,country.Id, region.Id );
        sellToAccount.EP_Puma_Company__c = comp.id;
        sellToAccount.EP_Payment_Method__c = paymentMethod.id;
        sellToAccount.EP_Requested_Payment_Terms__c = EP_Common_Constant.PREPAYMENT;
        Database.insert(sellToAccount, false);
        
    }
    
    /*static testMethod void checkCompanyValidations() {
        // TO DO: implement unit test
        
        Company__c c = [Select id,EP_Disallow_DD_for_PP_customers__c 
                        from Company__c 
                        Where name=:EP_Common_Constant.EPUMA limit 1];
        
        system.assertEquals(false, c.EP_Disallow_DD_for_PP_customers__c);
        c.EP_Disallow_DD_for_PP_customers__c = true;
        Database.saveResult sr = Database.update(c, false);
        system.assertEquals(false, sr.isSuccess());
    }*/
}