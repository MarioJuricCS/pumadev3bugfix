/**
    @Author         Accenture
    @Name           EP_ROImportStagingMapper 
    @Createdate     12/28/2016
    @Description    This class contains all SOQLs related to EP_RO_Import_Staging Object
    @Version        1.0
    @Reference      NA
*/
public with sharing class EP_ROImportStagingMapper {
    
    /**     
        * @Author       Accenture
        * @Name         EP_ROImportStagingMapper 
        * @Description  Constructor
    */
    public EP_ROImportStagingMapper() { } 
    
    public Id getRecordTypeIdByName(string recordTypeName) {
    	return Schema.SObjectType.EP_RO_Import_Staging__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }
    
    public list<EP_RO_Import_Staging__c> getRecordByFileId(Id fileId) {
    	return [SELECT  EP_Ambient_Delivered_Quantity__c,
    					EP_Ambient_Loaded_Quantity__c,
    					EP_BOL_Number__c,
    					EP_CompanyCode__c,
    					EP_Contract_Number__c,
    					EP_Contract__c,
    					EP_Customer_PO_Number__c,
    					EP_Delivered_Date__c,
    					EP_Delivery_Docket_Number__c,
    					EP_Delivery_Reference_No__c,
    					EP_Duplicate_Identifier__c,
    					EP_File_Name__c,
    					EP_Loading_Date__c,
    					EP_Ordered_quantity__c,
    					EP_Order_Number__c,
    					EP_Product_Code__c,
    					EP_Product__c,
    					EP_Sell_To_Composit_key__c,
    					EP_Sell_To_Number__c,
    					EP_Sell_To__c,
    					EP_Ship_To_Composit_Key__c,
    					EP_Ship_To_Number__c,
    					EP_Ship_To__c,
    					EP_Spreadsheet_Row_Number__c,
    					EP_Standard_Delivered_Quantity__c,
    					EP_Standard_Loaded_Quantity__c,
    					EP_Status__c,
    					EP_Supplier_Number__c,
    					EP_Supplier__c,
    					EP_SupplyLocation__c,
    					EP_Supply_Location_Code__c,
    					EP_Transporter_Number__c,
    					EP_Transporter__c,
    					Id,
    					Name,
    					RecordTypeId,
    					Staging_Order_Identifier__c  
    					FROM EP_RO_Import_Staging__c 
    					where EP_File_Name__c =: fileId];
    }
}