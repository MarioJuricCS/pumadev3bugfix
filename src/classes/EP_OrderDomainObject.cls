/**
 *  @Author <Aravindhan Ramalingam>
 *  @Name <EP_OrderDomainObject>
 *  @CreateDate <04/02/2017>
 *  @Description <Domain object to store Order and Order related status>
 *  @NovaSuite Fix -- comments added
 *  @Version <1.0>
 */
 public class EP_OrderDomainObject {
    private csord__Order__c localorder;
    private List<csord__Order_Line_Item__c> orderitemlist;
    //Represents the Current Order Status. This status field will change while passing the context through state machine
    private String status;
    //represents the Status of the order right before sending into the State Machine.  Initial state of this will be the current status of Order
    //****do not remove or modify this *******
    public String previousStatus;    
    private boolean hasCreditIssues = false;
    private boolean hasOverdue = false;
    private EP_OrderMapper orderMapper = new EP_OrderMapper();
    private EP_StockHoldingLocationMapper stockHoldingLocationMapper = new EP_StockHoldingLocationMapper();
    private EP_InventoryMapper inventoryMapper = new EP_InventoryMapper();
    private EP_PriceBookEntryMapper pricebookentryMapper = new EP_PriceBookEntryMapper();
    public String availableSlots;
    private final String strClassPrefix = 'EP_';
    private final String strOrderType   = 'Order';
    private final String strStrategy    = 'Strategy';
    private final String strProducts    = 'Products';
    private EP_AccountDomainObject sellToAccountDomain;
    private EP_AccountDomainObject shipToAccountDomain;
    public EP_DeliveryType  delType;
    public EP_ProductSoldAs  psaType;
    public EP_VendorManagement  vmiType;
    public EP_Ordertype  oType;

    public EP_OrderEpoch  orderEpochType;
    public EP_ConsignmentType  consignType;
    //Defect Fix Start - #57360
    public boolean isQuantityChanged = false;
    public boolean isStockLocationChanged = false;
    //Defect Fix End - #57360
    public EP_OrderDomainObject(csord__Order__c orderObj){
        localorder = orderObj;
        orderitemlist = localorder.csord__Order_Line_Items__r;
        //set only once throughout in the context and never set explicitly.
        previousStatus = orderObj.status__c;
    }

    // Create domain object by passing by order ID
    public EP_OrderDomainObject(Id orderId)  {
        system.debug('ID is ' + orderId);
        localOrder = orderMapper.getCsRecordById(orderId);
        orderitemlist = localorder.csord__Order_Line_Items__r;
        //set only once throughout in the context and never set explicitly.
        previousStatus = localorder.status__c;
    }

    public EP_AccountDomainObject getSellToAccountDomain() {

        if (sellToAccountDomain == null) {
            system.debug('Order Sell To is ' + localorder.csord__Account__c);
            sellToAccountDomain = new EP_AccountDomainObject(localorder.csord__Account__c); 
            return sellToAccountDomain;
        }
        return sellToAccountDomain;
    }

    public EP_AccountDomainObject getShipToAccountDomain() {
        if (shipToAccountDomain == null) {
            shipToAccountDomain = new EP_AccountDomainObject(localorder.EP_ShipTo__c); 
            return shipToAccountDomain;
        }
        return shipToAccountDomain;
    }


    /**
    * @author Kalesph
    * @date 03/21/2017
    * @description Sets the orderitems locally in the orderdomain object
    * @param list<Orderitem>
    * @return 
    */
    public void setOrderItems(List<csord__Order_Line_Item__c> listOfOrderItems){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setOrderItems');
        orderitemlist = listOfOrderItems;
        System.debug('orderitemlist*****:-'+orderitemlist);
    }

    /**
    * @author Kalesph
    * @date 03/21/2017
    * @description returns the order items for the associated order object
    * @param 
    * @return list<Orderitem>
    */ 
    public List<csord__Order_Line_Item__c> getOrderItems(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getOrderItems');
        return orderitemlist;
    }

    // Only for testing
    public void setWinDMSStatus(String value) {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setWinDMSStatus');
        localorder.EP_WinDMS_Status__c = value;
    }

    public void setStatus(String statusvalue){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setStatus');
        localorder.status__c = statusvalue;
    }

    public String getStatus(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getStatus');
        return localorder.status__c;
    }

    public void resetStatus(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','resetStatus');
        localorder.Status__c = previousStatus;
        status = previousStatus;
    }

    
    /** this method is used to set the order type
     *  @date      06/02/2017
     *  @name      setType
     *  @param     ordType
     *  @return    NA
     *  @throws    NA
     */
    /*public void setType(String ordType) {
        orderType = ordType;
        }*/

    /** this method is used to get the order category
     *  @date      06/02/2017
     *  @name      getOrderTypeClassification
     *  @param     NA
     *  @return    String
     *  @throws    NA
     */
     public String getOrderTypeClassification() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getOrderTypeClassification');
        String stateMachineType;
        if(EP_Common_Constant.SALES.equalsIgnoreCase(getType())){
            //Adding retro word if the order is retrospective
            stateMachineType = EP_OrderConstant.Order_Epoch_Retro.equalsIgnoreCase(getEpochType())? EP_OrderConstant.ORDER_RETRO:'';
            // Constructing VMI/NonVMI + Consignment/NonConsignment
            stateMachineType = stateMachineType + getVMIType() + getConsignmentType();
        }
        else{
            stateMachineType = getType();
        }       
        return stateMachineType;
    }


    /** this method is used to get the order type
     *  @date      06/02/2017
     *  @name      getType
     *  @param     NA
     *  @return    String
     *  @throws    NA
     */
     public String getType(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getType');
        if (isRetrospective() && isConsumption()){
            return EP_Common_Constant.CONSUMPTION;
        }
        else if(isTransfer()){
            return EP_Common_Constant.TRANSFER_ORDER;
        }
        else {
            return EP_Common_Constant.SALES;
        }
    } 


    /** this method is used to get the order epoch
     *  @date      06/02/2017
     *  @name      getEpochType
     *  @param     NA
     *  @return    String
     *  @throws    NA
     */
     public String getEpochType() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getEpochType');
        return localOrder.EP_Order_Epoch__c;
    } 


    /** this method is used to check if the order is retrospective
     *  @date      06/02/2017
     *  @name      isRetrospective
     *  @param     NA
     *  @return    Boolean 
     *  @throws    NA
     */
     public Boolean isRetrospective() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','isRetrospective');
        return localorder != null && (EP_Common_Constant.EPOC_RETROSPECTIVE).equalsignorecase(localOrder.EP_Order_Epoch__c);  
    }

    /** this method is used to check if the order is packged or not
     *  @date      04/03/2017
     *  @name      isPackaged
     *  @param     NA
     *  @return    Boolean 
     *  @throws    NA
     */
     public Boolean isPackaged() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','isPackaged');
        return localorder != null && localOrder.EP_Order_Product_Category__c == EP_Common_Constant.PRODUCT_PACKAGED;  
    }

    /** this method is used to check if the order is packged or not
     *  @date      04/03/2017
     *  @name      isBulk
     *  @param     NA
     *  @return    Boolean 
     *  @throws    NA
     */
     public Boolean isBulk() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','isBulk');
        return localorder != null && localOrder.EP_Order_Product_Category__c == EP_Common_Constant.PRODUCT_BULK;  
    }

    /** this method is used to check the VMI Type
     *  @date      06/02/2017
     *  @name      getVMIType
     *  @param     NA
     *  @return    String
     *  @throws    NA
     */
     public String getVMIType() {

        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getVMIType');
        String shipToType = EP_Common_Constant.BLANK;
        if (localOrder.EP_ShipTo__c == null) {
            shipToType = EP_Common_Constant.NONVMI;
        }
        else if(localorder.EP_ShipTo__r.RecordType.DeveloperName != EP_Common_Constant.STORAGE_SHIP_TO_DEV_NAME) {
            //EP_AccountDomainObject accountDomainObj = new EP_AccountDomainObject(localOrder.EP_ShipTo__c);
            shipToType = getShipToAccountDomain().localAccount.EP_ShipTo_Type__c;
        }

        return shipToType;
    }   


    /** this method is used to check the Bulk/Packaged type
     *  @date      06/02/2017
     *  @name      getProductSoldAsType
     *  @param     NA
     *  @return    String
     *  @throws    NA
     */
     public String getProductSoldAsType() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getProductSoldAsType');
        return localOrder.EP_Order_Product_Category__c;
    }  


    /** this method is used to check the consignment type
     *  @date      06/02/2017
     *  @name      getConsignmentType
     *  @param     NA
     *  @return    String
     *  @throws    NA
     */
     public String getConsignmentType() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getConsignmentType');
        String consignmentType = EP_AccountConstant.NON_CONSIGNMENT;
        if (localOrder.EP_ShipTo__c != NULL && getShipToAccountDomain().localAccount.EP_Consignment_Type__c !=NULL) {
            //EP_AccountDomainObject accountDomainObj = new EP_AccountDomainObject(localOrder.EP_ShipTo__c);
            consignmentType = getShipToAccountDomain().localAccount.EP_Consignment_Type__c;
        }
        
        return consignmentType;
    } 


    /** this method is used to get the delivery type
     *  @date      06/02/2017
     *  @name      getDeliveryType
     *  @param     NA
     *  @return    String
     *  @throws    NA
     */
     public String getDeliveryType() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getDeliveryType');
        return EP_GeneralUtility.removeHyphen(localOrder.EP_Delivery_Type__c);
    } 


    /** this method is used to get the record type
     *  @date      06/02/2017
     *  @name      getRecordType
     *  @param     NA
     *  @return    String
     *  @throws    NA
     */
    /*public String getRecordType() {
        return OrderRecordType;
        }*/


    /** this method is used to get the ship to type
     *  @date      06/02/2017
     *  @name      getShipToType
     *  @param     NA
     *  @return    String
     *  @throws    NA
     */
    /*public String getShipToType(){
        return shipToType;
        }*/   


    /** this method is used to check Overdue amount
     *  @date      06/02/2017
     *   @name      isOrderSellToInvoiceOverdue
     *  @param     NA
     *  @return    Boolean
     *  @throws    NA
     */
     public boolean isOrderSellToInvoiceOverdue(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','isOrderSellToInvoiceOverdue');
        //EP_AccountDomainObject accountDomainObj = new EP_AccountDomainObject(localorder.EP_Sell_To__c);
        EP_AccountService service = new EP_AccountService(getSellToAccountDomain());
        HasOverdue = service.isOverDueInvoice();
        system.debug('isOrderSellToInvoiceOverdue ' + HasOverdue );
        return HasOverdue;
    }


    /** getter method
     *  @date      06/02/2017
     *  @name      setOrder
     *  @param     NA
     *  @return    Order
     *  @throws    NA
     */
     public csord__Order__c getOrder(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getOrder');
        return localorder;
    }  
    

    /** setter method
     *  @date      06/02/2017
     *  @name      setOrder
     *  @param     Order ord
     *  @return    NA
     *  @throws    NA
     */
     public void setOrder(csord__Order__c orderObj){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setOrder');
        localorder = orderObj;
    }

    /** Checks if inventory is Low   
     *  @date      06/02/2017
     *  @name      isInventoryLow
     *  @param     NA
     *  @return    Boolean
     *  @throws    NA
     */
     public Boolean isInventoryLow(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','isInventoryLow');    
        List<EP_Inventory__c> inventoryList = getInventoryForOrderLines().values();
        // Get the Inventory records and looping through
        if(!inventoryList.isEmpty()){
            
            for(EP_Inventory__c inventoryObj : inventoryList ) {
                // Check if Inventory is Low or No 
                if(inventoryObj.EP_Inventory_Availability__c.equals(EP_Common_Constant.LOW_INV)
                    || inventoryObj.EP_Inventory_Availability__c.equals(EP_Common_Constant.NO_INV)) {
                    return true;
                }  
            }
        }
        else{
            system.debug('Inventory list not available');
            return true;
        }    
        return false;
    }

    public map<id,EP_Inventory__c> getInventoryForOrderLines(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getInventoryForOrderLines');
        Set<Id> setProdIds = new Set<Id>();
        //Set<Id> setPricebookEntryIds = new Set<Id>();
        map<id,EP_Inventory__c> inventoryProductIDMap = new map<id,EP_Inventory__c>();
        //Looping through the Order Lines
        for(csord__Order_Line_Item__c orderItemObj : orderitemlist) {
            //Adding PriceEntry Id 
            setProdIds.add(orderItemObj.EP_Product__c);
        }
        
        //Looping through the Pricebook entries
        //for(PricebookEntry priceBookEntryObj : pricebookentryMapper.getRecordsByPricebookEntry(setPricebookEntryIds)) {
            //Adding ProductId
          //  setProdIds.add(priceBookEntryObj.Product2Id);
        //}
        system.debug('Product ids are ' + setProdIds);
        //Get Stock Holding Location
        List<EP_Stock_Holding_Location__c> listStockHoldingLocation = stockHoldingLocationMapper.getActiveStockHoldingLocationBySellToId(localorder.AccountId__c);
        if(!listStockHoldingLocation.isEmpty()) {
            EP_Stock_Holding_Location__c stockholdLocationObj = listStockHoldingLocation[0];
        }
        system.debug('Stock holding location is ' + localorder.EP_Stock_Holding_Location__c);
        List<EP_Inventory__c> inventoryList = inventoryMapper.getInventoryByProdId(setProdIds, localorder.EP_Stock_Holding_Location__c);
        for(EP_Inventory__c inventory: inventoryList){
            inventoryProductIDMap.put(inventory.EP_Product__c, inventory);
        }
        return inventoryProductIDMap;
    }

    /**update Inventory availaility for order line item.
     *  @date      06/02/2017
     *  @name      setOrderItemWithInventoryAvailability
     *  @param     NA
     *  @return    NA
     *  @throws    NA
    */     
    public void setOrderItemWithInventoryAvailability() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setOrderItemWithInventoryAvailability');
        map<id,EP_Inventory__c> invMap = getInventoryForOrderLines();
        system.debug('invMap is ' + invMap);
        for(csord__Order_Line_Item__c oItem: localorder.csord__Order_Line_Items__r){
            system.debug('Product id in order items is ' + oItem.EP_Product__c);
            if(invMap.ContainsKey(oItem.EP_Product__c)) {
                oItem.EP_Inventory_Availability__c = invMap.get(oItem.EP_Product__c).EP_Inventory_Availability__c;
            }
        }
    }


    /** This method sets Supply location Priciing fied if NULL
     *  @date      02/02/2017
     *  @name      setSupplyLocPricing
     *  @param     Order orderObj
     *  @return    NA
     *  @throws    NA
     */
    /*public void setSupplyLocPricing(Order orderObj) {
        Set<Id> setShipToIds = new Set<Id>(); 
        Map<Id,Id> mapshipToSupplyOption = new Map<Id,Id>(); 
        if (orderObj.EP_Supply_Location_Pricing__c == NULL && orderObj.EP_ShipTo__c != NULL) {
            List<EP_Stock_Holding_Location__c> listSupplyOptions =  stockHoldingLocationMapper.getStockHoldingLocationByIds(new set<Id>{orderObj.EP_ShipTo__c}); 
            if(!listSupplyOptions.isEMpty()) {
                orderObj.Stock_Holding_Location__c = listSupplyOptions[0].Stock_Holding_Location__c;
            }
        }
        }*/

    
    
     /** This method is used to is to set sell entity
     *  @date      09/03/2017
     *  @name      setSellTo
     *  @param     Account
     *  @return    Boolean 
     *  @throws    NA
     */    
     public void setSellTo(Account sellTo){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setSellTo');
        if (sellTo != NULL){ 
            localOrder.EP_Sell_To__c = sellTo.Id;         
        }
    }

     /** This method is used to is to set the Bill entity attributes
     *  @date      09/03/2017
     *  @name      setBillTo
     *  @param     Account
     *  @return    Boolean 
     *  @throws    NA
     */    
     public void setBillTo(Account billTo){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setBillTo');
        System.debug('*******billTo:--'+billTo);
        if (billTo != NULL) {  
            localOrder.EP_Bill_To__r = billTo;
             localOrder.EP_Bill_To__c = billTo.Id;
            localOrder.EP_Bill_TO_Email__c = billTo.EP_Email__c;
            localOrder.EP_Overdue_Amount__c = billTo.EP_Overdue_Amount__c;
        }
    }

    /** This method is used to is consignment order
     *  @date      13/02/2017
     *  @name      isConsignment
     *  @param     String SelectedAccount,String strSelectedDeliveryType
     *  @return    Boolean 
     *  @throws    NA
     */
     public Boolean isConsignment() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','isConsignment');
        if(!EP_Common_Constant.EX_RACK.equalsIgnoreCase(localOrder.EP_Delivery_Type__c)) {
            return getConsignmentType() == EP_AccountConstant.CONSIGNMENT;
        }
        return false;
    } 

    /** This method is used to check if it is consignment order
     *  @date      09/03/2017
     *  @name      isConsumption
     *  @param     NA
     *  @return    Boolean 
     *  @throws    NA
     */    
     @TestVisible
     private Boolean isConsumption(){
        EP_GeneralUtility.Log('Private','EP_OrderDomainObject','isConsumption');
        return localorder != null && EP_Common_Constant.CONSUMPTION_ORDER.equalsignorecase(localorder.EP_Order_Category__c);
    }
    
     /** This method is used to check if it is transfer order
     *  @date      09/03/2017
     *  @name      isTransfer
     *  @param     NA
     *  @return    Boolean 
     *  @throws    NA
     */
     // Requirement 59441 - start
     public Boolean isTransfer(){
        EP_GeneralUtility.Log('Private','EP_OrderDomainObject','isTransfer');
        System.debug('EP_Order_Type__c***:-'+localorder.EP_Order_Type__c);
        return localorder != null && EP_Common_Constant.ORD_CAT_TRANSFER_ORD.equalsIgnoreCase(localorder.EP_Order_Type__c);
    }
    // Requirement 59441 - end
    
    /** This method is used to is consignment order
     *  @date      13/02/2017
     *  @name      getOrderLineItems
     *  @param     None
     *  @return    Boolean 
     *  @throws    NA
     */
     public void deleteOrderLineItems() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getOrderLineItems');
        EP_OrderItemMapper EP_OrderItemMapperobj = new EP_OrderItemMapper();
        List<csord__Order_Line_Item__c> orderItemsToDelete = EP_OrderItemMapperobj.getCSOrderLineItemsforOrder(localOrder.id);
        if(orderItemsToDelete.size() > 0)
            EP_OrderItemMapperobj.doCSDelete(orderItemsToDelete);
        
    } 

    /**
    * @Author       Rahul Jain
    * @Name         setOrderStockHoldingLocation
    * @Date         03/24/2017
    * @Description  
    * @Param        
    * @return        
    */
    public void setOrderStockHoldingLocation() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setOrderStockHoldingLocation');
        //Defect Fix Start - #57360
        string beforeUpdateStockHolding = localorder.EP_Stock_Holding_Location__c;
        for(csord__Order_Line_Item__c ordItem : orderitemlist){
            if(ordItem.EP_Stock_Holding_Location__c != null) {
                localorder.Stock_Holding_Location__c = ordItem.EP_Stock_Holding_Location__c;
                localorder.EP_Stock_Holding_Location__c = ordItem.EP_Stock_Holding_Location__r.Stock_Holding_Location__c;
                break;
            }
        }
        
        if(beforeUpdateStockHolding != localorder.EP_Stock_Holding_Location__c) {
            isStockLocationChanged = true;
        }
        //Defect Fix END - #57360
    }

    /** This method is used to get Order Type
    *  @date      05/02/2017
    *  @name      getTypeObject
    *  @param     NA
    *  @return    EP_Ordertype
    *  @throws    NA
    */  

    public EP_Ordertype getTypeObject() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getType');

        oType = (EP_Ordertype)Type.forName(strClassPrefix+getType()+strOrderType).newInstance();
        oType.setOrderDomain(this);
        return oType;
    }
    

    /** This method is used to get Order VMI  Type
    *  @date      16/02/2017
    *  @name      getVMITypeObject
    *  @param     NA
    *  @return    EP_VendorManagement
    *  @throws    NA
    */  
    public EP_VendorManagement getVMITypeObject() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getVMIType');

        vmiType = (EP_VendorManagement)Type.forName(strClassPrefix+getVMIType()+strStrategy).newInstance();
        return vmiType;
    }


    /** This method is used to get OrderEpoch Type
    *  @date      17/02/2017
    *  @name      getEpochTypeObject
    *  @param     NA
    *  @return    EP_OrderEpoch
    *  @throws    NA
    */  
    public EP_OrderEpoch getEpochTypeObject() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getEpochType');

        orderEpochType = (EP_OrderEpoch)Type.forName(strClassPrefix+getEpochType()).newInstance();
        return orderEpochType;
    }

    /** This method is used to get Order Consignment Type
    *  @date      17/02/2017
    *  @name      getConsignmentTypeObject
    *  @param     NA
    *  @return    EP_ConsignmentType
    *  @throws    NA
    */  
    public EP_ConsignmentType getConsignmentTypeObject() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getConsignmentType');

        consignType = (EP_ConsignmentType)Type.forName(strClassPrefix+getConsignmentType()).newInstance();
        return consignType;
    }

    /** This method is used to get Order Product Sold As Type
    *  @date      17/02/2017
    *  @name      getProductSoldAsType
    *  @param     NA
    *  @return    EP_ProductSoldAs
    *  @throws    NA
    */  
    public EP_ProductSoldAs getProductSoldAsTypeObject() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getProductSoldAsType');
        
        psaType = (EP_ProductSoldAs)Type.forName(strClassPrefix+getProductSoldAsType()+strProducts).newInstance();
        return psaType;
    }

    /** This method is used to get Order Product Sold As Type
    *  @date      17/02/2017
    *  @name      getDeliveryType
    *  @param     NA
    *  @return    EP_DeliveryType
    *  @throws    NA
    */  
    public EP_DeliveryType getDeliveryTypeObject() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getDeliveryType');
        
        delType = (EP_DeliveryType)Type.forName(strClassPrefix+getDeliveryType()).newInstance();
        return delType;
    }

    /** This method updates ordertype
    *  @date      02/04/2017
    *  @name      doUpdateOrderTypeField
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public void setOrderTypeField() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setOrderTypeField');
        String strOrderType = getConsignmentType();
        if (EP_AccountConstant.NON_CONSIGNMENT.equalsignorecase(strOrderType)) {
            localorder.Type__c = EP_Common_Constant.NON_CONSIGNMENT;
        }else{
            localorder.Type__c = getConsignmentType();
        }
        System.debug('**orderObj**'+localorder.Type__c); 
    }

    /** This method updates payment terms
    *  @date      03/02/2017
    *  @name      doUpdatePaymentTermAndMethod
    *  @param     NA
    *  @return    NA
    *  @throws    NA
    */
    public void updatePaymentTermAndMethod(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','doUpdatePaymentTermAndMethod');
        getTypeObject();
        oType.doUpdatePaymentTermAndMethod();
    }

    /** This method finds the correct record type if based on the Order. 
    *  @date      24/02/2017
    *  @name      findRecordType
    *  @param     NA
    *  @return    Id
    *  @throws    NA
    */
    public Id findRecordType() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','findRecordType');
        getTypeObject();
        return oType.findRecordType();
    }

    /** This method validates the Order Start Date
    *  @date      26/02/2017
    *  @name      getDeliveryTypes
    *  @param     NA
    *  @return    List<String>
    *  @throws    NA
    */
    public List<String> getDeliveryTypes() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getDeliveryTypes');
        getTypeObject();
        return oType.getDeliveryTypes();
    }

    /** This method get the ship to of the customer 
    *  @date      03/02/2017
    *  @name      getShipTos
    *  @param     Id accountId
    *  @return    List<Account>
    *  @throws    NA
    */
    public List<Account> getShipToAccounts(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','getShipTos');
        System.debug('*****:---'+localorder+this);
        getTypeObject();
        System.debug('*****:---'+localorder.accountId__c);
        return oType.getShipTos(localorder.accountId__c);
    } 

    /** This method checks for correct loading date for Order 
    *  @date      22/02/2017
    *  @name      isValidLoadingDate
    *  @param     NA
    *  @return    Boolean 
    *  @throws    NA
    */
    public Boolean isValidLoadingDate(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','isValidLoadingDate');
        getTypeObject();
        return oType.isValidLoadingDate(localorder);
    }

    /** This method checks for correct expected delivery date of the Order 
    *  @date      22/02/2017
    *  @name      isValidExpectedDate
    *  @param     NA
    *  @return    Boolean 
    *  @throws    NA
    */
    public Boolean isValidExpectedDate(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','isValidExpectedDate');
        getTypeObject();
        return oType.isValidExpectedDate(localorder);
    }


    /** This method validates the Order Start Date
    *  @date      26/02/2017
    *  @name      isValidOrderDate
    *  @param     NA
    *  @return    Boolean 
    *  @throws    NA
    */  
    public Boolean isValidOrderDate() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','isValidOrderDate');
        getTypeObject();
        return oType.isValidOrderDate(localorder);
    }


    /** This method sets retrospective orders
     *  @date      07/03/2017
     *  @name      setRetroOrderDetails
     *  @param     NA
     *  @return    NA
     *  @throws    NA
     */
     public void setRetroOrderDetails(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','setRetroOrderDetails');
        getTypeObject();
        oType.setRetroOrderDetails();
     }

     /** This method determines if customer PO should be applicable and shown for input. 
    *  @date      24/02/2017
    *  @name      showCustomerPO
    *  @param     NA
    *  @return    Boolean 
    *  @throws    NA
    */
    public Boolean showCustomerPOAllowed(){
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','showCustomerPO');
        getTypeObject();
        return oType.showCustomerPO();
    }

    public boolean isOrderItemQuantityChanged(){
        for(csord__Order_Line_Item__c oItem: orderitemlist){
            if(oItem.EP_Prior_Quantity__c != null && oItem.Quantity__c != oItem.EP_Prior_Quantity__c){
                return true;
            }
        }
        return false;
    }

    public void revertOrderItemQuantity(){
        for(csord__Order_Line_Item__c oItem: orderitemlist){
            if(oItem.EP_Prior_Quantity__c != null){
                oItem.Quantity__c = oItem.EP_Prior_Quantity__c;
            }
        }
    }

    public void updateTransportService() {
        EP_GeneralUtility.Log('Public','EP_OrderDomainObject','updateTransportService');
        getTypeObject();
        oType.updateTransportService();
    }
    //Defect 57591 Start
        /** update Trasporter fields on Order--Only for VMI Orders
     *  @date      10/02/2017
     *  @name      doUpdateTransportFields
     *  @param     csord__Order__c objOrder
     *  @return    NA
     *  @throws    NA
     */  
    public void doUpdateTransportFields(csord__Order__c orderObj) {
        Set<Id> setAccountIds = new Set<Id>();
        setAccountIds.add(orderObj.AccountId__c);
        setAccountIds.add(orderObj.EP_Transporter_Pricing__c);
        setAccountIds.add(orderObj.EP_Supply_Location_Pricing__c);
        setAccountIds.add(orderObj.EP_Transporter__c);
        Map<Id,Account> mapAccounts = new EP_AccountMapper().getMapOfRecordsByIds(setAccountIds);
        if (orderObj.Stock_Holding_Location__c != null) {
            EP_Stock_Holding_Location__c stockholdingLocationObj = stockHoldingLocationMapper.getRecordsById(orderObj.Stock_Holding_Location__c);   
            orderObj.EP_Supply_Location_Name__c = stockholdingLocationObj.Stock_Holding_Location__r.Name;
        }
        if(hasStockLocationPricing(mapAccounts,orderObj)) {
            orderObj.EP_NAV_Pricing_Stock_Holding_Location_ID__c = mapAccounts.get(orderObj.EP_Supply_Location_Pricing__c).EP_Nav_Stock_Location_Id__c;
        }
        if(hasTransportPricingVendorId(mapAccounts,orderObj)) {
            orderObj.EP_Transporter_Pricing_No__c = mapAccounts.get(orderObj.EP_Transporter_Pricing__c).EP_NAV_Vendor_Id__c;
        }  
        if(hasTransportVendorId(mapAccounts,orderObj)) {
            orderObj.EP_Transporter_Code__c = mapAccounts.get(orderObj.EP_Transporter__c).EP_NAV_Vendor_Id__c;
        }  
        }

    /** checks for Stock Holding Location Pricing if available
     *  @date      10/02/2017
     *  @name      hasTransportVendorId
     *  @param     Map<Id,Account> mapAccounts,csord__Order__c orderObj
     *  @return    Boolean 
     *  @throws    NA
     */  
     public Boolean hasStockLocationPricing(Map<Id,Account> mapAccounts,csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Private','EP_OrderDomainObject','hasStockLocationPricing');
        return (mapAccounts.containsKey(orderObj.EP_Supply_Location_Pricing__c) 
            && (!String.isBlank(mapAccounts.get(orderObj.EP_Supply_Location_Pricing__c).EP_Nav_Stock_Location_Id__c)));
    }


    /** checks for Transporter Pricing if available
     *  @date      10/02/2017
     *  @name      hasTransportVendorId
     *  @param     Map<Id,Account> mapAccounts,csord__Order__c orderObj
     *  @return    Boolean 
     *  @throws    NA
     */
     public Boolean hasTransportPricingVendorId(Map<Id,Account> mapAccounts,csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Private','EP_OrderDomainObject','hasTransportPricingVendorId');
        return (mapAccounts.containsKey(orderObj.EP_Transporter_Pricing_No__c) 
            && mapAccounts.get(orderObj.EP_Transporter_Pricing_No__c).EP_NAV_Vendor_Id__c != null);
    }


    /** checks for Vendor(Transporter) if available
     *  @date      10/02/2017
     *  @name      hasTransportVendorId
     *  @param     Map<Id,Account> mapAccounts,csord__Order__c orderObj
     *  @return    Boolean 
     *  @throws    NA
     */  
     public Boolean hasTransportVendorId(Map<Id,Account> mapAccounts,csord__Order__c orderObj) {
        EP_GeneralUtility.Log('Private','EP_OrderDomainObject','hasTransportVendorId');
        return (mapAccounts.containsKey(orderObj.EP_Transporter__c) 
            && mapAccounts.get(orderObj.EP_Transporter__c).EP_NAV_Vendor_Id__c != null);
    }
    //Defect 57591 End
}