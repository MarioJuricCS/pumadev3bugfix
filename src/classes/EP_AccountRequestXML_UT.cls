@isTest
public with sharing class EP_AccountRequestXML_UT {
    
     @testSetup
   	public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
  	}
  	
    static testMethod void init_test() {
        Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountRequestXML objAccReqXML = new EP_AccountRequestXML();
        objAccReqXML.recordId =acct.id;
        Test.startTest();
        objAccReqXML.init();
        Test.stopTest();
        
        System.assertEquals(objAccReqXML.objAccount <> null, true);
        
    }
    static testMethod void createStatusPayLoad_test() {
    	Account acct = EP_TestDataUtility.getSellTo();
        EP_AccountRequestXML objAccReqXML = new EP_AccountRequestXML();
        objAccReqXML.recordId =acct.id;
        Test.startTest();
        objAccReqXML.init();
        objAccReqXML.createStatusPayLoad();
        Test.stopTest();
        
        System.assertEquals(objAccReqXML.MSGNode<> null, true);
        
    }
    static testMethod void setEncryption_test() {
        EP_AccountRequestXML objAccReqXML = new EP_AccountRequestXML();
        objAccReqXML.messageType =EP_Common_Constant.SFDC_TO_NAV_CUSTOMER_CREATION;
        Test.startTest();
        objAccReqXML.setEncryption();
        Test.stopTest();
        
        System.assertEquals(objAccReqXML.isEncryptionEnabled, true);
        
    }
}