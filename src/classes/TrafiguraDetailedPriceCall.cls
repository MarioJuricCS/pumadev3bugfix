/*
   @Author          CloudSense
   @Name            TrafiguraDetailedPriceCall
   @CreateDate      22/08/2018
   @Description     This class is responsible for initiating Detailed price call before NAV SYNC
   @Version         1.1
 
*/


global class TrafiguraDetailedPriceCall implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
    
    Map<String,StepData> stepDataMap = new Map<String,StepData> ();
    
    public Boolean performCallouts(List<SObject> data) {
        
        List<CSPOFA__Orchestration_Step__c> stepList= (List<CSPOFA__Orchestration_Step__c>)data;
        List<CSPOFA__Orchestration_Step__c> extendedList = [Select
                                                                id,CSPOFA__Orchestration_Process__r.Order__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,CSPOFA__Status__c,CSPOFA__Completed_Date__c,CSPOFA__Message__c,CSPOFA__Orchestration_Process__r.Order__r.csord__Identification__c,CSPOFA__Orchestration_Process__r.Order__r.EP_Puma_Company_Code__c,CSPOFA__Orchestration_Process__r.Order__r.Invoice_Consolidation_Basis__c, CSPOFA__Orchestration_Process__r.Order__r.RepriceRequired__c
                                                            from 
                                                                CSPOFA__Orchestration_Step__c 
                                                            where 
                                                            id in :stepList];

        Boolean attachExists = false;
        //handle this in bulk
        if(extendedList.size() > 0) {
            List<Attachment> pricingResponseAtt = [select id from Attachment where Name = :EP_Common_Constant.ORDER_PRICE_RESPONSE_DETAILED and ParentId = :extendedList[0].CSPOFA__Orchestration_Process__r.Order__r.csord__Identification__c];
            if (pricingResponseAtt.size() > 0) {
                attachExists = true;
            }   
        }
                                          
        for(CSPOFA__Orchestration_Step__c step:extendedList){
            try{
                if(step.CSPOFA__Orchestration_Process__r.Order__r.Invoice_Consolidation_Basis__c != EP_Common_Constant.ORDER_SUMMARISED_PRICE 
                    && (step.CSPOFA__Orchestration_Process__r.Order__r.RepriceRequired__c || !attachExists)) {
                    getPrices(step.id, step.CSPOFA__Orchestration_Process__r.Order__r.EP_Puma_Company_Code__c,step.CSPOFA__Orchestration_Process__r.Order__r.Invoice_Consolidation_Basis__c,step.CSPOFA__Orchestration_Process__r.Order__r.csord__Identification__c);
                    }
                }
            catch (Exception e){
                EP_loggingService.loghandledException(e, EP_Common_Constant.EPUMA, 'process', 'TrafiguraDetailedPriceCall',apexPages.severity.ERROR);
            }
        }
        
     return true; 
     
    }
    
    private void getPrices(String stepId, String companyCode, String PCB, String basketId) {
       
        String messageType = 'SEND_ORDER_PRICING_REQUEST';
        string xmlMessage = '';
        string responseBody = '';
        string endPoint = '';
        String outcome = '';
        EP_CS_OutboundMessageSetting__c msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(messageType);
            
        //Retrieves the existing regular price call
        xmlMessage = [select body from Attachment where Name = 'pricingRequest' and ParentId = :basketId][0].body.toString();
        //Updates the previous price call message with the new parameters
        Integer startSeq = xmlMessage.indexOf('<seqId>')+7;
        Integer endSeq = xmlMessage.indexOf('</seqId>');
        String seqId = xmlMessage.substring(startSeq, endSeq);
        xmlMessage = xmlMessage.replace(seqId, '');
       
        Integer startPCB = xmlMessage.indexOf('<priceConsolidationBasis>')+25;
        Integer endPCB = xmlMessage.indexOf('</priceConsolidationBasis>');
        String oldPCB = xmlMessage.substring(startPCB, endPCB);
        xmlMessage = xmlMessage.replace(oldPCB, PCB);
        endPoint = msgSetting.End_Point__c.replace(EP_Common_Constant.COMPANY_CODE_URL_TOKAN, companyCode);

        EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(endPoint, EP_Common_Constant.POST, xmlMessage, msgSetting.EP_Subscription_Key__c);
        EP_IntegrationServiceResult result = EP_OutboundMessageService.doCallOutCS(intService, msgSetting);

        responseBody = result.getResponse().getBody();

        if(responseBody.startsWith(EP_Common_Constant.BOM_STR)) {
           responseBody = responseBody.replaceFirst(EP_Common_Constant.BOM_STR, EP_Common_Constant.BLANK);
        }
        stepDataMap.put(stepId, new stepData(basketId, xmlMessage, responseBody));
    }
    
    public List<sObject> process(List<SObject> data) {
        List<sObject> result = new List<sObject>(); 
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        List<Attachment> attList = new List<Attachment> ();
        List<String> basketList = new List<String> ();
        for (CSPOFA__Orchestration_Step__c step : stepList) {
            if(stepDataMap.containsKey(step.id)) {
                StepData stepData = stepDataMap.get(step.id);
                Attachment attResponse = new Attachment(
                    ParentId = stepData.basketId,
                    Name = EP_Common_Constant.ORDER_PRICE_RESPONSE_DETAILED,
                    Body = Blob.valueOf(stepData.response)
                );
                attList.add(attResponse);
                Attachment attRequest = new Attachment(
                    ParentId = stepData.basketId,
                    Name = EP_Common_Constant.ORDER_PRICE_REQUEST_DETAILED,
                    Body = Blob.valueOf(stepData.request)
                );
                attList.add(attRequest);
                basketList.add(stepData.basketId);
            }

            step.CSPOFA__Status__c ='Complete';
            step.CSPOFA__Completed_Date__c=Date.today();
            step.CSPOFA__Message__c = 'Custom step succeeded';
            result.add(step);
        }

        List<Attachment> pricingResponseAtt = [
            select id
            from Attachment
            where (Name = :EP_Common_Constant.ORDER_PRICE_RESPONSE_DETAILED or Name = :EP_Common_Constant.ORDER_PRICE_REQUEST_DETAILED)
            and ParentId in: basketList
        ];
        if (!pricingResponseAtt.isEmpty()) {
            delete pricingResponseAtt;
        }
        
        if(!attList.isEmpty()) {
            insert attList;
        }
        
        return result;  
    }
    
    public class StepData {
        public String basketId {get;set;}
        public String request {get;set;}
        public String response {get;set;}
        
        public StepData(String basketId, String request, String response) {
            this.basketId = basketId;
            this.request = request;
            this.response = response;
        }
	}
    
}