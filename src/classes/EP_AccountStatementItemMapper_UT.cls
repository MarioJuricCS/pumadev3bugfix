/*
   @Author <Accenture>
   @name <EP_AccountStatementMapper_UT>
   @Description <This is the test class for the class of EP_AccountStatementMapper>
   @Version <1.0>
*/
@isTest
private class EP_AccountStatementItemMapper_UT {
	/**
	* @author Accenture
	* @date 13/03/2018
	* @description create test data for all required object in test methods
	*/
	 @TestSetup
	 static void initData() {
	 	list<EP_Customer_Account_Statement_Item__c> lstCASItem = new list<EP_Customer_Account_Statement_Item__c>();
	 	// Create Bill TO Account
        Account TempAccount = EP_TestDataUtility.createBillToAccount();
        insert TempAccount;
	 	//create Customer Payment 
	 	EP_Customer_Payment__c oPayment = new EP_Customer_Payment__c(Name = '0000012', EP_Bill_To__c = TempAccount.Id, EP_Amount_Paid__c = -10.00, 
                                                EP_Payment_Date__c = system.today(), EP_Payment_Key__c = 'AU00114124_0000012_AUN_ent-1'  );
        insert oPayment;
        
        //Create Customer Account Statement Item
	 	EP_Customer_Account_Statement_Item__c tempPAyCASItem = EP_TestDataUtility.testCreateCASItem(TempAccount.id,oPayment);
	 	tempPAyCASItem.recordtypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERACCOUNTSTATEMENTITEM_OBJECT
	 			,EP_Common_Constant.PAYMENTIN_RECORD_TYPE_NAME);	
	 	
	 	lstCASItem.add(tempPAyCASItem);
	 	
	 	//Create Customer Other Adjustment
	 	EP_Customer_Other_Adjustment__c custAdjust = EP_TestDataUtility.createCustomerOtherAdjustment(TempAccount.Id,null,TempAccount.EP_Puma_Company_Code__c);
	 	custAdjust.EP_Client_ID__c = '12345asdsfwerwerw'; 
	 	insert custAdjust;
	 		 	
	 	EP_Customer_Account_Statement_Item__c tempOtherCASItem = EP_TestDataUtility.testCreateOtherCASItem(TempAccount.id,custAdjust);
	 	tempOtherCASItem.recordtypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.CUSTOMERACCOUNTSTATEMENTITEM_OBJECT
	 			,EP_Common_Constant.RECORD_TYPE_OTHER_CUSTOMER_ADJUSTMENT);	
 		lstCASItem.add(tempOtherCASItem);
 		
	 	insert lstCASItem;
	 	
	 }
	/**
    * @author Accenture
    * @date 13/03/2018
    * @description Positive Test to check getStatemrntItemId method.
    */
    static testMethod void getStatemrntItemId_PositiveTest(){
    	map<string,Id> mapStatementItemKeyToId = new map<string,Id>();
    	list<EP_Customer_Payment__c> lstCustomerPayment = [select id,Name,EP_Bill_To__c,EP_Payment_Date__c from EP_Customer_Payment__c limit 1];
    	Test.startTest();
            mapStatementItemKeyToId = EP_AccountStatementItemMapper.getStatemrntItemId(lstCustomerPayment);
        Test.stopTest();
        
        //Assert
        system.assertEquals(mapStatementItemKeyToId.isEmpty() , false);
    }
    /**
    * @author Accenture
    * @date 13/03/2018
    * @description Negative Test to check getStatemrntItemId method.
    */
    static testMethod void getStatemrntItemId_NegativeTest(){
    	map<string,Id> mapStatementItemKeyToId = new map<string,Id>();
    	list<EP_Customer_Payment__c> lstCustomerPayment = new list<EP_Customer_Payment__c>();
    	Test.startTest();
            mapStatementItemKeyToId = EP_AccountStatementItemMapper.getStatemrntItemId(lstCustomerPayment);
        Test.stopTest();
        
        //Assert
        system.assertEquals(mapStatementItemKeyToId.isEmpty() , true);
    }
    
    /**
    * @author Accenture
    * @date 13/03/2018
    * @description Positive Test to check setStatemrntItemId method.
    */
    static testMethod void setStatemrntItemId_PositiveTest(){
    	EP_Customer_Account_Statement_Item__c oCASItemRecord = [select id,EP_Customer_Account_Statement_Item_Key__c from EP_Customer_Account_Statement_Item__c limit 1];
    	Test.startTest();
            EP_AccountStatementItemMapper.setStatemrntItemId(oCASItemRecord);
        Test.stopTest();
        
        //Assert
        system.assert(oCASItemRecord.id != null);
    }
    
    /**
    * @author Accenture
    * @date 13/03/2018
    * @description Negative Test to check setStatemrntItemId method.
    */
    static testMethod void setStatemrntItemId_NegativeTest(){
    	EP_Customer_Account_Statement_Item__c oCASItemRecord = New EP_Customer_Account_Statement_Item__c();
    	Test.startTest();
            EP_AccountStatementItemMapper.setStatemrntItemId(oCASItemRecord);
        Test.stopTest();
        
        //Assert
        system.assert(oCASItemRecord.id == null);
    }
    
    /**
    * @author Accenture
    * @date 13/03/2018
    * @description Positive Test to check getStatemrntItemId method.
    */
    static testMethod void getStatemrntItemId_PostiveTest(){
    	map<string,Id> mapStatementItemKeyToId = new map<string,Id>();
    	List<EP_Customer_Other_Adjustment__c> customerAdjustmentsToUpdate = [select id,EP_Bill_To__c,Name,EP_Submission_Date__c from EP_Customer_Other_Adjustment__c limit 1];
    	Test.startTest();
            mapStatementItemKeyToId = EP_AccountStatementItemMapper.getStatemrntItemId(customerAdjustmentsToUpdate);
        Test.stopTest();
        
        //Assert
        system.assertEquals(mapStatementItemKeyToId.isEmpty() ,false);
    }
    
    /**
    * @author Accenture
    * @date 13/03/2018
    * @description Negative Test to check getStatemrntItemId method.
    */
    static testMethod void getStatemrntItemId_TestNegative(){
    	map<string,Id> mapStatementItemKeyToId = new map<string,Id>();
    	List<EP_Customer_Other_Adjustment__c> customerAdjustmentsToUpdate = new list<EP_Customer_Other_Adjustment__c>();
    	Test.startTest();
            mapStatementItemKeyToId = EP_AccountStatementItemMapper.getStatemrntItemId(customerAdjustmentsToUpdate);
        Test.stopTest();
        
        //Assert
        system.assertEquals(mapStatementItemKeyToId.isEmpty() ,true);
    }
}