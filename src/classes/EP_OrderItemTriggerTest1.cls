/**
 * @author <Kamal Garg>
 * @name <EP_OrderItemTriggerTest1>
 * @createDate <28/12/2015>
 * @description <Test class for testing automation tests for E2E 024_002b> 
 * @version <1.0>
 */
@isTest
private class EP_OrderItemTriggerTest1 {

    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the Customer.
     */
    private static Account billToAccount;
    private static Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1]; 
    private static  User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
    
    static testMethod void testOrderPrice() {
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'Australia');
        insert contry1;
        EP_Region__c reg ;
        reg = EP_TestDataUtility.createCountryRegion('North-Australia',contry1.id );
        database.insert(reg);
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        sellToAccount .EP_Country__c = contry1.id;
        sellToAccount .EP_Delivery_Pickup_Country__c = contry1.id;
        sellToAccount .EP_Cntry_Region__c = reg.id;
        sellToAccount.EP_Company_Is_Tax_Exempt__c = false;
        sellToAccount.EP_VAT_Exempted__c = false;
        insert sellToAccount;
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,null); //Add one SHL
        Database.insert(SHL1,false);
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
     
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
        insert orderObj;
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = createProduct('Diesel');
        Product2 taxProductObj = createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        Database.insert(productsToBeInserted, false);
        //Test Class Fix Start
        PricebookEntry pricebookEntryObj = [select id ,CurrencyIsoCode, pricebook2id,product2id,product2.Name from PricebookEntry where product2id =: productObj.id limit 1];
        PricebookEntry taxPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: taxProductObj.id limit 1];
        orderObj.CurrencyIsoCode = pricebookEntryObj.CurrencyIsoCode;
        update orderObj;
        /*List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
        Database.insert(pricebookEntryToBeInserted);
        */
        //Test Class Fix End
        OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
        insert orderItemObj;
        // verify the results
        List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem];
        
        Decimal price = 0.0;
        for(OrderItem obj :orderItemsList){
            price += obj.UnitPrice*obj.Quantity;
        }
        Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id];
        system.assertEquals(orderObj1.TotalAmount, price);
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the Customer.
     */
    static testMethod void testOrderPrice1() {
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'Australia');
        insert contry1;
        EP_Region__c reg ;
        reg = EP_TestDataUtility.createCountryRegion('North-Australia',contry1.id );
        database.insert(reg);
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        sellToAccount.EP_Company_Is_Tax_Exempt__c = false;
        sellToAccount .EP_Country__c = contry1.id;
        sellToAccount .EP_Delivery_Pickup_Country__c = contry1.id;
        sellToAccount .EP_Cntry_Region__c = reg.id;
        sellToAccount.EP_VAT_Exempted__c = true;
        insert sellToAccount;
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,null); //Add one SHL
        Database.insert(SHL1,false);
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
        insert orderObj;
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = createProduct('Diesel');
        Product2 taxProductObj = createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        Database.insert(productsToBeInserted, false);
        
        //Test Class Fix Start
        PricebookEntry pricebookEntryObj = [select id ,CurrencyIsoCode, pricebook2id,product2id,product2.Name from PricebookEntry where product2id =: productObj.id limit 1];
        PricebookEntry taxPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: taxProductObj.id limit 1];
        orderObj.CurrencyIsoCode = pricebookEntryObj.CurrencyIsoCode;
        update orderObj;
        /*List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
        Database.insert(pricebookEntryToBeInserted);
        */
        //Test Class Fix End
        
        OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
        insert orderItemObj;
        // verify the results
        List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem];
        
        Decimal price = 0.0;
        for(OrderItem obj :orderItemsList){
            if(obj.EP_Is_Taxes__c == true){
                system.assertEquals(obj.UnitPrice, 5.0);
            }
            price += obj.UnitPrice*obj.Quantity;
        }
        Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id];
        system.assertEquals(orderObj1.TotalAmount, price);
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the Customer.
     */
    static testMethod void testOrderPrice2() {
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'Australia');
        insert contry1;
        EP_Region__c reg ;
        reg = EP_TestDataUtility.createCountryRegion('North-Australia',contry1.id );
        database.insert(reg);
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        sellToAccount.EP_Company_Is_Tax_Exempt__c = true;
        sellToAccount .EP_Country__c = contry1.id;
        sellToAccount .EP_Delivery_Pickup_Country__c = contry1.id;
        sellToAccount .EP_Cntry_Region__c = reg.id;
        sellToAccount.EP_VAT_Exempted__c = false;
        insert sellToAccount;
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,null); //Add one SHL
        Database.insert(SHL1,false);
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
        insert orderObj;
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = createProduct('Diesel');
        Product2 taxProductObj = createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        Database.insert(productsToBeInserted, false);
        
       //Test Class Fix Start
        PricebookEntry pricebookEntryObj = [select id ,CurrencyIsoCode, pricebook2id,product2id,product2.Name from PricebookEntry where product2id =: productObj.id limit 1];
        PricebookEntry taxPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: taxProductObj.id limit 1];
        orderObj.CurrencyIsoCode = pricebookEntryObj.CurrencyIsoCode;
        update orderObj;
        /*List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
        Database.insert(pricebookEntryToBeInserted);
        */
        //Test Class Fix End
        
        OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
        insert orderItemObj;
        // verify the results
        List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem];
        
        Decimal price = 0.0;
        for(OrderItem obj :orderItemsList){
            if(obj.EP_Is_Taxes__c == true){
                system.assertEquals(obj.UnitPrice, 0.14);
            }
            price += obj.UnitPrice*obj.Quantity;
        }
        Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id];
        system.assertEquals(orderObj1.TotalAmount, price);
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the CSC Customer.
     */
    static testMethod void testOrderPriceCsc() {
        
        System.runAs(cscUser){
        
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'Australia');
        insert contry1;
        EP_Region__c reg ;
        reg = EP_TestDataUtility.createCountryRegion('North-Australia',contry1.id );
        database.insert(reg);
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        sellToAccount .EP_Country__c = contry1.id;
        sellToAccount .EP_Delivery_Pickup_Country__c = contry1.id;
        sellToAccount .EP_Cntry_Region__c = reg.id;
        sellToAccount.EP_Company_Is_Tax_Exempt__c = false;
        sellToAccount.EP_VAT_Exempted__c = false;
        insert sellToAccount;
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,null); //Add one SHL
        Database.insert(SHL1,false);
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
   
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
        insert orderObj;
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = createProduct('Diesel');
        Product2 taxProductObj = createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        Database.insert(productsToBeInserted, false);
        
        //Test Class Fix Start
        PricebookEntry pricebookEntryObj = [select id ,CurrencyIsoCode, pricebook2id,product2id,product2.Name from PricebookEntry where product2id =: productObj.id limit 1];
        PricebookEntry taxPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: taxProductObj.id limit 1];
        orderObj.CurrencyIsoCode = pricebookEntryObj.CurrencyIsoCode;
        update orderObj;
        /*List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
        Database.insert(pricebookEntryToBeInserted);
        */
        //Test Class Fix End
        
        OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
        insert orderItemObj;
        // verify the results
        List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem];
        
        Decimal price = 0.0;
        for(OrderItem obj :orderItemsList){
            price += obj.UnitPrice*obj.Quantity;
        }
        Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id];
        system.assertEquals(orderObj1.TotalAmount, price);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the CSC Customer.
     */
    static testMethod void testOrderPrice1Csc() {
        
        System.runAs(cscUser){
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'Australia');
        insert contry1;
        EP_Region__c reg ;
        reg = EP_TestDataUtility.createCountryRegion('North-Australia',contry1.id );
        database.insert(reg);
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        sellToAccount .EP_Country__c = contry1.id;
        sellToAccount .EP_Delivery_Pickup_Country__c = contry1.id;
        sellToAccount .EP_Cntry_Region__c = reg.id;
        sellToAccount.EP_Company_Is_Tax_Exempt__c = false;
        sellToAccount.EP_VAT_Exempted__c = true;
        insert sellToAccount;
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,null); //Add one SHL
        Database.insert(SHL1,false);
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
        insert orderObj;
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = createProduct('Diesel');
        Product2 taxProductObj = createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        Database.insert(productsToBeInserted, false);
        
        //Test Class Fix Start
        PricebookEntry pricebookEntryObj = [select id ,CurrencyIsoCode, pricebook2id,product2id,product2.Name from PricebookEntry where product2id =: productObj.id limit 1];
        PricebookEntry taxPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: taxProductObj.id limit 1];
        orderObj.CurrencyIsoCode = pricebookEntryObj.CurrencyIsoCode;
        update orderObj;
        /*List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
        Database.insert(pricebookEntryToBeInserted);
        */
        //Test Class Fix End
        
        OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
        insert orderItemObj;
        // verify the results
        List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem];
        
        Decimal price = 0.0;
        for(OrderItem obj :orderItemsList){
            if(obj.EP_Is_Taxes__c == true){
                system.assertEquals(obj.UnitPrice, 5.0);
            }
            price += obj.UnitPrice*obj.Quantity;
        }
        Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id];
        system.assertEquals(orderObj1.TotalAmount, price);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description Test Method to calculate and display the Order Price(Product Price) to the CSC Customer.
     */
    static testMethod void testOrderPrice2Csc() {
        System.runAs(cscUser){
        EP_Country__c contry1 = EP_TestDataUtility.createCountryRecord('Australia','AU', 'Australia');
        insert contry1;
        EP_Region__c reg ;
        reg = EP_TestDataUtility.createCountryRegion('North-Australia',contry1.id );
        database.insert(reg);
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null, null);
        sellToAccount .EP_Country__c = contry1.id;
        sellToAccount .EP_Delivery_Pickup_Country__c = contry1.id;
        sellToAccount .EP_Cntry_Region__c = reg.id;
        sellToAccount.EP_Company_Is_Tax_Exempt__c = true;
        sellToAccount.EP_VAT_Exempted__c = false;
        insert sellToAccount;
        EP_Stock_holding_location__c SHL1 = EP_TestDataUtility.createSellToStockLocation(sellToAccount.id,true,null,null); //Add one SHL
        Database.insert(SHL1,false);
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Non-VMI Orders').getRecordTypeId();
        
        Order orderObj = createOrder(sellToAccount.Id, orderRecTypeId, Test.getStandardPricebookId());
        insert orderObj;
        
        List<Product2> productsToBeInserted = new List<Product2>();
        Product2 productObj = createProduct('Diesel');
        Product2 taxProductObj = createProduct('TaxProduct');
        productsToBeInserted.add(productObj);
        productsToBeInserted.add(taxProductObj);
        Database.insert(productsToBeInserted, false);
        
        //Test Class Fix Start
        PricebookEntry pricebookEntryObj = [select id ,CurrencyIsoCode, pricebook2id,product2id,product2.Name from PricebookEntry where product2id =: productObj.id limit 1];
        PricebookEntry taxPricebookEntryObj = [select id ,pricebook2id,CurrencyIsoCode, product2id,product2.Name from PricebookEntry where product2id =: taxProductObj.id limit 1];
        orderObj.CurrencyIsoCode = pricebookEntryObj.CurrencyIsoCode;
        update orderObj;
        /*List<PricebookEntry> pricebookEntryToBeInserted = new List<PricebookEntry>();
        PricebookEntry pricebookEntryObj = createPricebookEntry(productObj.Id, Test.getStandardPricebookId());
        PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        pricebookEntryToBeInserted.add(pricebookEntryObj);
        pricebookEntryToBeInserted.add(taxPricebookEntryObj);
        Database.insert(pricebookEntryToBeInserted);
        */
        //Test Class Fix End
        
        OrderItem orderItemObj = createOrderItem(pricebookEntryObj.Id, orderObj.Id);
        insert orderItemObj;
        // verify the results
        List<OrderItem> orderItemsList = [SELECT Id, UnitPrice, Quantity, EP_Is_Taxes__c FROM OrderItem];
        
        Decimal price = 0.0;
        for(OrderItem obj :orderItemsList){
            if(obj.EP_Is_Taxes__c == true){
                system.assertEquals(obj.UnitPrice, 0.14);
            }
            price += obj.UnitPrice*obj.Quantity;
        }
        Order orderObj1 = [SELECT TotalAmount FROM ORDER WHERE Id=:orderObj.Id];
        system.assertEquals(orderObj1.TotalAmount, price);
        }
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test Order record
     * @params Id,Id,Id
     * @return Order
     */
    private static Order createOrder(Id acctId, Id recTypeId, Id pricebookId){
        Order obj = new Order();
        obj.AccountId = acctId;
        obj.RecordTypeId = recTypeId;
        obj.Status = 'Draft';
        obj.CurrencyIsoCode = 'USD';
        obj.PriceBook2Id = pricebookId;
        obj.EffectiveDate = system.today();
        
        return obj;
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test Product2 record
     * @params String
     * @return Product2
     */
    private static Product2 createProduct(String name){
        Product2 obj = new product2();
        obj.Name = name;
        obj.CurrencyIsoCode = 'USD';
        obj.IsActive = true;
        
        return obj;
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test PricebookEntry record
     * @params Id,Id
     * @return PricebookEntry
     */
    private static PricebookEntry createPricebookEntry(Id productId, Id pricebookId){
        PricebookEntry obj = new PricebookEntry();
        obj.Pricebook2Id = pricebookId;
        obj.Product2Id = productId;
        obj.UnitPrice = 1;
        obj.IsActive = true;
        obj.EP_Is_Sell_To_Assigned__c=True;
        obj.EP_VAT_GST__c = 14;
        obj.EP_Additional_Taxes__c = 5.0;
        obj.CurrencyIsoCode = 'USD';
        
        return obj;
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test OrderItem record
     * @params Id, Id
     * @return OrderItem
     */
    private static OrderItem createOrderItem(Id pricebookEntryId, Id orderId){
        OrderItem obj = new OrderItem();
        obj.PricebookEntryId = pricebookEntryId;
        obj.UnitPrice = 1;
        obj.Quantity = 1;
        obj.OrderId = orderId;
        obj.EP_Is_Freight_Price__c = false;
        obj.EP_Is_Taxes__c = false;
        
        return obj;
    }
}