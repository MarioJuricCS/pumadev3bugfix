@IsTest
private class StringBuilderTests {

	@IsTest
	static void testAppend() {
		Test.startTest();

		StringBuilder bldr = new StringBuilder('a');

		System.assertEquals(
			1,
			bldr.buffer.size(),
			'String was not appended to buffer when constructor with string was used'
		);

		System.assertEquals(
			1,
			bldr.length,
			'Buffer length was not set correct on append  when constructor with string was used'
		);

		bldr.append('bab');

		System.assertEquals(
			2,
			bldr.buffer.size(),
			'String was not appended to buffer'
		);

		System.assertEquals(
			4,
			bldr.length,
			'Buffer length was not set correct on append'
		);

		StringBuilder bldr2 = new StringBuilder();
		bldr2.append('b').append('cd');

		bldr.append(bldr2);

		System.assertEquals(
			4,
			bldr.buffer.size(),
			'Strings of other buffer were not appended to buffer'
		);

		System.assertEquals(
			7,
			bldr.length,
			'Buffer length was not set correct on append of another buffer'
		);

		bldr.append(new Set<String> { 'ab', 'c' });

		System.assertEquals(
			10,
			bldr.length,
			'Buffer length was not set correct on append of set of strings'
		);

		System.assertEquals(
			6,
			bldr.buffer.size(),
			'Elements of appended set of strings were not added to buffer'
		);

		bldr.append(new List<String> { 'ab', 'c', 'd' });

		System.assertEquals(
			14,
			bldr.length,
			'Buffer length was not set correct on append of list of strings'
		);

		System.assertEquals(
			9,
			bldr.buffer.size(),
			'Elements of appended list of strings were not added to buffer'
		);

		Test.stopTest();
	}

	@IsTest
	static void testClear() {
		Test.startTest();

		StringBuilder bfr = new StringBuilder();

		bfr.append('a');
		bfr.append('b');
		bfr.append('c').append('d');

		System.assertEquals(
			4,
			bfr.buffer.size(),
			'String was not appended to buffer'
		);

		System.assertEquals(
			4,
			bfr.length,
			'Buffer length was not set correct on append'
		);

		bfr.clear();

		System.assertEquals(
			0,
			bfr.buffer.size(),
			'Buffer was not cleared upon clear() method invocation'
		);

		System.assertEquals(
			0,
			bfr.length,
			'Buffer length did not reset upon clear() method invocation'
		);

		Test.stopTest();
	}

	@IsTest
	static void testToString() {
		Test.startTest();

		StringBuilder bldr = new StringBuilder();

		List<String> appendStrings = new List<String> {
			'a',
			'b',
			'cd',
			'efgh',
			'random string'
		};

		for (String s : appendStrings) {
			bldr.append(s);
		}

		System.assertEquals(
			String.join(appendStrings, StringBuilder.DEFAULT_DELIMITER),
			bldr.toString(),
			'To string method is expected to be implemented as String.join over internal buffer list with default delimiter.'
		);

		System.assertEquals(
			String.join(appendStrings,'\n'),
			bldr.toString('\n'),
			'To string method is expected to be implemented as String.join over internal buffer list with delimiter as argument to overloaded' +
			' toString() method.'
		);

		System.assertEquals(
			String.join(appendStrings,'!!!!Test!!!!'),
			bldr.toString('!!!!Test!!!!'),
			'To string method is expected to be implemented as String.join over internal buffer list with delimiter as argument to overloaded' +
			' toString() method.'
		);

		Test.stopTest();
	}

	@IsTest
	static void testToStringWithCollection() {
		Test.startTest();

		StringBuilder bldr = new StringBuilder();

		List<String> appendStrings = new List<String> {
			'a',
			'b',
			'cd',
			'efgh',
			'random string'
		};

		bldr.append(appendStrings);

		System.assertEquals(
			String.join(appendStrings, StringBuilder.DEFAULT_DELIMITER),
			bldr.toString(),
			'To string method is expected to be implemented as String.join over internal buffer list with default delimiter.'
		);

		System.assertEquals(
			String.join(appendStrings,'\n'),
			bldr.toString('\n'),
			'To string method is expected to be implemented as String.join over internal buffer list with delimiter as argument to overloaded' +
			' toString() method.'
		);

		System.assertEquals(
			String.join(appendStrings,'!!!!Test!!!!'),
			bldr.toString('!!!!Test!!!!'),
			'To string method is expected to be implemented as String.join over internal buffer list with delimiter as argument to overloaded' +
			' toString() method.'
		);

		bldr.clear();

		bldr.append(new Set<String> (appendStrings));

		System.assertEquals(
			String.join(appendStrings, StringBuilder.DEFAULT_DELIMITER),
			bldr.toString(),
			'To string method is expected to be implemented as String.join over internal buffer list with default delimiter.'
		);

		System.assertEquals(
			String.join(appendStrings,'\n'),
			bldr.toString('\n'),
			'To string method is expected to be implemented as String.join over internal buffer list with delimiter as argument to overloaded' +
			' toString() method.'
		);

		System.assertEquals(
			String.join(appendStrings,'!!!!Test!!!!'),
			bldr.toString('!!!!Test!!!!'),
			'To string method is expected to be implemented as String.join over internal buffer list with delimiter as argument to overloaded' +
			' toString() method.'
		);

		Test.stopTest();
	}
}