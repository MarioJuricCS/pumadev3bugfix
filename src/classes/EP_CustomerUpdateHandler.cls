/* 
   @Author          Accenture
   @name            EP_CustomerUpdateHandler
   @CreateDate      03/10/2017
   @Description     Handler class to handle the customer update inbound interface
   @Version         1.0
*/
public class EP_CustomerUpdateHandler extends EP_InboundHandler {
    
    /**
    * @author           Accenture
    * @name             processRequest
    * @date             03/13/2017
    * @description      Will process the customer inbound request and sends back the response
    * @param            string
    * @return           string
    */
    public override string processRequest(string jsonRequest){ 
        EP_GeneralUtility.Log('Public','EP_CustomerUpdateHandler','processRequest');
        string jsonResponse ='';
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        EP_MessageHeader HeaderCommon = new EP_MessageHeader();
        EP_IntegrationUtil.isCallout = true;
        try {
            EP_CustomerUpdateStub stub = parse(jsonRequest); 
            HeaderCommon = stub.MSG.HeaderCommon;
            if(EP_COMMON_CONSTANT.CSTMR_OUTBOUND_STGNG.equalsIgnoreCase(HeaderCommon.ObjectName)){
                List<EP_CustomerUpdateStub.customer> customers = stub.MSG.payload.any0.customers.customer;
                EP_CustomerUpdateHelper.setCustomerAttributes(customers); 
                ackDatasets = createAcknowledgementDatasets(customers);
            } else if(EP_COMMON_CONSTANT.CSTMR_BANK_OUT_STGNG.equalsIgnoreCase(HeaderCommon.ObjectName)){
                List<EP_CustomerUpdateStub.custBank> custBank = stub.MSG.payload.any0.custBanks.custBank;
                EP_CustomerUpdateHelper.setBankAttributes(custBank);
                ackDatasets = createAcknowledgementDatasets(custBank);
            }
            jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_UPDATE,false,EP_Common_Constant.BLANK,HeaderCommon,ackDatasets);
        } catch(Exception ex){
            
            jsonResponse = EP_AcknowledgementHandler.createAcknowledgement(EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_UPDATE,true,ex.getMessage(),HeaderCommon,ackDatasets);
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, EP_Common_Constant.NAV_TO_SFDC_CUSTOMER_UPDATE,EP_CustomerUpdateHandler.class.getName(), ApexPages.Severity.ERROR);
        }
        return jsonResponse;            
    }
    
    /**
    * @author           Accenture
    * @name             parse
    * @date             03/13/2017
    * @description      Method to parse i.e. deserialize the json string into EP_CustomerUpdateStub stub class
    * @param            String
    * @return           EP_CustomerUpdateStub
    */
    @TestVisible
    private static EP_CustomerUpdateStub parse(String json){
        EP_GeneralUtility.Log('private','EP_CustomerUpdateHandler','parse');
        return (EP_CustomerUpdateStub) System.JSON.deserialize(json, EP_CustomerUpdateStub.class);
    }
    /*Novasuite Fix comments added*/
     /**
    * @author           Accenture
    * @name             createAcknowledgementDatasets
    * @date             03/13/2017
    * @param            List<EP_CustomerUpdateStub.custBank>
    * @return           List<EP_AcknowledgementStub.dataset>
    */
    @TestVisible
    private static List<EP_AcknowledgementStub.dataset> createAcknowledgementDatasets(List<EP_CustomerUpdateStub.custBank> custBanks){
        EP_GeneralUtility.Log('private','EP_CustomerFundsUpdateHandler','createAcknowledgementDatasets');
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
        List<EP_Bank_Account__c> banksToUpsert = new List<EP_Bank_Account__c>();
        for(EP_CustomerUpdateStub.custBank bank : custBanks){
            banksToUpsert.add(bank.SFBankAccount);
        }
        //doDML(customersToUpdate,ackDatasets); 
        EP_AcknowledgementUtil.doDML(banksToUpsert,ackDatasets);
        return ackDatasets;
    }
    
    /**
    * @author           Accenture
    * @name             createAcknowledgementDatasets
    * @date             03/13/2017
    * @description      Will process the customer records and creates the appropriate datasets
    * @param            List<EP_CustomerUpdateStub.customer>
    * @return           List<EP_AcknowledgementStub.dataset>
    */
    @TestVisible
    private static List<EP_AcknowledgementStub.dataset> createAcknowledgementDatasets(List<EP_CustomerUpdateStub.customer> customers){
        EP_GeneralUtility.Log('private','EP_CustomerUpdateHandler','createAcknowledgementDatasets');
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
        boolean recordFailed= false;
        List<Account> customersToUpdate = new List<Account>();
        List<EP_Bank_Account__c> banksToUpsert = new List<EP_Bank_Account__c>();
        //List<Account> shipToUpdate = new List<Account>();
        for(EP_CustomerUpdateStub.customer cust : customers){
            if(string.isNotBlank(cust.errorDescription)){
                recordFailed = true;
                ackDataset = EP_AcknowledgementUtil.createDataSet(EP_COMMON_CONSTANT.CUSTOMER,cust.seqId,cust.errorDescription,cust.errorDescription);
                ackDatasets.add(ackDataset);
                getErrorDatasetOfBankAccounts(cust.custBanks,ackDatasets,cust.errorDescription);
            } else {
                customersToUpdate.add(cust.SFAccount);
                getBankAccountsToUpsert(cust.custBanks,banksToUpsert);
                //L4 #45361 Code changes start
                getShipToUpdate(cust.shipToAddresses, customersToUpdate);
                //L4 #45361 Code changes End
            }
        }
            
        //doDML(customersToUpdate,banksToUpsert,ackDatasets,recordFailed);
        SavePoint sp = DataBase.setSavePoint();
        //L4 #45361 Code changes Start
        boolean bankAccountFailed = EP_AcknowledgementUtil.doDML(banksToUpsert,ackDatasets);
        EP_CustomerUpdateHelper.setPreferredBankAccount(customers);
        boolean customerFailed = EP_AcknowledgementUtil.doDML(customersToUpdate,ackDatasets);
        //L4 #45361 Code changes End
        if(recordFailed || customerFailed || bankAccountFailed){
            DataBase.RollBack(sp);
        }
        return ackDatasets;             
    }
    /**
    * @author           Accenture
    * @name             getErrorDatasetOfBankAccounts
    * @date             03/13/2017
    * @description      Gets the dataset of bank accounts for the error condition 
    * @param            List<custBank>,List<EP_AcknowledgementStub.dataset>,string
    * @return           List<EP_AcknowledgementStub.dataset>
    */
    @TestVisible
    private static void getErrorDatasetOfBankAccounts(EP_CustomerUpdateStub.custBanks custBanks, List<EP_AcknowledgementStub.dataset> ackDatasets, string errorDescription){
        EP_GeneralUtility.Log('private','EP_CustomerUpdateHandler','getErrorDatasetOfBankAccounts');
        if(custBanks !=null && custBanks.custBank!=null){
            EP_AcknowledgementStub.dataset ackDataset = new EP_AcknowledgementStub.dataset();
            for(EP_CustomerUpdateStub.custBank bank : custBanks.custBank){
                ackDataset = EP_AcknowledgementUtil.createDataSet(EP_COMMON_CONSTANT.BANK_ACCOUNT,bank.seqId,errorDescription,errorDescription);
                ackDatasets.add(ackDataset);
            }
        }
    }
    
    /**
    * @author           Accenture
    * @name             getBankAccountsToUpsert
    * @date             03/13/2017
    * @description      Gets the list of bank accounts which can be processed
    * @param            List<custBank> custBank
    * @return           List<EP_Bank_Account__c>
    */
    @TestVisible
    private static void getBankAccountsToUpsert(EP_CustomerUpdateStub.custBanks custBanks, List<EP_Bank_Account__c> banksToUpsert){
        EP_GeneralUtility.Log('private','EP_CustomerUpdateHandler','getBankAccountsToUpsert');
        if(custBanks !=null && custBanks.custBank!=null){
            for(EP_CustomerUpdateStub.custBank bank : custBanks.custBank){
                banksToUpsert.add(bank.SFBankAccount);
            }
        }
    }
    //L4 #45361 Code changes start
    /**
    * @author           Accenture
    * @name             getShipToUpdate
    * @date             11/21/2017
    * @description      Gets the list of ShipTo's accounts which can be processed
    * @param            EP_CustomerUpdateStub.shipToAddresses, List<Account> customersToUpdate
    * @return           NA
    */
    @TestVisible
    private static void getShipToUpdate(EP_CustomerUpdateStub.shipToAddresses shipToAddresses, List<Account> customersToUpdate) {
        EP_GeneralUtility.Log('private','EP_CustomerUpdateHandler','getShipToUpdate');
        if(shipToAddresses !=null && shipToAddresses.shipToAddress!=null){
            for(EP_CustomerUpdateStub.shipToAddress shipTo : shipToAddresses.shipToAddress){
                if(shipTo.SFShipToObject != null) {
                    customersToUpdate.add(shipTo.SFShipToObject);
                }
            }
        }
    }
    //L4 #45361 Code changes End
}