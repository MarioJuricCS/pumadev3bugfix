/*
   @Author          Accenture
   @Name            EP_AutomaticTechnicalRetryBatch_UT - #59186
   @CreateDate      10/07/2017
   @Description     This Test class for EP_AutomaticTechnicalRetryBatch Class
   @Version         1.0
*/
@isTest
public class EP_ScheduleAutomaticRetry_UT {
    
	@testSetup static void init() {
		List<EP_CS_Communication_Settings__c> lCummunicationSetting = Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
      	List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
      	List<EP_INTEGRATION_CUSTOM_SETTING__c> lIntegrationCustomSetting = Test.loadData(EP_INTEGRATION_CUSTOM_SETTING__c.sObjectType, 'EP_INTEGRATION_CUSTOM_SETTING_TESTDATA');
      	List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    }
    
    @isTest static void execute_test(){
        Test.StartTest();
        EP_ScheduleAutomaticRetry  atr = new EP_ScheduleAutomaticRetry();
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        String jobId = system.schedule('RetryBatchSchedule', CRON_EXP, atr); 
        Test.stopTest();        
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        // Verify the expressions are the same
        System.assertEquals(CRON_EXP, ct.CronExpression);
        // Verify the next time the job will run
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
    }
}