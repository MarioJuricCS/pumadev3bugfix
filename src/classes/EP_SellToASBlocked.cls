/*
 *  @Author <Accenture>
 *  @Name EP_SellToASBlocked
 *  @NovaSuite Fix -- comments added
 */
public class EP_SellToASBlocked extends EP_AccountState{
     /***NOvasuite fix constructor removed**/

    /*
     *  @Author <Accenture>
     *  @Name setAccountDomainObject
     */    
    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_SellToASBlocked','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }
    /*
     *  @Author <Accenture>
     *  @Name doOnEntry
     */    
    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_SellToASBlocked','doOnEntry');
        /*if(!this.account.isStatuschanged()){
	        EP_AccountService accService = new EP_AccountService(this.account);
	        accService.doActionSendUpdateRequestToNavAndLomo();
        }*/
        //Code changes for L4 #45362 Start
        if(this.account.isStatuschanged()){
	        EP_AccountService accService = new EP_AccountService(this.account);
	        //accService.doActionSendUpdateRequestToNavAndLomo();
	        accService.doActionSendCreateRequestToWinDMS();
        }
        //Code changes for L4 #45362 End
    }  
    /*
     *  @Author <Accenture>
     *  @Name doOnExit
     */    
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_SellToASBlocked','doOnExit');
    }
    /*
     *  @Author <Accenture>
     *  @Name doTransition
     */    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_SellToASBlocked','doTransition');
        return super.doTransition();
    }
    /*
     *  @Author <Accenture>
     *  @Name isInboundTransitionPossible
     */    
    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_SellToASBlocked','isInboundTransitionPossible');
        return super.isInboundTransitionPossible();
    }
}