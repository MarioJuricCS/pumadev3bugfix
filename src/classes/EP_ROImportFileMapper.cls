/**
    @Author         Accenture
    @Name           EP_ROImportFileMapper 
    @Createdate     12/28/2016
    @Description    This class contains all SOQLs related to File Object
    @Version        1.0
    @Reference      NA
*/
public with sharing class EP_ROImportFileMapper {
    
    /**     
        * @Author       Accenture
        * @Name         EP_ROImportFileMapper 
        * @Description  Constructor
    */
    public EP_ROImportFileMapper() { } 
    
    public list<EP_File__c> getRecordsByFileNameAndCheckSumKey(string fileName, string checkSumKey) {
    	return [select id from EP_File__c where Name =:fileName and EP_CheckSum_Key__c =:checkSumKey limit 1];
    }
    
    public EP_File__c getFileRecordById(Id fieldId) {
    	return [select Id, Name, EP_CheckSum_Key__c,EP_Company__c,EP_Status__c,EP_Company_Code__c,EP_In_Process__c from EP_File__c where Id=:fieldId limit 1];
    }
}