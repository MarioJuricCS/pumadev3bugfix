/*
    @Author          Accenture
    @Name            EP_ROImportStaging
    @CreateDate      
    @Description     virtual class is used to RO Import Staging Object
    @Version         1.0
    @Reference       NA
*/
public virtual class EP_ROImportStaging {
    public Id fileId;
    public list<EP_RO_Import_Staging__c> stagingRecords = new list<EP_RO_Import_Staging__c>();
    
    public virtual void processStagingDataValidation(Id fileId){
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','processStagingDataValidation');
        try {
        	init(fileId);
	    	masterDataVerification();
	    	updateStagingRecords();
	    	updateFileRecord();
        } catch(exception exp) {
            EP_LoggingService.logHandledException(exp, EP_Common_Constant.EPUMA, 'processStagingDataValidation',EP_ROImportStaging.class.getName(), ApexPages.Severity.ERROR);
        }
    }
    
    public virtual void init(Id fileId){
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','init');
    	this.fileId = fileId;
    	stagingRecords = new EP_ROImportStagingMapper().getRecordByFileId(this.fileId);
    	createDataSets();
    }
    
    public virtual void masterDataVerification(){
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','masterDataVerification');
    }
    
    public virtual void updateStagingRecords() {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','updateStagingRecords');
    	for(EP_RO_Import_Staging__c stagingRec : stagingRecords) {
    		if(string.isNotBlank(stagingRec.EP_Remark_Reason__c) && !stagingRec.EP_Status__c.equalsIgnoreCase('Duplicate')){
    			stagingRec.EP_Status__c = 'Error';
    		}
    	}
    	database.update(stagingRecords);
    }

    public virtual void updateFileRecord() {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','updateFileRecord');
		EP_File__c fileRec = new EP_File__c(Id = fileId);
		fileRec.EP_Status__c = 'Processed';
		fileRec.EP_In_Process__c = false;
    	database.update(fileRec);
    }
    
    @testVisible 
    private void createDataSets() {
    	EP_GeneralUtility.Log('public','EP_ROImportStaging','createDataSets');
    	set<string> acCompositeKeySet = new set<string>();
    	set<string> deliveryDocketSet = new set<string>();
    	set<string> orderNumberSet = new set<string>();
    	set<string> navStockLocationSet = new set<string>();//i.	Supply location 
    	set<string> productCodeSet = new set<string>();//EP_Composite_Id__c
    	set<string> transporterSet = new set<string>();//EP_Vendor_Unique_Id__c 
    	set<string> supplierNumberSet = new set<string>();//EP_Vendor_Unique_Id__c
    	set<string> contractNumberSet  = new set<string>();//
    	
    	for(EP_RO_Import_Staging__c stagingRec : stagingRecords) {
    		acCompositeKeySet.add(stagingRec.EP_Sell_To_Composit_key__c);
    		acCompositeKeySet.add(stagingRec.EP_Ship_To_Composit_Key__c);
    		deliveryDocketSet.add(stagingRec.EP_Delivery_Docket_Number__c);
    		orderNumberSet.add(stagingRec.EP_Order_Number__c);
    		navStockLocationSet.add(stagingRec.EP_Supply_Location_Code__c);
    		productCodeSet.add(stagingRec.EP_Product_Unique_Key__c);
    		transporterSet.add(stagingRec.EP_Transporter_Number__c);
    		supplierNumberSet.add(stagingRec.EP_Supplier_Number__c);
    		contractNumberSet.add(stagingRec.EP_Contract_Number__c);
    	}
    }
}