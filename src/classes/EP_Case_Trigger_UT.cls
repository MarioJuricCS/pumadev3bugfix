/* 
      @Author <Accenture>
       @name <EP_Case_Trigger_UT>
       @CreateDate <08/05/2018>
       @Description  <This is apex test class for EP_Case_Trigger> 
       @Version <1.0>
    */
    @isTest
    public class EP_Case_Trigger_UT{
    
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description create test data for all required object in test methods
        */
        @TestSetup
        static void initData(){
             //create trigger Setting Custom Setting
            Trigger_Settings__c TriggerSettings = new Trigger_Settings__c();
            TriggerSettings.Name = 'EP_Case_Trigger';
            TriggerSettings.IsActive__c = true;
            insert TriggerSettings;
            
            //create Company
           Company__c tempCompany = EP_TestDataUtility.createCompany('12345');
           tempCompany.EP_Close_Case_Expiration__c = 7;
           insert tempCompany;
           
            //Create Sell To Account
            Account tempSellToAcc = EP_TestDataUtility.createSellToAccount(null,null);
            tempSellToAcc.EP_Puma_Company__c = tempCompany.id;
            tempSellToAcc.EP_Status__c= EP_Common_Constant.STATUS_ACTIVE;
            tempSellToAcc.CurrencyIsoCode = 'AUD';
            insert tempSellToAcc;
            
            //create case record..
            Id CaseRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Customer Support Case').getRecordTypeId();
            Case tempCase = EP_TestDataUtility.createCase(CaseRecordTypeId);
            tempcase.AccountId = tempSellToAcc.id;
            tempcase.status = 'Open';
            insert tempcase;
        }
        
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description Test to check positive scenario of Case_Trigger Before update Event.
        */
        static testMethod void Case_Trigger_BeforeUpdatetEvent_Test(){
            Case tempCase = [Select Id,Status from case];
         
            Test.startTest();
            tempCase.Status = 'Closed';
            update tempCase;
            Test.stopTest();
            //Asert to check result
            case caseObj = [select id,status from case where id=:tempCase.id];
            System.assertEquals(caseObj.Status,'Closed');
        }
    }