@isTest
private class InitiateOrchestratorProcess_UT {
  
    static testMethod void runTest() {
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        INSERT pricebook;
        Product2 prod = new Product2(Name = 'Product');
        INSERT prod;
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        INSERT entry;
        
        csord__Order__c newOrder = EP_TestDataUtility.getSalesOrder();
        newOrder.EP_Error_Product_Codes__c = 'Test';       
        update newOrder; 
        
        LIST<csord__Order_Line_Item__c> oiList = [SELECT Id, OrderId__c, PricebookEntryId__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:newOrder.Id];
        csord__Order_Line_Item__c lineItem = oiList[0];
        lineItem.PricebookEntryId__c = String.valueOf(entry.Id);
        lineItem.EP_Product__c = prod.Id;
        UPDATE lineItem;
        
        
        CSPOFA__Orchestration_Process_Template__c moptc = new CSPOFA__Orchestration_Process_Template__c();
        moptc.Name = 'Test MOPTC';
        insert moptc;
        
        CSPOFA__Orchestration_Process__c mopc = new CSPOFA__Orchestration_Process__c();
        mopc.Name = 'Test MOPC';
        mopc.CSPOFA__Orchestration_Process_Template__c = moptc.Id;
        mopc.Order__c = newOrder.Id;
        insert mopc;
        
        CSPOFA__Orchestration_Step__c mosc = new CSPOFA__Orchestration_Step__c();
        mosc.Name = 'Test MOSC';
        mosc.CSPOFA__Orchestration_Process__c = mopc.Id;
        insert mosc;
       
        List<Id> orderIds= new List<Id>();
        orderIds.add(newOrder.Id);        
        
        Test.startTest();
        
        System.Assert(orderIds != null);
        InitiateOrchestratorProcess.execute(orderIds);
        newOrder.Cancellation_Check_Done__c  = False;    
        newOrder.EP_Sync_with_NAV__c = True;     
        update newOrder; 
        
        System.AssertEquals(newOrder.Cancellation_Check_Done__c,False);
        InitiateOrchestratorProcess.execute(orderIds);
        
        newOrder.Cancellation_Check_Done__c  = True;       
        update newOrder;
        
        CS_ORDER_SETTINGS__c orderSettings = new CS_ORDER_SETTINGS__c(Orchestrator_Cancel_Template__c = moptc.Id);
        insert orderSettings;
        
        System.AssertEquals(newOrder.Cancellation_Check_Done__c,True);
        InitiateOrchestratorProcess.execute(orderIds);
        
        Test.stopTest();
    }
}