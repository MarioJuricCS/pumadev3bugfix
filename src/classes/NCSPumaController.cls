global with sharing class NCSPumaController {
  
  @testVisible
  private String configurationId;
  @testVisible
  private String productDefinitionId;
  @testVisible
  private String configuration;
  @testVisible
  private String productDefinitionName;
  @testVisible
  private String parentDefinitionId;
  
  global NCSPumaController() {
    configurationId = ApexPages.currentPage().getParameters().get('configId');
    productDefinitionId = ApexPages.currentPage().getParameters().get('schemaId');
    parentDefinitionId = ApexPages.currentPage().getParameters().get('defId');
    productDefinitionName = ApexPages.currentPage().getParameters().get('defName');
    if (configurationId != null && productDefinitionId != null) {
      getJSONAttachment();
    } else {
      this.schema = '{}';
      this.configuration = '{}';
    }
  }
  
    public String configurationObject {
    public get{
      return configuration;
    }
  }
  public String configId {
    public get{
      return configurationId;
    }
  }
  public String definitionName {
    public get{
      return productDefinitionName;
    }
  } 
  public String definitionId {
    public get{
      return productDefinitionId;
    }
  }
  public String schema {
    public get {
      return schema;
    }
    public set;
  }
  public String schemaId {
    public get {
      return schemaId;
    }
    public set;
  }  
  public String renderType {
    public get {return renderType;}
    public set;
  }

  @RemoteAction
  global static Map<String, String> getMLEData(String productDefinitionName, String parentDefinitionId, String productDefinitionId, String configurationId) {
    Map<String, String> retMap = new Map<String, String>();
    List<Attachment> attList = new List<Attachment> ();
    List<Attachment> schemaAttList = new List<Attachment> ();
    
    attList =  [
      select Body, ParentId 
      from Attachment 
      where ParentId =:configurationId 
      and Name = :productDefinitionName + '_schema.json'
    ];

    List<csoe__Non_Commercial_Product_Association__c> assocList = [
      select csoe__Non_Commercial_Schema__c, csoe__Max__c, csoe__Non_Commercial_Schema__r.csoe__Schema__c
      from csoe__Non_Commercial_Product_Association__c 
      where csoe__Commercial_Product_Definition__c = :parentDefinitionId 
      and csoe__Non_Commercial_Schema__c = :productDefinitionId
    ];
    if (!assocList.isEmpty()) {
      String schema = assocList[0].csoe__Non_Commercial_Schema__r.csoe__Schema__c;
      String schemaId = assocList[0].csoe__Non_Commercial_Schema__c;
      String renderType = assocList[0].csoe__Max__c == 1? 'single' : 'multiple';
      retMap.put('schema', schema);
      retMap.put('schemaId', schemaId);
      retMap.put('renderType', renderType);
    } else {
      retMap.put('schema', '{}');
      retMap.put('schemaId', productDefinitionId);
      retMap.put('renderType', '');
    }
    if (!attList.isEmpty()) {
      String configuration = attList[0].body.toString();
      retMap.put('configuration', configuration);
    } else {
      String configuration = '{}';
      retMap.put('configuration', configuration);
    }
    retMap.put('configurationId', configurationId);
    retMap.put('definitionId', parentDefinitionId);
    retMap.put('definitionName', productDefinitionName);
    retMap.put('schemaId', productDefinitionId);
    return retMap;
  }


  @testVisible
  private void getJSONAttachment() {
    List<Attachment> attList = new List<Attachment> ();
    List<Attachment> schemaAttList = new List<Attachment> ();
    
    attList =  [
      select Body, ParentId 
      from Attachment 
      where ParentId =:configurationId 
      and Name = :productDefinitionName + '_schema.json'
    ];

    List<csoe__Non_Commercial_Product_Association__c> assocList = [
      select csoe__Non_Commercial_Schema__c, csoe__Max__c, csoe__Non_Commercial_Schema__r.csoe__Schema__c
      from csoe__Non_Commercial_Product_Association__c 
      where csoe__Commercial_Product_Definition__c = :parentDefinitionId 
      and csoe__Non_Commercial_Schema__c = :productDefinitionId
    ];
    if (!assocList.isEmpty()) {
      this.schema = assocList[0].csoe__Non_Commercial_Schema__r.csoe__Schema__c;
      this.schemaId = assocList[0].csoe__Non_Commercial_Schema__c;
      this.renderType = assocList[0].csoe__Max__c == 1? 'single' : 'multiple';
    } else {
      this.schema = '{}';
      this.schemaId = '';
      this.renderType = '';
    }
    if (!attList.isEmpty()) {
      configuration = attList[0].body.toString();
    } else {
      configuration = '{}';
    }
  }
  
  @RemoteAction
  global static String saveJSONData(String data, String configurationId, String schemaName){
    return csoe.NonCommercialSchemaController.saveJSONData(data, configurationId, schemaName);
  }
  
  @RemoteAction
  global static Map<String,Object> remoteAction(Map<String,Object> inputMap, String className) {
    return csoe.NonCommercialSchemaController.remoteAction(inputMap, className);
  }  
  
  @RemoteAction
  global static Map<String,String> getLookupValues(String fieldsMap, String objId) {
    return csoe.NonCommercialSchemaController.getLookupValues(fieldsMap, objId);
  }
  
  @TestVisible
  private String getJsonConfigurationField(cscfga__Json_Settings__c jsonSettingConfiguration, String fieldName) {
    if (jsonSettingConfiguration != null) {
      Map<String, Object> deserializedJsonMap = (Map<String, Object>) JSON.deserializeUntyped(jsonSettingConfiguration.cscfga__json_configuration__c);
      return string.valueOf(deserializedJsonMap.get(fieldName));
    }
    
    return '';
  }
}