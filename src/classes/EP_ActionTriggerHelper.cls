/*  
   @Author <Amit Singh>
   @name <EP_ActionTriggerHelper>
   @CreateDate <20/04/2016>
   @Description <This class is Helper class of action trigger>  
   @Version <1.0>
*/
public without sharing class EP_ActionTriggerHelper {
    
    private static final string NAV = 'NAV';
    private static final string CUSTOMER_UPDATE = 'CUSTOMER_UPDATE';
    private static final string QUEUE_STR = 'Queue';
    private static final string CLASSNAME = 'EP_ActionTriggerHelper';
    private static final string ASSIGN_QUEUE_AND_RECORDTYPE_MTD = 'assignQueueAndRecordType';
    /***L4-45352 start****/
    private static final string ISBSMGMREQUIRED = 'isBSMGMRequired';
    private static final string DEACTIVATEALLAPRICEBOOKENTRIES = 'DeactivateAllPriceBookEntries';
    private static final string ACTIVATEALLAPRICEBOOKENTRIES = 'ActivateAllPriceBookEntries';
    /***L4-45352 end****/
    private static final string QUERY_RECORDTYPE_BASEDON_RECORDTYPENAME_MTD = 'queryRecordTypeBasedOnRtypeName';
    private static final String IN_SELL_TO = ' EP_Sell_To__c IN:sSellTo';
    private static final String IN_SHIP_TO = ' EP_Ship_To__c IN:sShipTo';
    private static final String SELL_TO_SET = 'sSellTo';
    private static final String REVIEWQUEUE = 'EP_Review_Queue';
   
    /*DEFECT #28204 START*/
    private static final string RSTRCT_ACTION_OWNR_CHNG = 'restrictActionOwnerChange';
    /*DEFECT #28204 END*/
    
    /*
     * This method is assigning recrdTYpe for any action based on Rccord Type Name
     **/
    /***L4-45352 start****/
    public static void assignQueueAndRecordType(List<EP_Action__c> lNewActions){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHelper','assignQueueAndRecordType');
        try{
            if(lNewActions != null &&  !lNewActions.isEmpty()){
                list<Group> lstqueue = [select Id From Group Where DeveloperName =: REVIEWQUEUE And Type =: QUEUE_STR Limit 1];
                Set<string> recordTypeNameSet = new Set<string>();
                for(EP_Action__c act: lNewActions){
                    if(!string.isBlank(act.EP_Record_Type_Name__c)){
                        recordTypeNameSet.add(act.EP_Record_Type_Name__c);                    
                    }
                    if(lstqueue.size()>0)
                        act.ownerid=lstqueue[0].id;
                  }
                if(!recordTypeNameSet.isEmpty()){    
                    Map<string, RecordType> rtypeMap = queryRecordTypeBasedOnRtypeName(recordTypeNameSet);                        
                    for(EP_Action__c act: lNewActions) {                      
                        if(!string.isBlank(act.EP_Record_Type_Name__c) && rtypeMap.containsKey(act.EP_Record_Type_Name__c)){
                            act.RecordTypeId = rtypeMap.get(act.EP_Record_Type_Name__c).id;
                        }
                    }
                }
            }
        }
        Catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, ASSIGN_QUEUE_AND_RECORDTYPE_MTD, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    /*
     * @ Description : This method is for deactivating all associated priceboon entries on creation of new pricebook action on price book
     As per L4-45352 they will get activated on completion of this new review. in method 'ActivateAllPriceBookEntries'
     * @ params : List of actions 
     * @ Return Type : void
     **/
    public static void DeactivateAllPriceBookEntries(List<EP_Action__c> lNewActions){   
        List<pricebookentry> DeactivatePriceBookEntry = new List<pricebookentry>();
        EP_PriceBookEntryMapper PBEMapper = new EP_PriceBookEntryMapper();
        set<Id> PriceBookIds = new set<Id>();
        try{
            for(EP_Action__c actionVar: lNewActions){
                if(actionVar.EP_Product_List__c!=null){
                    PriceBookIds.add(actionVar.EP_Product_List__c);
                }
            }
            if(PriceBookIds.size()>0){
                for(pricebookentry VarPBE : PBEMapper.getRecordsByPricebookIds(PriceBookIds)){
                    if(VarPBE.isActive){
                        VarPBE.isActive = false;
                        DeactivatePriceBookEntry.add(VarPBE);
                    }
                }
                if(DeactivatePriceBookEntry.size()>0){
                    database.update(DeactivatePriceBookEntry);
                }
            }        
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, DEACTIVATEALLAPRICEBOOKENTRIES,  CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    /*
     * @ Description : This method is for activating all associated priceboon entries on completion of pricebook action on price book
     * @ params : Map of new and old actions 
     * @ Return Type : void
     **/
    public static void ActivateAllPriceBookEntries(set<id> PriceBookIds){ 
        List<pricebookentry> ActivatePriceBookEntry = new List<pricebookentry>();
        EP_PriceBookEntryMapper PBEMapper = new EP_PriceBookEntryMapper();
        try{
            if(PriceBookIds.size()>0){
                for(pricebookentry VarPBE : PBEMapper.getRecordsByPricebookIds(PriceBookIds)){
                    if(!VarPBE.isActive){                        
                        VarPBE.isActive = true;
                        VarPBE.EP_Is_Sell_To_Assigned__c = true;
                        ActivatePriceBookEntry.add(VarPBE);
                    }
                }
                if(ActivatePriceBookEntry.size()>0){
                    database.update(ActivatePriceBookEntry);
                }
            }        
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, ACTIVATEALLAPRICEBOOKENTRIES, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    
    
   /*
     * @ Description : This method is for creating BSM/GM review on Sell to account, if action of sell to is completed,
     *  then all associated ship to should be Account set up and all associated actions should be completed
     * if this condition satisfies then BSM/GM review should be created against sell to 
     * @ params : Map of new and old actions 
     * @ Return Type : void
     **/
    public static void isBSMGMRequired(map<Id,id> SellToAccountSet){   
        
        List<EP_Action__c> ListBSMGMCreated = new List<EP_Action__c>();
        set<Id> BSMToBereatedforAccounts = new set<Id>();
        try{
            if(SellToAccountSet.size()>0){
                BSMToBereatedforAccounts = ChecksForBSMGM(SellToAccountSet);
                if(BSMToBereatedforAccounts.size()>0){
                    ListBSMGMCreated = CreateBSMGM(BSMToBereatedforAccounts);
                    if(ListBSMGMCreated.size()>0){
                        Database.insert(ListBSMGMCreated);
                    }
                }
            }
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, ISBSMGMREQUIRED, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    
    /*
     * @ Description : This method returns list of BSM/GM that has to be created
     * @ params : set<Id> 
     * @ Return Type : List<EP_Action__c>
     **/
    private static List<EP_Action__c> CreateBSMGM(set<Id> AccountId){
       List<EP_Action__c> ListBSMGM = new List<EP_Action__c>();
       EP_Action__c BSMGM; 
       for(Id accId : AccountId){
            //Nova Suit Fix
           BSMGM = new EP_Action__c(); 
           BSMGM.EP_Account__c = accId;
           BSMGM.EP_Record_Type_Name__c = EP_Common_Constant.ACT_BSMGM_REVIEW_RT;
           BSMGM.EP_Action_Name__c = EP_Common_Constant.ACT_BSMGM_REVIEW_RT;
           BSMGM.EP_Status__c = EP_Common_Constant.ACT_NEW_STATUS;
           ListBSMGM.add(BSMGM);
       }
       return ListBSMGM;
    }
    
    /*
     * @ Description : This method returns accounts for which BSM/GM has to be created
     * @ params : Map of accounts associated to actions 
     * @ Return Type : set<Id>
     **/
    private static set<Id> ChecksForBSMGM(map<Id,Id> SellToAccountSet){
        set<Id> BSMRequiredForAccount = new set<Id>();
        List<Id> AccountIds = new List<Id>();
        /* L4 - 45352 -- BSM?GM BUG FIX changes start*/ 
        for(Account Acc : [select id,RecordTypeName__c,ParentId from Account where id in : SellToAccountSet.Values()])
        {
            if(Acc.RecordTypeName__c == EP_Common_Constant.SELL_TO){
               AccountIds.add(Acc.id);
            }
            if(Acc.RecordTypeName__c == EP_Common_Constant.NON_VMI_SHIP_TO || Acc.RecordTypeName__c == EP_Common_Constant.VMI_SHIP_TO){
                AccountIds.add(Acc.ParentId);
            }
        }
        for(Account acc : EP_AccountMapper.getAllShipToAndActionsFromSellTo(AccountIds)){
            if(acc.AccountActions__r.size()>0){  
                if(isActionCompleted(acc.AccountActions__r) && ((acc.ChildAccounts.size()>0 && isAccountsetup(acc.ChildAccounts)) || (acc.ChildAccounts.size()==0))){
                    BSMRequiredForAccount.add(acc.id);
                }
            }
        }
        return BSMRequiredForAccount;
    }
    
    /* L4 - 45352 -- BSM?GM BUG FIX changes end*/
    /*
     * @ Description : This method returns true if ship to is account set up
     * @ params : List of account 
     * @ Return Type : Boolean
     **/
     private static Boolean isAccountsetup(List<Account> shiptoList){
        set<id> accountIds = new set<id>();
        for(Account shipto : shiptoList){
           accountIds.add(shipto.id);
        }        
        if(accountIds.size()>0 && IsShipToActionCompleted(accountIds)){
             return true;   
        }
        return false;
    }
    
    /*
     * @ Description : This method returns true all ship to actions are completed
     * @ params : set of account ids 
     * @ Return Type : Boolean
     **/    
    private static Boolean IsShipToActionCompleted(set<id> accountIds){
        EP_ActionMapper mapper = new EP_ActionMapper();
        for(Ep_action__c varaction : mapper.getActionRecordsByAccountID(accountIds)){
            if(varaction.EP_status__c!= EP_Common_Constant.ACT_COMPLETED_STATUS){
                /* L4 - 45352 -- BSM?GM BUG FIX changes start*/
                return false;
                break;
                /* L4 - 45352 -- BSM?GM BUG FIX changes end*/
            }
        }
        return true;
    }
    /*
     * @ Description : This method returns true all actions are completed
     * @ params : List of actions 
     * @ Return Type : Boolean
     **/
    Private static Boolean isActionCompleted(List<EP_Action__c> actionList){
        for(EP_Action__c actionCompleted : actionList){
        /* L4 - 45352 -- BSM?GM BUG FIX changes start*/
            if(actionCompleted.EP_Record_Type_Name__c == EP_Common_Constant.ACT_BSMGM_REVIEW_RT){   
                return false; 
                break;    
            }
            else if(actionCompleted.EP_Record_Type_Name__c != EP_Common_Constant.ACT_BSMGM_REVIEW_RT && actionCompleted.EP_Status__c != EP_Common_Constant.ACT_COMPLETED_STATUS ){
                return false;                
            }
        /* L4 - 45352 -- BSM?GM BUG FIX changes end*/
        }
        return true;
    }
    /***L4-45352 end****/
    
    /*
     * @ Description : This method is quereing recordTypes and returns the a Map of Name and recordTYpe instance
     * @ params : set of string- recordTypeNames
     * @ Return Type : Map<string, RecordType> = Map<RecordTYpe Name, RecordTYpeInstance>
     **/
    public static Map<String, RecordType> queryRecordTypeBasedOnRtypeName(Set<string> rtypes){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHelper','queryRecordTypeBasedOnRtypeName');
        Map<string, RecordType> recordTypeMap = new Map<string, RecordType>();
        try{
            Integer nRows = EP_Common_Util.getQueryLimit();
            for(RecordType rt: [Select id, Name, developerName from RecordType Where name in : rtypes limit: nRows]){
                recordTypeMap.put(rt.Name, rt);
            } 
        }
        catch(Exception handledException){
            EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, QUERY_RECORDTYPE_BASEDON_RECORDTYPENAME_MTD, CLASSNAME, ApexPages.Severity.ERROR);
        }
        return recordTypeMap;
    }

     /* Code changes for #45436
     * @ Description : This method to restrict Action Owner Change
     * @ params : map<Id,EP_Action__c>, map<Id,EP_Action__c> 
     * @ Return Type : NA
     **/
    public static void restrictActionOwnerChange(map<Id,EP_Action__c>mOldActions,map<Id,EP_Action__c>mNewActions){
        set<Id>sUserGrpId = new set<Id>();
        map<Id,user> usersProfileMap = new map<Id,user>();
        list<EP_Action__c> actionList = new list <EP_Action__c>();
        map<string,string> profileReviewTypemapping = new map<string,string>();
        EP_UserMapper userMapper = new EP_UserMapper();
        try{
            /***** Escape Validation For Admin START******/
            EP_CS_Validation_Rule_Settings__c valRuleSetting = EP_CS_Validation_Rule_Settings__c.getInstance(UserInfo.getProfileId());
            for(Id action : mNewActions.keySet()){

                if(mNewActions.get(action).OwnerId != mOldActions.get(action).OwnerId && !valRuleSetting.EP_Skip_Action_Rules__c) {
                    sUserGrpId.add(mNewActions.get(action).OwnerId);
                    actionList.add(mNewActions.get(action));
                }    
            }
            usersProfileMap = userMapper.getUsers(sUserGrpId);
            setProfileReviewTypeMapping(profileReviewTypemapping);
            for(EP_Action__c action : actionList){
                //fire this rule only for those actions/Review which is setup in the Profile Review MetaData
                 if(profileReviewTypemapping.containsKey(action.Action_Type__c)){
                     //Check if owner can only assign this action to the users only, not for the any other Queue or Group
                    if(!usersProfileMap.containsKey(action.OwnerId)){
                        action.addError(Label.EP_Insufficient_Permissions);
                     } else {
                        //Check if owner is assigning the action to the same profile users for which this action is created
                        string profileName = usersProfileMap.get(action.OwnerId).Profile.Name;
                        if(!profileName.equalsIgnoreCase(profileReviewTypemapping.get(action.Action_Type__c))) {
                             action.addError(Label.EP_Insufficient_Permissions);
                        }
                     }
                 }
            }
        } catch(Exception handledException){
            EP_LoggingService.logHandledException (handledException, EP_Common_Constant.EPUMA, RSTRCT_ACTION_OWNR_CHNG, CLASSNAME, ApexPages.Severity.ERROR);
        }
    }
    
    /*
     * @ Description : This method to set Profile ReviewType Mapping
     * @ params : map<string,string>
     * @ Return Type : NA
     **/
    private static void setProfileReviewTypeMapping(map<string,string> profileReviewTypemapping) {
        for(Profile_Review_Mapping__mdt ReviewProfileMappinglist : [SELECT DeveloperName,Profile_Name__c FROM Profile_Review_Mapping__mdt] ){
            profileReviewTypemapping.put(ReviewProfileMappinglist.DeveloperName,ReviewProfileMappinglist.Profile_Name__c);
        }
    }
    
    //L4_45352_START
    /**
    * @author       Accenture
    * @name         updateShipToStatusToAccountSetup
    * @date         11/20/2017
    * @description  This method is used to updated shipto status to Account Setup once all actions are completed
    * @param        List<EP_Action__c> 
    * @return       NA
    */ 
    public static void updateShipToStatusToAccountSetup(List<EP_Action__c> actionList){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHelper','updateShipToStatusToAccountSetup');
        try{
            Set<String> actionStatus =  new set<String>{EP_Common_Constant.ACT_NEW_STATUS,EP_Common_Constant.ACT_ASSIGNED_STATUS};
            set<id> shipToIds = new set<id>();
            set<id> accountsToBeupdated = new set<id>();
            for(EP_Action__c actionObj : actionList){
                if(isShipToAction(actionObj)){
                    shipToIds.add(actionObj.EP_Account__c);
                }
            }
            //EP_ActionMapper actionMapper =  new EP_ActionMapper(shipToIds);
            Map<id,boolean> shipToReviewCompleteMap = getShipToReviewsCompletedStatus(shipToIds);
            for(Id shipToId : shipToIds){
                system.debug('### ShipToId ' + shipToId);
                if (shipToReviewCompleteMap.containsKey(shipToId)) {
                    system.debug('###completed actions -- ' + shipToReviewCompleteMap.get(shipToId));
                    if(shipToReviewCompleteMap.get(shipToId)){
                        accountsToBeupdated.add(shipToId);
                    }
                }
            }
            if(!accountsToBeupdated.isEmpty()){
                system.debug('###updating shipto -- ' + accountsToBeupdated);
                updateAccountStatus(accountsToBeupdated);
            }
        }catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'updateShipToStatusToAccountSetup' , 'EP_ActionTriggerHelper', ApexPages.Severity.ERROR);
        }         
    }
    /**
    * @author       Accenture
    * @name         updateAccountStatus
    * @date         11/20/2017
    * @description  This method is used to update shipto accounts passed to it and resets flags for resync
    * @param        set<id> 
    * @return       NA
    */
    @testvisible 
    private static void updateAccountStatus(set<id> accountsToBeupdated){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHelper','updateAccountStatus');
        list<Account> accounts = new List<Account>();
        for(Account acc : [select id, EP_Status__c,EP_Synced_PE__c ,EP_Synced_NAV__c ,EP_Synced_winDMS__c From Account Where Id IN:accountsToBeupdated ]){
            acc.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
            acc.EP_Synced_PE__c = false;
            acc.EP_Synced_NAV__c = false;
            acc.EP_Synced_winDMS__c = false;
            accounts.add(acc);
        }
        if(!accounts.isEmpty()){
            database.update(accounts,false);
        }
    }
    /**
    * @author       Accenture
    * @name         isShipToAction
    * @date         11/20/2017
    * @description  This method is used to check if action is related to ShipTo
    * @param        EP_Action_c
    * @return       NA
    */ 
    private static boolean isShipToAction(EP_Action__c actionObj){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHelper','isShipToAction');
        return (EP_Common_Constant.VMI_SHIP_TO.equalsIgnorecase(actionObj.EP_Account_Record_Type__c) ||
            EP_Common_Constant.NON_VMI_SHIP_TO.equalsIgnorecase(actionObj.EP_Account_Record_Type__c));
    }
    /**
    * @author       Accenture
    * @name         getShipToReviewsCompletedStatus
    * @date         11/20/2017
    * @description  This method is used to get map of shipto and its reviews competed status
    * @param        set<id> 
    * @return       NA
    */ 
    public static map<Id,boolean> getShipToReviewsCompletedStatus(Set<Id> accountIds){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHelper','getShipToReviewsCompletedStatus');
        map<Id,List<EP_Action__c>> mapAccountIdAndAction = new Map<Id,List<EP_Action__c>>();
        map<Id,boolean> mapAccountIdCompleted = new Map<Id,boolean>();
        mapAccountIdAndAction = getShipToIDActionMap(accountIds);   
        mapAccountIdCompleted = getMapOfShipToAndActionStatusComplete(mapAccountIdAndAction);
        return mapAccountIdCompleted;
    }
    /**
    * @author       Accenture
    * @name         getShipToReviewsCompletedStatus
    * @date         11/20/2017
    * @description  This method is used to get map of shipto and its reviews
    * @param        set<id> 
    * @return       NA
    */ 
    private static map<Id,List<EP_Action__c>> getShipToIDActionMap(set<id> accountIds){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHelper','getShipToIDActionMap');
        map<Id,List<EP_Action__c>> mapAccountIdAndActions = new Map<Id,List<EP_Action__c>>();
        EP_ActionMapper actionMapper = new EP_ActionMapper();
        for(EP_Action__c actions : actionMapper.getActionRecordsByAccountID(accountIds)){
            if(mapAccountIdAndActions.containsKey(actions.EP_Account__c)){
                mapAccountIdAndActions.get(actions.EP_Account__c).add(actions);
            }else{
                mapAccountIdAndActions.put(actions.EP_Account__c, new list<EP_Action__c> {actions});
            }
        }
        return mapAccountIdAndActions;
    }
    /**
    * @author       Accenture
    * @name         getShipToReviewsCompletedStatus
    * @date         11/20/2017
    * @description  This method is used to get map of shipto and its reviews
    * @param        map<Id,List<EP_Action__c>>
    * @return       NA
    */ 
    private static map<Id,boolean> getMapOfShipToAndActionStatusComplete(map<Id,List<EP_Action__c>> shipToActionMap){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHelper','getMapOfShipToAndActionStatusComplete');
        map<Id,boolean> mapAccountIdandReviewCompleted = new Map<Id,boolean>();
        for(Id shipToId : shipToActionMap.keySet()){
            boolean isCompleted = true;
            for(EP_Action__c actionObj : shipToActionMap.get(shiptoId)){
                system.debug('actionObj.EP_Status__c>>>'+actionObj.EP_Status__c);
                if(EP_Common_Constant.ACT_NEW_STATUS.equalsIgnoreCase(actionObj.EP_Status__c) || EP_Common_Constant.ACT_ASSIGNED_STATUS.equalsIgnoreCase(actionObj.EP_Status__c)){
                    isCompleted = false;
                    break;
                }
            }
            mapAccountIdandReviewCompleted.put(shipToId,isCompleted);
        }
        return mapAccountIdandReviewCompleted;
    }
    
    /**
    * @author       Krishan Gopal
    * @name         shareActionRecordWithUsers
    * @date         11/25/2017
    * @description  This method is used to share Action recods with Users
    * @param        List<EP_Action__c> 
    * @return       NA
    */ 
    public static void shareActionRecordWithUsers(List<EP_Action__c> actionList){
        EP_GeneralUtility.Log('Public','EP_ActionTriggerHelper','shareActionRecordWithUsers');
        try{
            EP_ActionShareUtility EP_ActionShareUtilityobj = new EP_ActionShareUtility();
            EP_ActionShareUtilityobj.sendemail = true;
            EP_ActionShareUtilityobj.shareRecords(EP_ActionShareUtilityobj.getValidUserList(actionList));
        }
        catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'shareActionRecordWithUsers' , 'EP_ActionTriggerHelper', ApexPages.Severity.ERROR);
        }
        
    }
    //L4_45352_END

    /*
     * 94086:UAT_EPA - For Retrospective orders outside of tolerance an approval workflow is required
    */
    public static void checkAndUpdateToleranceApproval(Map<Id,EP_Action__c> newActions, Map<Id,EP_Action__c> oldActions) {
        
        // 2.If Order status ="Awaiting Tolerance Approval" then Monitor the change of status for tolerance_approval action object.
        // 3.If tolerance_approval status changes to ‘Rejected’, then mark order as Draft.
        // 4.If tolerance_approval status changes to ‘Approved’ then move ahead in order lifecycle i.e with Credit check etc.. and Order submission process.
        Set<Id> orderIds = new Set<Id>();
        Set<Id> orderApprovedIds = new Set<Id>();
        
        for (EP_Action__c newAction :newActions.values()) {
            
            EP_Action__c oldAction = oldActions.get(newAction.Id);
            
            if (newAction.EP_Status__c != oldAction.EP_Status__c && newAction.EP_Status__c == '03-Rejected') {
                orderIds.add(newAction.EP_CSOrder__c);
            } else if (newAction.EP_Status__c != oldAction.EP_Status__c && newAction.EP_Status__c == '03-Approved' && newAction.EP_Action_Name__c == 'Tolerance Approval') {
                orderApprovedIds.add(newAction.EP_CSOrder__c);
            }
        }
        List<csord__Order__c> ordersTolerance = [SELECT Id, Need_RO_Dates_approval__c,tolerance_approval_needed__c,csord__Account__c FROM csord__Order__c WHERE Id IN :orderApprovedIds];
         if(ordersTolerance.size()>0 && ordersTolerance[0].Need_RO_Dates_approval__c == true){
             TrafiguraConfigurationControllerNew.createRODatesAction(ordersTolerance[0].id, ordersTolerance[0].csord__Account__c);          
         }
        // 3.If tolerance_approval status changes to �Rejected�, then mark order as Draft.
        if (!orderIds.isEmpty()) {

            List<csord__Order__c> orders = [SELECT Id, OrderStatus__c FROM csord__Order__c WHERE Id IN :orderIds AND csord__Status2__c = 'Awaiting Tolerance Approval'];
            
            for (csord__Order__c ord: orders) {
                ord.OrderStatus__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
                ord.csord__Status2__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
            }

            update orders;
        }

        // If Approved, remove flag and continue
        // 4.If tolerance_approval status changes to ‘Approved’ then move ahead in order lifecycle i.e with Credit check etc.. and Order submission process
        if (!orderApprovedIds.isEmpty()) {

            List<csord__Order__c> orders = new List<csord__Order__c>();

            for (Id orderId :orderApprovedIds) {

                csord__Order__c ord = new csord__Order__c();
                ord.Id = orderId;
                ord.Tolerance_Approval_Needed__c = false;

                orders.add(ord);
            }

            update orders;

            // Notify Orchestrator engine
            CSPOFA.Events.emit('update', orderApprovedIds);
        }
    }

    /**
    * @author       Cloudsense
    * @name         updateOrderRODatesApproval
    * @date         05/09/2018
    * @description  
    */
    public static void updateOrderRODatesApproval(Id orderId, String outcome){
        List<csord__Order__c> ordList = [select Id, Need_RO_Dates_approval__c, csord__Status2__c From csord__Order__c Where Id = :orderId];

        if(ordList.size() > 0){
            if(outcome.equals('03-Approved')){//################ TBD
                ordList[0].Need_RO_Dates_approval__c = false;

                // Notify Orchestrator engine
                CSPOFA.Events.emit('update', new Set<Id> { ordList[0].Id });

            } else if(outcome.equals('03-Rejected')){//################ TBD
                ordList[0].csord__Status2__c = EP_Common_Constant.ORDER_DRAFT_STATUS;
            }
            update ordList;
        }
    }
}
