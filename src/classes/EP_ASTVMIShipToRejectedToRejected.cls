/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToRejectedToRejected>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 08-Rejected to 08-Rejected>
*  @Version <1.0>
*/
public class EP_ASTVMIShipToRejectedToRejected extends EP_AccountStateTransition {

    public EP_ASTVMIShipToRejectedToRejected() {
        finalState = EP_AccountConstant.REJECTED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToRejectedToRejected','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToRejectedToRejected','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToRejectedToRejected','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToRejectedToRejected','doOnExit');

    }
}