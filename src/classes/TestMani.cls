public class TestMani {

    public static string LangConverter(){
        string Text;
        string language='Spanish';
          Map<String,String> orderStatusLangTransMap = new Map<String,String>();

        List<EP_FE_Order_Status_Lang_Translator__mdt> lstOrderStatusLangTranslator = [SELECT EP_FE_Converted_Language__c, EP_FE_Default__c,
                                                                                 EP_FE_Converted__c,EP_FE_Language_Status_key__c 
            FROM EP_FE_Order_Status_Lang_Translator__mdt 
            WHERE EP_FE_Converted_Language__c = :language];
           
        if(lstOrderStatusLangTranslator == null || lstOrderStatusLangTranslator.size() > 0) {
            for(EP_FE_Order_Status_Lang_Translator__mdt configuration : lstOrderStatusLangTranslator) {
                orderStatusLangTransMap.put(configuration.EP_FE_Default__c, configuration.EP_FE_Converted__c);
            }
        }
       
        
        
      String tmpBody;
         //String tmpBody = 'Ordered Schedule [2016-11-15 00:00:00]';
       
if(!string.isEmpty(tmpBody) ){
        String[] bodyList = tmpBody.split('\\['); 
        system.debug('==bodylist=='+ bodyList.size());
         string [] rightBracket;
        List<string> lst= new List<string>();
        
        for(String s : bodyList){
            if(s.contains(']'))
            {
                rightBracket=s.split('\\]');
                for(string ss: rightBracket){
                    if(!string.isEmpty(ss)){
                      system.debug('--ss---->'+ss);
                        lst.add(ss);
                    }
                    
                }
            }
            else
            {
                system.debug('---s--->'+s); 
                lst.add(s);
            }
             
        }
system.debug('==lst=='+lst);
        string concatenated='';
        system.debug('==orderStatusLangTransMap=='+ orderStatusLangTransMap);
         for(string s:lst){
            if(orderStatusLangTransMap.containsKey(s))
            {
                concatenated=concatenated+orderStatusLangTransMap.get(s)+' ';
            }else{
                concatenated=concatenated+s+' ';
            }
        }
        system.debug('==concatenated=='+ concatenated);
        }
        return 'name';
    }
}