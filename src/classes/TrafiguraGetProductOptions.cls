global class TrafiguraGetProductOptions implements csoe.IRemoteAction {

    global Map<String, Object> execute(Map<String, Object> inputMap) {
        Map<String, Object> returnMap = new Map<String, Object>();
        if (inputMap.get('AccountId') != null) {
            String accId = (String) inputMap.get('AccountId');
            String productType = (String) inputMap.get('ProductType');
            String orderType = (String) inputMap.get('OrderType');
            String supplyLocationId = (String) inputMap.get('LocationId');
            String orderEpoch = (String) inputMap.get('orderEpoch');
            Boolean isCommunity = (Boolean) inputMap.get('isCommunity');

            String shipToId = (String) inputMap.get('ShipToAccNumber');
            Boolean isVMI = false;

			if(shipToId != '' && shipToId != null){
                List<Account> shipToAcc = [select recordtype.name from Account where id =: shipToId limit 1];
                if(shipToAcc.size() > 0){
                    if(shipToAcc[0].recordtype.name == 'VMI Ship To'){
                        isVMI = true;
                    }
                }
            }
            List<EP_Product_Option__c> productOptions = [
                select Price_List__c 
                from EP_Product_Option__c 
                where Sell_To__c = :accid
                and Price_List__r.IsActive = true
            ];
            List<Id> pricebooks = new List<Id>(); 

            for (EP_Product_Option__c prodOp : productOptions) {
                pricebooks.add(prodOp.Price_List__c);
            }
            String productQuery = 'SELECT name, ProductCode, Product2.EP_Product_Sold_As__c , Product2.id, Product2.name, Product2.EP_Dirty_Product__c, Product2.EP_Company__c, Product2.Family, Product2.EP_Unit_of_Measure__c, Product2.EP_Min_Order_Qty__c from PricebookEntry where Pricebook2.Id IN :pricebooks and IsActive = true';
            String productTypeQuery = '';

            if (productType != null && orderType != null) {

                Boolean hasPackage = false;
                Boolean hasBulk = false;
                Boolean hasService = false;

                if(!isCommunity && orderType.equalsIgnoreCase(EP_OrderConstant.salesOrderType)){
                    hasService = true;
                }

                if(isVMI){
                    if(!productType.equals(EP_OrderConstant.productTypeService)){
                        hasPackage = true;
                    }
                } else {
                    if(productType.equals(EP_OrderConstant.productTypeBulk)){
                        hasBulk = true;
                    } else if(productType.equals(EP_OrderConstant.productTypePackaged)){
                        hasPackage = true;
                    }
                }

                if(hasService && hasBulk){
                    productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeBulk + '\' OR Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\')';
                } else if(hasService && hasPackage){
                    productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypePackaged + '\' OR Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\')';
                } else if(hasPackage){
                    productTypeQuery = ' AND Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypePackaged + '\'';
                } else if(hasBulk){
                    productTypeQuery = ' AND Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeBulk + '\'';
                } else if(hasService){
                    productTypeQuery = ' AND Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\'';
                }

                /*if (productType.equals(EP_OrderConstant.productTypeBulk)) {
                    if (orderType.equalsIgnoreCase(EP_OrderConstant.salesOrderType) && !isCommunity) {
                        productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeBulk + '\' OR Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\')';
                    } else {
                        productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeBulk + '\')';
                    }
                } else if(productType.equals(EP_OrderConstant.productTypeService) && !isCommunity) {
                    productTypeQuery = ' AND Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\'';
                } else if (productType.equals(EP_OrderConstant.productTypePackaged)) {
                    if (orderType.equalsIgnoreCase(EP_OrderConstant.salesOrderType) && !isCommunity) {
                        productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypePackaged + '\' OR Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypeService + '\')';
                    } else {
                        productTypeQuery = ' AND (Product2.EP_Product_Sold_As__c = \'' + EP_OrderConstant.productTypePackaged + '\')';
                    }
                }
            }
            if(isCommunity && productTypeQuery == '') {
                productTypeQuery = ' AND Product2.EP_Product_Sold_As__c != \'' + EP_OrderConstant.productTypeService + '\'';
            }*/
            }

            List<PricebookEntry> products = new List<PricebookEntry> ();
            if(isVMI== true && orderEpoch == 'Retrospective' && productType == 'Bulk') {
                List<EP_Tank__c> tankList = [Select id,EP_Product__c,EP_Product__r.ProductCode from EP_Tank__c where EP_Ship_To__c=:shipToId];
                if(!tankList.isEmpty()) {
                    Map<String,String> tankCodes = new Map<String,String>();
                    Set<String> prSet = new Set<String>();
                    for(EP_Tank__c tank:tankList) {
                        tankCodes.put(tank.EP_Product__r.ProductCode,tank.id);
                        prSet.add(tank.EP_Product__c);
                    }
                    returnMap.put('tankCodes', tankCodes);
                    if(isCommunity) {
                        products = [SELECT name, ProductCode, Product2.EP_Product_Sold_As__c , Product2.id, Product2.name, Product2.EP_Dirty_Product__c, Product2.EP_Company__c, Product2.Family, Product2.EP_Unit_of_Measure__c, Product2.EP_Min_Order_Qty__c from PricebookEntry where Product2.id in:prSet and Pricebook2.Id IN :pricebooks and IsActive = true and Product2.EP_Product_Sold_As__c != 'Service'];
                    }
                    else {
                        products = [SELECT name, ProductCode, Product2.EP_Product_Sold_As__c , Product2.id, Product2.name, Product2.EP_Dirty_Product__c, Product2.EP_Company__c, Product2.Family, Product2.EP_Unit_of_Measure__c, Product2.EP_Min_Order_Qty__c from PricebookEntry where Product2.id in:prSet and Pricebook2.Id IN :pricebooks and IsActive = true];
                    }
                }
            }
            else {
                products = Database.query(productQuery + productTypeQuery);
            }
            
            Set<Id> product2Ids = new Set<Id>();
            for (PricebookEntry pbe : products) {
                product2Ids.add(pbe.Product2.id);
            }
            List<EP_Inventory__c> inventories = [
                select id, EP_Product__c, EP_Inventory_Availability__c, EP_Storage_Location__c
                from EP_Inventory__c
                where EP_Product__c in :product2Ids
            ];
            List<Account> accs = [
                select id, name, EP_Puma_Company__c
                from Account
                where id = :accId
            ];
            if (!accs.isEmpty()) {
                List<EP_Total_Order_Quantity_Constraint__c> quantities = [
                    select Total_Order_Min_Quantity__c, EP_Total_Order_Max_Quantity__c, EP_Product__c, EP_Item_Category__c, EP_Delivery_Type__c, 
                        EP_Company__c
                    from EP_Total_Order_Quantity_Constraint__c
                    where EP_Product__c in :product2Ids
                ];
                returnMap.put('quantities', quantities);
            }
            List<EP_Stock_Holding_Location__c> stockLocation = [
                select id, name, Stock_Holding_Location__c
                from EP_Stock_Holding_Location__c
                where id = :supplyLocationId
            ];
            returnMap.put('products', products);
            returnMap.put('inventories', inventories);
            returnMap.put('stockLocation', stockLocation);
        }
        return returnMap;
    }
    
}