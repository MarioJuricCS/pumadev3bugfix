@isTest 
private class EP_CRM_LeadConvertControllerTest {
    @isTest 
    static void convertLeadTest_1() {    	    
    	Lead leadObj = createLead(getRunningUser());
    	insert leadObj;
    	System.assertNotEquals(leadObj, null);
    	
    	// Test Scenario
    	Test.startTest();
	    	EP_CRM_LeadConvertController controller = new EP_CRM_LeadConvertController(new ApexPages.StandardController(leadObj));
	    	
	    	// pref = null -- No activity is created
	    	PageReference pref = controller.convertLead();
	    	System.assertEquals(pref, null);
	    		
	    	// Create a task
	    	Task taskObj = createTask(leadObj.Id);
	    	insert taskObj;
	    	
	    	// AccountId is not set
	    	controller.bean.leadToConvert.Status = 'NONE';
	    	pref = controller.convertLead();
	    	System.assertEquals(pref, null);	
	    	
	    	// Status is not set
	    	controller.bean.leadToConvert.Status = 'Open';
	    	controller.bean.selectedAccountId = 'NONE';
	    	pref = controller.convertLead();
	    	System.assertEquals(pref, null);
	    	
	    	// set page values before converting lead
	    	controller.bean.selectedAccountId = 'NEW';
	    	controller.bean.doNotCreateOppty = false;
	    	controller.bean.sendOwnerEmail = false;
	    	
	    	pref = controller.convertLead();	
	    	System.assertEquals(pref, null);
	    	
	    	// set page values before converting lead
	    	controller.bean.selectedAccountId = 'NEW';
	    	controller.bean.leadToConvert.Status = 'Converted';
	    	controller.bean.doNotCreateOppty = false;
	    	controller.bean.sendOwnerEmail = false;
	    	
	    	pref = controller.convertLead();	
	    	System.assertNotEquals(pref, null);	    	
    	Test.stopTest();
    }
    
    @isTest 
    static void mergeContactTest_1() {
    	// Prepare Data
    	User userObj = getRunningUser();    	
    	System.assertNotEquals(userObj, null);
    	
    	// Create Account
    	Account accountObj = createAccount(userObj);
    	insert accountObj;
    	System.assertNotEquals(accountObj, null);
    	
    	// Create Contact
    	Contact contactObj = createContact(accountObj);
    	insert contactObj;
    	System.assertNotEquals(contactObj, null);
    	
    	// Create Account
    	Lead leadObj = createLead(userObj);
    	insert leadObj;
    	System.assertNotEquals(leadObj, null);
    	
    	// Create a task
    	Task taskObj = createTask(leadObj.Id);
    	insert taskObj;
    	
    	Id [] fixedSearchResults= new Id[1];
       	fixedSearchResults[0] = accountObj.Id;
       	Test.setFixedSearchResults(fixedSearchResults);
    	
    	// Test Scenario
    	Test.startTest();
	    	EP_CRM_LeadConvertController controller = new EP_CRM_LeadConvertController(new ApexPages.StandardController(leadObj));
	    	
	    	System.assertEquals(controller.bean.accounts.size(), 3);	    	
	    	
	    	// set page values before converting lead
	    	controller.bean.selectedAccountId = controller.bean.accounts[2].getValue();
	    	controller.bean.doNotCreateOppty = false;
	    	controller.bean.sendOwnerEmail = false;
	    	controller.bean.leadToConvert.Status = controller.bean.LeadStatusOption[0].getValue();
	    	
	    	PageReference pref = controller.bean.accountChanged();
	    	System.assertEquals(pref, null);
	    	
	    	pref = controller.bean.accountLookedUp();
	    	System.assertEquals(pref, null);
	    	
	    	pref = controller.convertLead();	
	    	System.assertNotEquals(pref, null);	    	
	    	System.assertEquals(pref.getURL(), '/apex/mergeContactPage');
	    	
	    	controller.bean.selectedContactId = 'NONE';
	    	pref = controller.mergeContact();	
	    	System.assertEquals(pref, null);	  
	    	
	    	controller.bean.selectedContactId = 'NEW';
	    	controller.bean.overwriteLeadSource = false;
	    	pref = controller.mergeContact();	
	    	System.assertNotEquals(pref, null);
	    		    	
    	Test.stopTest();
    }
    
    @isTest 
    static void mergeContactTest_2() {
    	// Prepare Data
    	User userObj = getRunningUser();    	
    	System.assertNotEquals(userObj, null);
    	
    	// Create Account
    	Account accountObj = createAccount(userObj);
    	insert accountObj;
    	System.assertNotEquals(accountObj, null);
    	
    	// Create Contact
    	Contact contactObj = createContact(accountObj);
    	insert contactObj;
    	System.assertNotEquals(contactObj, null);
    	
    	// Create Account
    	Lead leadObj = createLead(userObj);
    	insert leadObj;
    	System.assertNotEquals(leadObj, null);
    	
    	// Create a task
    	Task taskObj = createTask(leadObj.Id);
    	insert taskObj;
    	
    	Id [] fixedSearchResults= new Id[1];
       	fixedSearchResults[0] = accountObj.Id;
       	Test.setFixedSearchResults(fixedSearchResults);
    	
    	// Test Scenario
    	Test.startTest();
	    	EP_CRM_LeadConvertController controller = new EP_CRM_LeadConvertController(new ApexPages.StandardController(leadObj));
	    	
	    	System.assertEquals(controller.bean.accounts.size(), 3);
	    	
	    	// set page values before converting lead
	    	controller.bean.selectedAccountId = controller.bean.accounts[2].getValue();
	    	controller.bean.doNotCreateOppty = false;
	    	controller.bean.sendOwnerEmail = false;
	    	controller.bean.leadToConvert.Status = 'Converted';
	    	
	    	PageReference pref = controller.convertLead();	
	    	System.assertNotEquals(pref, null);	    	
	    	System.assertEquals(pref.getURL(), '/apex/mergeContactPage');	   
	    	
	    	String accountName = controller.bean.getSelectedAccountName();
	    	System.assertEquals(accountName, 'Test Company'); 		    
	    	
	    	List < SelectOption > options = controller.bean.getContacts();
	    	System.assertEquals(options.size(), 3);
	    	
	    	controller.bean.selectedContactId = options.get(2).getValue();
	    	controller.bean.overwriteLeadSource = false;
	    	pref = controller.mergeContact();	
	    	System.assertNotEquals(pref, null);	    		    	
    	Test.stopTest();
    }
    
    @isTest 
    static void userPermissionTest_1() {
    	// Prepare Data
    	Profile profileObj = [SELECT Id, Name FROM Profile WHERE Name = 'Read Only' limit 1];
    	System.assertNotEquals(profileObj, null);
    	
    	User userObj = getRunningUser();  
    	System.assertNotEquals(userObj, null);
    	
    	Lead leadObj = createLead(userObj);
    	insert leadObj;
    	System.assertNotEquals(leadObj, null);
    		
    	// Create a task
    	Task taskObj = createTask(leadObj.Id);
    	insert taskObj;
    	
    	userObj = createUser('Read Only');
    	insert userObj;
    	System.assertNotEquals(userObj, null);
    	
    	System.runAs(userObj) {
    		// Test Scenario
	    	Test.startTest();
		    	EP_CRM_LeadConvertController controller = new EP_CRM_LeadConvertController(new ApexPages.StandardController(leadObj));    	
	    	Test.stopTest();
    	}
    }
    
    static User getRunningUser() {
    	return [SELECT Id, Name, EP_CRM_Geo1__c , EP_CRM_Geo2__c, ProfileId FROM User WHERE Id =: UserInfo.getUserId() limit 1]; 
    }
    
    static Lead createLead(User userObj){        
        Lead leadObj = new Lead();    
        leadObj.FirstName = 'Test FirstName' + System.now().millisecond();
        leadObj.LastName = 'Test LastName';
        leadObj.Email = 'sample@test.com';
        leadObj.Title = 'Test Title';
        leadObj.Company = 'Test Company';       
        leadObj.State = 'Test state';
        leadObj.City = 'Test city';
        leadObj.Street = 'Test street';
        leadObj.Fax = 'Test fax';
        leadObj.EP_Services_Interested_In__c = 'TBD';
        leadObj.EP_Company_Tax_Number__c = 'TestN';
        leadObj.EP_Indicative_Order_Frequency__c = 'TBD';
        leadObj.EP_Requested_Payment_Terms__c = 'Prepayment';      
        leadObj.EP_Requested_Payment_Method__c = 'Direct Debit';
        leadObj.EP_IBAN__c = 'Test IBAN'; 
        leadObj.EP_Bank_Name__c = 'Test Name'; 
        leadObj.EP_Preferred_Mode_of_Communication__c = 'Email';
        leadObj.Phone = '987654321'; 
        leadObj.EP_Preferred_Time_of_Communication__c = 'Morning'; 
        leadObj.EP_Products_Interested_In__c = 'Fuel';
        leadObj.EP_Indicative_Fuel_Volume__c = 45.00;
        leadObj.EP_Indicative_Lubes_Volume__c = 32.00;
        leadObj.PostalCode = '12323';
        leadObj.country = 'US';
        leadObj.Status = 'Open';
        leadObj.EP_CRM_Geo1__c = userObj.EP_CRM_Geo1__c;
        leadObj.EP_CRM_Geo2__c = userObj.EP_CRM_Geo2__c;
        return leadObj;
    }
    
    static Task createTask(Id whatId) {
    	Task taskObj = new Task();
    	taskObj.ActivityDate = Date.today().addDays(7);
    	taskObj.Subject='Sample Task';
	    taskObj.WhoId = whatId;    
	    taskObj.OwnerId = UserInfo.getUserId();
	    taskObj.Status='In Progress';
	    taskObj.Type = 'Call';
		return taskObj;
    }
    
    static Account createAccount(User userObj){
    	Id SELL_TO_RT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sell To').getRecordTypeId();
        Account sellToAcc = new Account( 
                                Name = 'Test Company',
                                EP_Status__c = '01-Prospect', 
                                RecordTypeId = SELL_TO_RT, 
                                BillingStreet = '123 st',
                                BillingCity = 'Gurgaon',
                                BillingState = 'Haryana',
                                BillingPostalCode = '123456',
                                BillingCountry = 'India',
								EP_CRM_Geo1__c = userObj.EP_CRM_Geo1__c,
        						EP_CRM_Geo2__c = userObj.EP_CRM_Geo2__c
                            );
        return sellToAcc;
    }
    
    static Contact createContact(Account accountObj) {
        Contact contactObj = new Contact(
        	AccountID= accountObj.Id, 
        	LastName = 'Test LastName', 
        	CurrencyIsoCode = 'GBP', 
        	Email = 'test@xyz.com'
        );            
        return contactObj;
    }
    
    static User createUser(String ProfileName) {
       	Profile profileObj = [SELECT Id FROM Profile WHERE Name = :ProfileName]; 
        User userObj = new User(
        	Alias = 'standt', 
        	Email = 'standarduser@pumaEne.com', 
            EmailEncodingKey = 'UTF-8', 
            LastName = 'Testing', 
            LanguageLocaleKey = 'en_US', 
            LocaleSidKey = 'en_US', 
            ProfileId = profileObj.Id, 
            TimeZoneSidKey = 'Asia/Kolkata', 
            UserName = 'standarduser@pumaEne.com',
            EP_User_UTC_Offset__c = 5.50, 
            EP_CRM_Geo1__c = 'Africa',
			EP_CRM_Geo2__c = 'Ghana'
        );
        return userObj;
    }
}