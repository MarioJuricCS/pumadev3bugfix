<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>EP_OM001_Send_SMS_Message_R1</fullName>
        <apiVersion>38.0</apiVersion>
        <description>This outbound message is used to push the SMS notifications to the Twilio endpoint</description>
        <endpointUrl>https://weepumasitemgmntchannelsalesforceservicedev01.azurewebsites.net:443/TankDipReading.svc</endpointUrl>
        <fields>EP_Mobile_Phone__c</fields>
        <fields>EP_SMS_Message__c</fields>
        <fields>Id</fields>
        <fields>Name</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>apiuser@epuma.com</integrationUser>
        <name>EP-OM001 Send SMS Message-R1</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>EP_MissedTankDipSMSNotification</fullName>
        <actions>
            <name>EP_OM001_Send_SMS_Message_R1</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EP_SMS_Messages__c.EP_SMS_Message__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
