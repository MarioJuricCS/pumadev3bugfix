/*
   @Author <Vinay Jaiswal>
   @name <EP_InvoiceTriggerHelper>
   @Description <This class handles requests from EP_InvoiceTriggerHandler class>
   @Version <1.0>
*/
public with sharing class EP_InvoiceTriggerHelper {
   	/*
      This method is used to validate invoice for Prepayment
   	*/ 
   	public static void validateInvoice(List<EP_Invoice__c> lNewInvoice){
       for (EP_Invoice__c oInvoice : lNewInvoice) {
            if(oInvoice.EP_Type__c <> NULL && oInvoice.EP_Type__c.equalsIgnoreCase(EP_Common_Constant.PREPAYMENT_INVOICE) ){
                oInvoice.EP_Is_Proforma_Invoice__c =  true;
                oInvoice.EP_Invoice_Type__c = EP_Common_Constant.Proforma;
            }
       	}
	}
}