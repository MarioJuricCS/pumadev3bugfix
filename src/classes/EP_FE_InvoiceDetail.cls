/* 
   @Author <Alper Genc>
   @name <EP_FE_InvoiceDetail >
   @CreateDate <03/06/2016>
   @Description <This class >  
   @Version <1.0>
*/
global with sharing class EP_FE_InvoiceDetail {
    public EP_Invoice__c record {get; set;}
    public List<csord__Order__c> relatedOrders {get; set;}
    public String pdfLink {get; set;}

    // Map between the orders and the related invoice line items, returning the actual invoiced items
    public Map<String, List<EP_Invoice_Line_Item__c>> relatedActualOrderItems {get; set;}
/* 
   @Author <Alper Genc>
   @name <EP_FE_InvoiceDetail >
   @CreateDate <03/06/2016>
   @Description <This class >  
   @Version <1.0>
*/    
    public EP_FE_InvoiceDetail() {
        relatedOrders = new List<csord__Order__c>();
    }
}