/***L4-45352 start****/
/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <EP_ProductListExtensionHelper_UT >                            *
*  @Description <Test Class to cover EP_ProductListExtension, EP_ProductListExtensionContext, EP_ProductListExtensionHelper> 
*  @Version <1.0>                                              *
****************************************************************/
@isTest
public class EP_ProductListExtensionHelper_UT {
private static Pricebook2 priceBookObj;
private static Product2 product1;
private static Product2 product2;
private static PricebookEntry priceBookEntry1;
private static PricebookEntry priceBookEntry2;
private static EP_Product_Option__c productOption1;
private static Account SellTo;
private static Account ShipTo;
private static EP_Tank__c VarTank;//EP_Action__c
private static EP_Action__c VarAction;//
private static Account VarstorageLocationAccount;
private static EP_Stock_Holding_Location__c VarstorageLocation;
private static String COUNTRY_NAME = 'Australia';
private static String COUNTRY_CODE = 'AU';
private static String COUNTRY_REGION = 'Australia';

static void dataSetup()
{
    priceBookObj = EP_TestDataUtility.createPricebook2(true);
    product1 = EP_TestDataUtility.createProduct2(false);
    product2 = EP_TestDataUtility.createProduct2(false);
    product1 .EP_Is_System_Product__c = true;
    product2 .EP_Is_System_Product__c = true;
    Insert product2 ;
    Insert product1 ;
    priceBookEntry1 = EP_TestDataUtility.createPricebookEntry(product1.Id, priceBookObj.id);
    priceBookEntry2 = EP_TestDataUtility.createPricebookEntry(product2.Id, priceBookObj.id);
    
    SellTo = EP_TestDataUtility.createSellToAccount(NULL, NULL);
    insert SellTo; 
    ShipTo = EP_TestDataUtility.createShipToAccount(SellTo.Id, null);
    insert ShipTo;
    BusinessHours bhrs= [Select Name from BusinessHours where IsActive =true AND IsDefault =true LIMIT 1];
        
    EP_Country__c country1 = EP_TestDataUtility.createCountryRecord( COUNTRY_NAME, COUNTRY_CODE, COUNTRY_REGION);
    Database.insert(country1); 
    VarTank = EP_TestDataUtility.createTestRecordsForTank(null,'tank',ShipTo.id,product1.id);
    
    productOption1 = EP_TestDataUtility.createProductOption(SellTo.Id, priceBookObj.id);
    priceBookEntry1.EP_Is_Sell_To_Assigned__c = true;
    priceBookEntry1.IsActive = false;
    insert priceBookEntry1;
    priceBookEntry2.EP_Is_Sell_To_Assigned__c = true;
    priceBookEntry2.IsActive = false;
    insert priceBookEntry2;
    VarAction = EP_TestDataUtility.createPriceBookAction(priceBookObj.id);
    VarstorageLocationAccount = EP_TestDataUtility.createStorageLocAccount(country1.id,bhrs.id);
    insert VarstorageLocationAccount;
    ID sellToSHLRTID= EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', 'Sell-To Supply Location');
    VarstorageLocation = EP_TestDataUtility.createSellToStockLocation(SellTo.id,false,VarstorageLocationAccount.id,sellToSHLRTID);
    insert VarstorageLocation; 
}

/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <DeletedProduct_test>                            *
*  @Description <This method covers  DeletedProduct method deletion scenario> 
*  @Version <1.0>                                              *
****************************************************************/     
    static testMethod void DeletedProduct_test(){
        dataSetup();
        Pagereference pageRef = Page.EP_ProductList;
        Test.setCurrentPage(pageRef);
        EP_ProductListExtension controller = new EP_ProductListExtension(new ApexPages.StandardController(priceBookObj));
        EP_ProductListExtensionContext localObjcontext = new EP_ProductListExtensionContext(priceBookObj);
        EP_ProductListExtensionHelper localObj = new EP_ProductListExtensionHelper(localObjcontext);
        Test.startTest();
            SellTo.EP_Status__c = '02-Basic Data Setup';
            update SellTo;
            localObjcontext.PriceBookdelId = '0';
            localObjcontext.PriceBookId = priceBookObj.id;
            
            localObjcontext.ListProd = localObjcontext.PriceBookEntryMapper.getALLRecordsByPriceBookId(localObjcontext.PriceBookId);
            controller.ctx=localObjcontext;
            //localObj.DeleteProducts(localObjcontext.RowNum);
            controller.DeleteProducts();            
        Test.stoptest();
        System.AssertEquals('0',localObjcontext.PriceBookdelId);       
    }
/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <DeletedProduct_test>                            *
*  @Description <This method covers  DeletedProduct method error scenario> 
*  @Version <1.0>                                              *
****************************************************************/  
    static testMethod void DeletedProductError_test(){
        dataSetup();
        Pagereference pageRef = Page.EP_ProductList;
        Test.setCurrentPage(pageRef);
        EP_ProductListExtension controller = new EP_ProductListExtension(new ApexPages.StandardController(priceBookObj));
        EP_ProductListExtensionContext localObjcontext = new EP_ProductListExtensionContext(priceBookObj);
        EP_ProductListExtensionHelper localObj = new EP_ProductListExtensionHelper(localObjcontext);
        Test.startTest();
           
            localObjcontext.PriceBookdelId = '0';
            localObjcontext.PriceBookId = priceBookObj.id;
            
            localObjcontext.ListProd = localObjcontext.PriceBookEntryMapper.getALLRecordsByPriceBookId(localObjcontext.PriceBookId);
            controller.ctx=localObjcontext;
            //localObj.DeleteProducts(localObjcontext.RowNum);  
            controller.DeleteProducts();          
        Test.stoptest(); 
        System.AssertEquals('0',localObjcontext.PriceBookdelId);      
    }
    
/***************************************************************
*  @Author <Accenture>                                         *
*  @Name <CreatePriceBookReview_test>                            *
*  @Description <This method covers  CreatePriceBookReview method > 
*  @Version <1.0>                                              *
****************************************************************/    
    static testMethod void CreatePriceBookReview_test() {
        dataSetup();
        Pagereference pageRef = Page.EP_ProductList;
        Test.setCurrentPage(pageRef);
        EP_ProductListExtension controller = new EP_ProductListExtension(new ApexPages.StandardController(priceBookObj));
        EP_ProductListExtensionContext localObjcontext = new EP_ProductListExtensionContext(priceBookObj);
        EP_ProductListExtensionHelper localObj = new EP_ProductListExtensionHelper(localObjcontext);
        Test.startTest();
            SellTo.EP_Status__c = '02-Basic Data Setup';
            update SellTo;
            VarAction.EP_Status__c = '03-Completed';
            VarAction.EP_Parent_Owner_Email__c='test@gmail.com';
            update VarAction;
            localObjcontext.PriceBookId = priceBookObj.id;
            localObjcontext.ListProd = localObjcontext.PriceBookEntryMapper.getALLRecordsByPriceBookId(localObjcontext.PriceBookId);           
            controller.CreatePriceBookReview();
        Test.stoptest();
        System.AssertNotEquals(null,SellTo.id);        
    }
}