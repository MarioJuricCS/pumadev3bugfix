/**
 * @author <Sandeep Kumar>
 * @name <EP_SupplyLocationTransprtrTriggerHandler>
 * @createDate <06/11/2016>
 * @description
 * @version <1.0>
 */
@
isTest
public class EP_SupplyLocTransprtrTriggerHandlerTest {

    private static Account testShipToAccount;
    private static Account testSellToAccount;
    private static Company__c testCompany;
    private static Account testTransporterAccount;
    private static Account testTransporterAccount2;
    private static EP_Supply_Location_Transporter__c testSupplyLocationTransporter;
    private static final string ERROR_MESSAGE = 'Please ensure atleast one default transporter is associated with this Supply Location';


    // Const
    private static final String TRANSPORTER_TYPE = 'Transporter';
    private static String COUNTRY_NAME = 'Australia';
    private static String COUNTRY_CODE = 'AU';
    private static String COUNTRY_REGION = 'Australia';
    private static String REGION_NAME = 'North-Australia';


    /*********************************************************************************************
     *@Description : This method valids doBeforeInsert method of EP_SupplyLocationTransprtrTriggerHandler class
     *@Params      :                    
     *@Return      : Void                                                                             
     *********************************************************************************************/
    private static testMethod void testDoBeforeInsert() {

        System.runAs(EP_TestDataUtility.SYSTEM_ADMIN_USER) {
            Test.startTest();
            testCompany = EP_TestDataUtility.createCompany('AUN');
            testCompany.EP_Disallow_DD_for_PP_customers__c = TRUE;
            Database.insert(testCompany);

            EP_Country__c testCountry =
                EP_TestDataUtility.createCountryRecord(
                    COUNTRY_NAME,
                    COUNTRY_CODE,
                    COUNTRY_REGION);
            Database.insert(testCountry);

            EP_Region__c testRegion = EP_TestDataUtility.createCountryRegion(REGION_NAME, testCountry.Id);
            Database.insert(testRegion);
            //EP_AccountTriggerHandler.isExecuteAfterInsert = true;
            //EP_AccountTriggerHandler.isExecuteBeforeInsert = true;
            EP_UserTriggerHandler_R1.isExecuteBeforeInsert = false;
            EP_UserTriggerHandler_R1.isExecuteAfterInsert = false;
            EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy =
                EP_TestDataUtility.createCustomerHierarchyForNAV(1);    testSellToAccount = testAccountHierarchy.lCustomerAccounts[0];
            testShipToAccount = testAccountHierarchy.lShipToAccounts[0];

            BusinessHours testBusinessHours = [SELECT Name FROM BusinessHours
                WHERE IsActive = TRUE
                AND IsDefault = TRUE
                LIMIT: EP_Common_Constant.ONE
            ];

            Account testStorageLocation = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);

            Database.insert(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            Database.update(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(testStorageLocation);

            // Create a supply location

            Id strSupplyLocationSellToRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    EP_Stock_Holding_Location__c testSupplyOption1 =
                EP_TestDataUtility.createSellToStockLocation(
                    testSellToAccount.Id,
                    TRUE,
                    testStorageLocation.Id,
                    strSupplyLocationSellToRecordTypeId);
            testSupplyOption1.EP_Trip_Duration__c = 1;
            testSupplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption1.EP_Is_Pickup_Enabled__c = True;
            testSupplyOption1.EP_Duty__c = 'Excise Free';
            Database.insert(testSupplyOption1);
            Test.stopTest();
            testTransporterAccount =
                EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE,
                    '123456NAV',
                    testCompany.Id);
            Database.insert(testTransporterAccount);

            Id strSupplyLocationDeliveryRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.DLVRY_REC_TYPE_NAME);
            EP_Stock_Holding_Location__c testSupplyOption2 =
                EP_TestDataUtility.createShipToStockLocation(
                    testShipToAccount.Id,
                    TRUE,
                    testStorageLocation.Id,
                    strSupplyLocationDeliveryRecordTypeId);
            testSupplyOption2.EP_Trip_Duration__c = 1;
            testSupplyOption2.EP_Ship_To__c = testShipToAccount.Id;
            testSupplyOption2.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption2.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption2.EP_Is_Pickup_Enabled__c = TRUE;
            testSupplyOption2.EP_Duty__c = 'Excise Free';
            testSupplyOption2.EP_Transporter__c = testTransporterAccount.Id;
            Database.insert(testSupplyOption2);

            // Create a separate transporter
            testTransporterAccount2 =
                EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE,
                    '789NAV',
                    testCompany.Id);
            Database.insert(testTransporterAccount2);

            testSupplyLocationTransporter =
                EP_TestDataUtility.createSupplyLocationTransporter(
                    testTransporterAccount.Id,
                    testSupplyOption2.Id,
                    TRUE);
            Database.insert(testSupplyLocationTransporter);

            update testSupplyLocationTransporter;    
            try {
                delete testSupplyLocationTransporter;
            } catch (Exception e) {
                System.debug('++++++++++++++++' + e);
                Boolean expectedExceptionThrown = e.getMessage().contains(ERROR_MESSAGE) ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }

        }
    }

    /*********************************************************************************************
     *@Description : This method valids doBeforeUpdate method of EP_SupplyLocationTransprtrTriggerHandler class
     *@Params      :                    
     *@Return      : Void                                                                             
     *********************************************************************************************/
    private static testMethod void testDoBeforeUpdate() {

        System.runAs(EP_TestDataUtility.SYSTEM_ADMIN_USER) {
            testCompany = EP_TestDataUtility.createCompany('AUN');
            testCompany.EP_Disallow_DD_for_PP_customers__c = TRUE;
            Database.insert(testCompany);

            EP_Country__c testCountry =
                EP_TestDataUtility.createCountryRecord(
                    COUNTRY_NAME,
                    COUNTRY_CODE,
                    COUNTRY_REGION);
            Database.insert(testCountry);

            EP_Region__c testRegion = EP_TestDataUtility.createCountryRegion(REGION_NAME, testCountry.Id);
            Database.insert(testRegion);
            //EP_AccountTriggerHandler.isExecuteAfterInsert = true;
            //EP_AccountTriggerHandler.isExecuteBeforeInsert = true;
            EP_UserTriggerHandler_R1.isExecuteBeforeInsert = false;
            EP_UserTriggerHandler_R1.isExecuteAfterInsert = false;
            EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy =
                EP_TestDataUtility.createCustomerHierarchyForNAV(1);    testSellToAccount = testAccountHierarchy.lCustomerAccounts[0];
            testShipToAccount = testAccountHierarchy.lShipToAccounts[0];

            BusinessHours testBusinessHours = [SELECT Name FROM BusinessHours
                WHERE IsActive = TRUE
                AND IsDefault = TRUE
                LIMIT: EP_Common_Constant.ONE
            ];

            Account testStorageLocation = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);

            Database.insert(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            Database.update(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(testStorageLocation);

            // Create a supply location

            Id strSupplyLocationSellToRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    EP_Stock_Holding_Location__c testSupplyOption1 =
                EP_TestDataUtility.createSellToStockLocation(
                    testSellToAccount.Id,
                    TRUE,
                    testStorageLocation.Id,
                    strSupplyLocationSellToRecordTypeId);
            testSupplyOption1.EP_Trip_Duration__c = 1;
            testSupplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption1.EP_Is_Pickup_Enabled__c = True;
            testSupplyOption1.EP_Duty__c = 'Excise Free';
            Database.insert(testSupplyOption1);

            testTransporterAccount =
                EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE,
                    '123456NAV',
                    testCompany.Id);
            Database.insert(testTransporterAccount);

            Id strSupplyLocationDeliveryRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.DLVRY_REC_TYPE_NAME);
            EP_Stock_Holding_Location__c testSupplyOption2 =
                EP_TestDataUtility.createShipToStockLocation(
                    testShipToAccount.Id,
                    TRUE,
                    testStorageLocation.Id,
                    strSupplyLocationDeliveryRecordTypeId);
            testSupplyOption2.EP_Trip_Duration__c = 1;
            testSupplyOption2.EP_Ship_To__c = testShipToAccount.Id;
            testSupplyOption2.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption2.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption2.EP_Is_Pickup_Enabled__c = TRUE;
            testSupplyOption2.EP_Duty__c = 'Excise Free';
            testSupplyOption2.EP_Transporter__c = testTransporterAccount.Id;
            Database.insert(testSupplyOption2);

            // Create a separate transporter
            testTransporterAccount2 =
                EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE,
                    '789NAV',
                    testCompany.Id);
            Database.insert(testTransporterAccount2);

            testSupplyLocationTransporter =
                EP_TestDataUtility.createSupplyLocationTransporter(
                    testTransporterAccount.Id,
                    testSupplyOption2.Id,
                    TRUE);
            Database.insert(testSupplyLocationTransporter);

            update testSupplyLocationTransporter;

            try {

                testSupplyLocationTransporter.EP_Is_Default__c = false;
                update testSupplyLocationTransporter;
            } catch (Exception e) {
                System.debug('++++++++++++++++' + e);
                Boolean expectedExceptionThrown = e.getMessage().contains(ERROR_MESSAGE) ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
            Test.startTest();
            Account testTransporterAccount3 =
                EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE,
                    '790NAV',
                    testCompany.Id);
            Database.insert(testTransporterAccount3);
            List < EP_Supply_Location_Transporter__c > testSupplyLocationTrasprtList =
                new List < EP_Supply_Location_Transporter__c > ();
            testSupplyLocationTrasprtList.add(EP_TestDataUtility.createSupplyLocationTransporter(
                testTransporterAccount2.Id,
                testSupplyOption2.Id,
                FALSE));
            testSupplyLocationTrasprtList.add(EP_TestDataUtility.createSupplyLocationTransporter(
                testTransporterAccount3.Id,
                testSupplyOption2.Id,
                FALSE));
            Database.insert(testSupplyLocationTrasprtList);

            testSupplyLocationTrasprtList[0].EP_Is_Default__c = true;
            testSupplyLocationTrasprtList[1].EP_Is_Default__c = true;
            update testSupplyLocationTrasprtList;}

            Test.stopTest();
    }

    /*********************************************************************************************
     *@Description : This method valids doBeforeDelete method of EP_SupplyLocationTransprtrTriggerHandler class
     *@Params      :                    
     *@Return      : Void                                                                             
     *********************************************************************************************/
    private static testMethod void testDoBeforeInsertdefaultFalse() {
        
        System.runAs(EP_TestDataUtility.SYSTEM_ADMIN_USER) {
            testCompany = EP_TestDataUtility.createCompany('AUN');
            testCompany.EP_Disallow_DD_for_PP_customers__c = TRUE;
            Database.insert(testCompany);

            EP_Country__c testCountry =
                EP_TestDataUtility.createCountryRecord(
                    COUNTRY_NAME,
                    COUNTRY_CODE,
                    COUNTRY_REGION);
            Database.insert(testCountry);

            EP_Region__c testRegion = EP_TestDataUtility.createCountryRegion(REGION_NAME, testCountry.Id);
            Database.insert(testRegion);
            //EP_AccountTriggerHandler.isExecuteAfterInsert = true;
            //EP_AccountTriggerHandler.isExecuteBeforeInsert = true;
            EP_UserTriggerHandler_R1.isExecuteBeforeInsert = false;
            EP_UserTriggerHandler_R1.isExecuteAfterInsert = false;
            EP_TestDataUtility.WrapperCustomerHierarchy testAccountHierarchy =
                EP_TestDataUtility.createCustomerHierarchyForNAV(1);    testSellToAccount = testAccountHierarchy.lCustomerAccounts[0];
            testShipToAccount = testAccountHierarchy.lShipToAccounts[0];

            BusinessHours testBusinessHours = [SELECT Name FROM BusinessHours
                WHERE IsActive = TRUE
                AND IsDefault = TRUE
                LIMIT: EP_Common_Constant.ONE
            ];

            Account testStorageLocation = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);

            Database.insert(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
            Database.update(testStorageLocation);
            testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(testStorageLocation);

            // Create a supply location

            Id strSupplyLocationSellToRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    EP_Stock_Holding_Location__c testSupplyOption1 =
                EP_TestDataUtility.createSellToStockLocation(
                    testSellToAccount.Id,
                    TRUE,
                    testStorageLocation.Id,
                    strSupplyLocationSellToRecordTypeId);
            testSupplyOption1.EP_Trip_Duration__c = 1;
            testSupplyOption1.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption1.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption1.EP_Is_Pickup_Enabled__c = True;
            testSupplyOption1.EP_Duty__c = 'Excise Free';
            Database.insert(testSupplyOption1);
            Test.startTest();
            testTransporterAccount =
                EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE,
                    '123456NAV',
                    testCompany.Id);
            Database.insert(testTransporterAccount);

            Id strSupplyLocationDeliveryRecordTypeId =
                EP_Common_Util.fetchRecordTypeId(
                    'EP_Stock_Holding_Location__c',
                    EP_Common_Constant.DLVRY_REC_TYPE_NAME);
            EP_Stock_Holding_Location__c testSupplyOption2 =
                EP_TestDataUtility.createShipToStockLocation(
                    testShipToAccount.Id,
                    TRUE,
                    testStorageLocation.Id,
                    strSupplyLocationDeliveryRecordTypeId);
            testSupplyOption2.EP_Trip_Duration__c = 1;
            testSupplyOption2.EP_Ship_To__c = testShipToAccount.Id;
            testSupplyOption2.EP_Supplier_Contract_Advice__c = EP_Common_Constant.NONE;
            testSupplyOption2.EP_Use_Managed_Transport_Services__c = 'N/A';
            testSupplyOption2.EP_Is_Pickup_Enabled__c = TRUE;
            testSupplyOption2.EP_Duty__c = 'Excise Free';
            testSupplyOption2.EP_Transporter__c = testTransporterAccount.Id;
            Database.insert(testSupplyOption2);

            // Create a separate transporter
            testTransporterAccount2 =
                EP_TestDataUtility.createTestVendorWithoutCompany(TRANSPORTER_TYPE,
                    '789NAV',
                    testCompany.Id);
            Database.insert(testTransporterAccount2);

            try {
                testSupplyLocationTransporter =
                    EP_TestDataUtility.createSupplyLocationTransporter(
                        testTransporterAccount.Id,
                        testSupplyOption2.Id,
                        FALSE);
                Database.insert(testSupplyLocationTransporter);
                testSupplyLocationTransporter.EP_Is_Default__c = true;
                update testSupplyLocationTransporter;
            } catch (Exception e) {
                System.debug('++++++++++++++++' + e);
                Boolean expectedExceptionThrown = e.getMessage().contains(ERROR_MESSAGE) ? true : false;
            }
            Test.stopTest();
        }
    }

    /*********************************************************************************************
     *@Description : This method valid for EP_UserTriggerHandler_R1 and EP_ProductListTriggerHandler class
     *@Params      :                    
     *@Return      : Void                                                                             
     *********************************************************************************************/
    private static testMethod void EP_UserTriggerHandlerMethod() {
        List < User > lstUser = new List < User > ();
        User usrInsert = EP_TestDataUtility.createRunAsUser();
        Database.insert(usrInsert);

        lstUser.add(usrInsert);
        EP_UserTriggerHandler_R1.doAfterInsert(lstUser);

        usrInsert.LastName = 'TESTREC';
        Database.update(usrInsert);
        Test.startTest();
        EP_UserTriggerHandler_R1.doAfterUpdate(lstUser);

        List < Pricebook2 > lstPricebook2 = new List < Pricebook2 > ();
        Pricebook2 customPricebook = new Pricebook2(Name = 'Custom Price Book', isActive = TRUE);
        Pricebook2 customPB = new Pricebook2(Name = 'PriceBook A', isActive = true);
        lstPricebook2.add(customPricebook);
        lstPricebook2.add(customPB);
        Database.insert(lstPricebook2);
        Database.update(lstPricebook2);
        Test.stopTest();
    }
}