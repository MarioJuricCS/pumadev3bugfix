<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>To store association of Supply Location and Transporters (Vendors)</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>EP_Is_Default__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Whether the associated Transporter is default for the associated Supply location</description>
        <externalId>false</externalId>
        <label>Is Default?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>EP_Supply_Location_Transporter_Key__c</fullName>
        <caseSensitive>false</caseSensitive>
        <description>Store key of Transporter and Supply Location combination</description>
        <externalId>false</externalId>
        <label>Transporter Already Exists</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>EP_Supply_Location__c</fullName>
        <description>stores Supply Location</description>
        <externalId>false</externalId>
        <label>Supply Location</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>EP_Stock_Holding_Location__c.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>Delivery_Supply_Location</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>EP_Stock_Holding_Location__c</referenceTo>
        <relationshipLabel>Supply Location Transporters</relationshipLabel>
        <relationshipName>EP_Supply_Location_Transporters</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>EP_Transporter__c</fullName>
        <description>Stores Transporter (Vendor Account)</description>
        <externalId>false</externalId>
        <label>Transporter</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordType.DeveloperName</field>
                <operation>equals</operation>
                <value>EP_Vendor</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Supply Location Transporters</relationshipLabel>
        <relationshipName>EP_Supply_Location_Transporters</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <label>Supply Location Transporter</label>
    <listViews>
        <fullName>ALL</fullName>
        <columns>NAME</columns>
        <columns>EP_Supply_Location__c</columns>
        <columns>EP_Transporter__c</columns>
        <columns>EP_Is_Default__c</columns>
        <columns>EP_Supply_Location_Transporter_Key__c</columns>
        <filterScope>Everything</filterScope>
        <label>ALL</label>
    </listViews>
    <listViews>
        <fullName>All1</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>SLT-{0000000}</displayFormat>
        <label>Supply Location Transporter Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Supply Location Transporters</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>EP_Add_Blocked_Transporter_as_Default</fullName>
        <active>true</active>
        <description>Can not add blocked transporter as default</description>
        <errorConditionFormula>AND(
EP_Is_Default__c,
OR(
ISNEW(),
NOT(PRIORVALUE(EP_Is_Default__c))
),
ISPICKVAL(EP_Transporter__r.EP_Status__c,&apos;06-Blocked&apos;)
)</errorConditionFormula>
        <errorDisplayField>EP_Is_Default__c</errorDisplayField>
        <errorMessage>Blocked transporter can not be default for supply location</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>EP_Transporter_coordinator_can_modify</fullName>
        <active>true</active>
        <description>Only Admin or Transport Co-ordinator  can modify supply Location transporter</description>
        <errorConditionFormula>NOT(OR(
$Profile.Name = &apos;System Administrator&apos;,
$Profile.Name = &apos;EP - Interface User&apos;,
$Profile.Name = &apos;Puma Logistics Agent_R1&apos;
))</errorConditionFormula>
        <errorMessage>Only Admin or Transport Co-ordinator  can create/modify Supply Location transporter</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
