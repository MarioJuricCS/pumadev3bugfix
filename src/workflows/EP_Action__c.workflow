<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_BSM_GM_Review_Rejection_Notification_mail_to_Sell_To_owner</fullName>
        <description>BSM/GM Review Rejection Notification mail to Sell-To owner</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Parent_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Sell_To_BSM_GM_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>EP_Email_to_notify_Logistics_Rejection_for_Ship_to</fullName>
        <description>Email to notify Logistics Rejection for Ship to</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Parent_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Action_Shipto_Logistics_Rejection_Notification</template>
    </alerts>
    <alerts>
        <fullName>EP_Inventory_Review_Rejection_Notification</fullName>
        <description>Inventory Review Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Parent_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Inventory_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>EP_KYC_Review_Rejection_Notification_mail_to_Sell_To_owner</fullName>
        <description>KYC Review Rejection Notification mail to Sell-To owner</description>
        <protected>false</protected>
        <recipients>
            <field>EP_Parent_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Sell_To_KYC_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>EP_notify_action_owner_on_Creation</fullName>
        <description>notify action owner on creation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Review_Action_Assignment_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>EP_Update_Action_Status_Value</fullName>
        <field>EP_Status__c</field>
        <literalValue>02-Assigned</literalValue>
        <name>Update Action Status Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Update_Parent_Owner_Email</fullName>
        <field>EP_Parent_Owner_Email__c</field>
        <formula>IF(NOT(ISNULL(EP_Account__c)), EP_Account__r.Owner.Email , IF(NOT(ISNULL( EP_Tank__c)), EP_Tank__r.EP_Ship_To__r.Owner.Email, IF(NOT(ISNULL( EP_Tank__c)), EP_Bank_Account__r.EP_Account__r.Owner.Email, &apos;&apos;)))</formula>
        <name>EP_Update_Parent_Owner_Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EP_Change_Action_Status_Value</fullName>
        <actions>
            <name>EP_Update_Action_Status_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the action owner is changed action status value will get changed to assigned.</description>
        <formula>AND(ISCHANGED(OwnerId), LEFT(PRIORVALUE(OwnerId),3)= &apos;00G&apos;, LEFT(OwnerId,3)= &apos;005&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_Notify_Action_Owner</fullName>
        <actions>
            <name>EP_notify_action_owner_on_Creation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>EP_Populate_Action_Parent_Record_Owner_Email</fullName>
        <actions>
            <name>EP_Update_Parent_Owner_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EP_Action__c.EP_Status__c</field>
            <operation>notEqual</operation>
            <value>03-Completed,03-Rejected,03-Approved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
