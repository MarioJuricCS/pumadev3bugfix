/********* Test class for coverage of Order Pricing Trigger and Doc Util*****/

@isTest

public class EP_DocUtil_Test{
    
   private static Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('VMI Orders').getRecordTypeId();


   private static Id pricebookId = Test.getStandardPricebookId();

    
    //L4 - #45419, #45420, #45421 - Start
    private static testMethod void deleteAttachmentsByName_UnitTest() {
         System.runAs(EP_TestDataUtility.createRunAsUser()) {
            Account accObj = EP_TestDataUtility.getSellTo();
            Attachment attach = EP_AccountNotificationTestDataUtility.createAttachment(accObj.id);
            
            test.startTest();
                EP_DocumentUtil.deleteAttachmentsByName(new list<Attachment>{attach});
            test.stopTest();
            
            Integer attchCount = [SELECT count() FROM Attachment];
            System.assertEquals(0, attchCount);
        }
    }
    //L4 - #45419, #45420, #45421 - END

    private static testMethod void fillDocumentsWrapper_Test(){
        Map<String, Object> mapDocumentTagInformations = new Map<String, Object>();
        EP_ManageDocument.DocumentMetaData docMeta = new EP_ManageDocument.DocumentMetaData();
        docMeta.documentTemplateName = 'Test';
        //mapDocumentTagInformations.put(EP_Common_Constant.DOCUMENT_META_DATA_TAG,docMeta);
        mapDocumentTagInformations.put(EP_Common_Constant.DOCUMENT_REF_ID_TAG,'Test');
        mapDocumentTagInformations.put(EP_Common_Constant.DOCUMENT_URL_TAG,'Test');
        mapDocumentTagInformations.put(EP_Common_Constant.DOCUMENT_FILE_NAME_TAG,'Test');
        mapDocumentTagInformations.put(EP_Common_Constant.DOCUMENT_EMBEDDED_TAG,'Test');
        mapDocumentTagInformations.put(EP_Common_Constant.DOCUMENT_UU_ID_TAG,'Test');
        Test.startTest();
        EP_ManageDocument.Document managedDocument = EP_DocumentUtil.fillDocumentsWrapper(mapDocumentTagInformations);
        Test.stopTest();
        System.assert(managedDocument!=null);
    }

    private static testMethod void fillDocumentMetaDataWrapper_Test(){
        Map<String, Object> mapDocumentMetaDataInformations = new Map<String, Object>();
        mapDocumentMetaDataInformations.put(EP_Common_Constant.DOCUMENT_TEMPLATE_NAME_TAG,'Test');
        mapDocumentMetaDataInformations.put(EP_Common_Constant.COLLATION_KEY_TAG,'Test');
        Test.startTest();
        EP_ManageDocument.DocumentMetaData managedDocument = EP_DocumentUtil.fillDocumentMetaDataWrapper(mapDocumentMetaDataInformations);
        Test.stopTest();
        System.assert(managedDocument!=null);
    }

    static testMethod void fillMetaDataWrapper_Test(){
        Test.startTest();
        Map<String, Object> mapMetaDataInformations = new Map<String, Object>();
        mapMetaDataInformations.put(EP_Common_Constant.NAME_TAG,EP_Common_Constant.NAME_TAG);
        mapMetaDataInformations.put(EP_Common_Constant.VALUE_TAG,EP_Common_Constant.VALUE_TAG);
        EP_ManageDocument.MetaData managedDocumentMetaData = EP_DocumentUtil.fillMetaDataWrapper(mapMetaDataInformations);
        Test.stopTest();
        System.assert(managedDocumentMetaData!=null);
        
    }

    static testMethod void fillCustomerInvoiceWrapper_Test(){
        set<String> metaDataName = new set<String>{EP_Common_Constant.INVOICE_NR_TAG,EP_Common_Constant.BILL_TO_TAG,
                                                    EP_Common_Constant.SHIP_TO_TAG,EP_Common_Constant.SEQ_ID_TAG,
                                                    EP_Common_Constant.INVOICE_DATE_TAG,EP_Common_Constant.INVOICE_DUE_DATE_TAG,
                                                    EP_Common_Constant.ISSUEDFROMID,EP_Common_Constant.ENTRYNR_TAG};
        EP_ManageDocument.MetaDatas managedDocumentMetaDatas = new EP_ManageDocument.MetaDatas();
        EP_ManageDocument.MetaData MetaDat;
        List<EP_ManageDocument.MetaData> metaDataList = new List<EP_ManageDocument.MetaData>();
        for(String str : metaDataName){
            MetaDat = new EP_ManageDocument.MetaData();
            MetaDat.name = str;
            MetaDat.value = str;
            metaDataList.add(MetaDat);
        }
        managedDocumentMetaDatas.metaData = metaDataList;
        Test.startTest();
        EP_ManageDocument.CustomerInvoiceWrapper managedDocCustomerInvoiceWraper = EP_DocumentUtil.fillCustomerInvoiceWrapper(managedDocumentMetaDatas);
        Test.stopTest();
        System.assert(managedDocCustomerInvoiceWraper!=null);
    }

    static testMethod void fillCustomerCreditMemoWrapper_Test(){
        set<String> metaDataName = new set<String>{EP_Common_Constant.CREDITMEMO_NR_TAG,EP_Common_Constant.SEQ_ID_TAG,
                                                    EP_Common_Constant.DOCDATE_TAG,EP_Common_Constant.CURRENCY_TAG,
                                                    EP_Common_Constant.ISSUED_TO_ID_TAG,EP_Common_Constant.ISSUED_TO_NAME_TAG,
                                                    EP_Common_Constant.ISSUEDFROMID,EP_Common_Constant.ENTRYNR_TAG};
        EP_ManageDocument.MetaDatas managedDocumentMetaDatas = new EP_ManageDocument.MetaDatas();
        List<EP_ManageDocument.MetaData> metaDataList = new List<EP_ManageDocument.MetaData>();
        EP_ManageDocument.MetaData MetaDat;
        for(String str : metaDataName){
            MetaDat = new EP_ManageDocument.MetaData();
            MetaDat.name = str;
            MetaDat.value = str;
            metaDataList.add(MetaDat);
        }
        managedDocumentMetaDatas.metaData = metaDataList;
        Test.startTest();
        EP_ManageDocument.CustomerCreditMemoWrapper managedDocCustomerInvoiceWraper = EP_DocumentUtil.fillCustomerCreditMemoWrapper(managedDocumentMetaDatas);
        Test.stopTest();
        System.assert(managedDocCustomerInvoiceWraper!=null);
    }

    static testMethod void fillCustomerAccountStatementWrapper_Test(){
        set<String> metaDataName = new set<String>{EP_Common_Constant.ISSUED_TO_ID_TAG,EP_Common_Constant.DOCDATE_TAG,
                                                    EP_Common_Constant.STATEMENT_START_DATE_TAG,EP_Common_Constant.STATEMENT_END_DATE_TAG,
                                                    EP_Common_Constant.SEQ_ID_TAG,EP_Common_Constant.ISSUED_TO_NAME_TAG,
                                                    EP_Common_Constant.TYPE,EP_Common_Constant.ISSUEDFROMID,
                                                    EP_Common_Constant.OPENINGBALANCE_TAG,EP_Common_Constant.CAS_TOTALCREDITS,
                                                    EP_Common_Constant.CAS_TOTALDEBITS,EP_Common_Constant.CLOSINGBALANCE_TAG,
                                                    EP_Common_Constant.TOTALORDERINPROGRESS,EP_Common_Constant.TOTALCURRENT,
                                                    EP_Common_Constant.TOTALOVERDUE,EP_Common_Constant.TOTALDUEAMOUNT,EP_Common_Constant.CAS_CURRENCYID};
        EP_ManageDocument.MetaDatas managedDocumentMetaDatas = new EP_ManageDocument.MetaDatas();
        List<EP_ManageDocument.MetaData> metaDataList = new List<EP_ManageDocument.MetaData>();
        EP_ManageDocument.MetaData MetaDat;
        for(String str : metaDataName){
            MetaDat = new EP_ManageDocument.MetaData();
            MetaDat.name = str;
            MetaDat.value = str;
            metaDataList.add(MetaDat);
        }
        managedDocumentMetaDatas.metaData = metaDataList;
        Test.startTest();
        EP_ManageAccountStatementDocument.CustomerAccountStatementWrapper managedDocCustomerInvoiceWraper = EP_DocumentUtil.fillCustomerAccountStatementWrapper(managedDocumentMetaDatas);
        Test.stopTest();
        System.assert(managedDocCustomerInvoiceWraper!=null);
    }

    static testMethod void fillCustomerDeliveryDocketWrapper_Test(){
        set<String> metaDataName = new set<String>{EP_Common_Constant.SHIPTOID_TAG,EP_Common_Constant.ISSUED_TO_ID_TAG,
                                                    EP_Common_Constant.CREDITMEMO_NR_TAG,EP_Common_Constant.LOADINGDATE_TAG,
                                                    EP_Common_Constant.TRIP_ID_TAG,EP_Common_Constant.CUST_ORDER_NR_TAG,
                                                    EP_Common_Constant.ISSUEDFROMID};
        EP_ManageDocument.MetaDatas managedDocumentMetaDatas = new EP_ManageDocument.MetaDatas();
        List<EP_ManageDocument.MetaData> metaDataList = new List<EP_ManageDocument.MetaData>();
        EP_ManageDocument.MetaData MetaDat;
        for(String str : metaDataName){
            MetaDat = new EP_ManageDocument.MetaData();
            MetaDat.name = str;
            MetaDat.value = str;
            metaDataList.add(MetaDat);
        }
        managedDocumentMetaDatas.metaData = metaDataList;
        Test.startTest();
        EP_ManageDocument.CustomerDeliveryDocketWrapper managedDocCustomerInvoiceWraper = EP_DocumentUtil.fillCustomerDeliveryDocketWrapper(managedDocumentMetaDatas);
        Test.stopTest();
        System.assert(managedDocCustomerInvoiceWraper!=null);
    }

    static testMethod void fillbolDocWrapper_Test(){
        set<String> metaDataName = new set<String>{EP_Common_Constant.BOL_DATE_TAG,EP_Common_Constant.TRIP_ID_TAG,
                                                    EP_Common_Constant.LOADING_ORDER_TAG,EP_Common_Constant.ISSUEDFROMID};
        EP_ManageDocument.MetaDatas managedDocumentMetaDatas = new EP_ManageDocument.MetaDatas();
        List<EP_ManageDocument.MetaData> metaDataList = new List<EP_ManageDocument.MetaData>();
        EP_ManageDocument.MetaData MetaDat;
        for(String str : metaDataName){
            MetaDat = new EP_ManageDocument.MetaData();
            MetaDat.name = str;
            MetaDat.value = str;
            metaDataList.add(MetaDat);
        }
        managedDocumentMetaDatas.metaData = metaDataList;
        Test.startTest();
        EP_ManageBOLDocument.bolDocWrapper managedDocCustomerInvoiceWraper = EP_DocumentUtil.fillbolDocWrapper(managedDocumentMetaDatas);
        Test.stopTest();
        System.assert(managedDocCustomerInvoiceWraper!=null);
    }

    static testMethod void fillDirectDebitAdviceWrapper_Test(){
        set<String> metaDataName = new set<String>{EP_Common_Constant.BILL_TO_TAG,EP_Common_Constant.SEQ_ID_TAG,
                                                    EP_Common_Constant.DOCDATE_TAG,EP_Common_Constant.PAYMENT_PROPOSAL_NO_TAG
                                                    ,EP_Common_Constant.PAYMENT_PROPOSAL_NO_TAG};
        EP_ManageDocument.MetaDatas managedDocumentMetaDatas = new EP_ManageDocument.MetaDatas();
        List<EP_ManageDocument.MetaData> metaDataList = new List<EP_ManageDocument.MetaData>();
        EP_ManageDocument.MetaData MetaDat;
        for(String str : metaDataName){
            MetaDat = new EP_ManageDocument.MetaData();
            MetaDat.name = str;
            MetaDat.value = str;
            metaDataList.add(MetaDat);
        }
        managedDocumentMetaDatas.metaData = metaDataList;
        Test.startTest();
        EP_ManageDirectDebitAdviceDocument.DirectDebitAdviceWrapper managedDocCustomerInvoiceWraper = EP_DocumentUtil.fillDirectDebitAdviceWrapper(managedDocumentMetaDatas);
        Test.stopTest();
        System.assert(managedDocCustomerInvoiceWraper!=null);
    }

    static testMethod void fillOrderConfirmationWrapper_Test(){
        set<String> metaDataName = new set<String>{EP_Common_Constant.SHIP_TO_TAG,EP_Common_Constant.SELL_TO_TAG,
                                                    EP_Common_Constant.DOCDATE_TAG,EP_Common_Constant.CUSTOMER_NAME_TAG
                                                    ,EP_Common_Constant.ISSUEDFROMID,EP_Common_Constant.DOCTITLE_TAG};
        EP_ManageDocument.MetaDatas managedDocumentMetaDatas = new EP_ManageDocument.MetaDatas();
        List<EP_ManageDocument.MetaData> metaDataList = new List<EP_ManageDocument.MetaData>();
        EP_ManageDocument.MetaData MetaDat;
        for(String str : metaDataName){
            MetaDat = new EP_ManageDocument.MetaData();
            MetaDat.name = str;
            MetaDat.value = str;
            metaDataList.add(MetaDat);
        }
        managedDocumentMetaDatas.metaData = metaDataList;
        Test.startTest();
        EP_ManageOrderConfirmationDocument.OrderConfirmationWrapper managedDocCustomerInvoiceWraper = EP_DocumentUtil.fillOrderConfirmationWrapper(managedDocumentMetaDatas);
        Test.stopTest();
        System.assert(managedDocCustomerInvoiceWraper!=null);
    }
}