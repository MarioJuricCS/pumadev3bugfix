<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_Notify_Bank_Account_Owner_for_KYC_Review_Completion</fullName>
        <description>Notify Bank Account Owner for KYC Review Completion</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/EP_Bank_Account_KYC_Completion_Notiification</template>
    </alerts>
</Workflow>
