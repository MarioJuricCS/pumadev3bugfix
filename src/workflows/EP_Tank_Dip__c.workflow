<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EP_EM001_Send_Reminder_Email</fullName>
        <description>EP_EM001-Send Reminder Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/EP_Retail_Tank_Dip_Not_Entered</template>
    </alerts>
    <fieldUpdates>
        <fullName>EP_Check_Entered_in_Last_14_Days</fullName>
        <field>EP_Tank_Dip_Entered_In_Last_14_Days__c</field>
        <literalValue>1</literalValue>
        <name>Check Entered in Last 14 Days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_FU001_Update_Last_Tank_Dip_Reading</fullName>
        <description>This field update is used to set the last tank dip field to ambient quantity of the last tank dip record</description>
        <field>EP_Last_Dip_Reading__c</field>
        <formula>EP_Ambient_Quantity__c</formula>
        <name>EP-FU001-Update Last Tank Dip Reading</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>EP_Tank__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_FU002_Update_Tank_Capacity_At_Dip</fullName>
        <description>This field update is used to set the tank capacity at dip entry on the dip record based on the capacity of the related tank</description>
        <field>EP_Tank_Capacity_At_Tank_Dip_Entry__c</field>
        <formula>EP_Tank__r.EP_Capacity__c</formula>
        <name>EP-FU002-Update Tank Capacity At Dip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_FU003_Update_Last_Dip_Ship_To_Date</fullName>
        <description>This field update is used to track the date/time of the last reading entered by the user converted into the ship-to timezone</description>
        <field>EP_Last_Dip_Ship_To_Date_Time__c</field>
        <formula>EP_Tank_Dip_Reading_Ship_To_Date_Time__c</formula>
        <name>EP-FU003-Update Last Dip Ship-To Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>EP_Tank__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_FU004_Update_Last_PHD_Ship_To_Date</fullName>
        <description>This field update is used to track the date/time of the last placeholder dip converted into the ship-to timezone</description>
        <field>EP_Last_Placeholder_Ship_To_Date_Time__c</field>
        <formula>EP_Tank_Dip_Reading_Ship_To_Date_Time__c</formula>
        <name>EP_FU004_Update_Last_PHD_Ship_To_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>EP_Tank__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>EP_Uncheck_Entered_in_Last_14_days</fullName>
        <field>EP_Tank_Dip_Entered_In_Last_14_Days__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Entered in Last 14 days</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EP-WR005-Update Last Placeholder Tank Dip Reading</fullName>
        <actions>
            <name>EP_FU004_Update_Last_PHD_Ship_To_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>EP_Tank_Dip__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Placeholder</value>
        </criteriaItems>
        <criteriaItems>
            <field>EP_Tank_Dip__c.EP_Is_Last_Available_Tank_Dip__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EP_Tank__c.EP_Last_Dip_Reading_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This workflow is used to update the last placeholder tank dip reading field on the tank record based on the ambient quantity of the latest tank dip record</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_Update_Tank_Dip_Entered_In_Last_14_Days_Field</fullName>
        <actions>
            <name>EP_Check_Entered_in_Last_14_Days</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>EP_Uncheck_Entered_in_Last_14_days</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>EP_Tank_Dip__c.CreatedDate</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>EP_WR001_Update_Last_Tank_Dip_Reading</fullName>
        <actions>
            <name>EP_FU001_Update_Last_Tank_Dip_Reading</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>EP_FU003_Update_Last_Dip_Ship_To_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>EP_Tank_Dip__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Actual</value>
        </criteriaItems>
        <criteriaItems>
            <field>EP_Tank_Dip__c.EP_Is_Last_Available_Tank_Dip__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EP_Tank__c.EP_Last_Dip_Reading_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This workflow is used to update the last tank dip reading field on the tank record based on the ambient quantity of the latest tank dip record</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>EP_WR002_Update_Tank_Capacity_At_Dip_Entry</fullName>
        <actions>
            <name>EP_FU002_Update_Tank_Capacity_At_Dip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EP_Tank_Dip__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Actual</value>
        </criteriaItems>
        <description>This workflow is used to store the tank capacity at the point in time when the tank dip record was entered. The field value is used for historical reporting purposes</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
