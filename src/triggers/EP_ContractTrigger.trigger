/**
 * @author <Jai Singh>
 * @name <EP_ContractTrigger>
 * @createDate <29/07/20156
 * @description <This is Contract trigger>
 * @version <1.0>
 */
trigger EP_ContractTrigger on Contract (after update){
	if(trigger.isAfter && trigger.isUpdate)
	{
		EP_ContractTriggerHandler.doAfterUpdate(trigger.oldMap, trigger.newMap);
	}
}