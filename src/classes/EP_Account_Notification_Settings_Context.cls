/****************************************************************
* @author       Accenture                                       *
* @name         EP_Account_Notification_Settings_Context        *
* @Created Date 21/12/2017										*
* @description  Class to declare variables                      *
****************************************************************/
public with sharing class EP_Account_Notification_Settings_Context {
	public List<notificationAccountWrapper> notificationAccountList{get;set;}
	public Account account;
	public List<EP_Notification_Account_Settings__c> accNotifyList;
	public EP_Notification_Account_Settings__c notificationAccountObj;
	public set<id> templateContactIdSet;
	public set<id> templateUserIdSet;
	public set<id> contactIdSet;
	public set<id> userIdSet;
	public map<String,string> mapIdName;
	public Id folderId{get;set;}
	public notificationAccountWrapper wrapperObj;

	public EP_Account_Notification_Settings_Context(Account acc) {
		init();
		account = acc;
	}

/****************************************************************
* @author       Accenture                                       *
* @name         init                                            *
* @description  method to initialize variable                   *
* @param        NA 					                            *
* @return       void                                            *
****************************************************************/
	private void init(){
		notificationAccountList = new List<notificationAccountWrapper>();
		accNotifyList = new List<EP_Notification_Account_Settings__c>();
		templateContactIdSet = new set<id>();
		templateUserIdSet = new set<id>();
		contactIdSet = new set<id>();
		userIdSet = new set<id>();
		mapIdName = new map<string,string>();
	}

// Wrapper Class
	public class notificationAccountWrapper{
		public Account acc{get;set;}
		public EP_Notification_Account_Settings__c notifyAcc{get;set;}
		public String inputHiddenContactTemplate{get;set;}
		public String inputHiddenUserTemplate{get;set;}
		public String inputHiddenContact{get;set;}
		public String inputHiddenUser{get;set;}
		public String contactName{get;set;}
		public String userName{get;set;}
		public String contactTemplateName{get;set;}
		public String userTemplateName{get;set;}

		public notificationAccountWrapper(EP_Notification_Account_Settings__c notifyAcc, map<String,string> mapIDString){
			this.notifyAcc = notifyAcc;
			this.inputHiddenContactTemplate = notifyAcc.EP_Notification_Template_Contact__c;
			this.inputHiddenUserTemplate = notifyAcc.EP_Notification_Template_User__c;
			this.inputHiddenContact = notifyAcc.EP_Recipient_Contact__c;
			this.inputHiddenUser = notifyAcc.EP_Recipient_User__c;
			this.contactTemplateName = mapIDString.containsKey(notifyAcc.EP_Notification_Template_Contact__c) ? 
																	mapIDString.get(notifyAcc.EP_Notification_Template_Contact__c): 
																	notifyAcc.EP_Notification_Template_Contact__c;
			this.userTemplateName = mapIDString.containsKey(notifyAcc.EP_Notification_Template_User__c) ? 
																mapIDString.get(notifyAcc.EP_Notification_Template_User__c) : 
																notifyAcc.EP_Notification_Template_User__c;
			this.contactName = mapIDString.containsKey(notifyAcc.EP_Recipient_Contact__c) ? 
														mapIDString.get(notifyAcc.EP_Recipient_Contact__c) : 
														notifyAcc.EP_Recipient_Contact__c;
			this.userName = mapIDString.containsKey(notifyAcc.EP_Recipient_User__c) ? 
													mapIDString.get(notifyAcc.EP_Recipient_User__c) :
													notifyAcc.EP_Recipient_User__c;
			
		}
	}
}