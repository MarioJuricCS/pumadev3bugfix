/**
*  @Author <Accenture>
*  @Name <EP_LoadPlanEvent_Subscription>
*  @CreateDate <09/05/2018>
*  @Description <event subscription class for Load Plan>
*  @Version <1.0>
*/
public class EP_LoadPlanEvent_Subscription extends EP_Event_Subscription{
   public EP_LoadPlanEvent_Subscription() {
        subscriptionEventObj.eventType = system.label.EP_LOADPLAN;
    }
}