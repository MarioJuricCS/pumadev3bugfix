@isTest
private class EP_TankTriggerHelper_R1_UT {
   
    static testMethod void setCurrencyOnTank_Test()
      {    
        
        List<EP_Tank__c> lsTank =  EP_Tank_TestDataUtility.createTank(1,true);
        insert lsTank;
        Test.StartTest();
        EP_TankTriggerHelper_R1.setCurrencyOnTank(lsTank);
        Test.stopTest();
               
      }
    
    static testMethod void restrictUserToUpdateTankWhenActionNotAssigned_Test()
    {
        List<EP_Tank__c> lsTank =  EP_Tank_TestDataUtility.createTank(1,true);
        Map<Id, EP_Tank__c> mapOldTank = new Map<Id, EP_Tank__c>();
        for(EP_Tank__c obj : lsTank){
            mapOldTank.put(obj.id,obj);
        }
        Test.StartTest();
        EP_TankTriggerHelper_R1.restrictUserToUpdateTankWhenActionNotAssigned(lsTank,mapOldTank);
        Test.stopTest();
    }
    
    static testMethod void validateProduct_Test()
      {
          
      	List<EP_Tank__c> lsTank =  EP_Tank_TestDataUtility.createTank(1,true);
        Test.StartTest();
        EP_TankTriggerHelper_R1.validateProduct(lsTank);
        Test.stopTest();
               
      }
}