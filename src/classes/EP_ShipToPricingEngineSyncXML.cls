/* 
@Author          Accenture
@name            EP_ShipToPricingEngineSyncXML 
@Description     This class is use to created the Account Creation XML for pricing Engine Defect 78616 
@Version         1.0
*/
public with sharing class EP_ShipToPricingEngineSyncXML extends EP_AccountRequestXML {
    
    public account shipToAddressObj;
    /**
    * @author           Accenture
    * @name             init
    * @description      retrive the records to bind the values in XML
    * @param            NA
    * @return           NA
    */
    public  override void init(){
        EP_GeneralUtility.Log('Public','EP_ShipToPricingEngineSyncXML','init');
        MSGNode = doc.createRootElement(EP_AccountConstant.MSG, null, null);
        EP_AccountMapper objAccMap = new EP_AccountMapper(); 
        shipToAddressObj = objAccMap.getAccountRecord(recordId);
        objAccount = objAccMap.getAccountRecord(shipToAddressObj.parentId);
        super.setEncryption();
    }

   /**
    * @author           Accenture
    * @name             createPayload 
    * @date             13/Nov/2017
    * @description      This method is used to create Payload for Account to Sync with PE
    * @param            NA
    * @return           NA
    */
    public override void createPayload(){
        EP_GeneralUtility.Log('Public','EP_ShipToPricingEngineSyncXML','createPayload');
        Dom.XMLNode PayloadNode = MSGNode.addChildElement(EP_AccountConstant.PAYLOAD ,null,null);
        Dom.XmlNode AnyNode = PayloadNode.addChildElement(EP_AccountConstant.ANY0,null, null);        
        DOM.Document tempDoc = new DOM.Document();
        Dom.XMLNode customersNode = tempDoc.createRootElement(EP_AccountConstant.CUSTOMERS,null, null);
        Dom.XMLNode customerNode = customersNode.addChildElement(EP_AccountConstant.CUSTOMER,null,null);
        String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, objAccount.id); 
        customerNode.addChildElement(EP_AccountConstant.SEQID,null,null).addTextNode(getValueforNode(seqid));
        //Identifier node
        Dom.XMLNode identifierNode = customerNode.addChildElement(EP_AccountConstant.IDENTIFIER,null,null);
        identifierNode.addChildElement(EP_AccountConstant.CUSTID,null,null).addTextNode(getValueforNode(objAccount.EP_Account_Id__c));
        identifierNode.addChildElement(EP_AccountConstant.ENTRPSID,null,null).addTextNode(getValueforNode(objAccount.EP_EnterpriseId__c));  
        customerNode.addChildElement(EP_AccountConstant.CUSTCATGRY,null,null).addTextNode(getValueforNode(objAccount.EP_Customer_Category__c));
        customerNode.addChildElement(EP_AccountConstant.NAME,null,null).addTextNode(getValueforNode(objAccount.name));
        customerNode.addChildElement(EP_AccountConstant.NAME2,null,null).addTextNode(getValueforNode(objAccount.EP_Account_Name_2__c));
        customerNode.addChildElement(EP_AccountConstant.ADDRESS,null,null).addTextNode(getValueforNode(objAccount.BillingStreet));
        customerNode.addChildElement(EP_AccountConstant.CITY,null,null).addTextNode(getValueforNode(objAccount.BillingCity));
        customerNode.addChildElement(EP_AccountConstant.PHONE,null,null).addTextNode(getValueforNode(objAccount.phone));
        customerNode.addChildElement(EP_AccountConstant.MOBILEPHONE,null,null).addTextNode(getValueforNode(objAccount.EP_Mobile_Phone__c));
        customerNode.addChildElement(EP_AccountConstant.CURRENCYID,null,null).addTextNode(getValueforNode(objAccount.CurrencyIsoCode));
        customerNode.addChildElement(EP_AccountConstant.LANGCODE,null,null).addTextNode(getValueforNode(objAccount.EP_Language_Code__c));
        customerNode.addChildElement(EP_AccountConstant.CNTRYCODE,null,null).addTextNode(getValueforNode(objAccount.EP_Country_Code__c));
        customerNode.addChildElement(EP_AccountConstant.BILLTOCUSTID,null,null).addTextNode(getValueforNode(objAccount.EP_BillToCustomerNr__c));
        customerNode.addChildElement(EP_AccountConstant.FAX,null,null).addTextNode(getValueforNode(objAccount.Fax));
        customerNode.addChildElement(EP_AccountConstant.POSTCODE,null,null).addTextNode(getValueforNode(objAccount.BillingPostalCode));
        customerNode.addChildElement(EP_AccountConstant.COUNTY,null,null).addTextNode(getValueforNode(objAccount.BillingState));
        customerNode.addChildElement(EP_AccountConstant.EMAIL,null,null).addTextNode(getValueforNode(objAccount.EP_Email__c));
        customerNode.addChildElement(EP_AccountConstant.WEBSITE,null,null).addTextNode(getValueforNode(objAccount.Website));        
        customerNode.addChildElement(EP_AccountConstant.RQSTDPMTMTHDID,null,null).addTextNode(getValueforNode(objAccount.EP_RequestedPaymentMethod__r.EP_Payment_Method_Code__c));
        customerNode.addChildElement(EP_AccountConstant.REQPTMTRM,null,null).addTextNode(getValueforNode(objAccount.EP_RequestedPaymentTerms__r.EP_Payment_Term_Code__c)); 
        customerNode.addChildElement(EP_AccountConstant.PCKGDRQSTDPYMNTTERM,null,null).addTextNode(getValueforNode(objAccount.EP_Requested_Packaged_Payment_Terms__r.EP_Payment_Term_Code__c)); 
        customerNode.addChildElement(EP_AccountConstant.BILLBASIS,null,null).addTextNode(getValueforNode(objAccount.EP_Billing_Basis__c));
        customerNode.addChildElement(EP_AccountConstant.BILLMTHD,null,null).addTextNode(getValueforNode(objAccount.EP_Billing_Method__c));
        customerNode.addChildElement(EP_AccountConstant.BILLFRQNCY,null,null).addTextNode(getValueforNode(objAccount.EP_Billing_Frequency__c));
        customerNode.addChildElement(EP_AccountConstant.BILLCONSOLBASIS,null,null).addTextNode(getValueforNode(objAccount.EP_Invoice_Consolidation_basis__c));
        customerNode.addChildElement(EP_AccountConstant.DLVRYTYPE,null,null).addTextNode(getValueforNode(objAccount.EP_Delivery_Type__C));
        customerNode.addChildElement(EP_AccountConstant.MANUALINVCINGALLOWED,null,null).addTextNode(getValueforNode(objAccount.EP_ManualInvoicingAllowed_NAV__c));
        customerNode.addChildElement(EP_AccountConstant.SALESPERSONCODE,null,null).addTextNode(getValueforNode(objAccount.EP_Salesperson__r.EP_Salesperson_Code__c ));
        customerNode.addChildElement(EP_AccountConstant.ISVATEXEMPTED,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_VAT_Exempted__c)));
        customerNode.addChildElement(EP_AccountConstant.DEFERINVOICE,null,null).addTextNode(getValueforNode(objAccount.EP_Defer_Invoice__c));
        customerNode.addChildElement(EP_AccountConstant.DEFERUPPERLMT,null,null).addTextNode(getValueforNode(objAccount.EP_Defer_Upper_Limit__c));
        customerNode.addChildElement(EP_AccountConstant.ALLOWRLSONPYMNTPROOF,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Allow_release_on_proof_of_payment__c)));
        Dom.XMLNode acntStmntFrqncyNode = customerNode.addChildElement(EP_AccountConstant.ACNTSTMNTFRQNCY,null,null);
        acntStmntFrqncyNode.addChildElement(EP_AccountConstant.DAILY,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Account_Statement_Periodicity_D__c)));
        acntStmntFrqncyNode.addChildElement(EP_AccountConstant.WEEKLY,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Account_Statement_Periodicity_W__c)));
        acntStmntFrqncyNode.addChildElement(EP_AccountConstant.FORTNIGHTLY,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Account_Statement_Periodicity_F__c)));
        acntStmntFrqncyNode.addChildElement(EP_AccountConstant.MONTHLY,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Account_Statement_Periodicity_M__c)));
        customerNode.addChildElement(EP_AccountConstant.STMNTTYPALL,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_All_Items_Account_Statements__c)));
        customerNode.addChildElement(EP_AccountConstant.STMNTTYPOPEN,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Sys_Open_Items_Account_Statements__c)));
        customerNode.addChildElement(EP_AccountConstant.COMBINEDINVOICING,null,null).addTextNode(getValueforNode(objAccount.EP_Combined_Invoicing__c));
        customerNode.addChildElement(EP_AccountConstant.PRICECONSOLBASIS,null,null).addTextNode(getValueforNode(objAccount.EP_Price_Consolidation_Basis__c));
        customerNode.addChildElement(EP_AccountConstant.BUSINESSSEGMENT,null,null).addTextNode(getValueforNode(objAccount.EP_Business_Segment_Code__c));
        customerNode.addChildElement(EP_AccountConstant.BUSINESSCHANNEL,null,null).addTextNode(getValueforNode(objAccount.EP_Business_Channel_Code__c));
        customerNode.addChildElement(EP_AccountConstant.EXCISEDUTY,null,null).addTextNode(getValueforNode(objAccount.EP_Excise_duty__c));
        customerNode.addChildElement(EP_AccountConstant.ISPOREQ,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Is_Customer_Reference_Mandatory__c)));
        customerNode.addChildElement(EP_AccountConstant.POREQBY,null,null).addTextNode(getValueforNode(objAccount.EP_Customer_s_PO_number_required_by__c));
        customerNode.addChildElement(EP_AccountConstant.INVCDUEDTBSDON,null,null).addTextNode(getValueforNode(objAccount.EP_Invoice_Due_Date_Based_on__c));
        customerNode.addChildElement(EP_AccountConstant.CUSTACTIVATIONDATE,null,null).addTextNode(formatDateAsString(objAccount.EP_Customer_Activation_Date__c));
        customerNode.addChildElement(EP_AccountConstant.CUSTSTATUS,null,null).addTextNode(getValueforNode(objAccount.EP_NewStatus__c));
        customerNode.addChildElement(EP_AccountConstant.REASONFORSTATUSCHNG,null,null).addTextNode(getValueforNode(objAccount.EP_Reason_For_Deactivation__c));
        customerNode.addChildElement(EP_AccountConstant.SEARCHNAME,null,null).addTextNode(getValueforNode(objAccount.EP_Search_Name__c));
        customerNode.addChildElement(EP_AccountConstant.TRADINGNAME,null,null).addTextNode(getValueforNode(objAccount.EP_Trading_Name__c));
        customerNode.addChildElement(EP_AccountConstant.TELEXNO,null,null).addTextNode(getValueforNode(objAccount.EP_Telex_No__c));
        customerNode.addChildElement(EP_AccountConstant.ADDRESS2,null,null);
        customerNode.addChildElement(EP_AccountConstant.LOGOID,null,null).addTextNode(getValueforNode(objAccount.EP_Logo_Id__c));
        customerNode.addChildElement(EP_AccountConstant.ISCAPTIVCUST,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Customer_Of_Customer__c)));
        customerNode.addChildElement(EP_AccountConstant.REQPMTCRDLMT,null,null).addTextNode(getValueforNode(objAccount.EP_Recommended_Credit_Limit__c)); 
        customerNode.addChildElement(EP_AccountConstant.REPRCNGONAVGPRDCPRCNG,null,null).addTextNode(getValueforNode(objAccount.EP_Repricing_Based_On__c));
        customerNode.addChildElement('cnsldtdInvcPrntOptn',null,null).addTextNode(getValueforNode(objAccount.EP_Consolidated_Invoice_Print_Options__c));
        //add kyc details 
        addKYCDetails(customerNode); 
        customerNode.addChildElement(EP_AccountConstant.PRFRDBANKACC,null,null).addTextNode(getValueforNode(objAccount.EP_Preferred_Bank__r.EP_Bank_Code__c));
        customerNode.addChildElement(EP_AccountConstant.VATREGNR,null,null).addTextNode(getValueforNode(objAccount.EP_Company_Tax_Number__c));
        customerNode.addChildElement(EP_AccountConstant.custRefVsbl,null,null).addTextNode(getValueforNode(transformBooleanValue(objAccount.EP_Is_Customer_Reference_Visible__c)));
        customerNode.addChildElement(EP_AccountConstant.VERSIONNR,null,null);
        //ShiptoAddresses node
        Dom.XMLNode shipToAddressesNode = customerNode.addChildElement(EP_AccountConstant.SHIPTOADDRESSES,null,null);
        addShipToAddresses(shipToAddressesNode);
        //Bank node start
        Dom.XMLNode custBanksNode = customerNode.addChildElement(EP_AccountConstant.CUSTBANKS,null,null);
        //bank node end
        customerNode.addChildElement(EP_AccountConstant.CLIENTID,null,null).addTextNode(getValueforNode(objAccount.EP_Puma_Company_Code__c));
        //Encoding payload by calling encode XML method in superclass
        AnyNode.addTextNode(encodeXML(tempDoc));
    }
    /**
    * @author           Accenture
    * @name             addShipToAddresses
    * @description      This method is used to create the xml ralated to Shippng Address
    * @param            Dom.XMLNode 
    * @return           NA
    */
    @testvisible 
    private void addShipToAddresses(Dom.XMLNode shipToAddressesNode){       
        EP_GeneralUtility.Log('Public','EP_ShipToPricingEngineSyncXML','addShipToAddresses');
        Dom.XMlNode shipToAddressNode = shipToAddressesNode.addChildElement(EP_AccountConstant.SHIPTOADDRESS,null,null);
        String seqid = EP_IntegrationUtil.reCreateSeqId(messageId, shipToAddressObj.id); 
        shipToAddressNode.addChildElement(EP_AccountConstant.SEQID,null,null).addTextNode(getValueforNode(seqid));
        Dom.XMLNode identifierNode = shipToAddressNode.addChildElement(EP_AccountConstant.IDENTIFIER,null,null);
        identifierNode.addChildElement(EP_AccountConstant.SHIPTOID,null,null).addTextNode(getValueforNode(shipToAddressObj.EP_Account_Id__c));
        identifierNode.addChildElement(EP_AccountConstant.CUSTID,null,null).addTextNode(getValueforNode(shipToAddressObj.Parent.EP_Account_Id__c));
        identifierNode.addChildElement(EP_AccountConstant.ENTRPRSID,null,null).addTextNode(getValueforNode(shipToAddressObj.EP_EnterpriseId__c));        
        shipToAddressNode.addChildElement(EP_AccountConstant.NAME,null,null).addTextNode(getValueforNode(shipToAddressObj.Name));
        shipToAddressNode.addChildElement(EP_AccountConstant.NAME2,null,null).addTextNode(getValueforNode(shipToAddressObj.EP_Account_Name_2__c));
        shipToAddressNode.addChildElement(EP_AccountConstant.CNTRYCODE,null,null).addTextNode(getValueforNode(shipToAddressObj.EP_Country_Code__c));
        shipToAddressNode.addChildElement(EP_AccountConstant.ADDRESS,null,null).addTextNode(getValueforNode(shipToAddressObj.ShippingStreet));
        shipToAddressNode.addChildElement(EP_AccountConstant.POSTCODE,null,null).addTextNode(getValueforNode(shipToAddressObj.ShippingPostalCode));
        shipToAddressNode.addChildElement(EP_AccountConstant.CITY,null,null).addTextNode(getValueforNode(shipToAddressObj.ShippingCity));
        shipToAddressNode.addChildElement(EP_AccountConstant.PHONE,null,null).addTextNode(getValueforNode(shipToAddressObj.Phone));
        shipToAddressNode.addChildElement(EP_AccountConstant.WEBSITE,null,null).addTextNode(getValueforNode(shipToAddressObj.Website));
        shipToAddressNode.addChildElement(EP_AccountConstant.FAX,null,null).addTextNode(getValueforNode(shipToAddressObj.Fax));
        shipToAddressNode.addChildElement(EP_AccountConstant.EMAIL,null,null).addTextNode(getValueforNode(shipToAddressObj.EP_Email__c));
        shipToAddressNode.addChildElement(EP_AccountConstant.COUNTY,null,null).addTextNode(getValueforNode(shipToAddressObj.ShippingState));
        shipToAddressNode.addChildElement(EP_AccountConstant.ISVMI,null,null).addTextNode(getValueforNode(shipToAddressObj.EP_Is_VMI_ShipTo__c));
        shipToAddressNode.addChildElement(EP_AccountConstant.SHIPTOTYPE,null,null).addTextNode(getValueforNode(shipToAddressObj.EP_Ship_To_Type__c));
        shipToAddressNode.addChildElement(EP_AccountConstant.STATUS,null,null).addTextNode(getValueforNode(shipToAddressObj.EP_ShipTo_Status_NAV__c)); 
        shipToAddressNode.addChildElement(EP_AccountConstant.VERSIONNR,null,null);            
    }
    
    @testvisible 
    private void addKYCDetails(DOM.XmlNode customerNode){
        List<EP_Action__c> kycDetails = new List<EP_Action__c>();
        string kyctype ='';
        string kycUpdtdBy = '';
        string kycUpdatedOn ='';
        string kycStatus = '';
        string reasonForKycRjctn='';
        EP_ActionMapper actionmapper = new EP_ActionMapper();
        kycDetails = actionmapper.getKycDetailsForSellTo(recordId);
        if(!kycDetails.isEmpty()){     
            kyctype = kycDetails[0].EP_KYC_Type_NAV__c;
            kycUpdtdBy = kycDetails[0].LastModifiedBy.Name;
            kycUpdatedOn = formatDateAsString(kycDetails[0].LastModifiedDate);
            kycStatus = kycDetails[0].EP_KYC_Status_NAV__c;
            /* defect 83697*/           
            reasonForKycRjctn = kycDetails[0].EP_Reason_For_Status__c ;
            /* defect 83697*/
        }
        customerNode.addChildElement(EP_AccountConstant.KYCTYPE,null,null).addTextNode(getValueforNode(kycType));
        customerNode.addChildElement(EP_AccountConstant.KYCUPDTDBY,null,null).addTextNode(getValueforNode(kycUpdtdBy)); 
        customerNode.addChildElement(EP_AccountConstant.KYCUPDATEDON,null,null).addTextNode(getValueforNode(kycUpdatedOn)); 
        customerNode.addChildElement(EP_AccountConstant.KYCSTATUS,null,null).addTextNode(getValueforNode(kycStatus)); 
        customerNode.addChildElement(EP_AccountConstant.REASONFORKYCRJCTN,null,null).addTextNode(getValueforNode(reasonForKycRjctn));
    } 
}