@isTest
private class GetAndDeleteInventoryReviewTest{

    @isTest
    static void testCustomStep(){
        Opportunity opp = TQTestUtils.buildOpportunity();
        
        csord__Order__c order = new csord__Order__c();
        order.Name = 'Test order';
        order.csord__Identification__c = 'testOrder0';
        order.csord__Status__c = 'Active';
        order.csord__Status2__c = 'Active';
        order.csordtelcoa__Opportunity__c	= opp.Id;
        insert order;
        
        EP_Action__c action = new EP_Action__c();
        Id recordTypeId = Schema.SObjectType.EP_Action__c.getRecordTypeInfosByName().get('Inventory Review').getRecordTypeId();
        action.RecordTypeId = recordTypeId;
        action.EP_CSOrder__c = order.Id;
        action.EP_Status__c = '01-New';
        insert action;
        
        CSPOFA__Orchestration_Process_Template__c moptc = new CSPOFA__Orchestration_Process_Template__c();
        moptc.Name = 'Test MOPTC';
        insert moptc;
        
        CSPOFA__Orchestration_Process__c mopc = new CSPOFA__Orchestration_Process__c();
        mopc.Name = 'Test MOPC';
        mopc.CSPOFA__Orchestration_Process_Template__c = moptc.Id;
        mopc.Order__c = order.Id;
        insert mopc;
        
        CSPOFA__Orchestration_Step__c mosc = new CSPOFA__Orchestration_Step__c();
        mosc.Name = 'Test MOSC';
        mosc.CSPOFA__Orchestration_Process__c = mopc.Id;
        insert mosc;
        
        list<CSPOFA__Orchestration_Step__c> moscList = new list<CSPOFA__Orchestration_Step__c>();
        moscList.add(mosc);
        
        Test.startTest();
        GetAndDeleteInventoryReview customStep = new GetAndDeleteInventoryReview();
        customStep.process(moscList);
        Test.stopTest();
        
        List<EP_Action__c> actionList = [SELECT Id FROM EP_Action__c WHERE EP_CSOrder__c =: order.Id LIMIT 1];
        System.assertEquals(1, actionList.size(), 'No record was deleted.');
    }
}