/**
   @author          Accenture
   @name            EP_IntegrationRecordMapper
   @CreateDate      12/27/2016
   @Description     This class contains all SOQLs related to Order Object
   @Version         1.0
   @reference       NA
*/
public class EP_IntegrationRecordMapper {
    /**
    *  Constructor. 
    *  @name            EP_IntegrationRecordMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */  
    public EP_IntegrationRecordMapper() {}
    /** This method is used to get profile records by Name
    *  @author          Accenture
    *  @name            getProfilesByName
    *  @param           List<string> list of profile names
    *  @return          list<Profile>
    *  @throws          NA
    */
    public List<EP_IntegrationRecord__c> getParentsRecords(set<id> integrationRecords){
        list<EP_IntegrationRecord__c> recordList = new list<EP_IntegrationRecord__c>();
        /*QUERY PROFILE BASED ON PROFILE NAME*/
        for(list<EP_IntegrationRecord__c> intRecordList : [select id, EP_XML_Message__c,EP_Attempt__c,EP_Message_Type__c,EP_Object_Record_Name__c from EP_IntegrationRecord__c where Id IN :integrationRecords AND EP_ParentNode__c = null]) {
             recordList.addAll(intRecordList);
        } 
        return recordList;
    }
    
    public List<EP_IntegrationRecord__c> getRecordsByMessageIdOrSeqId(string messageId, set<string> setSeqIds){
        list<EP_IntegrationRecord__c> recordList = new list<EP_IntegrationRecord__c>(); 
        //Code changes for Requirement - #59462(Reliable Messaging) and #59463(Message Idempotency)
        recordList =[select id,
        					EP_Error_Description__c,
        					EP_Status__c,
        					EP_SeqId__c,
        					EP_Response_Message__c, 
        					EP_Process_Result__c,
        					EP_Object_Type__c,
        					EP_Object_ID__c,
        					EP_Attempt__c, 
        					EP_ParentNode__c 
        					from EP_IntegrationRecord__c 
        					where (EP_Message_ID__c=:messageId OR EP_SeqId__c IN: setSeqIds)];
        return recordList; 
    }
}