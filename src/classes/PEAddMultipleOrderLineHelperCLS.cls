public class PEAddMultipleOrderLineHelperCLS { 

    public static List<TrafiguraConfigurationController.WrapperpOrderLineList> addNewRowToAccList(List<TrafiguraConfigurationController.WrapperpOrderLineList> OrderLineObjList){
        TrafiguraConfigurationController.WrapperpOrderLineList newRecord = new TrafiguraConfigurationController.WrapperpOrderLineList();
        csord__Order_Line_Item__c newOrderLineRecord = new csord__Order_Line_Item__c();        
        newRecord.orderLine = newOrderLineRecord;
        newRecord.index = OrderLineObjList.size();
        OrderLineObjList.add(newRecord);
        return OrderLineObjList;
    }
    
    
     public static List<TrafiguraConfigurationController.WrapperpOrderLineList> removeRowToAccountList(Integer rowToRemove, List<TrafiguraConfigurationController.WrapperpOrderLineList> waAccountList){
        waAccountList.remove(rowToRemove);
        return waAccountList;
    }
    
    public static void save(List<TrafiguraConfigurationController.WrapperpOrderLineList> orderLineList) {
        system.debug('==orderLineList==>'+orderLineList.size());
        List<csord__Order_Line_Item__c> OrderLinesRecordsToBeInserted = new List<csord__Order_Line_Item__c>();
        if(orderLineList !=null && !orderLineList.isEmpty()){
            for(TrafiguraConfigurationController.WrapperpOrderLineList eachRecord : orderLineList ){
                csord__Order_Line_Item__c accTemp = eachRecord.orderLine;
                OrderLinesRecordsToBeInserted.add(accTemp);
               
            }
            system.debug('==OrderLinesRecordsToBeInserted==>'+OrderLinesRecordsToBeInserted.size());
            insert OrderLinesRecordsToBeInserted;
        }
    }
}