/* 
  @Author <Pooja Dhiman>
  @name <EP_InternalUser_ToSetup_PortalUser_Test>
  @CreateDate <03/12/2015>
  @Description <This is the test class to create Community Portal User with a sell-to and VMI Ship-to Accounts. The parent 
  sell-to Account should be able to access the child Account and the user should be able to submit the tank dip  >
  @Version <1.0>
*/
@isTest
private class EP_InternalUser_ToSetup_PortalUser_Test {
   /*
        This method will create test data for accounts with record type Sell to do not have access to Billing Address but have
        access to Shipping address
    */ 
    static testMethod void testCreateAccountDataForSellTo(){
        //Insert a Sell-to Account
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null,null);
        insert sellToAccount; 
        //CREATE CSC USER
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
        System.runAs(cscUser) {
        try{
        	sellToAccount.ShippingStreet ='test';
        	update sellToAccount;
        	Test.stopTest();
        }
        catch(Exception e){
        	system.Assert(e.getMessage().contains('insufficient access rights on cross-reference id'));
        }
        try{
        	sellToAccount.BillingStreet='test';
        	update sellToAccount;
        }
        catch(Exception e)
        {
        	System.assertEquals(true,sellToAccount.BillingStreet=='test');
        }
        }
    }
     /*
        This method will create test data for VMI Account Submitting Tank Dip
    */ 
    static testMethod void testCreateAccountDataforSubmittingTankDip() { 
        List<Account> accountList = new List<Account>();
        Product2 product;
        
        //Insert a Sell-to Account
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null,null);
        insert sellToAccount;  
        
        //Insert a Ship-to Account
        Account vmiShipToAccount1 = EP_TestDataUtility.createShipToAccount(sellToAccount.id,null); 
        Account vmiShipToAccount2 = EP_TestDataUtility.createShipToAccount(sellToAccount.id,null); 
        Account vmiShipToAccount3 = EP_TestDataUtility.createShipToAccount(sellToAccount.id,null); 
        accountList.add(vmiShipToAccount1);
        accountList.add(vmiShipToAccount2);
        accountList.add(vmiShipToAccount3);         
        
        Id nonVMIrecordtypeid = EP_Common_Util.fetchRecordTypeId('Account','Non-VMI Ship To');
        Account nonVMIShipToAccount =  EP_TestDataUtility.createShipToAccount(sellToAccount.id,nonVMIrecordtypeid);
        accountList.add(nonVMIShipToAccount);
        
        insert accountList;
        
        //CREATE PRODUCT
        product = EP_TestDataUtility.createTestRecordsForProduct();
        
        //Create Tank
        List<EP_Tank__c> tanksToBeInserted = new List<EP_Tank__c>();
        EP_Tank__c tank1= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount1.id,product.id);
        EP_Tank__c tank2= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount2.id,product.id);
        EP_Tank__c tank3= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount3.id,product.id);
        tanksToBeInserted.add(tank1);
        tanksToBeInserted.add(tank2);
        tanksToBeInserted.add(tank3);
        insert tanksToBeInserted;
        
        // Update Account Status to Basic Data Setup
        List<Account> acctsToBeUpdated = new List<Account>();
        vmiShipToAccount1.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        vmiShipToAccount2.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        vmiShipToAccount3.EP_Status__c=EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        acctsToBeUpdated.add(vmiShipToAccount1);
        acctsToBeUpdated.add(vmiShipToAccount2);
        acctsToBeUpdated.add(vmiShipToAccount3);
        update acctsToBeUpdated;
         // Update Account Status to Account Setup
         List<Account> acctsToBeUpdated1= new List<Account>();
        vmiShipToAccount1.EP_Status__c = EP_Common_Constant.STATUS_SET_UP;
        vmiShipToAccount2.EP_Status__c=EP_Common_Constant.STATUS_SET_UP;
        vmiShipToAccount3.EP_Status__c=EP_Common_Constant.STATUS_SET_UP;
        acctsToBeUpdated1.add(vmiShipToAccount1);
        acctsToBeUpdated1.add(vmiShipToAccount2);
        acctsToBeUpdated1.add(vmiShipToAccount3);
        update acctsToBeUpdated1;

          //CREATE CSC USER
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        List<Account> accShipToList=[Select Id, Name from Account where ParentId=:sellToAccount.Id LIMIT 3]; 
        
        PageReference pageRef = Page.EP_TankDipSubmitPage_R1;
        Test.setCurrentPage(pageRef);//Applying page context here  
         ApexPages.currentPage().getParameters().put('id', vmiShipToAccount1.Id);
         ApexPages.currentPage().getParameters().put('Date', '201810150517191');
         EP_TankDipSubmitPageControllerClass_R1 tankDippageObj=new EP_TankDipSubmitPageControllerClass_R1();
        Test.startTest();
        System.runAs(cscUser){       
                tankDippageObj.strSelectedTanksIDs=tank1.ID;             
                tankDippageObj.saveStockRecords();
                system.debug('***Tankdip***'+tankDippageObj.strSelectedTanksIDs+'***TankID'+tank1.Id);
                system.debug('***TankSize***'+tankDippageObj.tanks.size());
        }
        Test.stopTest();  
        System.assertEquals(true,accShipToList.size()==3);   
        System.assertEquals(true,tankDippageObj.tanks.size()==1);                      
}
    /*
        This method will create test data for accounts with record type Bill to do not have access to Billing Address but have
        access to Shipping address
    */ 
    static testMethod void testCreateAccountDataForBillTo(){
    	 //Insert a Bill-to Account
        Account billToAccount = EP_TestDataUtility.createBillToAccount();
        insert billToAccount;
        //CREATE CSC USER
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
        System.runAs(cscUser) {
        try{
        	billToAccount.ShippingStreet ='test';
        	update billToAccount;
        	Test.stopTest();
        }
        catch(Exception e){
        	system.Assert(e.getMessage().contains('insufficient access rights on cross-reference id'));
        }
        try{
        	billToAccount.BillingStreet='test';
        	update billToAccount;
        }
        catch(Exception e)
        {
        	System.assertEquals(true,billToAccount.BillingStreet=='test');
        }
        }
    }
    /*
        This method will create test data for accounts with record type NonVMIShip to do not have access to Billing Address but 
        have access to Shipping Address
    */ 
    static testMethod void testCreateAccountDataForNonVMIShipto(){
    	//Insert a Sell-to Account
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null,null);
        insert sellToAccount;
    	//Insert a NonVMIShip-to Account
        Id nonVMIrecordtypeid = EP_Common_Util.fetchRecordTypeId('Account','Non-VMI Ship To');
        Account nonVMIShipToAccount =  EP_TestDataUtility.createShipToAccount(sellToAccount.id,nonVMIrecordtypeid);
        insert nonVMIShipToAccount;
         //CREATE CSC USER
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
        System.runAs(cscUser) {
        	try{
        	nonVMIShipToAccount.BillingStreet='test';
        	update nonVMIShipToAccount;
        	}
        	catch(Exception e){
        		system.Assert(e.getMessage().contains('insufficient access rights on cross-reference id'));
        	}
        	try{
        	nonVMIShipToAccount.ShippingStreet='test';
        	update nonVMIShipToAccount;
        }
        catch(Exception e)
        {
        	System.assertEquals(true,nonVMIShipToAccount.ShippingStreet=='test');
        }
        }
    }
     /*
        This method will create test data for accounts with record type VMIShip to do not have access to Billing Address but 
        have access to Shipping Address
    */ 
    static testMethod void testCreateAccountDataForVMIShipto(){
    	//Insert a Sell-to Account
        Account sellToAccount = EP_TestDataUtility.createSellToAccount(null,null);
        insert sellToAccount;
    	//Insert a VMIShip-to Account
        Account VMIShipToAccount =  EP_TestDataUtility.createShipToAccount(sellToAccount.id,null);
        insert VMIShipToAccount;
         //CREATE CSC USER
        Profile cscAgent = [Select id from Profile Where Name = 'Puma CSC Agent_R1' Limit 1];
        User cscUser = EP_TestDataUtility.createUser(cscAgent.id);
        Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ,EP_Common_Constant.VMI_SHIP_TO );
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        Test.startTest();
        System.runAs(cscUser) {
        	try{
        	VMIShipToAccount.BillingStreet='test';
        	update VMIShipToAccount;
        	}
        	catch(Exception e){
        		system.Assert(e.getMessage().contains('insufficient access rights on cross-reference id'));
        	}
        	try{
        	VMIShipToAccount.ShippingStreet='test';
        	update VMIShipToAccount;
        }
        catch(Exception e)
        {
        	System.assertEquals(true,VMIShipToAccount.ShippingStreet=='test');
        }
        }
    }
}