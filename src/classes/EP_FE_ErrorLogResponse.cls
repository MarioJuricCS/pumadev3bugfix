/* 
  @Author <Ram Rai>
   @name <EP_FE_ErrorLogResponse>
   @CreateDate <25/04/2016>
   @Description <The purpose of this class is to provide the Response to the calling API.>  
   @Version <1.0>
*/
global with sharing class EP_FE_ErrorLogResponse extends EP_FE_Response {
    global static final Integer ERROR_NOT_VALID_COMPONENT = -1;
    global static final Integer ERROR_NOT_VALID_PERFORMED_ACTION = -2;
    global static final Integer ERROR_NOT_VALID_EXCEPTION_DESCRIPTION = -3; 
    global static final Integer ERROR_NOT_VALID_EXCEPTION_DETAILS = -5;     
    global static final Integer ERROR_NOT_VALID_RUNNING_USER = -4; 
    global static final Integer ERROR_NOT_VALID_HANDLED = -6;
    global static final Integer ERROR_NOT_VALID_BROWSER = -7;
    global static final Integer ERROR_NOT_VALID_DEVICE = -8;
    global static final Integer ERROR_NOT_VALID_OPERATING_SYSTEM = -9;
    global static final Integer ERROR_NOT_VALID_POST_REQUEST = -11;
    global static final Integer ERROR_INSERTING_EXCEPTION = -10;
}