/**
  * @author       Accenture
  * @name         EP_MonitorExportLibClass_UT
  * @date         22/03/2018
  * @description  Unit Test Class for EP_MonitorExportLibClass
  */
@isTest
public class EP_MonitorExportLibClass_UT {
    
     static testMethod void returnPayloadBodyTest() {
        Test.startTest();
          List<sObject> lExceptions = Test.loadData(EP_Exception_Log__c.sObjectType,'Exception_Log_Test_Data');
            List<sObject> lIntegrationRecords = new List<sObject>();
         String strResponse =  EP_MonitorExportLibClass.returnPayloadBody(lExceptions,
                                                                            EP_Common_Constant.EXCEPTION_LOGS,
                                                                                EP_Common_Constant.EXCEPTION_LOGS + 
                                                                                    EP_Common_Constant.NULL_VAR);
        String strResponsenull =EP_MonitorExportLibClass.returnPayloadBody(lIntegrationRecords, 'SUCCESS', 'FAILURE');
        Test.stopTest();
    }
    static testMethod void returnQueryLimitFromRequestTest() {
        Test.startTest();
        List<String> lParamNames = new List<String>();
        lParamNames.add(EP_Common_Constant.FROM_DATE_TIME_FILTER_NAME);
        lParamNames.add(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
        lParamNames.add(EP_Common_Constant.limitException);
        
        String  dToday = System.now().format('MM-dd-yyyy');
        String  dYesterday = System.now().addDays(-1).format('MM-dd-yyyy');

        List<String> lParamValues = new List<String>();
        lParamValues.add(dYesterday);
        lParamValues.add(dToday);
        lParamValues.add('100');
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/v4/IntegrationRecords';
        req.httpMethod = 'GET';
        
        for (Integer i = 0; i < lParamNames.size(); i++)
        {
            req.addParameter(lParamNames[i], lParamValues[i]);
        }
        
        RestContext.request = req;
        RestContext.response = res;
            Integer intLimit = EP_MonitorExportLibClass.returnQueryLimitFromRequest(EP_Common_Constant.limitException);
            Date dSearchDate = EP_MonitorExportLibClass.returnQueryDateFilterFromRequest(EP_Common_Constant.TO_DATE_TIME_FILTER_NAME);
            system.assert(intLimit==100);
            system.assertEquals(system.today(),dSearchDate);
        Test.stopTest();
    }
}