public with sharing class EP_Import_Retail_Order_Context {
    public String strXMLFileContents {get;set;}
    public String strXMLFileName {get;set;}
    public String strCSVColumnSquence {get;set;}
    public Transient Blob blbXMLFile {get;set;}
    public Boolean CSVParseSuccess{get;set;}
    public string strErrorMessage{get;set;}
    public EP_File__c ctxfileRecord;
    
    /* 
        Constructor
    */
    public EP_Import_Retail_Order_Context (EP_File__c fileRec) {
      ctxfileRecord = fileRec;
      system.debug('ctxfileRecord +++++++++++++++'+ctxfileRecord );
      system.debug('strXMLFileContents +++++++++++++++'+strXMLFileContents );
      
        init();
    }

    /* 
        method to init the variables
    */
    private void init() {
        strErrorMessage = '';
        CSVParseSuccess = false;
    }
}