/*   
     @Author <Accenture>
     @name <EP_FE_ErrorLogEndpointTest.cls>     
     @Description <This class is the test class for EP_FE_ErrorLogEndpoint>   
     @Version <1.1> 
*/
@isTest
Private class EP_FE_ErrorLogEndpointTest{

    private static User sysAdmUser = EP_FE_TestDataUtility.getRunAsUser();

    /*********************************************************************************************
    *@Description : This is the Test method to create Exception Record from Rest Service .                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
      static testMethod void testLogException(){
        
        List<EP_FE_ErrorLogRequest> objLogReqList = new List<EP_FE_ErrorLogRequest>();
        EP_FE_ErrorLogRequest objLogReq = new EP_FE_ErrorLogRequest();

        System.runAs(sysAdmUser){
            Test.startTest();
            objLogReq.componentName= 'test';
            objLogReq.PerformedAction = 'test';
            objLogReq.description= 'test';
            objLogReq.errorDetail= 'test';
            objLogReq.wasHandled= true;
            objLogReq.browser = 'test';       
            objLogReq.device = 'test';
            
            objLogReqList.add(objLogReq);

            RestRequest req = new RestRequest(); 

            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/error/log/' ;      
            RestContext.request = req;

            EP_FE_ErrorLogEndpoint.logError(objLogReqList);                  
            EP_Exception_Log__c exc = [select EP_Component__c from EP_Exception_Log__c limit 20000];  
            Test.stopTest();  
            System.assertEquals(exc.EP_Component__c,'test');
        }
      }    
  /*********************************************************************************************
    *@Description : This is the Test method to pass Null Exception Record from Rest Service .                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    static testMethod void testLogExceptionNull(){

        //List<EP_FE_ErrorLogRequest> objLogReqList = new List<EP_FE_ErrorLogRequest>();
        
        Test.startTest();
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest(); 

        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
        req.requestURI = '/services/apexrest/FE/V1/error/log/' ;      
        RestContext.request = req;
        
        Integer exc = [select count() from EP_Exception_Log__c limit 10000];  
        Test.stopTest();   
        System.assertEquals(exc, 0);  
                             
      }     
      
 /*********************************************************************************************
    *@Description : This is the Test method to pass Null Exception Record from Rest Service .                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    static testMethod void testLogExceptionCatch(){

        List<EP_FE_ErrorLogRequest> objLogReqList = new List<EP_FE_ErrorLogRequest>();
        objLogReqList = null; 

        Test.startTest();   
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest(); 

        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
        req.requestURI = '/services/apexrest/FE/V1/error/log/' ;      
        RestContext.request = req;
             
        EP_FE_ErrorLogEndpoint.logError(objLogReqList); 
        
        Integer exc = [select count() from EP_Exception_Log__c limit 10000];    
        Test.stopTest(); 
        System.assertEquals(exc, 0);   
                
      }                
}