/* 
   @Author <Vikas Malik>
   @name <EP_ChangeRequestService>
   @CreateDate <05/01/2016>
   @Description <This class acts as the helper class for EP_ChangeRequestController VF page>
   @Version <1.0>
*/
public without sharing class EP_ChangeRequestService {
    private static final String CLASS_NAME = 'EP_ChangeRequestService';
    private static final string ADD_FVALUE_INSOBJECT = 'addFieldValueInSObject';
   
    /*
     * this method adds field value based on field Type for sObject
     */
    public static void addFieldValueInSObject(sObject sObjRec, string fieldAPIName, string fieldvalue, Map<String, Schema.SObjectField> fieldMap){EP_GeneralUtility.Log('public','EP_ChangeRequestService','addFieldValueInSObject'); 
        Schema.DisplayType fielddataType = fieldMap.get(fieldAPIName).getDescribe().getType();
        try{
            if(fielddataType.equals(Schema.DisplayType.Double)){
               sObjRec.put(fieldAPIName, Double.valueOf(fieldvalue));
            }else if(fielddataType.equals(Schema.DisplayType.Currency)){
                sObjRec.put(fieldAPIName, Decimal.valueOf(fieldvalue));
            }else if(fielddataType.equals(Schema.DisplayType.boolean)){
                sObjRec.put(fieldAPIName, boolean.valueOf(fieldvalue));
            }else if(fielddataType.equals(Schema.DisplayType.DATE)){
                sObjRec.put(fieldAPIName, date.parse(fieldvalue));
            }else if(fielddataType.equals(Schema.DisplayType.DATETIME)){
                sObjRec.put(fieldAPIName, dateTime.parse(fieldvalue));
            }else{
                sObjRec.put(fieldAPIName, fieldvalue);
            }
        }
        Catch(Exception e){
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, ADD_FVALUE_INSOBJECT, CLASS_NAME, ApexPages.Severity.ERROR);
        }      
    }
}