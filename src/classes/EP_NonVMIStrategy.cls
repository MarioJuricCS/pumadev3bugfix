/**
* @Author      : Kalpesh Thakur<kalpesh.j.thakur@accenture.com>
* @name        : EP_NonVMIStrategy
* @CreateDate  : 31/01/2017
* @Description : This class executes logic for NonVMI Orders
* @Version     : <1.0>
* @reference   : N/A
*/

public class EP_NonVMIStrategy extends EP_VendorManagement {

	EP_AccountMapper accountMapper = new EP_AccountMapper();
	EP_PaymentTermMapper paymentTermMapper = new EP_PaymentTermMapper();

	public EP_NonVMIStrategy() {
	}
	/** This method is used to update Payment Fields on ORder for NonVMI Orders
	*  @date     01/02/2017
	*  @name     updatePaymentTermAndMethod
	*  @param    List<Order>
	*  @return   NA
	*  @throws   NA
	*/
	public override void doUpdatePaymentTermAndMethod(csord__Order__c orderObj) {
		EP_GeneralUtility.Log('Public','EP_NonVMIStrategy','doUpdatePaymentTermAndMethod');
		Map<String, EP_Customer_Support_Settings__c> localSupportNumbers = EP_Customer_Support_Settings__c.getAll();
		// Query Account Records
		System.debug('*****orderObj.AccountId:-'+orderObj.AccountId__c);
		Account objAccount = accountMapper.getAccountRecordById(orderObj.AccountId__c);
		Map<String,EP_Payment_Term__c> mapPayTerm = new Map<String,EP_Payment_Term__c>();
		List<EP_Payment_Term__c> lstPTerm = new List<EP_Payment_Term__c>();
		//Looping through the Order Records to get AccountIds
		for( EP_Payment_Term__c objPayTerm : paymentTermMapper.getAllPaymentTerms()){
			mapPayTerm.put(objPayTerm.Name,objPayTerm);
		}
		// Looping through the records to update the Payment fields
		if(objAccount!= null && objAccount.EP_Bill_To_Account__c != null ) {
			orderObj.EP_Email__c = objAccount.EP_Email__c; 
			setPaymentFields(orderObj,objAccount.EP_Bill_To_Account__r, localSupportNumbers);
		}
		else if (objAccount!= null) {
			orderObj.EP_Email__c = objAccount.EP_Email__c; 
			setPaymentFields(orderObj,objAccount, localSupportNumbers);
		}
	} 


	/** This method is used to update Payment Fields for Bill-To and Sell-TO Accounts
	*  @date     01/02/2017
	*  @name     setPaymentFields
	*  @param    Order orderObj,Account objAcc,Map<String,EP_Payment_Term__c> mapPayTerm,
	*				String strPaymentTerm,Map<String, EP_Customer_Support_Settings__c> localSupportNumbers
	*  @return   NA
	*  @throws   NA
	*/
	private void setPaymentFields(csord__Order__c orderObj,Account objAcc,Map<String, EP_Customer_Support_Settings__c> localSupportNumbers) {
		EP_GeneralUtility.Log('Public','EP_NonVMIStrategy','setPaymentFields');
		orderObj.BillingCountry__c = objAcc.BillingCountry;
		orderObj.BillingCity__c = objAcc.BillingCity;    
		orderObj.EP_Billing_Basis__c = objAcc.EP_Billing_Basis__c;
		orderObj.EP_Price_Consolidation_Basis__c = objAcc.EP_Price_Consolidation_Basis__c;

		// set Local Number
		super.setLocalNumberonOrder(localSupportNumbers.values(),objAcc,orderObj);

		// set PaymentTerm Value
		setPaymentTermValue(objAcc, orderObj);

		//sets payment term for packaged order 
		super.setPaymntTermValForPckgOdr(objAcc.EP_Package_Payment_Term__c,orderObj);
	}



	/** This method is used to set Payment Term value on Order
	*  @date     01/02/2017
	*  @name     setPaymentTermValue
	*  @param    String strPaymentTerm,Account objAcc,Order orderObj,
	*	Map<String,EP_Payment_Term__c> mapPayTerm
	*  @return   NA
	*  @throws   NA
	*/
	private void setPaymentTermValue(Account objAcc,csord__Order__c orderObj) {
		EP_GeneralUtility.Log('Public','EP_NonVMIStrategy','setPaymentTermValue');
		EP_PaymentTermMapper paymentTermMapper = new EP_PaymentTermMapper();
		if(orderObj.EP_Payment_Term__c != EP_Common_Constant.PREPAYMENT && orderObj.EP_Payment_Term__c <> null) {
			EP_Payment_Term__c objPayTerm = paymentTermMapper.getRecordsByName(objAcc.EP_Payment_Term_Lookup__r.name);
			orderObj.EP_Payment_Term_Value__c = objPayTerm.Id;
		}
		else if (orderObj.EP_Payment_Term__c == EP_Common_Constant.PREPAYMENT) {
			EP_Payment_Term__c objPayTerm = paymentTermMapper.getRecordsByName(EP_Common_Constant.Pre_Payment);
			orderObj.EP_Payment_Term_Value__c = objPayTerm.Id;	
		}
		else if (orderObj.EP_Payment_Term__c == null || orderObj.EP_Payment_Term__c == EP_Common_Constant.BLANK) {
			EP_Payment_Term__c objPayTerm = paymentTermMapper.getRecordsByName(objAcc.EP_Payment_Term_Lookup__r.name);
			orderObj.EP_Payment_Term_Value__c = objPayTerm.Id;
		}
	}


	/**  to fetch the record type of order
	*  @date      05/02/2017
	*  @name      findRecordType
	*  @param     NA
	*  @return    Id 
	*  @throws    NA
	*/
	public override Id findRecordType() {
		EP_GeneralUtility.Log('Public','EP_NonVMIStrategy','findRecordType');
		return EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ORDER, EP_Common_Constant.NONVMI_ORDER_RECORD_TYPE_NAME);
	}
}