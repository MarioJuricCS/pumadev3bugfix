/* ================================================
 * @Class Name : EP_CRM_PipelineReportBean 
 * @author : TCS
 * @Purpose: This model class is used to prepare report contents for opportunity volume.
 * @created date: 15/11/2017
 ================================================*/
public with sharing class EP_CRM_PipelineReportBean extends EP_CRM_UserAuthorization {
	// It is used to take filter input for filtering report data
    public Opportunity opportunityObj { get {
    	if(opportunityObj == null) {
    		opportunityObj = new Opportunity();	
    	} 
    	return opportunityObj;
    } set; }
    
    // Returns list of wrapper object..holds all reports information which is used populates the volume reports
    public List < EP_CRM_PipelineReportWrapper > wrapperList { get {
    	if(wrapperList == null) {
    		wrapperList = new List < EP_CRM_PipelineReportWrapper > ();
    	}
    	return wrapperList;
    } set; }
	
	// It returns report run date value as per year...default to current system date
	public Date reportDateValue { get {
		if(reportDateValue == null) {
			reportDateValue = System.today();
		} else if (Integer.valueOf(selectedDateRange) > system.today().year()) {
			reportDateValue = Date.newInstance(Integer.valueOf(selectedDateRange), 01, 01);
		} else if (Integer.valueOf(selectedDateRange) < system.today().year()) {
			reportDateValue = Date.newInstance(Integer.valueOf(selectedDateRange), 12, 31);
		} else if (Integer.valueOf(selectedDateRange) == system.today().year()) {
			reportDateValue = System.today();
		}
		return reportDateValue;
	} set; }

	// Returns map of all months in string with Integer value as key
    public Map < Integer, String > getMonthInString() {
    	Map < Integer, String > monthInString = new Map < Integer, String > ();
    	monthInString.put(1, 'Jan-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(2, 'Feb-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(3, 'Mar-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(4, 'Apr-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(5, 'May-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(6, 'Jun-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(7, 'Jul-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(8, 'Aug-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(9, 'Sep-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(10, 'Oct-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(11, 'Nov-' + String.valueOf(reportDateValue.year()).right(2));
    	monthInString.put(12, 'Dec-' + String.valueOf(reportDateValue.year()).right(2));
    	return monthInString;
    }

	// Default Constructor
    public EP_CRM_PipelineReportBean() {
    	// Write initialization part here...
    }
    
    // Parameterized constructor as per logged in user 
    public EP_CRM_PipelineReportBean(User loggedInUserObj) {
    	this.loggedInUserObj = loggedInUserObj;
    	setDefaultValues();
    } 
    
    // It prepares all default values required before loading reports first time.
    private void setDefaultValues() {
    	if(getRegionalExecutiveUser()) {
    		opportunityObj.EP_CRM_Geo1__c = getGeo1();
    	} else if(!getRegionalExecutiveUser() && !getGlobalUser()) {
    		opportunityObj.EP_CRM_Geo1__c = getGeo1();
    		opportunityObj.EP_CRM_Geo2__c = getGeo2();
    	}
    	// Set default UoM value
    	if(getGlobalUser()) {
    		selectedUoM = EP_CRM_Constants.CUBIC_MTR;
    	} else if(getAmericaUser()) {
    		selectedUoM = EP_CRM_Constants.GALLON;
    	} else if(getAfricaUser()) {
    		selectedUoM = EP_CRM_Constants.LITRE;
    	} else if(getAustraliaUser()) {
    		selectedUoM = EP_CRM_Constants.LITRE;
    	}
    	// Get all customer segement names and check with role name
    	opportunityObj.EP_CRM_Customer_Segment__c = getRoleSegmentValue();
    	if(String.isBlank(opportunityObj.EP_CRM_Customer_Segment__c)) {
    		opportunityObj.EP_CRM_Customer_Segment__c = EP_CRM_Constants.RETAIL;
    	} 	
    	if(opportunityObj.EP_CRM_Customer_Segment__c == EP_CRM_Constants.HIGH_STREET) {
			opportunityObj.EP_CRM_Product_Interest_Group__c = EP_CRM_Constants.LUBRICANTS;
		} else {
			opportunityObj.EP_CRM_Product_Interest_Group__c = EP_CRM_Constants.FUELS;
		}
    }
    
    // Returns selected date range (In Years only)...If blank then sets to current year.
    public String selectedDateRange { get {
    	if(String.isBlank(selectedDateRange)) {
    		selectedDateRange = String.valueOf(System.today().year());
    	} 
    	return selectedDateRange;
    } set; }
    
    // Returns date range options (last 2 years + current year + future 2 years)
    public List < SelectOption > getDateRangeOptions() {
    	List < SelectOption > options = new List < SelectOption > ();
    	Integer currentYear = System.today().year(); 
    	for(Integer i=currentYear - 2; i <= currentYear + 2; i++) {
    		options.add(new SelectOption(String.valueOf(i), String.valueOf(i)));	
    	}
    	return options;
    }
    
    // Variable to hold selected UoM value
    public String selectedUoM { get; set; }
    
    // Returns list of UoM options
    public List < SelectOption > getUoMOptions() {
    	List < SelectOption > options = new List < SelectOption > ();
    	options.add(new SelectOption(EP_CRM_Constants.BLANK, EP_CRM_Constants.NONE));
    	options.add(new SelectOption(EP_CRM_Constants.LITRE, EP_CRM_Constants.LITRE));
    	options.add(new SelectOption(EP_CRM_Constants.GALLON, EP_CRM_Constants.GALLON));
    	options.add(new SelectOption(EP_CRM_Constants.CUBIC_MTR, EP_CRM_Constants.CUBIC_MTR));
    	options.add(new SelectOption(EP_CRM_Constants.METRIC_TON, EP_CRM_Constants.METRIC_TON));
    	options.add(new SelectOption(EP_CRM_Constants.KILOGRAM, EP_CRM_Constants.KILOGRAM));
    	options.add(new SelectOption(EP_CRM_Constants.UNIT, EP_CRM_Constants.UNIT));
    	return options;
    }
    
    // Returns true is BITUMEN product is selected 
    public Boolean getBitumenSelected() {
    	if(String.isNotBlank(opportunityObj.EP_CRM_Product_Interest_Group__c)) {
	    	if((opportunityObj.EP_CRM_Product_Interest_Group__c).contains(EP_CRM_Constants.BITUMEN)) {
	    		return true;
	    	}	
    	}
    	return false;
    }

	/****** Generates Header for dynamic month ********/
    public Map < Integer, Decimal > getOpenOpportunityMap () {
    	Map < Integer, Decimal > openOpportunityMap = new Map < Integer, Decimal > ();
    	for(Integer i = reportDateValue.month(); i <= 12; i++) {
    		if(reportDateValue >= system.today()) {
    			openOpportunityMap.put(i, 0);
    		}
    	}
    	return openOpportunityMap;
    }
    
    /****** Generates Header for dynamic month ********/
    public Map < Integer, Decimal > getClosedOpportunityMap () {
    	Map < Integer, Decimal > closedOpportunityMap = new Map < Integer, Decimal > ();
    	for(Integer i = 1; i <= reportDateValue.month(); i++) {
    		closedOpportunityMap.put(i, 0);
    	}
    	return closedOpportunityMap;
    }
    
    /************* Populates total of months *************/
    public Map < Integer, Decimal > getTotalOpenOpportunityAmountMap () {
    	Map < Integer, Decimal > totalOpenOpportunityAmountMap = getOpenOpportunityMap();
    	for(EP_CRM_PipelineReportWrapper wrapperObj: wrapperList) {
    		for(Integer month: wrapperObj.openOpportunityMap.keySet()) {
    			totalOpenOpportunityAmountMap.put(month, (totalOpenOpportunityAmountMap.get(month) + wrapperObj.openOpportunityMap.get(month)));
    		}
    	}
    	return totalOpenOpportunityAmountMap;
    }
    
    /************* Populates total of months *************/
    public Map < Integer, Decimal > getTotalClosedOpportunityAmountMap () {
    	Map < Integer, Decimal > totalClosedOpportunityAmountMap = getClosedOpportunityMap();
    	for(EP_CRM_PipelineReportWrapper wrapperObj: wrapperList) {
    		for(Integer month: wrapperObj.closedOpportunityMap.keySet()) {
    			totalClosedOpportunityAmountMap.put(month, (totalClosedOpportunityAmountMap.get(month) + wrapperObj.closedOpportunityMap.get(month)));
    		}
    	}
    	return totalClosedOpportunityAmountMap;
    }
    
    // Returns total closed won opportunities amount for report run date
    public Decimal getCurrentMonthTotalClosedAmount() {
    	Decimal totalAmount = 0;
    	for(EP_CRM_PipelineReportWrapper wrapperObj: wrapperList) {
    		if(wrapperObj.closedOpportunityMap.get(reportDateValue.month()) != null) {
    			totalAmount = totalAmount + wrapperObj.closedOpportunityMap.get(reportDateValue.month());
    		}
    	}
    	return totalAmount;
    }
    
    // Returns total open opportunities amount for each month
    public Decimal getTotalOpenOpportunityAmount() {
    	Decimal totalOpenOpportunityAmount = 0;
    	for(EP_CRM_PipelineReportWrapper wrapperObj: wrapperList) {
    		totalOpenOpportunityAmount = totalOpenOpportunityAmount + wrapperObj.remainingForecastAmount;
    	}
    	return totalOpenOpportunityAmount;
    }
    
    // Returns total closed won opportunities amount for each month
    public Decimal getTotalClosedOpportunityAmount() {
    	Decimal totalClosedOpportunityAmount = 0;
    	for(EP_CRM_PipelineReportWrapper wrapperObj: wrapperList) {
    		totalClosedOpportunityAmount = totalClosedOpportunityAmount + wrapperObj.closedWonAmount;
    	}
    	return totalClosedOpportunityAmount;
    }
    
    // Returns total full year amount (Closed won + open) opportunities for each month
    public Decimal getTotalFullYearAmount() {
    	Decimal totalFullYearAmount = 0;
    	for(EP_CRM_PipelineReportWrapper wrapperObj: wrapperList) {
    		totalFullYearAmount = totalFullYearAmount + wrapperObj.getFullYearAmount();
    	}
    	return totalFullYearAmount;
    }
}