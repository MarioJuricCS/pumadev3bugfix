/* 
  @Author <Ashok Arora>
  @name <EP_StockHoldingLocationTest>
  @CreateDate <13/11/2015>
  @Description <This is the test class for Stock Holding Location Trigger>
  @Version <1.0>
 
*/
@isTest
private class EP_StockHoldingLocationTest {
        static Account testStorageLocation;
        static Account testStorageLocation2;
        static Account testShipToAccount;
        static Account testSellToAccount;
        
    static void setup() {
        String COUNTRY_NAME = 'Australia';
        String COUNTRY_CODE = 'AU';
        String COUNTRY_REGION = 'Australia';
        //Creating ShipTo and SellTo Data
        testShipToAccount = EP_TestDataUtility.getShipToPositiveScenario();
        EP_AccountDomainObject domainObj = new EP_AccountDomainObject(testShipToAccount.ParentId);
        testSellToAccount = domainObj.localAccount;
        //Creating Country Data
        EP_Country__c testCountry = EP_TestDataUtility.createCountryRecord(COUNTRY_NAME,COUNTRY_CODE,COUNTRY_REGION);
        Database.insert(testCountry);
        //Business Hours Data
        BusinessHours testBusinessHours = [SELECT Name FROM BusinessHours WHERE IsActive = TRUE AND IsDefault = TRUE LIMIT: EP_Common_Constant.ONE];
        //Creating Storage Location Data
        testStorageLocation = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);
        Database.insert(testStorageLocation);
        testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(testStorageLocation);
        testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(testStorageLocation);
        testStorageLocation2 = EP_TestDataUtility.createStorageLocAccount(testCountry.Id, testBusinessHours.Id);
        Database.insert(testStorageLocation2);
        testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        Database.update(testStorageLocation2);
        testStorageLocation.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        Database.update(testStorageLocation2);    
    }
    static testMethod void validateSupplyOption_InsertTest(){
        setup();
        Test.startTest();
        EP_StorageLocationTriggerHandler.executeStorageLocationTrigger = true;
        Id strSupplyLocationSellToRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', EP_Common_Constant.EX_RACK_REC_TYPE_NAME);  
        EP_Stock_Holding_Location__c testSellToSupplyOption1 = EP_TestDataUtility.createSellToStockLocation(testSellToAccount.Id,False,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);
        Database.insert(testSellToSupplyOption1);
        Test.stopTest();
    }
    static testMethod void validateSupplyOption_UpdateTest(){
        setup();
        Test.startTest();
        EP_StorageLocationTriggerHandler.executeStorageLocationTrigger = true;
        Id strSupplyLocationSellToRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    
        EP_Stock_Holding_Location__c testSellToSupplyOption2 = EP_TestDataUtility.createSellToStockLocation(testSellToAccount.Id,False,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);
        Database.insert(testSellToSupplyOption2);
        testSellToSupplyOption2.EP_Is_Pickup_Enabled__c = false;
        Database.update(testSellToSupplyOption2);
        Test.stopTest();
    }
    static testMethod void validateShipToSupplyOption_Test(){
        setup();
        Test.startTest();
        EP_StorageLocationTriggerHandler.executeStorageLocationTrigger = true;
        Id strSupplyLocationSellToRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    
        Id strSupplyLocationDeliveryRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.DLVRY_REC_TYPE_NAME);
        EP_Stock_Holding_Location__c testSellToSupplyOption1 = EP_TestDataUtility.createSellToStockLocation(testSellToAccount.Id,False,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);
        Database.insert(testSellToSupplyOption1);
        EP_Stock_Holding_Location__c testShipToToSupplyOption2 = EP_TestDataUtility.createShipToStockLocation(testShipToAccount.Id,false,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);
        Database.insert(testShipToToSupplyOption2);
        Test.stopTest();
    }
    static testMethod void deleteShipToSupplyOption_Test(){
        setup();        
        EP_StorageLocationTriggerHandler.executeStorageLocationTrigger = true;
        Id strSupplyLocationSellToRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    
        Id strSupplyLocationDeliveryRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.DLVRY_REC_TYPE_NAME);
        EP_Stock_Holding_Location__c testSellToSupplyOption1 = EP_TestDataUtility.createSellToStockLocation(testSellToAccount.Id,False,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);
        Database.insert(testSellToSupplyOption1);
        EP_Stock_Holding_Location__c testShipToToSupplyOption2 = EP_TestDataUtility.createShipToStockLocation(testShipToAccount.Id,false,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);
        Database.insert(testShipToToSupplyOption2);
        Test.startTest();
            Database.delete(testShipToToSupplyOption2);
        Test.stopTest();
    }
    //Duplicate Supply Option check
    static testMethod void validateSellToSupplyOption_DuplicateTest(){
        setup();
        try{
            Test.startTest();
            EP_StorageLocationTriggerHandler.executeStorageLocationTrigger = true;
            Id strSupplyLocationSellToRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    
            Id strSupplyLocationDeliveryRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.DLVRY_REC_TYPE_NAME);
            EP_Stock_Holding_Location__c testSellToSupplyOption1 = EP_TestDataUtility.createSellToStockLocation(testSellToAccount.Id,False,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);
            Database.insert(testSellToSupplyOption1);
            EP_Stock_Holding_Location__c testShipToToSupplyOption2 = EP_TestDataUtility.createSellToStockLocation(testSellToAccount.Id,false,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);            
            Database.insert(testShipToToSupplyOption2);       
            Test.stopTest();
        }catch(Exception e){            
            system.assert(e.getMessage().contains(system.Label.EP_Duplicate_Stock_Holding_Location));
        }
    }
    //Supply Location not assigned to SellTo
    static testMethod void validateShipToSupplyOption_Test2(){
        setup();    
        EP_StorageLocationTriggerHandler.executeStorageLocationTrigger = true;
        try{
            Test.startTest();
            Id strSupplyLocationSellToRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    
            Id strSupplyLocationDeliveryRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.DLVRY_REC_TYPE_NAME);
            EP_Stock_Holding_Location__c testSellToSupplyOption1 = EP_TestDataUtility.createSellToStockLocation(testSellToAccount.Id,False,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);
            Database.insert(testSellToSupplyOption1);
            EP_Stock_Holding_Location__c testShipToToSupplyOption2 = EP_TestDataUtility.createShipToStockLocation(testShipToAccount.Id,false,testStorageLocation2.Id,strSupplyLocationSellToRecordTypeId);
            Database.insert(testShipToToSupplyOption2);
            Test.stopTest();
        }catch(Exception e){            
            system.assert(e.getMessage().contains(system.Label.EP_Supply_Location_on_Ship_To_Not_Assigned_on_Sell_To_Error));
        }
    }
    //Deleting primary supply option
    static testMethod void deleteShipToSupplyOption_Test2(){
        setup();        
        EP_StorageLocationTriggerHandler.executeStorageLocationTrigger = true;
        Id strSupplyLocationSellToRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c', EP_Common_Constant.EX_RACK_REC_TYPE_NAME);    
        Id strSupplyLocationDeliveryRecordTypeId = EP_Common_Util.fetchRecordTypeId('EP_Stock_Holding_Location__c',EP_Common_Constant.DLVRY_REC_TYPE_NAME);
        EP_Stock_Holding_Location__c testSellToSupplyOption1 = EP_TestDataUtility.createSellToStockLocation(testSellToAccount.Id,False,testStorageLocation.Id,strSupplyLocationSellToRecordTypeId);
        Database.insert(testSellToSupplyOption1);
        EP_Stock_Holding_Location__c testSellToSupplyOption2 = [Select Id from EP_Stock_Holding_Location__c Where EP_Sell_To__c=:testSellToAccount.id AND Stock_Holding_Location__c =:testStorageLocation.id Limit 1];
        try{
            Test.startTest();
                Database.delete(testSellToSupplyOption2);
            Test.stopTest();
        }catch(Exception e){            
            system.assert(e.getMessage().contains(system.Label.EP_On_Supply_Location_Deletion));
        }
    }
}