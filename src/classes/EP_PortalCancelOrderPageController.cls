public with sharing class EP_PortalCancelOrderPageController {

    /*
    @Author      CloudSense
    @name        cancelOrder
    @CreateDate  27/02/2018
    @Description Method to cancel an order (from Portal)
    @Version     1.0
    */
    
    @RemoteAction
    public static String cancelOrder(String orderId){

        try{
            String result = '';

            if(orderId != '' && orderId != null){

                List<csord__Order__c> orderList = [
                    SELECT Id, EP_Order_Locked__c, Cancellation_Check_Done__c, EP_CutOff_Check_Required__c, csord__Status2__c
                    FROM csord__Order__c WHERE Id =:orderId LIMIT 1];

                if(orderList != null && orderList.size() > 0){
                    csord__Order__c orderToCancel = orderList[0];

                    Boolean isCutOff = false;

                    if(orderToCancel.EP_Order_Locked__c){
                        result = Label.EP_Order_Cancel_Msg;
                    //} else if(orderToCancel.csord__Status2__c == 'Draft'){
                    //    result = Label.EP_Order_Draft;
                    } else if(orderToCancel.Cancellation_Check_Done__c){
                        result = Label.EP_Order_Already_Checked;
                    } else if(orderToCancel.EP_CutOff_Check_Required__c == 'Yes'){
                        Datetime localDateTime = datetime.now();
                        String localDate = localDateTime.format('MM/dd/YYYY');
                        String localTime = localDateTime.format('HH:mm:ss');
                        TimeZone tz = UserInfo.getTimeZone();
                        String localTimeZoneOffset = tz.getOffset(localDateTime)/60000*(-1)+''; //Convert milliseconds to minutes

                        result = Label.EP_Cut_off_Matrix_Failed;
                        isCutOff = TrafiguraConfigurationControllerNew.processCutOffMatrixCheck(orderId, localTime, localDate, localTimeZoneOffset); 
						system.debug('isCutOffcheck'+isCutOff);
                    } else {
                        isCutOff = true;
                    }

                    if(isCutOff){
                        orderToCancel.Cancellation_Check_Done__c = true;
                        update orderToCancel;
                        result = Label.EP_Cut_off_Matrix_Passed;
                    } 
                } else {
                    result = string.format(Label.EP_Order_Id_not_found, new List<String>{orderId});
                }
            } else {
                result = Label.EP_Order_Id_cannot_be_null;
            }

            return result;
        }
        catch(Exception e){
            EP_LoggingService.logServiceException(e, UserInfo.getOrganizationId(), EP_Common_constant.EPUMA, 'cancelOrder', 'EP_PortalCancelOrderPageController', EP_Common_constant.ERROR, UserInfo.getUserId(), EP_Common_constant.TARGET_SF,  EP_Common_Constant.BLANK, EP_Common_Constant.BLANK);
            system.debug('CancelOrder error: '+e.getStackTraceString());
            return 'Process error.';
        }
    }
    
}