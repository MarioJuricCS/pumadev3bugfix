/* 
  @Author <CloudSense>
   @name <TrafiguraEditConfigurationController>
   @CreateDate <10/12/2017>
   @Description Class used to forward to Edit Configuration Page
   @Version <1.0>
 */

global class TrafiguraEditConfigurationController {
    
public csord__Order__c  order{get;set;}

     global TrafiguraEditConfigurationController() {
         
     }
	 
	public TrafiguraEditConfigurationController(ApexPages.StandardController controller) {

    }
	
	/* @name <openEditOrderPagefromLineItem >
    * @date  <31/07/2018>
    * @description <method to forward to the edit configuration page>
    * @param NA
    * @return String: PageReference
    */
     public PageReference openEditOrderLinePageFromStandard(){ 
     try{    
         
        csord__Order_Line_Item__c orderLineItem = [Select Id, csord__Order__c From csord__Order_Line_Item__c where Id = :ApexPages.currentPage().getParameters().get('id') limit 1];         
        order = [SELECT 
                        Id, Name,csord__Identification__c,csordtelcoa__Product_Configuration__c, EP_Is_Credit_Exception_Cancelled_Sync__c, Cancellation_Check_Done__c,EP_Order_Locked__c
                   FROM 
                        csord__Order__c
    
                    WHERE 
                        Id = :orderLineItem.csord__Order__c];
        if (order.Cancellation_Check_Done__c){
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Can not edit a cancelled Order'));
          PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
          return null;
        }
        
        if (!order.Cancellation_Check_Done__c && order.EP_Order_Locked__c) {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'You can not edit this Order'));
          PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
          return null;
        }       
      
       } 
       catch(Exception handledException){
           EP_loggingService.loghandledException(handledException, EP_Common_Constant.EPUMA, 'openEditOrderPageFromStandard', 'TrafiguraEditConfigurationController',apexPages.severity.ERROR);
      }
        string pageRef = openEditOrderPageWithId2(order.Id);
        PageReference pr = new PageReference(pageRef);
        return pr;
    } 
    
    /* @name <openEditOrderPage >
    * @date  <13/07/2018>
    * @description <method to forward to the edit configuration page>
    * @param NA
    * @return String: PageReference
    */
     public PageReference openEditOrderPageFromStandard(){ 
     try{    
          order = [SELECT 
                        Id, Name,csord__Identification__c,csordtelcoa__Product_Configuration__c, EP_Is_Credit_Exception_Cancelled_Sync__c, Cancellation_Check_Done__c,EP_Order_Locked__c
                   FROM 
                        csord__Order__c
    
                    WHERE 
                        Id = :ApexPages.currentPage().getParameters().get('id')];
        if (order.Cancellation_Check_Done__c){
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Can not edit a cancelled Order'));
          PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
          return null;
        }
        
        if (!order.Cancellation_Check_Done__c && order.EP_Order_Locked__c) {
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'You can not edit this Order'));
          PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
          return null;
        }       
      
       } 
       catch(Exception handledException){
           EP_loggingService.loghandledException(handledException, EP_Common_Constant.EPUMA, 'openEditOrderPageFromStandard', 'TrafiguraEditConfigurationController',apexPages.severity.ERROR);
      }
        string pageRef = openEditOrderPageWithId2(order.Id);
        PageReference pr = new PageReference(pageRef);
        return pr;
    } 
     
    /* @name <openEditOrderPage >
     * @date  <10/12/2017>
     * @description <method to forward to the edit configuration page>
     * @param NA
     * @return String: PageReference
    */
     public PageReference openEditOrderPage(){ 
     try{    
          order = [SELECT 
                        Id, Name,csord__Identification__c,csordtelcoa__Product_Configuration__c, EP_Is_Credit_Exception_Cancelled_Sync__c, Cancellation_Check_Done__c
                   FROM 
                        csord__Order__c
    
                    WHERE 
                        Id = :ApexPages.currentPage().getParameters().get('id')];
        if(order.Cancellation_Check_Done__c){
          ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Can not cancel a cancelled Order'));
          PageReference pr = new PageReference(ApexPages.currentPage().getParameters().get('retUrl'));
          return null;
        }
        system.debug('order is :' +order);
        
        order.EP_Is_Credit_Exception_Cancelled_Sync__c = true;
        update order;
       } catch(Exception handledException){
           EP_loggingService.loghandledException(handledException, EP_Common_Constant.EPUMA, 'openEditOrderPage', 'TrafiguraEditConfigurationController',apexPages.severity.ERROR);
      }
    PageReference pr=new PageReference ('/apex/customconfiguration?configId='+order.csordtelcoa__Product_Configuration__c+'&linkedId='+order.csord__Identification__c);
    return pr;
    }

    /* @name <openEditOrderPageWithId >
     * @date  <10/12/2017>
     * @description <method to forward to the edit configuration page>
     * @param Id
     * @return String: PageReference
    */
     webservice static String openEditOrderPageWithId(Id orderId){
     csord__Order__c order;
     try{    
          order = [SELECT 
                        Id, Name,csord__Identification__c,csordtelcoa__Product_Configuration__c, EP_Is_Credit_Exception_Cancelled_Sync__c, Cancellation_Check_Done__c
                   FROM 
                        csord__Order__c
    
                    WHERE 
                        Id = :orderId];
                       
        system.debug('order is :' +order);
        
        order.EP_Is_Credit_Exception_Cancelled_Sync__c = true;
        update order;
       } catch(Exception handledException){
           EP_loggingService.loghandledException(handledException, EP_Common_Constant.EPUMA, 'openEditOrderPageWithId', 'TrafiguraEditConfigurationController',apexPages.severity.ERROR);
      }
    String pr = '/apex/customconfiguration?configId='+String.valueOf(order.csordtelcoa__Product_Configuration__c)+'&linkedId='+String.valueOf(order.csord__Identification__c);
    return pr;
    }

     webservice static String openEditOrderPageWithId2(Id orderId){
     csord__Order__c order;
     String schemaId = '';
     String schemaName = 'Puma Energy Order';
     try{    
          order = [SELECT 
                        Id, Name,csord__Identification__c,csordtelcoa__Product_Configuration__c, csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c
                        ,csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name,
                        EP_Is_Credit_Exception_Cancelled_Sync__c, Cancellation_Check_Done__c
                   FROM 
                        csord__Order__c
                   WHERE 
                        Id = :orderId];
        csoe__Non_Commercial_Product_Association__c schema= [Select id,name,csoe__Non_Commercial_Schema__c from csoe__Non_Commercial_Product_Association__c where csoe__Commercial_Product_Definition__c =: order.csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c];
        if(schema != null) {
            schemaId = schema.csoe__Non_Commercial_Schema__c;
        }                       
        order.EP_Is_Credit_Exception_Cancelled_Sync__c = true;
        update order;
       } catch(Exception handledException){
           EP_loggingService.loghandledException(handledException, EP_Common_Constant.EPUMA, 'openEditOrderPageWithId', 'TrafiguraEditConfigurationController',apexPages.severity.ERROR);
      }
    String pr = '/apex/customConfigurationNew?configId='+String.valueOf(order.csordtelcoa__Product_Configuration__c)+'&linkedId='+String.valueOf(order.csord__Identification__c)+
                '&defname='+schemaName+'&defId='+String.valueOf(order.csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c)+
                '&schemaId='+schemaId;
    return pr;
    }    

}