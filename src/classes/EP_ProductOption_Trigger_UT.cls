/* 
      @Author <Kamendra Singh>
       @name <EP_ProductOption_Trigger_UT>
       @CreateDate <25/11/2016>
       @Description  <This is apex test class for EP_ProductOption_Trigger> 
       @Version <1.0>
    */
    @isTest
    public class EP_ProductOption_Trigger_UT{
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description create test data for all required object in test methods
        */
        @TestSetup
        static void initData(){
             //create trigger Setting Custom Setting
            Trigger_Settings__c TriggerSettings = new Trigger_Settings__c();
            TriggerSettings.Name = 'EP_ProductOption_Trigger';
            TriggerSettings.IsActive__c = true;
            insert TriggerSettings;
            
            //create Price Book 
            PriceBook2 tempPricebook = EP_TestDataUtility.createPriceBookWithCompany();
            
            //Create Sell To Account
            Account tempSellToAcc = EP_TestDataUtility.createSellToAccount(null,null);
            tempSellToAcc.EP_Puma_Company__c = temppricebook.EP_Company__c;
            tempSellToAcc.EP_Status__c= EP_Common_Constant.STATUS_ACTIVE;
            tempSellToAcc.CurrencyIsoCode = 'AUD';
            database.insert(tempSellToAcc);
            
            //create Ship To Account
            Account tempShipToAcc = EP_TestDataUtility.createShipToAccount(tempSellToAcc.Id,EP_TestDataUtility.VMI_SHIP_RT );
            database.insert(tempShipToAcc);
            
            //create Product2 
            Product2 tempProduct = EP_TestDataUtility.createProduct2(true); 
            
            //Create PriceBookEntry
            PricebookEntry tempPriceBookEntry = EP_TestDataUtility.createPricebookEntry(tempProduct.id,tempPricebook.id);
            tempPriceBookEntry.EP_Attached_to_Standard_Product_List__c = true;
            tempPriceBookEntry.EP_Is_Sell_To_Assigned__c = false;
            database.insert(tempPriceBookEntry);
            
            //Create Tank...
            EP_Tank__c  tempTank = EP_TestDataUtility.createTestEP_Tank(tempShipToAcc.id,tempProduct.id);
            database.insert(tempTank);
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check positive scenario of ProductOption_Trigger Before Insert Event.
        */
        static testMethod void ProductOption_Trigger_BeforeInsertEvent_PositiveTest(){
            Id tempAccountId  =  [select id from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT].id;
            Id tempPricebookId  =  [select id from PriceBook2].id;
            try{
                Test.startTest();
                //Insert Product Option
                EP_Product_Option__c objProdOption = EP_TestDataUtility.createProductOption(tempAccountId,tempPricebookId);
                Test.stopTest();
            }
            catch(exception ex){
                //Asert to check result
                System.assertEquals(ex.getMessage(),'Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, The selected Product List contains product from different currency that the one on the Customer account. Please select the correct Product list: []');
            }
            
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Negative scenario of ProductOption_Trigger Before Insert Event.
        */
        static testMethod void ProductOption_Trigger_BeforeInsertEvent_NegativeTest(){
            Id tempAccountId  =  [select id from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT].id;
            //Update Account CurrencyIsoCode
            Account tempAccount = new Account(Id=tempAccountId,CurrencyIsoCode='GBP');
            database.update(tempAccount);
            
            Id tempPricebookId  =  [select id from PriceBook2].id;
            
            try{
                Test.startTest();
                //Insert Product Option
                EP_Product_Option__c objProdOption = EP_TestDataUtility.createProductOption(tempAccountId,tempPricebookId);
                Test.stopTest();
            }
            catch(exception ex){
                //Asert to check result
                System.assertEquals(ex.getMessage(),'');
            }
            
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Positive scenario of ProductOption_Trigger Before Update Event.
        */
        static testMethod void ProductOption_Trigger_BeforeUpdatetEvent_PositiveTest(){
            Id tempAccountId  =  [select id from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT].id;
            //Update Account CurrencyIsoCode
            Account tempAccount = new Account(Id=tempAccountId,CurrencyIsoCode='GBP');
            database.update(tempAccount);
            
            Id tempPricebookId  =  [select id from PriceBook2].id;
            
            EP_Product_Option__c objProdOption = EP_TestDataUtility.createProductOption(tempAccountId,tempPricebookId);
            
            
            Product2 tempProduct = [select id from Product2];
        
            //create Price Book 
            PriceBook2 tempPricebook = EP_TestDataUtility.createPricebook2(false);
            tempPricebook.Name = 'PriceBook B';
            database.insert(tempPricebook);
            
            //Create PriceBookEntry
            PricebookEntry tempPriceBookEntry = EP_TestDataUtility.createPricebookEntry(tempProduct.id,tempPricebook.id);
            tempPriceBookEntry.EP_Attached_to_Standard_Product_List__c = true;
            tempPriceBookEntry.EP_Is_Sell_To_Assigned__c = false;
            database.insert(tempPriceBookEntry);
            
            try{
                Test.startTest();
                //update Product Option with price book
                objProdOption.Price_List__c = tempPricebook.id;
                database.update(objProdOption);
                
                Test.stopTest();
            }
            catch(exception ex){
                //Asert to check result
                System.assertEquals(ex.getMessage(),'Update failed. First exception on row 0 with id '+objProdOption.id+'; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, You are unable to remove the Product List associated to this customer as it has products that are associated to customer’s operational tank: []');
            }
            
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Negative scenario of ProductOption_Trigger Before Update Event.
        */
        static testMethod void ProductOption_Trigger_BeforeUpdatetEvent_NegativeTest(){
            Id tempAccountId  =  [select id from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT].id;
            //Update Account CurrencyIsoCode
            Account tempAccount = new Account(Id=tempAccountId,CurrencyIsoCode='GBP');
            database.update(tempAccount);
            
            Id tempPricebookId  =  [select id from PriceBook2].id;
            
            EP_Product_Option__c objProdOption = EP_TestDataUtility.createProductOption(tempAccountId,tempPricebookId);
            
            try{
                Test.startTest();
                //update Product Option with price book
                objProdOption.Price_List__c = tempPricebookId;
                database.update(objProdOption);
                
                Test.stopTest();
            }
            catch(exception ex){
                //Asert to check result
                System.assertEquals(ex.getMessage(),'');
            }
            
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Positive scenario of ProductOption_Trigger Before delete Event.
        */
        static testMethod void ProductOption_Trigger_BeforeDeleteEvent_PositiveTest(){
            Id tempAccountId  =  [select id from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT].id;
            //Update Account CurrencyIsoCode
            Account tempAccount = new Account(Id=tempAccountId,CurrencyIsoCode='GBP');
            database.update(tempAccount);
            
            Id tempPricebookId  =  [select id from PriceBook2].id;
            
            EP_Product_Option__c objProdOption = EP_TestDataUtility.createProductOption(tempAccountId,tempPricebookId);
            
            try{
                Test.startTest();
                //delete Product Option record
                database.delete(objProdOption);
                
                Test.stopTest();
            }
            catch(exception ex){
                //Asert to check result
                System.assertEquals(ex.getMessage(),'Delete failed. First exception on row 0 with id '+objProdOption.id+'; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, You are unable to remove the Product List associated to this customer as it has products that are associated to customer’s operational tank: []');
            }
            
        }
        
        /**
        * @author kamendra.singh@accenture.com
        * @date 25/11/2017
        * @description Test to check Negative scenario of ProductOption_Trigger Before delete Event.
        */
        static testMethod void ProductOption_Trigger_BeforeDeleteEvent_NegativeTest(){
            Id tempAccountId  =  [select id from Account where recordTypeId =:EP_TestDataUtility.SELLTO_RT].id;
            //Update Account CurrencyIsoCode
            Account tempAccount = new Account(Id=tempAccountId,CurrencyIsoCode='GBP');
            database.update(tempAccount);
            
            Id tempPricebookId  =  [select id from PriceBook2].id;
            
            EP_Product_Option__c objProdOption = EP_TestDataUtility.createProductOption(tempAccountId,tempPricebookId);
            
            EP_Tank__c tempTank = [select id from EP_Tank__c];
            //delete Sell To related tank records
            database.delete(tempTank);
            
            try{
                Test.startTest();
                //delete Product Option record
                database.delete(objProdOption);
                
                Test.stopTest();
            }
            catch(exception ex){
                //Asert to check result
                System.assertEquals(ex.getMessage(),'');
            }
            
        }
    }