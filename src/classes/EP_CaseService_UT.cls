/* 
      @Author <Accenture>
       @name <EP_CaseService_UT>
       @CreateDate <08/05/2018>
       @Description  <This is apex test class for EP_CaseService> 
       @Version <1.0>
    */
    @isTest
    public class EP_CaseService_UT{
    
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description create test data for all required object in test methods
        */
        @TestSetup
        static void initData(){
            //create Company
           Company__c tempCompany = EP_TestDataUtility.createCompany('12345');
           tempCompany.EP_Close_Case_Expiration__c = 7;
           insert tempCompany;
           
            //Create Sell To Account
            Account tempSellToAcc = EP_TestDataUtility.createSellToAccount(null,null);
            tempSellToAcc.EP_Puma_Company__c = tempCompany.id;
            tempSellToAcc.EP_Status__c= EP_Common_Constant.STATUS_ACTIVE;
            tempSellToAcc.CurrencyIsoCode = 'AUD';
            insert tempSellToAcc;
            
            //create case record..
            Id CaseRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('Customer Support Case').getRecordTypeId();
            BusinessHours tempBusinessHours = [select id,MondayStartTime,MondayEndTime from BusinessHours where IsDefault=true];
            Case tempCase = EP_TestDataUtility.createCase(CaseRecordTypeId);
            tempcase.AccountId = tempSellToAcc.id;
            tempcase.status = 'Open';
            tempcase.BusinessHoursId = tempBusinessHours.id;            
            insert tempcase;
        }
        
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description Test to EP_CaseService constructor.
        */
        static testMethod void EP_CaseService_Test(){
            map<Id,case> CaseTriggerOldMap = new map<Id,case>();
            list<Case> tempCase = [Select Id,Status from case];
            
            for(case objCase : tempCase){
                objCase.Status = 'Closed';
                CaseTriggerOldMap.put(objCase.id,objCase);
            }
            
            Test.startTest();
                EP_CaseDomainObject CaseDomainObject = new EP_CaseDomainObject(tempCase,CaseTriggerOldMap);
                EP_CaseService CaseService = new EP_CaseService(CaseDomainObject);
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
        }
        
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description Test to doBeforeUpdateHandle method.
        */
        static testMethod void doBeforeUpdateHandle_Test(){
            map<Id,case> CaseTriggerOldMap = new map<Id,case>();
            
            list<Case> tempCase = [Select Id,Status,BusinessHoursId,AccountId,closeddate from case];
            list<case> updateCase = new list<Case>();
            for(case objCase : tempCase){
                CaseTriggerOldMap.put(objCase.id,objCase);
                case Caseobj = new case(id=objCase.id,Status='Open',BusinessHoursId=objCase.BusinessHoursId,AccountId=objCase.AccountId);
                updateCase.add(Caseobj);
            }
            
            Test.startTest();
                EP_CaseDomainObject CaseDomainObject = new EP_CaseDomainObject(updateCase,CaseTriggerOldMap);
                EP_CaseService CaseService = new EP_CaseService(CaseDomainObject);
                CaseService.doBeforeUpdateHandle();
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
            
        }
        
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description Test to getmapIdToBusinessHours method.
        */
        static testMethod void getmapIdToBusinessHours_Test(){
            map<Id,case> CaseTriggerOldMap = new map<Id,case>();
            
            list<Case> tempCase = [Select Id,Status,BusinessHoursId,AccountId,closeddate from case];
            list<case> updateCase = new list<Case>();
            for(case objCase : tempCase){
                CaseTriggerOldMap.put(objCase.id,objCase);
                case Caseobj = new case(id=objCase.id,Status='Closed',BusinessHoursId=objCase.BusinessHoursId,AccountId=objCase.AccountId);
                updateCase.add(Caseobj);
            }
            
            Test.startTest();
                EP_CaseDomainObject CaseDomainObject = new EP_CaseDomainObject(updateCase,CaseTriggerOldMap);
                EP_CaseService CaseService = new EP_CaseService(CaseDomainObject);
                CaseService.getmapIdToBusinessHours(updateCase,CaseTriggerOldMap);
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
            
        }
        
        /**
        * @author Accenture
        * @date 08/05/2018
        * @description Test to getMapIdToAcc method.
        */
        static testMethod void getMapIdToAcc_Test(){
            map<Id,case> CaseTriggerOldMap = new map<Id,case>();
            
            list<Case> tempCase = [Select Id,Status,BusinessHoursId,AccountId,closeddate from case];
            list<case> updateCase = new list<Case>();
            for(case objCase : tempCase){
                CaseTriggerOldMap.put(objCase.id,objCase);
                case Caseobj = new case(id=objCase.id,Status='Closed',BusinessHoursId=objCase.BusinessHoursId,AccountId=objCase.AccountId);
                updateCase.add(Caseobj);
            }
            
            Test.startTest();
                EP_CaseDomainObject CaseDomainObject = new EP_CaseDomainObject(updateCase,CaseTriggerOldMap);
                EP_CaseService CaseService = new EP_CaseService(CaseDomainObject);
                CaseService.getMapIdToAcc(updateCase,CaseTriggerOldMap);
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
            
        }
        
         /**
        * @author Accenture
        * @date 08/05/2018
        * @description Test to getMapIdToAcc method.
        */
        static testMethod void populateCloseCaseExpiration_Test(){
            map<Id,case> CaseTriggerOldMap = new map<Id,case>();
            
            list<Case> tempCase = [Select Id,Status,BusinessHoursId,AccountId,closeddate from case];
            list<case> updateCase = new list<Case>();
            for(case objCase : tempCase){
                CaseTriggerOldMap.put(objCase.id,objCase);
                case Caseobj = new case(id=objCase.id,Status='Closed',BusinessHoursId=objCase.BusinessHoursId,AccountId=objCase.AccountId);
                updateCase.add(Caseobj);
            }
            
            Test.startTest();
                EP_CaseDomainObject CaseDomainObject = new EP_CaseDomainObject(updateCase,CaseTriggerOldMap);
                EP_CaseService CaseService = new EP_CaseService(CaseDomainObject);
                CaseService.getMapIdToAcc(updateCase,CaseTriggerOldMap);
            Test.stopTest();
            //No Asserts requred , As this methdods delegates to other methods
            
        }
    }