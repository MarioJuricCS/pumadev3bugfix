global class SendCreditExceptionRequestHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
    
    Boolean calloutsPerformed = false;
    List <id> orderIdList = new List<Id>();
    
    private static final String CLASS_NAME = 'SendCreditExceptionRequestHandler';
    private static final String EXECUTE_SFDC_TO_NAV = 'executeSFDCTONAV';

    public Boolean performCallouts(List<SObject> data) {
        
    List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data; 
    system.debug('StepList is :' +stepList);
    List<CSPOFA__Orchestration_Step__c> extendedList = [Select 
                                                            id,CSPOFA__Orchestration_Process__r.Order__c 
                                                        from 
                                                            CSPOFA__Orchestration_Step__c where id in :stepList
                                                        ];
                                        
                                                        
    for(CSPOFA__Orchestration_Step__c step :extendedList){
        orderIdList.add(step.CSPOFA__Orchestration_Process__r.Order__c );
    }
                                                        
    List<EP_Credit_Exception_Request__c> crList =[SELECT 
                                                    id,EP_Bill_To__c,EP_Current_Order_Value__c,Order__c,EP_Status__c
                                                  FROM
                                                    EP_Credit_Exception_Request__c
                                                  WHERE
                                                   Order__c in : orderIdList and EP_Status__c = 'Open'
                                                    
                                                ];
                                                
     system.debug('CreditList is :' +crList);
     try{
            list<id> childIdList = new list<id>(); 
             for(EP_Credit_Exception_Request__c crp  : crList) {
                  EP_OutboundMessageService outboundService = new EP_OutboundMessageService(crp.id, 'SEND_CREDIT_EXCEPTION_REQUEST', 'AAF');
                  outboundService.sendOutboundMessage('CREDEXCEPTION_CREATE',childIdList);
              }             
          
          }
          Catch(Exception e){
            EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, EXECUTE_SFDC_TO_NAV, CLASS_NAME, ApexPages.Severity.ERROR);
          }
          
     calloutsPerformed = true;
     system.debug('callout performed flag is :' +calloutsPerformed);
     return calloutsPerformed;
    }
    
    public List<sObject> process(List<sObject> data) { 
        List<sObject> result = new List<sObject>(); 

        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        
        return result;
    }
    
}