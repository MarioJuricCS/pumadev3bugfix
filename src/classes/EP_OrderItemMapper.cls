/**
   @Author          CR Team
   @name            EP_OrderItemMapper
   @CreateDate      12/21/2016
   @Description     This class contains all SOQLs related to OrderItem Object
   @Version         1.0
   @reference       NA
   */
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    -------------------------------------------------------------------------------------------------------------
    12/27/2016          Shakti Mehtani          Added Method -> getRecordsWithProductsByIds()
    12/30/2016          Shakti Mehtani          Added Method -> getRecsByOrderIds()
    01/01/2016          Shakti Mehtani          Added Method -> getNoParentRecsByOrderIds()
    *************************************************************************************************************
    */
    public with sharing class EP_OrderItemMapper {
        
    /**
    *  Constructor. 
    *  @name            EP_OrderItemMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */   
    public EP_OrderItemMapper() {
        
    }
        
     /** This method is used to do delete operation on OrderItem Object
    *  @name             dodelete
    *  @param            List<OrderItem >
    *  @return           NA
    *  @throws        NA 
    */ 
    public void doDelete(List<OrderItem > recordDelete){
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','doDelete');
        delete recordDelete;
    }

    public void doCSDelete(List<csord__Order_Line_Item__c> recordDelete){
        delete recordDelete;
    }
    
    /** This method is used to get the open OrderItems of the Transporters
    *  @name            getOpenOrderItemsOfTransporters
    *  @param           set<id> ,set<string> 
    *  @return          list<orderItem>
    *  @throws          NA
    */ 
    public list<OrderItem> getOpenOrderItemsOfTransporters(set<id> idSet ,set<string> stringSet) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getOpenOrderItemsOfTransporters');
        list<OrderItem> orderItemObjList = new list<OrderItem>();
        for(list<OrderItem> rderItemList: [   SELECT Id, EP_3rd_Party_Stock_Supplier__c
            FROM OrderItem 
            WHERE EP_3rd_Party_Stock_Supplier__c IN :idSet 
            AND Order.Status IN :stringSet
            ]){
            orderItemObjList.addAll(rderItemList);                         
        }             
        return orderItemObjList ;
    }
    
    /** Returns no parent records by Order Id
    *  @author          Shakti Mehtani
    *  @date            01/01/2017
    *  @name            getNoParentRecsByOrderIds
    *  @param           set<id>
    *  @return          list<orderItem>
    *  @throws          NA
    */ 
    // Requirement 59441 - start
    public list<OrderItem> getNoParentRecsByOrderIds(set<Id> idSet) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getNoParentRecsByOrderIds');
        list<OrderItem> orderItemObjList = new list<OrderItem>();
        
        for(list<OrderItem> rderItemList: [   SELECT 
            OrderId
            , pricebookentry.product2.name
            , UnitPrice
            , EP_Pricing_Total_Amount__c
            , EP_Tax_Amount__c
            , quantity
            , EP_Quantity_UOM__c
            , EP_Total_Price__c
            , CurrencyIsoCode
            , EP_Tax_Percentage__c
            , EP_Invoice_Name__c
            , EP_Ambient_Delivered_Quantity__c
            , EP_Standard_Delivered_Quantity__c
            , EP_Ambient_Loaded_Quantity__c
            , EP_Standard_Loaded_Quantity__c
            , EP_BOL_Number__c 
            , EP_3rd_Party_Stock_Supplier__c 
            , EP_Contract__c
            , ( SELECT 
            OrderId,id , pricebookentry.product2.name
            , EP_Total_Price__c , UnitPrice
            , EP_Pricing_Total_Amount__c , EP_Tax_Amount__c
            , quantity , EP_Quantity_UOM__c
            , CurrencyIsoCode , EP_Tax_Percentage__c
            , EP_Invoice_Name__c 
            , EP_Ambient_Delivered_Quantity__c
            , EP_Standard_Delivered_Quantity__c
            , EP_Ambient_Loaded_Quantity__c
            , EP_Standard_Loaded_Quantity__c
            , EP_BOL_Number__c 
            , EP_3rd_Party_Stock_Supplier__c 
            , EP_Contract__c
            FROM order_Products__r 
            Order By UnitPrice desc 
            )
            FROM orderItem 
            WHERE orderId IN :idSet 
            AND EP_Parent_Order_Line_Item__c = null 
            ])  {
            orderItemObjList.addAll(rderItemList);                         
        }             
        return orderItemObjList;
    }

    public list<csord__Order_Line_Item__c> getCSNoParentRecsByOrderIds(set<Id> idSet) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getNoParentRecsByOrderIds');
        list<csord__Order_Line_Item__c> orderItemObjList = new list<csord__Order_Line_Item__c>();
        
        for(list<csord__Order_Line_Item__c> rderItemList: [   SELECT 
            OrderId__c
            , EP_Product__r.Name
            , UnitPrice__c
            , EP_Pricing_Total_Amount__c
            , EP_Tax_Amount__c
            , Quantity__c
            , EP_Quantity_UOM__c
            , EP_Total_Price__c
            , CurrencyIsoCode
            , EP_Tax_Percentage__c
            , EP_Invoice_Name__c
            , EP_Ambient_Delivered_Quantity__c
            , EP_Standard_Delivered_Quantity__c
            , EP_Ambient_Loaded_Quantity__c
            , EP_Standard_Loaded_Quantity__c
            , EP_BOL_Number__c 
            , EP_3rd_Party_Stock_Supplier__c 
            , EP_Contract__c
            FROM csord__Order_Line_Item__c 
            WHERE OrderId__c IN :idSet 
            AND EP_Parent_Order_Line_Item__c = null 
            ])  {
            orderItemObjList.addAll(rderItemList);                         
        }             
        return orderItemObjList;
    }

    public list<csord__Order_Line_Item__c> getCSOrderById(set<Id> idSet) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getNoParentRecsByOrderIds');
        list<csord__Order_Line_Item__c> orderItemObjList = new list<csord__Order_Line_Item__c>();
        
        for(list<csord__Order_Line_Item__c> rderItemList: [   SELECT 
            OrderId__c
            , EP_Product__r.Name
            , UnitPrice__c
            , EP_Pricing_Total_Amount__c
            , EP_Tax_Amount__c
            , Quantity__c
            , csord__Line_Description__c
            , csord__Total_Price__c
            , EP_VAT__c
            , Description__c
            , Order_Line_Number__c
            , ListPrice__c
            , EP_Product_Code__c  
            , EP_Quantity_UOM__c
            , EP_Total_Price__c
            , CurrencyIsoCode
            , EP_Tax_Percentage__c
            , EP_Invoice_Name__c
            , EP_Ambient_Delivered_Quantity__c
            , EP_Standard_Delivered_Quantity__c
            , EP_Ambient_Loaded_Quantity__c
            , EP_Standard_Loaded_Quantity__c
            , EP_BOL_Number__c 
            , EP_3rd_Party_Stock_Supplier__c 
            , EP_Contract__c
            , Has_Price_Indicative__c
            ,CS_OrderLine_Number__c
            FROM csord__Order_Line_Item__c 
            WHERE csord__Order__c IN :idSet])  {
            orderItemObjList.addAll(rderItemList);                         
        }     
        return orderItemObjList;
    }
	
	// Requirement 59441 - end
    /** This method is use to return list of order Item by OrderNumber
    *  @author          Rahul Jain
    *  @date            01/04/2017
    *  @name            getRecordsByOrderNumber
    *  @param           set of Order Number
    *  @return          list<orderItem>
    *  @throws          NA
    */ 
    public list<OrderItem> getInvoiceItemsByOrderNumber(set<string> ordNumberSet) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getInvoiceItemsByOrderNumber');
        list<OrderItem> orderItemList = new list<OrderItem>();
        for(list<OrderItem> orderItemObjList :[SELECT 
            id, 
            EP_Is_Standard__c ,
            EP_Parent_Order_Line_Item__c,
            OrderId, Quantity,
            EP_Invoice_Name__c, 
            PriceBookEntry.name,
            unitPrice, 
            order.Pricebook2Id, 
            OrderItemNumber, 
            PriceBookEntryId, 
            order.currencyisoCode, 
            EP_Product_Code__c 
            FROM OrderItem 
            WHERE Order.OrderNumber IN : ordNumberSet 
            AND EP_Is_Standard__c = false
            ]) {

            orderItemList.addAll(orderItemObjList);                            
        }             
        return orderItemList;
    }

    public List<csord__Order_Line_Item__c> getCSInvoiceItemsByOrderNumber(Set<String> ordNumberSet) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getInvoiceItemsByOrderNumber');
        list<csord__Order_Line_Item__c> orderItemList = new list<csord__Order_Line_Item__c>();
        for(list<csord__Order_Line_Item__c> orderItemObjList :[SELECT 
            id, 
            EP_Is_Standard__c ,
            EP_Parent_Order_Line_Item__c,
            OrderId__c, Quantity__c,
            EP_Invoice_Name__c,
            PriceBookEntryId__c, 
            //PriceBookEntry.name,
            UnitPrice__c, 
            csord__Order__r.Pricebook2Id__c, 
            OrderItemNumber__c, 
            //PriceBookEntryId, 
            csord__Order__r.CurrencyisoCode, 
            EP_Product_Code__c 
            FROM csord__Order_Line_Item__c 
            WHERE csord__Order__r.OrderNumber__c IN : ordNumberSet 
            AND EP_Is_Standard__c = false
            ]) {

            orderItemList.addAll(orderItemObjList);                            
        }             
        return orderItemList;
    }


    /** This method is use to return list of OrderLineItems and its Invoice Items
    *  @author          Rahul Jain
    *  @date            01/04/2017
    *  @name            getOrderLineWithInvoiceItem
    *  @param           set of Orders
    *  @return          list<orderItem>
    *  @throws          NA
    */ 
    public list<OrderItem> getOrderLineWithInvoiceItem(set<id> orderIdSet ) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getOrderLineWithInvoiceItem');
        list<OrderItem> orderItemList = new list<OrderItem>();
        for(list<OrderItem> orderItemObjList :[select Id, EP_Stock_Location_Id__c,
            EP_Ambient_Delivered_Quantity__c,
            EP_Standard_Delivered_Quantity__c,
            EP_Ambient_Loaded_Quantity__c,
            EP_Standard_Loaded_Quantity__c,
            Quantity,
            EP_Product_Code__c,
            EP_Accounting_Details__c,
            EP_Quantity_UOM__c,
            EP_Pricing_Response_Unit_Price__c,
            EP_BOL_Number__c,
            EP_Contract_Nav_Id__c,
            EP_Supplier_Nav_Vendor_Id__c,
            OrderItemNumber,
            orderId,
            Description,
            EP_Tank_Code__c,
            EP_Is_Tank_Fill__c,
            EP_Line_Id_NAV_XML__c,
            EP_Location_ID_NAV_XML__c,
            EP_Line_Id_WINDMS_XML__c,
            UnitPrice,
            EP_Total_Price__c,
            EP_Parent_Order_Line_Item__c,
            EP_Tax_Percentage__c,
            EP_Tax_Amount__c,
            EP_Pricing_Total_Amount__c,
            PricebookEntry.Product2.Name,
            EP_Is_Standard__c,
            EP_OrderItem_Number__c,
            Order.EP_Is_Imported_Order__c,
            order.Pricebook2Id,
            PriceBookEntryId,
            order.currencyisoCode,
            (Select Id,      
            OrderItemNumber,
            EP_Tax_Percentage__c,
            EP_Product_Code__c,
            Description,Quantity,
            EP_Tax_Amount__c,
            EP_Quantity_UOM__c,
            EP_Tax_Amount_XML__c,
            EP_Invoice_Name__c,
            UnitPrice,
            EP_Total_Price__c,
            EP_ChargeType__c,
            EP_Parent_Order_Line_item__c,
            EP_Pricing_Total_Amount__c,
            PricebookEntry.Product2.Name,
            EP_Is_Standard__c
            from Order_Products__r
            where EP_Is_Standard__c = false)
            from OrderItem 
            where orderId IN : orderIdSet  
            and EP_Is_Standard__c = true  
            ORDER BY EP_Parent_Order_Line_Item__c NULLS first 
            ,EP_Is_Standard__c DESC]) {

            orderItemList.addAll(orderItemObjList);                            
        }             
        return orderItemList;
    }

    /** This method is use to return list of OrderLineItems and its Invoice Items
    *  @author          Rahul Jain
    *  @date            01/04/2017
    *  @name            getOrderLineWithInvoiceItem
    *  @param           set of Orders
    *  @return          list<orderItem>
    *  @throws          NA
    */ 
    public list<csord__Order_Line_Item__c> getCsOrderLineWithInvoiceItem(set<id> orderIdSet ) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getOrderLineWithInvoiceItem');
        list<csord__Order_Line_Item__c> orderItemList = new list<csord__Order_Line_Item__c>();
        system.debug('pep ' + orderIdSet);
        for(list<csord__Order_Line_Item__c> orderItemObjList :[select Id, EP_Stock_Location_Id__c,
            EP_Ambient_Delivered_Quantity__c,
            EP_Standard_Delivered_Quantity__c,
            EP_Ambient_Loaded_Quantity__c,
            EP_Standard_Loaded_Quantity__c,
            EP_Product_Sold_As__c,
            Quantity__c,
            EP_Product_Code__c,
            EP_Accounting_Details__c,
            EP_Quantity_UOM__c,
            EP_Pricing_Response_Unit_Price__c,
            EP_BOL_Number__c,
            EP_Contract_Nav_Id__c,
            EP_Supplier_Nav_Vendor_Id__c,
            OrderItemNumber__c,
            orderId__c,
            EP_WinDMS_Line_Item_Reference_Number__c,
            Description__c,
            EP_Tank_Code__c,
            EP_Is_Tank_Fill__c,
            EP_Line_Id_NAV_XML__c,
            EP_Location_ID_NAV_XML__c,
            EP_Line_Id_WINDMS_XML__c,
            UnitPrice__c,
            EP_Total_Price__c,
            EP_Parent_Order_Line_Item__c,
            EP_Tax_Percentage__c,
            EP_Tax_Amount__c,
            EP_Pricing_Total_Amount__c,
            Product__r.Name,
            Order_Line_Number__c,
            CS_OrderLine_Number__c,
            //PricebookEntry.Product2.Name, //mategr
            EP_Is_Standard__c,
            EP_OrderItem_Number__c, 
            csord__Order__r.EP_Is_Imported_Order__c,
            csord__Order__r.Pricebook2Id__c,
            //PriceBookEntryId, //mategr
            PriceBookEntryId__c,
            csord__Order__r.currencyisoCode /*,
            (Select Id,      
            OrderItemNumber,
            EP_Tax_Percentage__c,
            EP_Product_Code__c,
            Description,Quantity,
            EP_Tax_Amount__c,
            EP_Quantity_UOM__c,
            EP_Tax_Amount_XML__c,
            EP_Invoice_Name__c,
            UnitPrice,
            EP_Total_Price__c,
            EP_ChargeType__c,
            EP_Parent_Order_Line_item__c,
            EP_Pricing_Total_Amount__c,
            PricebookEntry.Product2.Name,
            EP_Is_Standard__c
            from Order_Products__r
            where EP_Is_Standard__c = false) */
            from csord__Order_Line_Item__c 
            where csord__Order__c IN : orderIdSet  
            //and EP_Is_Standard__c = true  
            ORDER BY EP_Parent_Order_Line_Item__c NULLS first 
            ,EP_Is_Standard__c DESC]) {

            orderItemList.addAll(orderItemObjList);                            
        }
        system.debug('pep ' + orderItemList);             
        return orderItemList;
    }
    
    public list<OrderItem> getOrderItemsByOrderNumber(string orderNumber) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getOrderItemsByOrderNumber');
        list<OrderItem> orderItemList = new list<OrderItem>();
        for (list<OrderItem> orderItemObjList : [Select id, 
            priceBookentry.product2.name, 
            EP_Is_Standard__c, 
            EP_OrderItem_Number__c, 
            Order.EP_Is_Imported_Order__c ,
            OrderId, 
            Quantity, 
            unitPrice, 
            order.Pricebook2Id, 
            OrderItemNumber, 
            PriceBookEntryId, 
            order.currencyisoCode, 
            EP_Product_Code__c
            from OrderItem 
            where Order.OrderNumber =: orderNumber
            AND EP_Is_Standard__c = true]) {
            
            orderItemList.addAll(orderItemObjList);  
        }
        return orderItemList;
    }

    public list<csord__Order_Line_Item__c> getCSOrderItemsByOrderNumber(string orderNumber) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getCSOrderItemsByOrderNumber');
        list<csord__Order_Line_Item__c> orderItemList = new list<csord__Order_Line_Item__c>();
        for (list<csord__Order_Line_Item__c> orderItemObjList : [Select id, 
            Product__r.Name, 
            EP_Is_Standard__c, 
            EP_OrderItem_Number__c, 
            csord__Order__r.EP_Is_Imported_Order__c ,
            OrderId__c, 
            Quantity__c, 
            UnitPrice__c, 
            csord__Order__r.Pricebook2Id__c, 
            OrderItemNumber__c, 
            PriceBookEntryId__c, 
            csord__Order__r.CurrencyIsoCode, 
            EP_Product_Code__c
            from csord__Order_Line_Item__c 
            where csord__Order__r.OrderNumber__c =: orderNumber
            AND EP_Is_Standard__c = true]) {
            
            orderItemList.addAll(orderItemObjList);  
        }
        return orderItemList;
    }
    
    /** This method is use to return list of OrderLineItems and its Invoice Items
    *  @author          Accenture
    *  @date            01/04/2017
    *  @name            getOrderLineItemsforOrder
    *  @param           set of Orders
    *  @return          list<orderItem>
    *  @throws          NA
    */ 
    public list<OrderItem> getOrderLineItemsforOrder(string orderid) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getOrderItemsByOrderNumber');
      return [Select id, 
            priceBookentry.product2.name, 
            EP_Is_Standard__c, 
            EP_OrderItem_Number__c, 
            Order.EP_Is_Imported_Order__c ,
            EP_3rd_Party_Stock_Supplier__c,
            OrderId, 
            Quantity, 
            unitPrice, 
            order.Pricebook2Id, 
            OrderItemNumber, 
            PriceBookEntryId, 
            order.currencyisoCode, 
            EP_Product_Code__c
            from OrderItem 
            where Order.id =: orderid
            ];
    }

    /** This method is use to return list of OrderLineItems and its Invoice Items
    *  @author          CloudSense
    *  @date            07/12/2017
    *  @name            getOrderLineItemsforOrder
    *  @param           set of Orders
    *  @return          list<orderItem>
    *  @throws          NA
    */ 
    public list<csord__Order_Line_Item__c> getCSOrderLineItemsforOrder(string orderid) {
        EP_GeneralUtility.Log('Public','EP_OrderItemMapper','getCSOrderItemsByOrderNumber');
      return [Select Id, 
            Product__r.Name, 
            EP_Is_Standard__c, 
            EP_OrderItem_Number__c, 
            csord__Order__r.EP_Is_Imported_Order__c ,
            EP_3rd_Party_Stock_Supplier__c,
            OrderId__c, 
            Quantity__c, 
            UnitPrice__c, 
            csord__Order__r.Pricebook2Id__c, 
            OrderItemNumber__c, 
            //PriceBookEntryId, 
            csord__Order__r.CurrencyIsoCode, 
            EP_Product_Code__c
            from csord__Order_Line_Item__c 
            where OrderId__c =: orderid
            ];
    }
    
    
    
}