public with sharing class EP_AccountFactory {

    EP_AccountDomainObject accountDomainObject;
    Account objAccount;

    public EP_AccountFactory(EP_AccountDomainObject accDomainObj) {
       this.accountDomainObject = accDomainObj;
    }

   public EP_AccountType getAccountType() {
        EP_GeneralUtility.Log('Public','EP_AccountFactory','getAccountType');

        EP_AccountType  accountType;
         system.debug('accountDomainObject.localAccount.EP_Account_Type__c==='+accountDomainObject.localAccount.EP_Account_Type__c);
        accountType = (EP_AccountType)Type.forName('EP_'+ accountDomainObject.localAccount.EP_Account_Type__c).newInstance();
        system.debug('accountType==='+accountType);
        return accountType;
    }

    public EP_Account_VendorManagement getVendorType() {
        EP_GeneralUtility.Log('Public','EP_AccountFactory','getVendorType');
        EP_Account_VendorManagement  vendorType;    
        //system.debug('** AccountFactory - VendorType ** ' +accountDomainObject.localAccount.EP_ShipTo_Type__c );    
        vendorType = (EP_Account_VendorManagement)Type.forName('EP_'+accountDomainObject.localAccount.EP_ShipTo_Type__c+'Account').newInstance();
        return vendorType;
    }
}