/* 
@Author      CloudSense
@name        CreateBasketAndOfferController
@CreateDate  23/07/2017
@Description Class for creating the basket in the background and create MLE URL for the iframe
@Version     1.0
*/

public with sharing class CreateBasketAndOfferControllerNew
{
    private static final String CHANNEL_PARAM = system.label.ORDER_CHANNEL_CSC;
    private static final String COMMUNITY_CHANNEL = system.label.ORDER_CHANNEL_CUSTOMER_COMMUNITY;

    // Initialise
    public Account account;
    public String configurationId;
    public String basketId;
    public String schemaId;
    // fetch Custom setting
    public CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();
    // Populate offerId from custom setting
    public String offerId = csOrderSetting.OfferTemplateId__c;
    public cscfga__Product_Basket__c basket;
    public cscfga__Product_Configuration__c parentConfiguration;
    public cscfga__Product_Configuration__c childConfiguration;
    public cscfga__Attribute__c attribute1;
    public cscfga__Attribute__c attribute2;
    public cscfga__Attribute__c attribute3;
    public cscfga__Attribute__c attribute4;
    public cscfga__Attribute__c attribute5;
    public cscfga__Attribute__c attribute6;
    public cscfga__Attribute__c attribute7;
    public List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>();
    // Set the channel accordingly
    public String channel {
        get {
            if (this.channel == null) {
                this.channel = ApexPages.currentPage().getParameters().get(CHANNEL_PARAM);
            }
            return this.channel;
        }
        private set;
    }

    /**
    * @author       CloudSense
    * @name         CreateBasketAndOfferController
    * @date         23/07/2017
    * @description  Constructor for the class
    * @param        StandardController
    * @return       NA
    */  
    public CreateBasketAndOfferControllerNew(ApexPages.StandardController controller)
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','CreateBasketAndOfferController');
        account = (Account) controller.getRecord();  
    }
    
    /**
    * @author       CloudSense
    * @name         CreateBasketAndOfferController
    * @date         23/07/2017
    * @description  Create the basket, add the offer and launch the product configuration screen
    * @param        NA
    * @return       NA
    */  
    public PageReference createBasket()
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','createBasket');
        // Create the basket add the offer and update the basket name
        basketId = ProductConfigurationCloner.createBasketAndCloneOfferToBasket(account.Id, offerId);
        basket = [select id, name from cscfga__product_basket__c where id = :basketId];
        basket.Name = 'Order for ' + account.Name;
        update basket;

        // Set attributes that connect the child product with the parent product
        connectConfigurations();
        System.debug('CreateBasketAndOfferController - createBasket - basketId is ' + basketId);

        PageReference configPage = Page.customConfigurationNew;
        Map<String, String> configPageParams = configPage.getParameters();
        configPageParams.put('configId', parentConfiguration.id);
        configPageParams.put('linkedId', basketId);
        configPageParams.put('schemaId', schemaId);
        configPageParams.put('defId', parentConfiguration.cscfga__Product_Definition__c);
        configPageParams.put('defName', parentConfiguration.cscfga__Product_Definition__r.Name);
        configPage.setRedirect(true);
        return configPage;
    }
    
     public PageReference editBasket()
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','editBasket');
        // Create the basket add the offer and update the basket name
        try{
            basketId = ProductConfigurationCloner.createBasketAndCloneOfferToBasket(account.Id, offerId);
            basket = [select id, name from cscfga__product_basket__c where id = :basketId];
            basket.Name = 'Order for ' + account.Name;
            update basket;
        }Catch(Exception exp){
            
        }

        // Set attributes that connect the child product with the parent product
        connectConfigurations();
        System.debug('CreateBasketAndOfferController - createBasket - basketId is ' + basketId);

        PageReference configPage = Page.customConfigurationNew;
        Map<String, String> configPageParams = configPage.getParameters();
        configPageParams.put('configId', parentConfiguration.id);
        configPageParams.put('linkedId', basketId);
        configPageParams.put('schemaId', schemaId);
        configPageParams.put('defId', parentConfiguration.cscfga__Product_Definition__c);
        configPageParams.put('defName', parentConfiguration.cscfga__Product_Definition__r.Name);        
        configPage.setRedirect(true);
        return configPage;
    }

    /**
    * @author       CloudSense
    * @name         CreateBasketAndOfferController
    * @date         23/07/2017
    * @description  Set attributes that connect the child product with the parent product
    * @param        NA
    * @return       NA
    */  
        //
    public void connectConfigurations()
    {
        EP_GeneralUtility.Log('Public','CreateBasketAndOfferController','createBasket');
        
        system.debug('connectConfigurations mategr c1');
        // Get the parent and child product configurations
        parentConfiguration = [select id, name, cscfga__Product_Definition__c,cscfga__Product_Definition__r.Name from cscfga__product_configuration__c where name = : csOrderSetting.Parent_Order_Name__c and cscfga__Product_Basket__c = :basketId limit 1];
        if(parentConfiguration!=null) {
            csoe__Non_Commercial_Product_Association__c schema= [Select id,csoe__Non_Commercial_Schema__c from csoe__Non_Commercial_Product_Association__c where csoe__Commercial_Product_Definition__c =: parentConfiguration.cscfga__Product_Definition__c];
            if(schema != null) {
                schemaId = schema.csoe__Non_Commercial_Schema__c;
            }
        }
        String prefixUrl;
        
        if(String.isNotBlank(Network.getNetworkId())){
            prefixUrl=csOrderSetting.Community_MLE_URL__c;
        }else{
            prefixUrl = csOrderSetting.MLE_Org_URL__c;
        }

    }
    
   
}