/* 
   @Author <Accenture>
   @name <EP_PowerBIController>
   @CreateDate <02/01/2018>
   @Description <This is extension class of EP_PowerBI_Report Page>
   @Version <1.0>
*/
public class EP_PowerBIController extends EP_OAuthController {
    @TestVisible private static String APPLICATION_NAME = EP_Common_Constant.POWERBI;
       
    /**
    * @author       Accenture
    * @name         EP_PowerBIController
    * @date         02/01/2018
    * @description  Contructor
    * @param        NA
    * @return       NA
    */  
    public EP_PowerBIController () {
        this.application_name = APPLICATION_NAME;
    }
    
    public String getValidateResult(){
        EP_GeneralUtility.Log('public','EP_PowerBIController','getValidateResult');
        return validateResult;
    }

    
    /**
    * @author       Accenture
    * @name         redirectOnCallback
    * @date         02/01/2018
    * @description  this method validates the callback code and generates the access token
    * @param        NA
    * @return       PageRefernce
    */  
    public PageReference redirectOnCallback() {
        EP_GeneralUtility.Log('public','EP_PowerBIController','redirectOnCallback');
        return super.redirectOnCallback(null);
    }
    
    /**
    * @author       Accenture
    * @name         refreshAccessToken
    * @date         02/01/2018
    * @description  This method refreshes the access token
    * @param        NA
    * @return       PageRefernce
    */
    public PageReference refreshAccessToken() {
        EP_GeneralUtility.Log('public','EP_PowerBIController','refreshAccessToken');
        return super.refreshAccessToken(ApexPages.currentPage());
    }
}