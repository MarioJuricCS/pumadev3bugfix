/**
@Author <Jyotsna Yadav>
@name <EP_OrderItemTriggerTest>
@CreateDate <21/12/2015>
@Description <Test class for Order Item Trigger>
@Version <1.0>
*/


@isTest
private class EP_OrderItemTriggerTest{
    
    
    private static Account vmiShipToAccount,sellToAccount,billToAccount; 
    private static EP_TestDataUtility.WrapperCustomerHierarchy customHierarchy=null;
    private static Id strRecordTypeId = EP_Common_Util.fetchRecordTypeId(EP_Common_Constant.ACCOUNT_OBJ, EP_Common_Constant.VMI_SHIP_TO);
    static List<Account> accountsToBeUpdated;
    static String recType= Schema.SObjectType.EP_Tank_Dip__c.getRecordTypeInfosByName().get(EP_Common_Constant.PLACEHOLDER).getRecordTypeId();
    static String orderRecType = Schema.SObjectType.Order.getRecordTypeInfosByName().get('VMI Orders').getRecordTypeId();
    static Id pricebookId = Test.getStandardPricebookId();
    private static void updateAccountStatus(){  
        EP_ActionTriggerHandler.isExecuteAfterUpdate = true;
        EP_ActionTriggerHandler.isExecuteBeforeUpdate = true;
        //EP_AccountTriggerHandler.isExecuteAfterUpdate = true;
        //EP_AccountTriggerHandler.isExecuteBeforeUpdate = true;      
    }
      /**************************************************************************
    *@Description : This method will validate if the tank status is stopped then 
                    order status is cancelled for single tank.
                    
    *@Return      : void                                                      *    
    **************************************************************************/
    private static testmethod void testCase1(){
        
        Test.loadData(EP_Order_Fieldset_Sfdc_Windms_Intg__c.SobjectType,'TestData_EP_Order_Fieldset_Sfdc_Windms_Intg');
         EP_OrderItemTriggerHandler.isExecuteAfterUpdate = true;
         EP_OrderItemTriggerHandler.isExecuteAfterInsert = true;
      
         //EP_OrderTriggerHandler.isExecuteAfterUpdate = true;
         
         Pricebook2 customPB1 = new Pricebook2(Name='PriceBook A', isActive=true);
        insert customPB1; 
        billToAccount=EP_TestDataUtility.createBillToAccount();
        insert billToAccount;
        updateAccountStatus();
        billToAccount.EP_Status__c =EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update billToAccount;
        billToAccount.EP_Status__c =EP_Common_Constant.STATUS_ACTIVE;
        update billToAccount;
        sellToAccount = EP_TestDataUtility.createSellToAccount(billToAccount.id, NULL);
        insert sellToAccount;
        sellToAccount.EP_PriceBook__c= customPB1.Id;
        update sellToAccount;
        vmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        vmiShipToAccount.EP_Email__c = 'test@test.com';
        insert vmiShipToAccount;
        //vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        //vmiShipToAccount.EP_Ship_To_Type__c = EP_Common_Constant.CONSIGNMENT;
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        tankObj.EP_Tank_Status__c = 'New';        
        insert tankObj;
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update vmiShipToAccount;
        tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        update tankObj;
        tankObj.EP_Tank_Status__c = EP_Common_Constant.TANK_STOPPED_STATUS;
        tankObj.EP_Reason_Blocked__c = EP_Common_Constant.MAINTENANCE; // API Changed
        update tankObj;
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update vmiShipToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update sellToAccount;
        
        Test.startTest();
        
        Product2 taxProductObj = createProduct('TaxProduct');
        insert taxProductObj;
        
        
        
        PricebookEntry CustomPriceEntry = new PricebookEntry(
                                        Pricebook2Id = customPB1.Id , Product2Id = taxProductObj.Id,
                                        UnitPrice = 10000, IsActive = true,EP_Is_Sell_To_Assigned__c=True);
        insert CustomPriceEntry;
        Order orObj = EP_TestDataUtility.createOrder(sellToAccount.Id,orderRecType,customPB1.Id);
        orObj.EP_ShipTo__c = vmiShipToAccount.Id;
        insert orObj;
		//Test Class Fix Start
        //PricebookEntry taxPricebookEntryObj = createPricebookEntry(taxProductObj.Id, Test.getStandardPricebookId());
        //insert taxPricebookEntryObj;
        //Test Class Fix End
        orderItem op = EP_TestDataUtility.createorderItem(orObj.Id,customPB1.Id);
        op.EP_Tank__c = tankObj.id;
        insert op;
        
        //EP_OrderUpdateHandler.parse('Test');
        Test.stoptest();
        
        Order order = [Select Status from order where id=:orObj.Id];
        system.debug('*********'+order);
        system.assertEquals(order.status,EP_Common_Constant.ORDER_DRAFT_STATUS);
        
        
    }
     /**************************************************************************
    *@Description : This method will validate if the any tank status is stopped then 
                    order status is cancelled for multiple tank.
                    
    *@Return      : void                                                      *    
    **************************************************************************/
    
    private static testmethod void testCase2(){
    Test.loadData(EP_Order_Fieldset_Sfdc_Windms_Intg__c.SobjectType,'TestData_EP_Order_Fieldset_Sfdc_Windms_Intg');
        
        List<EP_Tank__c> tankList = new List<EP_Tank__c>();
       
        sellToAccount = EP_TestDataUtility.createSellToAccount(NULL, NULL);
        insert sellToAccount;
        sellToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        vmiShipToAccount = EP_TestDataUtility.createShipToAccount(sellToAccount.Id, strRecordTypeId);
        vmiShipToAccount.EP_Email__c = 'test@test.com';
        insert vmiShipToAccount;
        //vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        //vmiShipToAccount.EP_Ship_To_Type__c = EP_Common_Constant.CONSIGNMENT;
        Product2 prod= EP_TestDataUtility.createTestRecordsForProduct();
        //prod.isActive = FALSE;
        //update prod;
        EP_Tank__c tankObj= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        //tankObj.EP_Tank_Status__c = EP_Common_Constant.TANK_STOPPED_STATUS;
        tankObj.EP_Tank_Status__c = 'New';
        tankObj.EP_Tank_Code__c = '987655';
        tankList.add(tankObj);
        EP_Tank__c tankObj1= EP_TestDataUtility.createTestEP_Tank(vmiShipToAccount.Id,prod.Id);
        //tankObj1.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        tankObj1.EP_Tank_Status__c = 'New';
        tankObj1.EP_Tank_Code__c = '9876';
        tankList.add(tankObj1);
        insert tankList;
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_BASIC_DATA_SETUP;
        update vmiShipToAccount;
        
        Test.startTest(); 
        tankObj.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        update tankObj;

        tankObj.EP_Tank_Status__c = EP_Common_Constant.TANK_STOPPED_STATUS;
        tankObj.EP_Reason_Blocked__c = EP_Common_Constant.MAINTENANCE;
        update tankObj;
        tankObj1.EP_Tank_Status__c = EP_Common_Constant.BASIC_DATA_SETUP;
        update tankObj1;
        tankObj1.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        update tankObj1;
        vmiShipToAccount.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
        update vmiShipToAccount;

        Product2 taxProductObj = createProduct('TaxProduct');
        insert taxProductObj;
        
        Pricebook2 customPB1 = new Pricebook2(Name='PriceBook A', isActive=true);
        insert customPB1; 
        
        PricebookEntry CustomPriceEntry = new PricebookEntry(
                                        Pricebook2Id = customPB1.Id , Product2Id = taxProductObj.Id,
                                        UnitPrice = 10000, IsActive = true,EP_Is_Sell_To_Assigned__c=True);
        insert CustomPriceEntry;
        Order orObj = EP_TestDataUtility.createOrder(sellToAccount.Id,orderRecType,customPB1.Id);
        orObj.EP_ShipTo__c = vmiShipToAccount.Id;
        insert orObj;

        orderItem op = EP_TestDataUtility.createorderItem(orObj.Id,customPB1.Id);
        op.EP_Is_Standard__c = true;
        op.quantity = 20;
       
        insert op;
        orderItem op1 = EP_TestDataUtility.createorderItem(orObj.Id,customPB1.Id);
        op1.EP_Tank__c = tankObj.id;
        insert op1;
        Test.stopTest();
        Order order = [Select Status from order where id=:orObj.Id];
        system.assertEquals(order.Status,EP_Common_Constant.ORDER_DRAFT_STATUS);
        system.debug('*********'+order);
        
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test Product2 record
     * @params String
     * @return Product2
     */
    private static Product2 createProduct(String name){
        Product2 obj = new product2();
        obj.Name = name;
        obj.CurrencyIsoCode = 'USD';
        obj.IsActive = true;
        
        return obj;
    }
    
    /**
     * @author Kamal Garg
     * @date 28/12/2015
     * @description create test PricebookEntry record
     * @params Id,Id
     * @return PricebookEntry
     */
    private static PricebookEntry createPricebookEntry(Id productId, Id pbId){
        PricebookEntry obj = new PricebookEntry();
        obj.Pricebook2Id = pbId;
        obj.Product2Id = productId;
        obj.UnitPrice = 1;
        obj.IsActive = true;
        obj.EP_Is_Sell_To_Assigned__c=True;
        obj.EP_VAT_GST__c = 14;
        obj.EP_Additional_Taxes__c = 5.0;
        obj.CurrencyIsoCode = 'USD';
        
        return obj;
    }
    
    
}