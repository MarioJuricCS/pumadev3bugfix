/* ================================================
 * @Class Name : EP_CRM_TestEventTrigger
 * @author : Kamendra Singh
 * @Purpose: This class is used to test EP_CRM_EventTrigger apex trigger.
 * @created date: 08/07/2016
 ================================================*/
@isTest(seealldata=false)
private class EP_CRM_TestEventTrigger{
     /* This method is used to create all test data and test before insert event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testEventInsertUpdateTrigger(){
         Test.startTest();
         // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            insert acc;
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            //opp.Stagename='Closed Won';
            opp.Stagename='Prospecting';
            opp.Account=acc;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            insert opp;
            
            Event tempEvent = new Event ();
            tempEvent.OwnerId = UserInfo.getUserId();
            tempEvent.Subject='Donni';
            tempEvent.whatId = opp.Id;
            tempEvent.DurationInMinutes = 10;
            tempEvent.ActivityDateTime = system.now();
            
            insert tempEvent;
            
        //Check System assert---------
        Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id];
        
        //validate through system Assert 
        System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
        
        //update Event........
        tempEvent.Subject = 'test subject';
        
        update tempEvent;
        
         //Check System assert---------
        Opportunity tempOpp1 = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id];
        
        //validate through system Assert 
        System.Assert(tempOpp1.EP_CRM_Last_Activity_Date__c != null);
            
        test.stopTest();
    }
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testBulkEventDeleteTrigger(){
         Test.startTest();
         // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            insert acc;
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            //opp.Stagename='Closed Won';
            opp.Stagename='Prospecting';
            opp.Account=acc;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            insert opp;
            
            //insert Event..............
            list<event> lstEvent = new list<Event>();
            Event tempEvent = new Event ();
            tempEvent.OwnerId = UserInfo.getUserId();
            tempEvent.Subject='Donni';
            tempEvent.whatId = opp.Id;
            tempEvent.DurationInMinutes = 10;
            tempEvent.ActivityDateTime = system.now();
            
            Event tempEvent1 = new Event ();
            tempEvent1.OwnerId = UserInfo.getUserId();
            tempEvent1.Subject='Donni';
            tempEvent1.whatId = opp.Id;
            tempEvent1.DurationInMinutes = 10;
            tempEvent1.ActivityDateTime = system.now();
            
            lstEvent.add(tempEvent);
            lstEvent.add(tempEvent1);
            
            insert lstEvent;
            
            //delete Event........
            delete tempEvent;
            
        //Check System assert---------
        Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id];
        
        //validate through system Assert 
        System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c != null);
        test.stopTest();
    }
    
     /* This method is used to create all test data and test before delete event schenario.
    * @param : 
    * @return: 
    */  
    public static testMethod void testSingleEventDeleteTrigger(){
         Test.startTest();
         // Insert Account
            Account acc=new Account();
            acc.name='Test Accenture';
            acc.BillingCity = 'test City';
            acc.BillingStreet  = 'test street';
            acc.BillingState = 'test state';
            acc.BillingPostalCode = '201301';
            acc.BillingCountry = 'test';
            insert acc;
            
            //Insert Opportunity
            Opportunity opp=new Opportunity();
            opp.name='Test';
            //opp.Stagename='Closed Won';
            opp.Stagename='Prospecting';
            opp.Account=acc;
            opp.EP_CRM_Credit_Approval_Received__c = true;
            opp.CloseDate=Date.Today() + 5;
            insert opp;
            
            //insert Event..............
           
            Event tempEvent = new Event ();
            tempEvent.OwnerId = UserInfo.getUserId();
            tempEvent.Subject='Donni';
            tempEvent.whatId = opp.Id;
            tempEvent.DurationInMinutes = 10;
            tempEvent.ActivityDateTime = system.now();
            
            insert tempEvent;
            
            //delete Event........
            delete tempEvent;
            
        //Check System assert---------
        Opportunity tempOpp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where Id= : opp.Id];
        
        //validate through system Assert 
        System.Assert(tempOpp.EP_CRM_Last_Activity_Date__c == null);
        test.stopTest();
    }
}