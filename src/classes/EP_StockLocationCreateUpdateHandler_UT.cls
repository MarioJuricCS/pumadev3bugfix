@isTest
public class EP_StockLocationCreateUpdateHandler_UT{
    
    @testSetup static void init() {
        List<EP_CS_InboundMessageSetting__c> lInboundCustomSetting = Test.loadData(EP_CS_InboundMessageSetting__c.sObjectType, 'EP_CS_InboundMessageSettingTestData');
    }
   
    static testMethod void parse_test() {     
        EP_StockLocationStub stub = EP_TestDataUtility.createInboundStockLocationMessage();
        string jsonRequest = JSON.serialize(stub);  
        Test.startTest();
        EP_StockLocationStub result = EP_StockLocationCreateUpdateHandler.parse(jsonRequest);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    
    static testMethod void processRequest_test() {
        EP_StockLocationCreateUpdateHandler localObj = new EP_StockLocationCreateUpdateHandler();       
        EP_StockLocationStub stub = EP_TestDataUtility.createInboundStockLocationMessage();
        List<EP_StockLocationStub.inventoryLoc> invLoc = stub.MSG.payload.any0.inventoryLocs.inventoryLoc;
        string jsonRequest = JSON.serialize(stub);       
        Test.startTest();
        String result = localObj.processRequest(jsonRequest);
        Test.stopTest();
        System.assertEquals(true,result != null);
    }
    
    static testMethod void UpsertStorageLocation_Positivetest() {
        list<EP_StockLocationStub.InventoryLoc> lstStockLoc = new list<EP_StockLocationStub.InventoryLoc>();
        EP_StockLocationStub.InventoryLoc invLoc = EP_TestDataUtility.createInboundStorageLocation();
        lstStockLoc.add(invLoc); 
        Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;
        list<Account> lstAccount =EP_StockLocationCreateUpdateHelper.setStockLocationAttributes(lstStockLoc);
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        Test.startTest();
        boolean result =EP_StockLocationCreateUpdateHandler.UpsertStorageLocation(lstAccount,ackDatasets); 
        Test.stopTest(); 
        system.AssertEquals(false, result);                               
    }
    
    static testMethod void UpsertStorageLocation_Negativetest() {
        list<EP_StockLocationStub.InventoryLoc> lstStockLoc = new list<EP_StockLocationStub.InventoryLoc>();
        EP_StockLocationStub.InventoryLoc invLoc = EP_TestDataUtility.createInboundStorageLocation();
        lstStockLoc.add(invLoc); 
        EP_StockLocationStub.InventoryLoc invLoc1 = EP_TestDataUtility.createInboundStorageLocation();
        invLoc1.identifier.locationId =invLoc.identifier.locationId;
        lstStockLoc.add(invLoc1); 
        Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;
        list<Account> lstAccount =EP_StockLocationCreateUpdateHelper.setStockLocationAttributes(lstStockLoc);
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        Test.startTest();
        boolean result =EP_StockLocationCreateUpdateHandler.UpsertStorageLocation(lstAccount,ackDatasets); 
        Test.stopTest(); 
        system.AssertEquals(true, result);                               
    }
    
    static testMethod void generateAcnowledgement_Positivetest() {
        list<EP_StockLocationStub.InventoryLoc> lstStockLoc = new list<EP_StockLocationStub.InventoryLoc>();
        EP_StockLocationStub.InventoryLoc invLoc = EP_TestDataUtility.createInboundStorageLocation();
        lstStockLoc.add(invLoc); 
        Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;
        Schema.DescribeFieldResult fieldResult = Account.EP_Storage_Location_External_ID__c.getDescribe(); 
        Schema.sObjectField externalKey = fieldResult.getSObjectField();
        list<Account> lstAccount =EP_StockLocationCreateUpdateHelper.setStockLocationAttributes(lstStockLoc);
        List<Database.UpsertResult> results =  DataBase.upsert(lstAccount,externalKey,false);  
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        Test.startTest();
        boolean result =EP_StockLocationCreateUpdateHandler.generateAcnowledgement(lstAccount,results,ackDatasets); 
        Test.stopTest(); 
        system.AssertEquals(false, result);                               
    }
    
    static testMethod void generateAcnowledgement_Negativetest() {
        list<EP_StockLocationStub.InventoryLoc> lstStockLoc = new list<EP_StockLocationStub.InventoryLoc>();
        EP_StockLocationStub.InventoryLoc invLoc = EP_TestDataUtility.createInboundStorageLocation();
        lstStockLoc.add(invLoc); 
        EP_StockLocationStub.InventoryLoc invLoc1 = EP_TestDataUtility.createInboundStorageLocation();
        invLoc1.identifier.locationId =invLoc.identifier.locationId;
        lstStockLoc.add(invLoc1);
        Company__c  cmpny = EP_TestDataUtility.createCompany('AUN');
        insert cmpny;
        Schema.DescribeFieldResult fieldResult = Account.EP_Storage_Location_External_ID__c.getDescribe(); 
        Schema.sObjectField externalKey = fieldResult.getSObjectField();
        list<Account> lstAccount =EP_StockLocationCreateUpdateHelper.setStockLocationAttributes(lstStockLoc);
        List<Database.UpsertResult> results =  DataBase.upsert(lstAccount,externalKey,false);  
        List<EP_AcknowledgementStub.dataset> ackDatasets = new List<EP_AcknowledgementStub.dataset>();
        Test.startTest();
        boolean result =EP_StockLocationCreateUpdateHandler.generateAcnowledgement(lstAccount,results,ackDatasets); 
        Test.stopTest(); 
        system.AssertEquals(true, result);                               
    }





}