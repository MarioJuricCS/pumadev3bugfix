public class PEAddMultipleOrderNewLineHelperCLS { 

    public static List<TrafiguraConfigurationControllerNew.WrapperpOrderLineList> addNewRowToAccList(List<TrafiguraConfigurationControllerNew.WrapperpOrderLineList> OrderLineObjList){
        TrafiguraConfigurationControllerNew.WrapperpOrderLineList newRecord = new TrafiguraConfigurationControllerNew.WrapperpOrderLineList();
        csord__Order_Line_Item__c newOrderLineRecord = new csord__Order_Line_Item__c();        
        newRecord.orderLine = newOrderLineRecord;
        newRecord.index = OrderLineObjList.size();
        OrderLineObjList.add(newRecord);
        return OrderLineObjList;
    }
    
    
     public static List<TrafiguraConfigurationControllerNew.WrapperpOrderLineList> removeRowToAccountList(Integer rowToRemove, List<TrafiguraConfigurationControllerNew.WrapperpOrderLineList> waAccountList){
        waAccountList.remove(rowToRemove);
        return waAccountList;
    }
    
    public static void save(List<TrafiguraConfigurationControllerNew.WrapperpOrderLineList> orderLineList) {
        system.debug('==orderLineList==>'+orderLineList.size());
        List<csord__Order_Line_Item__c> OrderLinesRecordsToBeInserted = new List<csord__Order_Line_Item__c>();
        if(orderLineList !=null && !orderLineList.isEmpty()){
            for(TrafiguraConfigurationControllerNew.WrapperpOrderLineList eachRecord : orderLineList ){
                csord__Order_Line_Item__c accTemp = eachRecord.orderLine;
                OrderLinesRecordsToBeInserted.add(accTemp);
               
            }
            system.debug('==OrderLinesRecordsToBeInserted==>'+OrderLinesRecordsToBeInserted.size());
            insert OrderLinesRecordsToBeInserted;
        }
    }
}