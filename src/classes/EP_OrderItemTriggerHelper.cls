/**
 * @author <Accenture>
 * @name <EP_OrderItemTriggerHelper>
 * @createDate <21/12/2015>
 * @description <This class handles requests from EP_OrderItemTriggerHandler apex class> 
 * @version <1.0>
 */
public without sharing class EP_OrderItemTriggerHelper {
    
    private static final Integer LMT = EP_Common_Util.getQueryLimit(); 
    public static Boolean executeTaxFreight = false;
    private static Map<String,Boolean> itemEligibleForRebate;
    private static Map<Id,Boolean> productIdEligibleMap = new Map<Id,Boolean>();
    private static Map<Id,Boolean> orderIdEligibleMap = new Map<Id,Boolean>();
    public static Set<Id> orderIdsToSync = new Set<Id>();   
    
    /**
     * @author Jyotsna Ydav
     * @date 24/12/2015
     * @description This method sets the location of orderitem same as that of order
     */
    public static void setOrderItemLocation(List<OrderItem> newOrderItem){
        
        Set<Id> orderIdSet = new Set<Id>();
        try{
            for( OrderItem oItem : newOrderItem ){
                orderIdSet.add(oItem.OrderId);
                productIdEligibleMap.put(oItem.PricebookentryId,false);
                orderIdEligibleMap.put(oItem.OrderId,false);
               
            }
            system.debug('----'+orderIdEligibleMap+'----'+productIdEligibleMap);
            if(!orderIdSet.isEmpty()){
                Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get(EP_Common_Constant.VMI_ORDER_RECORD_TYPE_NAME).getRecordTypeId();
                Map<Id,Order> orderMap = new Map<Id,Order>([Select Id,Stock_Holding_Location__c,RecordTypeId from Order where ID IN:orderIdSet and RecordTypeId !=:orderRecTypeId LIMIT: LMT]);
                for(OrderItem oItem : newOrderItem){
                    if(oItem.EP_Stock_Holding_Location__c == null) {
                        if(orderMap.get(oItem.OrderId) != null) {
                            oItem.EP_Stock_Holding_Location__c = orderMap.get(oItem.OrderId).Stock_Holding_Location__c;
                        }
                        system.debug('@@@@'+oItem.PricebookentryId+'@@@@'+oItem.OrderId);
                        
                    }
                    oItem.EP_Eligible_for_Rebate__c = (productIdEligibleMap.get(oItem.PricebookentryId)&&orderIdEligibleMap.get(oItem.OrderId))?true:false;
                }
            }
        }
        catch(Exception e){
            EP_LoggingService.logHandledException(e,EP_Common_Constant.EPUMA,EP_Common_Constant.setOrderItemLocation_mthdName, EP_Common_Constant.EP_OrderItemTriggerHelper_mthdName, ApexPages.Severity.ERROR);
        }
    }   
    /*
    To set the order stock holding location
    */
    public static void setOrderLocation(List<OrderItem> newOrderItem){
        Map<Id,Id> stockLocMap = new Map<Id,Id>();
        List<Order> orderList = new List<Order>();
        Map<id,id> transporterMap = new Map<Id,Id>();
        Map<id,id> stockMap = new Map<Id,Id>();
        Id orderRecTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get(EP_Common_Constant.VMI_ORDER_RECORD_TYPE_NAME).getRecordTypeId();
        try{
            for(OrderItem o : newOrderItem){
                stockLocMap.put(o.OrderId,o.EP_Stock_Holding_Location__c);
            }
            
            for(EP_Supply_Location_Transporter__c transport : [SELECT id,EP_Transporter__c,EP_Supply_Location__c, EP_Transporter__r.AccountNumber FROM EP_Supply_Location_Transporter__c WHERE EP_Supply_Location__c IN: stockLocMap.values()  AND EP_Is_Default__c = TRUE LIMIT: LMT]){
                transporterMap.put(transport.EP_Supply_Location__c,transport.EP_Transporter__c);    
            }
            System.debug('******transporterMap*******'+transporterMap);
            for(EP_Stock_Holding_Location__c stk : [SELECT Id,Name,Stock_Holding_Location__r.id FROM EP_Stock_Holding_Location__c WHERE id IN: stockLocMap.values() LIMIT: LMT]){
                stockMap.put(stk.id,stk.Stock_Holding_Location__r.id);
            }
            System.debug('******stockMap*******'+stockMap);
            if(!stockLocMap.isEmpty()){
                for(Order ord : [Select id,Stock_Holding_Location__c,RecordTypeId from Order where id in: stockLocMap.keyset() LIMIT: LMT]){ 
                    if(ord.RecordTypeId == orderRecTypeId && ord.Stock_Holding_Location__c == null){
                        ord.Stock_Holding_Location__c = stockLocMap.get(ord.Id);
                        ord.EP_Supply_Location_Pricing__c = stockMap.get(ord.Stock_Holding_Location__c);
                        ord.EP_Transporter_Pricing__c = transporterMap.get(ord.Stock_Holding_Location__c);
                        orderList.add(ord);
                    }    
                }
            }
            if(!orderList.isEmpty()){
                update orderList;
            }
        }
        catch(Exception e){
            EP_LoggingService.logHandledException(e,EP_Common_Constant.EPUMA,EP_Common_Constant.setOrderLocation_mthdName, EP_Common_Constant.EP_OrderItemTriggerHelper_mthdName, ApexPages.Severity.ERROR);
        }   
    }
        /*
        Update Prior Quantity
        Jyotsna 1.178
        */
        public static void updatePriorQuantity(Map<Id,OrderItem> oldMap,Map<Id,OrderItem> newMap,Boolean isInsert){
            try{
                //EP_Orders.isInsertVMIInterface = isInsert;
                for( OrderItem oItem : newMap.values() ){
                    if( !isInsert && (oldMap.get(oItem.Id).quantity <> oItem .Quantity && oldMap.get(oItem.Id).quantity < oItem .Quantity) ){
                        oItem.EP_Prior_Quantity__c =  oldMap.get(oItem.Id).quantity; 
                        orderIdsToSync.add(oItem.orderId);  
                    }else{
                        orderIdsToSync.add(oItem.orderId);  
                    }
                }
            }
            catch(Exception e){
                EP_LoggingService.logHandledException(e,EP_Common_Constant.EPUMA,EP_Common_Constant.updatePriorQuantity_mthdName, EP_Common_Constant.EP_OrderItemTriggerHelper_mthdName, ApexPages.Severity.ERROR);
            }
            
        }
        /*
        Update Child Item Quantity
        */
        /* public static void updatechildItemQuantity(Map<Id,OrderItem> oldMap,Map<Id,OrderItem> newMap){
            List<OrderItem> childToUpdateList = new List<OrderItem>();
            OrderItem oItemObj;
            try{
                for( OrderItem oItem : [ Select quantity, (Select quantity from Order_Products__r) from OrderItem where Id IN : newMap.keySet() 
                                         and EP_Is_Standard__c = True LIMIT: LMT] ){
                    for( OrderItem childItem : oItem.Order_Products__r ){
                        if( childItem.Quantity != oItem.Quantity ){
                            oItemObj = new OrderItem(Id = childItem.Id);
                            oItemObj.Quantity = oItem.Quantity;
                            childToUpdateList.add( oItemObj );
                        }    
                    }
                }
                if( !childToUpdateList.isEmpty() ){
                    update childToUpdateList;
                }
            }
            catch(Exception e){
                EP_LoggingService.logHandledException(e,EP_Common_Constant.EPUMA,EP_Common_Constant.updatechildItemQuantity_mthdName, EP_Common_Constant.EP_OrderItemTriggerHelper_mthdName, ApexPages.Severity.ERROR);
            }           
        }*/
        
    }