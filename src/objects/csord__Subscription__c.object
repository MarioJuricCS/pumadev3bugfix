<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>An accessibility model to a Service (or Services). Governs the rights to access and use a Service.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>csord__Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The Account that governs this Subscription.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Account that governs this Subscription.</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Subscriptions</relationshipLabel>
        <relationshipName>Subscriptions</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csord__External_Identifier2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <description>External Id field used for bulk inserts of Orders &amp; Subscriptions objects.</description>
        <externalId>true</externalId>
        <inlineHelpText>External Id field used for bulk inserts of Orders &amp; Subscriptions objects.</inlineHelpText>
        <label>External Identifier</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>csord__External_Identifier__c</fullName>
        <deprecated>false</deprecated>
        <description>DEPRECATED. External Id field used for bulk inserts of Orders &amp; Subscriptions objects.</description>
        <externalId>true</externalId>
        <inlineHelpText>DEPRECATED. Use External Identifier2 field instead.</inlineHelpText>
        <label>DEPRECATED External Identifier</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csord__Identification__c</fullName>
        <deprecated>false</deprecated>
        <description>Identification string for the object, used as the reference from external systems.</description>
        <externalId>false</externalId>
        <inlineHelpText>E.g. &quot;spokeServiceAmsterdam&quot; in Hub&amp;Spokes scenario.</inlineHelpText>
        <label>Identification</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csord__Order_Request__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Order Request object which introduced the latest changes to this object.</description>
        <externalId>false</externalId>
        <inlineHelpText>Order Request object which introduced the latest changes to this object.</inlineHelpText>
        <label>Order Request</label>
        <referenceTo>csord__Order_Request__c</referenceTo>
        <relationshipLabel>Subscriptions</relationshipLabel>
        <relationshipName>Subscriptions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csord__Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The parent Order of this Subscription.</description>
        <externalId>false</externalId>
        <inlineHelpText>The parent Order of this Subscription.</inlineHelpText>
        <label>Order</label>
        <referenceTo>csord__Order__c</referenceTo>
        <relationshipLabel>Subscriptions</relationshipLabel>
        <relationshipName>Subscriptions</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csord__Solution__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The parent Solution of this Subscription.</description>
        <externalId>false</externalId>
        <inlineHelpText>The parent Solution of this Subscription.</inlineHelpText>
        <label>Solution</label>
        <referenceTo>csord__Solution__c</referenceTo>
        <relationshipLabel>Subscriptions</relationshipLabel>
        <relationshipName>Subscriptions</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csord__Status__c</fullName>
        <deprecated>false</deprecated>
        <description>The status of the Subscription in the fulfillment process.</description>
        <externalId>false</externalId>
        <inlineHelpText>Used for state transitions.</inlineHelpText>
        <label>Status</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csord__Total_One_Off_Charges__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>csord__Total_Service_One_Off_Charges__c + csord__Total_Sub_Line_Item_One_Off_Charges__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total One-Off Charges</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>csord__Total_Recurring_Charges__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>csord__Total_Service_Recurring_Charges__c + csord__Total_Sub_Line_Item_Recurring_Charges__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Recurring Charges</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>csord__Total_Service_One_Off_Charges__c</fullName>
        <deprecated>false</deprecated>
        <description>Sum of all the one off charges on all of the subscription’s services</description>
        <externalId>false</externalId>
        <inlineHelpText>Sum of all the one off charges on all of the subscription’s services</inlineHelpText>
        <label>Total Service One-Off Charges</label>
        <summarizedField>csord__Service__c.csord__Total_One_Off_Charges__c</summarizedField>
        <summaryForeignKey>csord__Service__c.csord__Subscription__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>csord__Total_Service_Recurring_Charges__c</fullName>
        <deprecated>false</deprecated>
        <description>Sum of all the recurring charges on all of the subscription’s services</description>
        <externalId>false</externalId>
        <inlineHelpText>Sum of all the recurring charges on all of the subscription’s services</inlineHelpText>
        <label>Total Service Recurring Charges</label>
        <summarizedField>csord__Service__c.csord__Total_Recurring_Charges__c</summarizedField>
        <summaryForeignKey>csord__Service__c.csord__Subscription__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>csord__Total_Sub_Line_Item_One_Off_Charges__c</fullName>
        <deprecated>false</deprecated>
        <description>Sum of all the one-off charges of the subscription’s subscription line items</description>
        <externalId>false</externalId>
        <inlineHelpText>Sum of all the one-off charges of the subscription’s subscription line items</inlineHelpText>
        <label>Total Sub. Line Item One-Off Charges</label>
        <summarizedField>csord__Subscription_Line_Item__c.csord__Total_Price__c</summarizedField>
        <summaryFilterItems>
            <field>csord__Subscription_Line_Item__c.csord__Is_Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>csord__Subscription_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>csord__Subscription_Line_Item__c.csord__Subscription__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>csord__Total_Sub_Line_Item_Recurring_Charges__c</fullName>
        <deprecated>false</deprecated>
        <description>Sum of all the recurring charges of the subscription’s subscription line items</description>
        <externalId>false</externalId>
        <inlineHelpText>Sum of all the recurring charges of the subscription’s subscription line items</inlineHelpText>
        <label>Total Sub. Line Item Recurring Charges</label>
        <summarizedField>csord__Subscription_Line_Item__c.csord__Total_Price__c</summarizedField>
        <summaryFilterItems>
            <field>csord__Subscription_Line_Item__c.csord__Is_Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>csord__Subscription_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>csord__Subscription_Line_Item__c.csord__Subscription__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Cancelled_By_Change_Process__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cancelled By Change Process</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Change_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Change Type</label>
        <picklist>
            <picklistValues>
                <fullName>Upgrade</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Downgrade</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Relocation</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cease</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Closed_Replaced__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Closed Replaced</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Product_Configuration__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Configuration</label>
        <referenceTo>cscfga__Product_Configuration__c</referenceTo>
        <relationshipLabel>Subscriptions</relationshipLabel>
        <relationshipName>Subscriptions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Replaced_Subscription__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Replaced Subscription</label>
        <referenceTo>csord__Subscription__c</referenceTo>
        <relationshipLabel>Replacement Subscriptions</relationshipLabel>
        <relationshipName>Subscriptions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Replacement_Subscription__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Replacement Subscription</label>
        <referenceTo>csord__Subscription__c</referenceTo>
        <relationshipLabel>Replaced Subscriptions</relationshipLabel>
        <relationshipName>Subscriptions1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Subscription_Number_Sequence__c</fullName>
        <deprecated>false</deprecated>
        <displayFormat>{000000000}</displayFormat>
        <externalId>false</externalId>
        <label>Subscription Number Sequence</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>AutoNumber</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Subscription_Number__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Subscription Number</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Subscription</label>
    <listViews>
        <fullName>csord__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Subscription Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Subscriptions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>csordtelcoa__Change_Request</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Change Request</masterLabel>
        <openType>sidebar</openType>
        <page>csordtelcoa__CreateChangeOrder</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>csordtelcoa__Visualise_Subscription_Delta</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Visualise Subscription Delta</masterLabel>
        <openType>sidebar</openType>
        <page>csordtelcoa__SubscriptionDeltaViewer</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
