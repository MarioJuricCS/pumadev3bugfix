/*
*  @Author <Accenture>
*  @Name <EP_ASTNonVMIShipToActiveToActive>
*  @CreateDate <28/02/2017>
*  @Description <Handles VMI Ship To Account status change from 05-Active to 05-Active>
*  @Version <1.0>
*/
public class EP_ASTNonVMIShipToActiveToActive extends EP_AccountStateTransition {

    public EP_ASTNonVMIShipToActiveToActive () {
        finalState = EP_AccountConstant.ACTIVE;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToActive','isTransitionPossible');
        return super.isTransitionPossible();
    }

    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToActive','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToActive','isGuardCondition');
        return true;
    }

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTNonVMIShipToActiveToActive','doOnExit');
    }
}