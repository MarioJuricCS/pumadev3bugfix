/**
 *  @Author <Kamal Garg>
 *  @Name <EP_OrderDynamicComponentPageController>
 *  @CreateDate <10/09/2016>
 *  @Description <This is used as a controller class for Order Dynamic Component Page>
 *  @Version <1.0>
 */
public with sharing class EP_OrderDynamicComponentPageController {

    public boolean isOrderConfirmationButtonVisible {
        get;
        set;
    }
    private ID orderId;
    private Order orderObj ;
    /**
     * @author <Kamal Garg>
     * @name <EP_OrderDynamicComponentPageController>
     * @date <10/09/2016>
     * @description <This method is used to load Order Status used to hide/unhide buttons>
     * @version <1.0>
     * @param ApexPages.StandardController
     * @return void
     */
    public EP_OrderDynamicComponentPageController(ApexPages.StandardController stdController) {
        isOrderConfirmationButtonVisible = false;
        
        Profile profileObj = [SELECT Name FROM Profile WHERE Id=:UserInfo.getProfileId() LIMIT:EP_Common_Constant.ONE];
        if(profileObj.Name.equalsIgnoreCase(EP_Common_Constant.CSC_AGENT_PROFILE)
        || profileObj.Name.equalsIgnoreCase(EP_Common_Constant.CSC_MANAGER_PROFILE)
        || profileObj.Name.equalsIgnoreCase(EP_Common_Constant.ADMIN_PROFILE)) 
        {
            orderId = stdController.getId();
            List<Order> records = [SELECT EP_Pricing_Status__c, EP_Sell_To__r.EP_Display_Price__c, Status, EP_Puma_Company_Code__c, (select id from OrderItems) FROM Order WHERE Id=:orderId LIMIT:EP_Common_Constant.ONE];
            system.debug('**records'+records);
            if(records.size() > 0) {
                orderObj = records.get(0);
                system.debug('**orderObj'+orderObj);
                system.debug('**orderObj'+orderObj.EP_Pricing_Status__c);
                system.debug('**orderObj'+orderObj.Status);
                system.debug('**orderObj'+orderObj.EP_Sell_To__r.EP_Display_Price__c);
                // Boolean flag to hide/unhide 'Generate Order Confirmation Doc' button on Order Details view.
                if(EP_OrderConfirmationConstant.PRICED.equals(orderObj.EP_Pricing_Status__c) 
                    && !(orderObj.Status.equals(EP_Common_Constant.ORDER_DRAFT_STATUS) && !(orderObj.EP_Sell_To__r.EP_Display_Price__c)))
                {
                    isOrderConfirmationButtonVisible = true;
                }
            }
        }
    }
    
    /**
     * @author <Kamal Garg>
     * @name <generateOrderConfirmation>
     * @date <10/09/2016>
     * @description <This method is used to send Order record details to OPMS>
     * @version <1.0>
     * @param 
     * @return PageReference
     */
    public PageReference generateOrderConfirmation() {
        String companyName = orderObj.EP_Puma_Company_Code__c;
        list<id> orderLineIdList = new list<id>();
        for(OrderItem orderItem : orderObj.OrderItems){
            orderLineIdList.add(orderItem.id);
        }
        EP_OutboundMessageService outboundService = new EP_OutboundMessageService(orderObj.Id, 'SEND_ORDER_CONFIRMATION_REQUEST', companyName);
        outboundService.sendOutboundMessage(EP_OrderConfirmationConstant.ORDER_CONFIRMATION,orderLineIdList);
        return null;
    }

}