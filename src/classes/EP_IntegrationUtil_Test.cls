/*   
     @Author <Sanchit Dua>
     @name <EP_IntegrationUtil_Test.cls>   
     @CreateDate <19/11/2015>   
     @Description <This class is used to cover the class EP_IntegrationUtil.>   
     @Version <1.1> 
     Revision updates:
     Added 3 methods for ok by jyotsna yadav
     Added 3 methods for error handling by <<Sanchit Dua>>
*/
@isTest
private class EP_IntegrationUtil_Test {
    //static member variable to be used as test data
    @testSetup static void init() {
		List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
    }
	
    /**
     * @author       Accenture
     * @name         reCreateSeqId_Positive
     * @date         13/10/2017
     * @description  Test Class to Test "reCreateSeqId" for Positive Scenario
     */
    @isTest
    static void reCreateSeqId_Positive() {
        String msgId = 'SFD-GBL-PER-12102017-17:32:19-015022';
		Account sellToAccount = EP_TestDataUtility.getSellTo();
		String result = EP_IntegrationUtil.reCreateSeqId(msgId,sellToAccount.Id);
		System.assertEquals(sellToAccount.Id+'-12102017173219-015022',result);
    }

    /**
     * @author       Accenture
     * @name         reCreateSeqId_Negative
     * @date         13/10/2017
     * @description  Test Class to Test "reCreateSeqId" for Negative Scenario
     */
    @isTest
    static void reCreateSeqId_Negative() {
        String msgId = null;
        String existingSeqId = null;
        String result = EP_IntegrationUtil.reCreateSeqId(msgId, existingSeqId);
        System.assertEquals(null, result);
    }
    /**
       Method to test the functionality of unique message Id.
      **/
    static testMethod void getMessageId_Test() {
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            String interfaceId = 'Customersync';
            test.startTest();
            	string msgId = EP_IntegrationUtil.getMessageId(interfaceId);
            test.stopTest();
		    System.assertNotEquals(interfaceId, msgId);
        }
    }

	
    /**
       Method to test the functionality of unique message Id.
    **/
    static testMethod void getMessageId2_Test() {
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            String interfaceId = 'Customersync';
            
            test.startTest();
            string msgId = EP_IntegrationUtil.getMessageId('source', 'local', 'processName', DateTime.now(), 'uniqueId');
            test.stopTest();
			System.assertNotEquals(interfaceId, msgId);
        }
    }

    /**
       Method to test the functionality of unique transaction Id.
    **/
    static testMethod void getTransactionId_Test() {
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            String interfaceId = 'Customersync';
            String transactionType = 'Create';
            
            test.startTest();
            string txId = EP_IntegrationUtil.getTransactionID(interfaceId, transactionType);
            test.stopTest();
			System.assertNotEquals(interfaceId, txId);
        }
    }
    static testMethod void getTransactionType_Test() {
   		System.runAs(EP_TestDataUtility.createRunAsUser()) {
        	EP_IntegrationTransactionSettings__c trnSetting = new EP_IntegrationTransactionSettings__c();
    		trnSetting.Name = 'CS-123';
    		insert trnSetting;
            
            test.startTest();
            	string txType = EP_IntegrationUtil.getTransactionType('CS');
            test.stopTest();
            
           	System.assertEquals(txType, trnSetting.Name);
        }
    }
    
    static testMethod void sendBulkOk_PositiveTest() {
    	System.runAs(EP_TestDataUtility.createRunAsUser()) {
        	Account sellToAccount = EP_TestDataUtility.getSellTo();
            
            test.startTest();
            	List < EP_IntegrationRecord__c > intRecList = EP_IntegrationUtil.sendBulkOk('transactionId', 'msgId', new List <Id>{sellToAccount.Id}, new List <String>{EP_Common_Constant.ACCOUNTS}, 'company', DateTime.now(), 'NAV' , new map < String, String >(), true);
            test.stopTest();
            
            System.assertNotEquals(0, intRecList.size());
       	}
    	
    }
    static testMethod void sendBulkOk_NegativeTest() {
    	System.runAs(EP_TestDataUtility.createRunAsUser()) {
        	Account sellToAccount = EP_TestDataUtility.getSellTo();
            
            test.startTest();
            	List < EP_IntegrationRecord__c > intRecList = EP_IntegrationUtil.sendBulkOk('transactionId', 'msgId', new List <Id>{sellToAccount.Id}, new List <String>{EP_Common_Constant.ACCOUNTS}, 'company', DateTime.now(), 'NAV' , null, true);
            test.stopTest();
            
            System.assertEquals(0, intRecList.size());
       	}
    	
    }
    static testMethod void sendBulkOk1_PositiveTest() {
    	System.runAs(EP_TestDataUtility.createRunAsUser()) {
        	Account sellToAccount = EP_TestDataUtility.getSellTo();
            
            test.startTest();
            	EP_IntegrationUtil.sendBulkOk(new List <Id>{sellToAccount.Id}, new List <String>{EP_Common_Constant.ACCOUNTS}, 'transactionId', 'msgId', 'company', DateTime.now(), 'NAV' , new map < String, String >());
            	//sendBulkOk(List < Id > objIdList, List < String > objTypeList, String transactionId, String msgId, String company, DateTime dtSent, String target, map < String, String > mChildParent);
            test.stopTest();
            
            List < EP_IntegrationRecord__c > intRecList = [select Id from EP_IntegrationRecord__c limit 1];
            System.assertNotEquals(0, intRecList.size());
       	}
    }
    static testMethod void sendBulkOk1_NegativeTest() {
    	System.runAs(EP_TestDataUtility.createRunAsUser()) {
        	Account sellToAccount = EP_TestDataUtility.getSellTo();
            
            test.startTest();
            	EP_IntegrationUtil.sendBulkOk(new List <Id>{sellToAccount.Id}, new List <String>{EP_Common_Constant.ACCOUNTS}, 'transactionId', 'msgId', 'company', DateTime.now(), 'NAV' , null);
            	//sendBulkOk(List < Id > objIdList, List < String > objTypeList, String transactionId, String msgId, String company, DateTime dtSent, String target, map < String, String > mChildParent);
            test.stopTest();
            
            List < EP_IntegrationRecord__c > intRecList = [select Id,EP_ParentNode__c from EP_IntegrationRecord__c limit 1];
            System.assertEquals(0, intRecList.size());
       	}
    }
    static testMethod void sendBulkError_Test() {
    	System.runAs(EP_TestDataUtility.createRunAsUser()) {
        	Account sellToAccount = EP_TestDataUtility.getSellTo();
            
            test.startTest();
            	List < EP_IntegrationRecord__c > intRecList = EP_IntegrationUtil.sendBulkError(new List <Id>{sellToAccount.Id}, new List <String>{EP_Common_Constant.ACCOUNTS}, 'transactionId', 'msgId','NAV', 'company', 'errorDescription', DateTime.now(), 'URL' , 'XML_Message');
    			//sendBulkError(List < ID > objectIds,List < String > objectTypes,String transactionId,String messageId, String target,String company,String errorDescription, Datetime dtSent, String URL, String XML_Message) {
            test.stopTest();
            
            System.assertNotEquals(0, intRecList.size());
       	}
    }
    static testMethod void createIntegrationRecordOutbound_Test() {
    	EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
	    insert country;
	    Account sellToAccount = EP_TestDataUtility.getSellTo();
		Account shipToTemp = EP_TestDataUtility.getShipTo();
		EP_Stock_Holding_Location__c stockHolding = EP_TestDataUtility.createStockLocation(shipToTemp.Id,true);
		insert stockHolding;
		
		Product2 prod = [SELECT Id FROM Product2 LIMIT 1];
		System.debug('*prod****'+prod);
		Map<ID, EP_Tank__c> tanksMap = EP_TestDataUtility.createTestTankRecords(new list<Account>{shipToTemp}, prod, 'New', 1);
		System.debug('*****tanksMap**'+tanksMap);
		
		Map<ID, EP_Tank_Dip__c> tankDipsMap  = EP_TestDataUtility.createTestTankDipsRecords(tanksMap.values(),Datetime.now(),true);
		EP_Credit_Exception_Request__c creditException = EP_TestDataUtility.createCreditException();
		EP_Bank_Account__c bankAccounts = EP_TestDataUtility.createBankAccount(sellToAccount.Id);
		bankAccounts.EP_Country__c = country.Id;
		insert bankAccounts;
		System.debug('*****tankDipsMap'+tankDipsMap);
		test.startTest();
				csord__Order__c newOrd = EP_TestDataUtility.getConsignmentOrderPositiveScenario();
			 /*csord__Order__c newOrd = new csord__Order__c();
	        	newOrd.Name = 'Test Order';
				newOrd.csord__Identification__c = 'Test Order';
			
			insert newOrd;*/
			
			csord__Order_Line_Item__c newOrdLine = new csord__Order_Line_Item__c();
			newOrdLine.Name = newOrd.id;
			newOrdLine.csord__Order__c = newOrd.id;
			newOrdLine.csord__Identification__c = newOrd.id;
			insert newOrdLine;
			EP_IntegrationUtil.objId_UniqueSeqIdMap.put(stockHolding.id, stockHolding.id);
			
	    	EP_IntegrationRecord__c intRec1 = EP_IntegrationUtil.createIntegrationRecordOutbound(stockHolding.id,EP_Common_Constant.STOCKLOCATIONS, 'transacionId-1', 'msgId-1', 'company', DateTime.now(), EP_Common_Constant.ERROR_SENT_STATUS, 'NAV', 'errorDescription');
	    	EP_IntegrationRecord__c intRec2 = EP_IntegrationUtil.createIntegrationRecordOutbound(newOrd.id,'Orders', 'transacionId-2', 'msgId-2', 'company', DateTime.now(), EP_Common_Constant.ERROR_SENT_STATUS, 'NAV', 'errorDescription');
	    	EP_IntegrationRecord__c intRec3 = EP_IntegrationUtil.createIntegrationRecordOutbound(bankAccounts.id,EP_Common_Constant.BANKACCOUNTS, 'transacionId-3', 'msgId-3', 'company', DateTime.now(), EP_Common_Constant.ERROR_RECEIVED_STATUS, 'NAV', 'errorDescription');
	    	EP_IntegrationRecord__c intRec4 = EP_IntegrationUtil.createIntegrationRecordOutbound(creditException.id,EP_Common_Constant.CREDITEXCEPTIONS, 'transacionId-4', 'msgId-4', 'company', DateTime.now(), EP_Common_Constant.ERROR_SYNC_STATUS, 'NAV', 'errorDescription');
	    	EP_IntegrationRecord__c intRec5 = EP_IntegrationUtil.createIntegrationRecordOutbound(new List<Id>(tanksMap.keySet()).get(0),EP_Common_Constant.TANKS_INT, 'transacionId-5', 'msgId-5', 'company', DateTime.now(), EP_Common_Constant.ERROR_SENT_STATUS, 'NAV', 'errorDescription');
    		EP_IntegrationRecord__c intRec6 = EP_IntegrationUtil.createIntegrationRecordOutbound(new List<Id>(tanksMap.keySet()).get(0),EP_Common_Constant.DIPS, 'transacionId-6', 'msgId-6', 'company', DateTime.now(), EP_Common_Constant.ERROR_SENT_STATUS, 'NAV', 'errorDescription');
    		EP_IntegrationRecord__c intRec7 = EP_IntegrationUtil.createIntegrationRecordOutbound(newOrdLine.id,'csord__Order_Line_Item__c', 'transacionId-7', 'msgId-7', 'company', DateTime.now(), EP_Common_Constant.ERROR_SENT_STATUS, 'NAV', 'errorDescription');
    		
    	test.stopTest();
    	//list<EP_IntegrationRecord__c> intRecList = [select Id from EP_IntegrationRecord__c limit 1];
    	System.assertNotEquals(null,intRec1.EP_Object_Record_Name__c);
    	System.assertNotEquals(null,intRec2.EP_Object_Record_Name__c);
    	System.assertNotEquals(null,intRec3.EP_Object_Record_Name__c);
    	System.assertNotEquals(null,intRec4.EP_Object_Record_Name__c);
    	System.assertNotEquals(null,intRec5.EP_Object_Record_Name__c);
    	System.assertNotEquals(null,intRec7.EP_Object_Record_Name__c);
    }
    
}