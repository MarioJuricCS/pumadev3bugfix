@isTest
public class EP_WINDMSOrderUpdateHelper_UT
{
    @testSetup static void init() {
        List<EP_Account_State_Mapping__c> lAccStateMapping = Test.loadData(EP_Account_State_Mapping__c.sObjectType, 'EP_Account_State_Mapping_TestData');
        List<EP_Order_State_Mapping__c> lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
        List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
        List<EP_Customer_Support_Settings__c>  lCustSuppSett = Test.loadData(EP_Customer_Support_Settings__c.sObjectType,'EP_Customer_Support_Settings');
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData');
        EP_INTEGRATION_CUSTOM_SETTING__c integration_Setting = new EP_INTEGRATION_CUSTOM_SETTING__c(Name = 'Request TimeOut',EP_Value__c = '120000');
        insert integration_Setting; 
    }
    static testMethod void createDataSets_test() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        
        Test.startTest();
            localObj.createDataSets(orderWrapperList);
        Test.stopTest();
        
        System.AssertEquals(true,localObj.orderNumberSet.size() > 0);
    }
    static testMethod void createDataMaps_test() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        Test.startTest();
        localObj.createDataMaps(orderWrapperList);
        Test.stopTest();
        System.AssertEquals(true,localObj.orderNumberOrderMap.keySet().Size() >0 );
        System.AssertEquals(true,localObj.orderWithStandardItemsMap.keySet().Size() >0);
    }
    static testMethod void setJSONAttributes_test() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        List<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        
        Test.startTest();
            localObj.createDataMaps(orderWrapperList);
            localObj.setJSONAttributes(orderWrapperList[0]);
        Test.stopTest();
        
        System.AssertEquals(orderWrapperList[0].orderStatusWinDms,orderWrapperList[0].sfOrder.EP_WinDMS_Status__c);
    }
    
    /*
    public static testMethod void setOrderAttributes_test() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        orderWrapperList[0].sfOrder = EP_TestDataUtility.getcurrentvmiorder();
        
        orderWrapperList[0].SFOrder.Status__c = EP_OrderConstant.OrderState_Planned;
        update orderWrapperList[0].SFOrder;
        
        orderWrapperList[0].identifier.orderIdSf = orderWrapperList[0].sfOrder.OrderNumber__c;
        localObj.orderNumberOrderMap.put(orderWrapperList[0].sfOrder.OrderNumber__c, orderWrapperList[0].sfOrder);
        String tripId ='123';
        
        orderWrapperList[0].SFOrder =[select id,OrderNumber__c,EP_WinDMS_Status__c, (select id from csord__Order_Line_Items__r) from csord__Order__c where Id=: orderWrapperList.get(0).SFOrder.Id];
        List<csord__Order_Line_Item__c> orderItems = [select id from csord__Order_Line_Item__c where OrderId__c=:orderWrapperList[0].SFOrder.Id];
        orderWrapperList[0].identifier.orderIdSf = orderWrapperList[0].sfOrder.OrderNumber__c;
        orderWrapperList[0].noOfLineItems = string.valueof(orderWrapperList[0].sfOrder.csord__Order_Line_Items__r.size());
        orderWrapperList[0].orderDlvryStartDt = '05.04.2017 01:31:00';
        orderWrapperList[0].orderStatusWinDms =EP_OrderConstant.OrderState_Planned;

        localObj.orderWithStandardItemsMap.put(orderWrapperList[0].identifier.orderIdSf, orderWrapperList[0].sfOrder);
        
        Test.startTest();
            localObj.setOrderAttributes(orderWrapperList,tripId);
        Test.stopTest();
        
        System.AssertEquals(orderWrapperList[0].seqId,orderWrapperList[0].sfOrder.EP_SeqId__c);
    }
    */

    /*
    public static testMethod void isValidOrder_PositiveScenarioTest() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        orderWrapperList.get(0).sfOrder = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        orderWrapperList.get(0).sfOrder.EP_WinDMS_Order_Number__c = 'WIN-1234';
        orderWrapperList.get(0).identifier.orderIdWinDMS = orderWrapperList.get(0).sfOrder.EP_WinDMS_Order_Number__c;
        update  orderWrapperList.get(0).sfOrder;
        list<OrderItem> orderItems = [SELECT ID FROM OrderItem WHERE EP_Is_Standard__c= TRUE AND EP_Is_Taxes__c = FALSE AND EP_Is_Freight_Price__c = FALSE AND OrderId =: orderWrapperList.get(0).sfOrder.Id];
        orderWrapperList.get(0).noOfLineItems = string.valueOf(orderItems.size());
        
        Test.startTest();
            localObj.createDataMaps(orderWrapperList);
            Boolean result = localObj.isValidOrder(orderWrapperList.get(0));
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    */

    static testMethod void isValidOrder_NegativeScenariotest() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        
        Test.startTest();
            Boolean result = localObj.isValidOrder(orderWrapperList.get(0));
        Test.stopTest();
        
        System.AssertEquals(false,result);
    }
    
    /*
    @isTest
    public static void isValidOrder_NegativeScenariotest_InvalidLineItems() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        orderWrapperList.get(0).sfOrder = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        orderWrapperList.get(0).sfOrder.EP_WinDMS_Order_Number__c = 'WIN-1234';
        orderWrapperList.get(0).identifier.orderIdWinDMS = orderWrapperList.get(0).sfOrder.EP_WinDMS_Order_Number__c;
        update  orderWrapperList.get(0).sfOrder;
        list<OrderItem> orderItems = [SELECT ID FROM OrderItem WHERE EP_Is_Standard__c= TRUE AND EP_Is_Taxes__c = FALSE AND EP_Is_Freight_Price__c = FALSE AND OrderId =: orderWrapperList.get(0).sfOrder.Id];
        orderWrapperList.get(0).noOfLineItems = string.valueOf(orderItems.size())+1;
        
        Test.startTest();
            localObj.createDataMaps(orderWrapperList);
            Boolean result = localObj.isValidOrder(orderWrapperList.get(0));
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    */
    
    /*
    public static testMethod void isOrderNumberExists_PositiveScenariotest() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        orderWrapperList[0].sfOrder = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        orderWrapperList[0].identifier.orderIdSf = orderWrapperList[0].sfOrder.OrderNumber__c;
        localObj.orderNumberOrderMap.put(orderWrapperList[0].sfOrder.OrderNumber__c, orderWrapperList[0].sfOrder);
        
        Test.startTest();
            Boolean result = localObj.isOrderNumberExists(orderWrapperList[0]);
        Test.stopTest();
        
        System.AssertEquals(true,result);
    }
    */

    static testMethod void isOrderNumberExists_NegativeScenariotest() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        
        Test.startTest();
            Boolean result = localObj.isOrderNumberExists(orderWrapperList[0]);
        Test.stopTest();
        
        System.AssertEquals(false,result);
    }
    
    /*
    public static testMethod void hasValidLineItems_PositiveScenariotest() {

        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        orderWrapperList.get(0).SFOrder = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        orderWrapperList.get(0).SFOrder =[select id,OrderNumber__c, (select id from csord__Order_Line_Items__r) from csord__Order__c where Id=: orderWrapperList.get(0).SFOrder.Id];
        List<csord__Order_Line_Item__c> orderItems = [select id from csord__Order_Line_Item__c where OrderId__c = :orderWrapperList[0].SFOrder.Id];
        orderWrapperList[0].identifier.orderIdSf = orderWrapperList[0].sfOrder.OrderNumber__c; 
        orderWrapperList[0].noOfLineItems = string.valueof(orderWrapperList[0].sfOrder.csord__Order_Line_Items__r.size());
        localObj.orderWithStandardItemsMap.put(orderWrapperList[0].identifier.orderIdSf, orderWrapperList[0].sfOrder);
        
        Test.startTest();
            Boolean result = localObj.hasValidLineItems(orderWrapperList[0]);
        Test.stopTest();
        
        System.AssertEquals(true,result);
    }
    */
    
    private static Account createAccount(Id recordTypeId) {
        
        Company__c company = new Company__c(Name = 'Test',
                                                EP_Company_Code__c = 'AAA',
                                                EP_Combined_Invoicing_Default__c = 'Create and Post',
                                                EP_Combined_Invoicing__c  = 'Delegate to Customer',
                                                EP_Window_Start_Hours__c = 8,
                                                EP_Window_End_Hours__c = 18,
                                                KYC_limit_in_KUSD__c = '10',
                                                EP_Company_Id__c = 01);
        insert company;
        
        Account acc = new Account();
        acc.Name = 'Test';
        acc.RecordTypeId = recordTypeId;
        acc.EP_Status__c = '05-Active';
        acc.EP_Puma_Company__c = company.Id;
        acc.BillingCity = 'testEPAccounttestCity';
        acc.BillingStreet = 'testEPAccounttestStreet';
        acc.BillingState = 'testEPAccounttestState';
        acc.BillingCountry = 'testEPAccounttestCountry';
        acc.BiLlingPostalCode = '89123456';
        acc.ShippingCity = 'testEPAccountShipCity';
        acc.ShippingStreet = 'testEPAccountShipStreet';
        acc.ShippingState = 'testEPAccountShipState';
        acc.EP_Eligible_for_Rebate__c =True;
        acc.ShippingCountry = 'testEPAccountShipCountry';
        acc.ShippingPostalCode = '78123465';
        insert acc;
        
        return acc;
    }
    
    /*
    @isTest
    public static void hasValidLineItems_NegativeScenariotest() {
        
        Profile profile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        
        User testUser = EP_TestDataUtility.createRunAsUser();
        testUser.ProfileID = profile.Id;
        testUser.TimeZoneSidKey = 'GMT';

        insert testUser;
    
        System.runAs(testUser) {
            
            RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND Name = 'VMI Ship To' LIMIT 1];
            
            Account nonVmiShipToAccount = createAccount(rt.Id);
            
            List<Account> objs = [SELECT Id, Name FROM Account];
            
            System.assertEquals(1, 2, JSON.serialize(objs));
            
            EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
            LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
            // orderWrapperList.get(0).sfOrder = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();

            orderWrapperList.get(0).sfOrder = EP_TestDataUtility.getCSOrder();
            
            orderWrapperList.get(0).identifier.orderIdSf = orderWrapperList.get(0).sfOrder.OrderNumber__c;
            orderWrapperList.get(0).noOfLineItems = '0';
            localObj.orderWithStandardItemsMap.put(orderWrapperList.get(0).sfOrder.OrderNumber__c, orderWrapperList.get(0).sfOrder);

            Test.startTest();
            
            Boolean result = localObj.hasValidLineItems(orderWrapperList.get(0));
            
            Test.stopTest();
            
            System.AssertEquals(false,result);
        }
    }
    */
    
    public static testMethod void isValidWINDMSID_PositiveScenariotest() {
        
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        EP_WINDMSOrderUpdateStub.OrderWrapper orderUpdateStub = new EP_WINDMSOrderUpdateStub.OrderWrapper();

        csord__Order__c obj = EP_TestDataUtility.getCurrentVMIOrder();
        obj.EP_WinDMS_Order_Number__c = 'WIN-11234';
        update obj;
        
        EP_WINDMSOrderUpdateStub.Identifier identifier = new EP_WINDMSOrderUpdateStub.Identifier();
        identifier.orderIdWinDMS = 'WIN-11234';
        identifier.orderIdSf = obj.Id;

        orderUpdateStub.sfOrder = obj;
        orderUpdateStub.identifier = identifier;
        
        localObj.orderNumberOrderMap.put(obj.Id, obj);

        Test.startTest();
        
        Boolean result = localObj.isValidWINDMSID(orderUpdateStub);
        
        Test.stopTest();
        
        System.AssertEquals(true,result);
    }

    public static testMethod void isValidWINDMSID_NegativeScenariotest() {
        
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        EP_OrderMapper orderMapperObj = new EP_OrderMapper();
        EP_WINDMSOrderUpdateStub.OrderWrapper orderUpdateStub = new EP_WINDMSOrderUpdateStub.OrderWrapper();

        csord__Order__c obj = EP_TestDataUtility.getCurrentVMIOrder();
        obj.EP_WinDMS_Order_Number__c = 'WIN-11234';
        update obj;
        
        EP_WINDMSOrderUpdateStub.Identifier identifier = new EP_WINDMSOrderUpdateStub.Identifier();
        identifier.orderIdWinDMS = obj.Id;
        identifier.orderIdSf = obj.Id;

        orderUpdateStub.sfOrder = obj;
        orderUpdateStub.identifier = identifier;
        
        localObj.orderNumberOrderMap.put(obj.Id, obj);

        Test.startTest();
        
        Boolean result = localObj.isValidWINDMSID(orderUpdateStub);
        
        Test.stopTest();
        
        System.AssertEquals(false,result);
    }

    /*
    public static testMethod void setOrderId_test() {
       EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        orderWrapperList.get(0).sfOrder = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        orderWrapperList.get(0).sfOrder.EP_WinDMS_Order_Number__c = 'WIN-11234';
        orderWrapperList.get(0).identifier.orderIdSf = orderWrapperList.get(0).sfOrder.OrderNumber__c;
        localObj.orderNumberOrderMap.put(orderWrapperList.get(0).sfOrder.OrderNumber__c, orderWrapperList.get(0).sfOrder);
        
        Test.startTest();
            localObj.setOrderId(orderWrapperList.get(0));
        Test.stopTest();
        
        System.AssertEquals(true,localObj.orderNumberOrderMap!=null);
    }
    */

    static testMethod void setOrderStatus_test() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        csord__Order__c sfOrder = EP_TestDataUtility.getcurrentvmiorder();
        sfOrder.Status__c = EP_OrderConstant.OrderState_Planned;
        update sfOrder;
        String newStatus = EP_OrderConstant.OrderState_Planned;
        
        Test.startTest();
            localObj.setOrderStatus(sfOrder,newStatus);
        Test.stopTest();
        
        System.AssertEquals(true, localObj.orderServiceMap != Null);

    }
    static testMethod void doPostActions_test() {
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        csord__Order__c sfOrder = EP_TestDataUtility.getcurrentvmiorder();
        sfOrder.Status__c = EP_OrderConstant.OrderState_Planned;
        update sfOrder;
        
        String newStatus = EP_OrderConstant.OrderState_Planned;
        localObj.setOrderStatus(sfOrder,newStatus);
        
        Set<ID>  orderIdSet = new Set<ID>{sfOrder.id};
        
        Test.startTest();
            localObj.doPostActions(orderIdSet);
        Test.stopTest();
        
        System.AssertEquals(true, localObj.orderServiceMap != Null);
        
    }

    /*
    public static testMethod void setDeliveryDate_test() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        orderWrapperList.get(0).sfOrder = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        orderWrapperList.get(0).orderDlvryStartDt = '05.04.2017 01:31:00';
        
        Test.startTest();
            localObj.setDeliveryDate(orderWrapperList.get(0));
        Test.stopTest();
        
        decimal expectedVal = 201704050131.00;
        System.AssertEquals(expectedVal,orderWrapperList.get(0).sfOrder.EP_Estimated_Delivery_Date_Time__c);
    }
    */

    /*
    public static testMethod void setOrderLoadCode_test() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        LIST<EP_WINDMSOrderUpdateStub.OrderWrapper> orderWrapperList = EP_TestDataUtility.getWINDMSOrderWrapper();
        // orderWrapperList.get(0).sfOrder = EP_TestDataUtility.getExRackOrder();
        
        csord__Order__c obj = EP_TestDataUtility.getCurrentVMIOrder();
        obj.EP_WinDMS_Order_Number__c = 'WIN-11234';
        update obj;
        
        // orderWrapperList.get(0).sfOrder = EP_TestDataUtility.getCurrentVMIOrder();
        
        orderWrapperList.get(0).sfOrder.EP_Delivery_Type__c = EP_Common_Constant.EX_RACK;
        localObj.orderNumberOrderMap.put(orderWrapperList[0].identifier.orderIdSf, orderWrapperList.get(0).sfOrder);
        String tripId ='123';
        
        Test.startTest();
            localObj.setOrderLoadCode(orderWrapperList.get(0),tripId);
        Test.stopTest();
        
        System.AssertEquals(true,orderWrapperList!=null);
    }
    */
    
    static testMethod void calculateEstimatedDeliveryDate_test() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        String orderDlvryStartDt = '05.04.2017 01:31:00';
        Test.startTest();
        Decimal result = localObj.calculateEstimatedDeliveryDate(orderDlvryStartDt);
        Test.stopTest();
        decimal expectedVal = 201704050131.00;
        System.AssertEquals(expectedVal,result);
    }
    static testMethod void calculateEstimatedDeliveryDate_NegativeScenariotest() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        String orderDlvryStartDt = '';
        Test.startTest();
        Decimal result = localObj.calculateEstimatedDeliveryDate(orderDlvryStartDt);
        Test.stopTest();
        decimal expectedVal = 201704050131.00;
        System.AssertEquals(null,result);
    }
    static testMethod void getOrdersByOrderNumber_test() {
        EP_WINDMSOrderUpdateHelper localObj = new EP_WINDMSOrderUpdateHelper();
        csord__Order__c ord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario(); 
        Set<string> orderNumberSet = new Set<String>{ord.OrderNumber__c};
            Test.startTest();
        Map<string, csord__Order__c> result = EP_WINDMSOrderUpdateHelper.getOrdersByOrderNumber(orderNumberSet);
        Test.stopTest();
        System.AssertEquals(true,localObj.orderNumberOrderMap != null);
    }
}
