public class EP_Import_Retail_Order_Helper{
    clsHeader header;
    clsLine lines;
    private EP_Import_Retail_Order_Context context;
    List<clsHeader> lstHeader;
    List<clsLine> lstMyLines;
    
    public EP_Import_Retail_Order_Helper(string inputXML){
        Dom.Document xml = new Dom.Document();
        xml.load(inputXML);
        system.debug('inputXML----'+inputXML);
        process(xml.getRootElement());
        
    }
    
     public void processXMLData() {
         system.debug('Entered----');
         system.debug('context.blbXMLFile.toString()----'+context.blbXMLFile.toString());
        context.strXMLFileContents = context.blbXMLFile.toString();
        
    }

    public void process(Dom.XmlNode inputNode){
        lstHeader = new List<clsHeader>();
        lstMyLines = new List<clsLine>(); 
        String strCurrentNodeName;
        system.debug('process inputNode----'+inputNode);
        system.debug('process inputNode----'+inputNode.getChildElements());
        for(Dom.XmlNode instChildNode :inputNode.getChildElements()){
            strCurrentNodeName = instChildNode.getName();
            if(strCurrentNodeName == 'header'){
                    header = new clsHeader();
                    header.process(instChildNode);
                    lstHeader.add(header);
                    system.debug('lstHeader++++++++++++'+lstHeader);
            }
            if(strCurrentNodeName == 'lines'){
                    lines = new clsLine();
                    lines.process(instChildNode);
                    lstMyLines.add(lines);
                    system.debug('lstMyLines++++++++++++'+lstMyLines);
            }
        }
    }

    public class clsSalesData{
        clsHeader header;
        
        List<clsLine> lstMyLines;

        public clsSalesData(){
            lstMyLines = new List<clsLine>();   
        }

        public void process(Dom.XmlNode inputNode){
            String strCurrentNodeName;
            system.debug('clsSalesData process inputNode----'+inputNode.getChildElements());
            for(Dom.XmlNode childNode :inputNode.getChildElements()){
                strCurrentNodeName = childNode.getName();
                if(strCurrentNodeName == 'header'){
                    header = new clsHeader();
                    header.process(childNode);
                }else if(strCurrentNodeName == 'lines'){
                    
                    clsLine tempLine;
                    for(Dom.XmlNode grandchildNode :childNode.getChildElements()){
                        tempLine = new clsLine();

                        tempLine.process(grandchildNode);
                        lstMyLines.add(tempLine);
                        system.debug('lstMyLines----'+lstMyLines);
                    }
                }
            }
        }
    }

  

    public class clsLine{
        integer intLineNr;
        integer intItemid;
        String  strDesc;
        decimal decQty;
        decimal decUnitPrice;

        public clsLine(){
        }

        public void process(Dom.XmlNode inputNode){
            String currentNodeName;
            system.debug('clsLine inputNode++++++++++++++++'+inputNode);
            for(Dom.XmlNode childNode :inputNode.getChildElements()){
                system.debug('clsLine currentNodeName++++++++++++++++'+currentNodeName);
            currentNodeName = childNode.getName();
                if(currentNodeName == 'lineNr'){
                    intLineNr = integer.valueOf(childNode.getText());
                }else if(currentNodeName == 'itemid'){
                    intItemid = integer.valueOf(childNode.getText());
                }else if(currentNodeName == 'desc'){
                    strDesc = childNode.getText();
                }else if(currentNodeName == 'qty'){
                    decQty = decimal.valueOf(childNode.getText());
                }else if(currentNodeName == 'unitPrice'){
                    decUnitPrice = decimal.valueOf(childNode.getText());
                }
            
            }
        }
    }
    
    public class clsHeader{
        string strCompanyCode;
        string strCreatedBy;
        string strFileName;
        integer intTransactionNr;
        integer intSellToId;
        integer intLocationId;
        integer intPosLocationId;
        date dtTransactionDt;
        date dtCreatedDt;
        
        

        public clsHeader(){
        }

        public void process(Dom.XmlNode inputNode){
            String currentNodeName;

            for(Dom.XmlNode childNode :inputNode.getChildElements()){
            currentNodeName = childNode.getName();
                if(currentNodeName == 'companyCode'){
                    strCompanyCode = childNode.getText();
                }else if(currentNodeName == 'transactionNr'){
                    intTransactionNr = integer.valueOf(childNode.getText());
                }else if(currentNodeName == 'sellToId'){
                    intSellToId = integer.valueOf(childNode.getText());
                }else if(currentNodeName == 'locationId'){
                    intLocationId = integer.valueOf(childNode.getText());
                }else if(currentNodeName == 'posLocationId'){
                    intPosLocationId = integer.valueOf(childNode.getText());
                }else if(currentNodeName == 'transactionDt'){
                    dtTransactionDt =  date.valueOf(childNode.getText());
                }else if(currentNodeName == 'createdDt'){
                    dtCreatedDt = date.valueOf(childNode.getText());
                }else if(currentNodeName == 'createdBy'){
                    strCreatedBy = childNode.getText();
                }else if(currentNodeName == 'fileName'){
                    strFileName = childNode.getText();
                }
            
            }
        }
    }
}