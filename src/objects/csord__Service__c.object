<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>A model of a service provided to a customer. A Service has a recurring component.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>csoe__Quantity_Decomposition_JSON__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity Decomposition JSON</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>csord__Activation_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>The date of activation.</description>
        <externalId>false</externalId>
        <inlineHelpText>The date of activation.</inlineHelpText>
        <label>Activation Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>csord__Client_Identifier__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <description>A field for the service owner (client) to store their own unique identifier on the Service Object, for ease of identification to external systems</description>
        <externalId>true</externalId>
        <inlineHelpText>A field for the service owner (client) to store their own unique identifier on the Service Object, for ease of identification to external systems</inlineHelpText>
        <label>Client Identifier</label>
        <length>100</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>csord__Deactivation_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>The date of deactivation.</description>
        <externalId>false</externalId>
        <inlineHelpText>The date of deactivation.</inlineHelpText>
        <label>Deactivation Date</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>csord__Deactivation_Reason__c</fullName>
        <deprecated>false</deprecated>
        <description>A reason to deactivate the service.</description>
        <externalId>false</externalId>
        <inlineHelpText>A reason to deactivate the service.</inlineHelpText>
        <label>Deactivation Reason</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csord__External_Identifier2__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <description>External Id field used for bulk inserts of Orders &amp; Subscriptions objects.</description>
        <externalId>true</externalId>
        <inlineHelpText>External Id field used for bulk inserts of Orders &amp; Subscriptions objects.</inlineHelpText>
        <label>External Identifier</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>csord__External_Identifier__c</fullName>
        <deprecated>false</deprecated>
        <description>DEPRECATED. External Id field used for bulk inserts of Orders &amp; Subscriptions objects.</description>
        <externalId>true</externalId>
        <inlineHelpText>DEPRECATED. Use External Identifier2 field instead.</inlineHelpText>
        <label>DEPRECATED External Identifier</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csord__Identification__c</fullName>
        <deprecated>false</deprecated>
        <description>Identification string for the object, used as the reference from external systems.</description>
        <externalId>false</externalId>
        <inlineHelpText>E.g. &quot;spokeServiceAmsterdam&quot; in Hub&amp;Spokes scenario.</inlineHelpText>
        <label>Identification</label>
        <length>255</length>
        <required>true</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csord__Order_Request__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Order Request object which introduced the latest changes to this object.</description>
        <externalId>false</externalId>
        <inlineHelpText>Order Request object which introduced the latest changes to this object.</inlineHelpText>
        <label>Order Request</label>
        <referenceTo>csord__Order_Request__c</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csord__Order__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The parent Order of this Service.</description>
        <externalId>false</externalId>
        <inlineHelpText>The parent Order of this Service.</inlineHelpText>
        <label>Order</label>
        <referenceTo>csord__Order__c</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csord__Service_Address__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The address to which this Service will be delivered.</description>
        <externalId>false</externalId>
        <inlineHelpText>The address to which this Service will be delivered.</inlineHelpText>
        <label>Service Address</label>
        <referenceTo>cscrm__Address__c</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csord__Service__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The parent Service of this Service.</description>
        <externalId>false</externalId>
        <inlineHelpText>The parent Service of this Service.</inlineHelpText>
        <label>Parent Service</label>
        <referenceTo>csord__Service__c</referenceTo>
        <relationshipLabel>Child Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csord__Solution__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>The parent Solution of this Service</description>
        <externalId>false</externalId>
        <inlineHelpText>The parent Solution of this Service</inlineHelpText>
        <label>Solution</label>
        <referenceTo>csord__Solution__c</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csord__Status__c</fullName>
        <deprecated>false</deprecated>
        <description>The status of the Service in the fulfillment process.</description>
        <externalId>false</externalId>
        <inlineHelpText>Used for state transitions.</inlineHelpText>
        <label>Status</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csord__Subscription__c</fullName>
        <deprecated>false</deprecated>
        <description>The parent Subscription of this Service.</description>
        <externalId>false</externalId>
        <inlineHelpText>The parent Subscription of this Service.</inlineHelpText>
        <label>Subscription</label>
        <referenceTo>csord__Subscription__c</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>csord__Total_One_Off_Charges__c</fullName>
        <deprecated>false</deprecated>
        <description>Sum of all the one-off charges of the service’s line items</description>
        <externalId>false</externalId>
        <inlineHelpText>Sum of all the one-off charges of the service’s line items</inlineHelpText>
        <label>Total One-Off Charges</label>
        <summarizedField>csord__Service_Line_Item__c.csord__Total_Price__c</summarizedField>
        <summaryFilterItems>
            <field>csord__Service_Line_Item__c.csord__Is_Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>csord__Service_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>csord__Service_Line_Item__c.csord__Service__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>csord__Total_Recurring_Charges__c</fullName>
        <deprecated>false</deprecated>
        <description>Sum of all the recurring charges of the service’s line items</description>
        <externalId>false</externalId>
        <inlineHelpText>Sum of all the recurring charges of the service’s line items</inlineHelpText>
        <label>Total Recurring Charges</label>
        <summarizedField>csord__Service_Line_Item__c.csord__Total_Price__c</summarizedField>
        <summaryFilterItems>
            <field>csord__Service_Line_Item__c.csord__Is_Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>csord__Service_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>csord__Service_Line_Item__c.csord__Service__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Cancelled_By_Change_Process__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cancelled By Change Process</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Delta_Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Delta Status</label>
        <picklist>
            <picklistValues>
                <fullName>Continuing In Subscription</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Added To Subscription</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Deleted From Subscription</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Main_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Parent_Product_Configuration__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Parent Product Configuration</label>
        <referenceTo>cscfga__Product_Configuration__c</referenceTo>
        <relationshipLabel>Services (Parent Product Configuration)</relationshipLabel>
        <relationshipName>Services1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Point_of_No_Return_Reached__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Inflight change is not possible on an Order whose child service has this checkbox ticked</description>
        <externalId>false</externalId>
        <inlineHelpText>Inflight change is not possible on an Order whose child service has this checkbox ticked</inlineHelpText>
        <label>Point of No-Return Reached</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Product_Basket__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Basket</label>
        <referenceTo>cscfga__Product_Basket__c</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Product_Bundle__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Bundle</label>
        <referenceTo>cscfga__Product_Bundle__c</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Product_Configuration__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Configuration</label>
        <referenceTo>cscfga__Product_Configuration__c</referenceTo>
        <relationshipLabel>Services</relationshipLabel>
        <relationshipName>Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Replaced_Service__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Replaced Service</label>
        <referenceTo>csord__Service__c</referenceTo>
        <relationshipLabel>Replacement Services</relationshipLabel>
        <relationshipName>Replaced_Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Replacement_Product_Configuration__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field is deprecated because it does not work well in MACD scenarios where several change request are created</description>
        <externalId>false</externalId>
        <inlineHelpText>Lookup to the Product Configuration that will be used for generation of a Service that will replace this Service instance</inlineHelpText>
        <label>DEPRECATED Replacement PC</label>
        <referenceTo>cscfga__Product_Configuration__c</referenceTo>
        <relationshipLabel>Replaced Services</relationshipLabel>
        <relationshipName>Replaced_Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Replacement_Service__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Replacement Service</label>
        <referenceTo>csord__Service__c</referenceTo>
        <relationshipLabel>Replaced Services</relationshipLabel>
        <relationshipName>Replacement_Services</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Service_Number_Sequence__c</fullName>
        <deprecated>false</deprecated>
        <displayFormat>{000000000}</displayFormat>
        <externalId>false</externalId>
        <label>Service Number Sequence</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>AutoNumber</type>
    </fields>
    <fields>
        <fullName>csordtelcoa__Service_Number__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Number</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Service</label>
    <listViews>
        <fullName>csord__All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Service Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Services</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>csordtelcoa__Modify_Service_Configuration</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Modify Service Configuration</masterLabel>
        <openType>sidebar</openType>
        <page>csordtelcoa__ModifyServiceConfiguration</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>csordtelcoa__View_Product_Configuration_Attributes</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>View Product Configuration Attributes</masterLabel>
        <openType>sidebar</openType>
        <page>csordtelcoa__ViewProductConfigAttributes</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
