/* ================================================
 * @Class Name : EP_CRM_EventTriggerHandler 
 * @author : Kamendra Singh
 * @Purpose: This Class is EP_CRM_EventTrigger trigger class and  is used to update Last Activity date on opportunity.
 * @created date: 08/07/2016
 ================================================*/
public with sharing class EP_CRM_EventTriggerHandler {
    private static set<id>oppIds = new set<id>();
/* ================================================
 * @Method Name : BeforeInsertUpdateEvent 
 * @author : Kamendra Singh
 * @Purpose: This method is used to update Last Activity date on opportunity on before insert and update Activity.
 * @created date: 08/07/2016
 ================================================*/
    public static void beforeInsertUpdateEvent(List<Event> lstEvent){
        try{
            for(Event objEvent : lstEvent){
                if(objEvent.whatid != null){
                string oppId = objEvent.whatid;
                if(oppId.startsWith(Label.EP_CRM_Oppty_Prefix)){
                    oppIds.add(objEvent.whatid);
                    }
                }
            }
            list<Opportunity> lstopp = [select id,EP_CRM_Last_Activity_Date__c from Opportunity where id IN : oppIds limit 50000];
            
            if(lstopp != null && !lstopp.isEmpty()){
                for(Opportunity objOpp : lstopp){
                    objOpp.EP_CRM_Last_Activity_Date__c  = system.now();
                }
                Database.SaveResult[] SR = Database.update(lstopp);
            }
        }
        catch(exception ex){
          system.debug(ex.getmessage());
        }
        
    }
/* ================================================
 * @Method Name : BeforeDeleteEvent 
 * @author : Kamendra Singh
 * @Purpose: This method is used to update Last Activity date on opportunity on before delete Activity.
 * @created date: 08/07/2016
 ================================================*/
    public static void beforeDeleteEvent(List<Event> triggerNew){
        try{
            list<Opportunity> updateOpplst = new list<Opportunity>();
            for(Event objEvent : triggerNew){
                if(objEvent.whatid != null){
                string oppId = objEvent.whatid;
                if(oppId.startsWith(Label.EP_CRM_Oppty_Prefix)){
                    oppIds.add(objEvent.whatid);
                    }
                }
            }
            
            for(Opportunity objOpp : [select id,EP_CRM_Last_Activity_Date__c,(select id,LastModifiedDate from Events where Id NOT IN: Trigger.old order by LastModifiedDate DESC),(select id,LastModifiedDate from tasks order by LastModifiedDate DESC) from Opportunity where Id IN : oppIds limit 50000]){
                List<Event> eventList = objOpp.Events;
                if(objOpp.Events.size()>=1){
                    objOpp.EP_CRM_Last_Activity_Date__c = eventList[0].LastModifiedDate;
                    updateOpplst.add(objOpp);
                }
                if(eventList.size()== 0 && objOpp.tasks.size() == 0){
                    objOpp.EP_CRM_Last_Activity_Date__c = null;
                    updateOpplst.add(objOpp);
                }
            }
            if(updateOpplst != null && !updateOpplst.IsEmpty()){
              Database.SaveResult[] SR = Database.update(updateOpplst);
            }
        }
        catch(exception ex){
          system.debug(ex.getmessage());
        }
    }
}