@isTest
public class EP_PortalOrderUtil_UT
{
    static String UOM = 'LT';
    static final String CUST_PO_NUMBER = '4444';
    
    static testMethod void getInventoryMap_GoodInventory_test() {
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        INSERT pricebook;
        Product2 prod = new Product2(Name = 'Product');
        INSERT prod;
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        INSERT entry;
        
        csord__Order__c newOrderRecord = EP_TestDataUtility.getSalesOrder();
        LIST<csord__Order_Line_Item__c> oiList = [SELECT Id, OrderId__c, PricebookEntryId__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:newOrderRecord.Id];
        csord__Order_Line_Item__c lineItem = oiList[0];
        lineItem.PricebookEntryId__c = String.valueOf(entry.Id);
        UPDATE lineItem;
        
        EP_Inventory__c inventory = EP_TestDataUtility.createInventory(null, String.valueOf(prod.Id));
        inventory.EP_Storage_Location__c = newOrderRecord.EP_Stock_Holding_Location__c;
        inventory.EP_Inventory_Availability__c = EP_Common_Constant.GOOD_INV;
        INSERT inventory;
        String strSelectedPickupLocationID = 'Test';
        
        Test.startTest();
            Map<Id,String> result = EP_PortalOrderUtil.getInventoryMap(oiList,newOrderRecord,strSelectedPickupLocationID);
        Test.stopTest();
        
        System.AssertEquals(true,result.size() == 1);
    }
    
    static testMethod void getInventoryMap_LowInventory_test() {
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        INSERT pricebook;
        Product2 prod = new Product2(Name = 'Product');
        INSERT prod;
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        INSERT entry;
        
        csord__Order__c newOrderRecord = EP_TestDataUtility.getSalesOrder();
        LIST<csord__Order_Line_Item__c> oiList = [SELECT Id, OrderId__c, PricebookEntryId__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:newOrderRecord.Id];
        csord__Order_Line_Item__c lineItem = oiList[0];
        lineItem.PricebookEntryId__c = String.valueOf(entry.Id);
        UPDATE lineItem;
        
        EP_Inventory__c inventory = EP_TestDataUtility.createInventory(null, String.valueOf(prod.Id));
        inventory.EP_Storage_Location__c = newOrderRecord.EP_Stock_Holding_Location__c;
        inventory.EP_Inventory_Availability__c = EP_Common_Constant.LOW_INV;
        INSERT inventory;
        String strSelectedPickupLocationID = 'Test';
        
        Test.startTest();
            Map<Id,String> result = EP_PortalOrderUtil.getInventoryMap(oiList,newOrderRecord,strSelectedPickupLocationID);
        Test.stopTest();
        
        System.AssertEquals(true,result.size() == 1);
    }
    
     static testMethod void getInventoryMap_NoInventory_test() {
        Pricebook2 pricebook = new Pricebook2(Name = 'Standard Price Book', isActive = TRUE);
        INSERT pricebook;
        Product2 prod = new Product2(Name = 'Product');
        INSERT prod;
        
        PricebookEntry entry = new PricebookEntry();
        entry.UnitPrice = 20;
        entry.Pricebook2Id = pricebook.Id;
        entry.Product2Id = prod.Id;
        INSERT entry;
        
        csord__Order__c newOrderRecord = EP_TestDataUtility.getSalesOrder();
        LIST<csord__Order_Line_Item__c> oiList = [SELECT Id, OrderId__c, PricebookEntryId__c FROM csord__Order_Line_Item__c WHERE OrderId__c =:newOrderRecord.Id];
        csord__Order_Line_Item__c lineItem = oiList[0];
        lineItem.PricebookEntryId__c = String.valueOf(entry.Id);
        UPDATE lineItem;
        
        EP_Inventory__c inventory = EP_TestDataUtility.createInventory(null, String.valueOf(prod.Id));
        inventory.EP_Storage_Location__c = newOrderRecord.EP_Stock_Holding_Location__c;
        inventory.EP_Inventory_Availability__c = EP_Common_Constant.NO_INV;
        INSERT inventory;
        String strSelectedPickupLocationID = 'Test';
        
        Test.startTest();
            Map<Id,String> result = EP_PortalOrderUtil.getInventoryMap(oiList,newOrderRecord,strSelectedPickupLocationID);
        Test.stopTest();
        
        System.AssertEquals(true,result.size() == 1);
    }
    
    
    static testMethod void validateExistingCEROnOrder_PositiveScenariotest() {
        csord__Order__c orderObj = EP_TestDataUtility.getSalesOrder();
        Id orderId = orderObj.id;
        Test.startTest();
        String result = EP_PortalOrderUtil.validateExistingCEROnOrder(orderId,orderObj.AccountId__c);
        Test.stopTest();
        System.AssertEquals(true,result != null);
    }
    static testMethod void validateExistingCEROnOrder_NegativeScenariotest() {
        csord__Order__c Orderobj = EP_TestDataUtility.getCurrentOrder();
        Id orderId = Orderobj.id;
        
        EP_Credit_Exception_Request__c credExReqObj = new EP_Credit_Exception_Request__c();
        credExReqObj.EP_Status__c = EP_Common_Constant.CREDIT_EXC_STATUS_PENDING_APPROVAL;
        credExReqObj.EP_Bill_To__c = Orderobj.AccountId__c;
        insert credExReqObj;
        
        Test.startTest();
        String result = EP_PortalOrderUtil.validateExistingCEROnOrder(orderId,Orderobj.AccountId__c);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    /* Petar
    static testMethod void createCreditExcRequest_test() {
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        Id orderId = ord.id;
        Id accountId = ord.AccountId__c;    
        Test.startTest();
        String result = EP_PortalOrderUtil.createCreditExcRequest(orderId,accountId);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }*/
    static testMethod void validateOverdueInvoice_PositiveScenariotest() {
        String currentRecordId = String.valueOf(EP_TestDataUtility.getSellTo().id); 
        Test.startTest();
        Boolean result = EP_PortalOrderUtil.validateOverdueInvoice(currentRecordId);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void validateOverdueInvoice_NegativeScenariotest() { //Always return false?
        String currentRecordId = String.valueOf(EP_TestDataUtility.getSellTo().id); 
        Test.startTest();
        Boolean result = EP_PortalOrderUtil.validateOverdueInvoice(currentRecordId);//Always return false?
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void getAvailableDates_test() { 

        EP_FE_NewOrderInfoResponse response = new EP_FE_NewOrderInfoResponse();
        Account stockLocAcc = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Stock_Holding_Location__c stockLoc = [SELECT Id, Stock_Holding_Location__r.EP_Business_Hours__c , EP_Sell_To__r.EP_Allow_Same_Day_Pickup__c FROM EP_Stock_Holding_Location__c LIMIT 1];
        Test.startTest();
        EP_PortalOrderUtil.getAvailableDates(response,stockLoc);
        Test.stopTest();
        System.AssertNotEquals(null,response.terminalInfoList.size() > 0);
    }
    static testMethod void fetchContracts_test() {

        Map<String,Trigger_Settings__c> trigerObj = Trigger_Settings__c.getAll();
        if(trigerObj.size() == 0){
            Trigger_Settings__c accountTriggerSetting = new Trigger_Settings__c(Name='EP_Account_Trigger', IsActive__c = false);
            database.insert(accountTriggerSetting); 
        }
        Account sellToAccountObj = EP_TestDataUtility.getSellToPositiveScenario();
        Account vendorAccountObj = EP_TestDataUtility.getVendorPositiveScenario();
        /* TFS fix 45559,45560,45567,45568 start EP_NAV_ID__c deprecated, EP_NAV_Vendor_Id__c is replaced by EP_Source_Entity_ID__c and EP_Vendor_Type__c is replaced by EP_VendorType__c*/
        vendorAccountObj.EP_VendorType__c = EP_Common_Constant.THIRD_PARTY_STCK_SUPPLIER;
        /* TFS fix 45559,45560,45567,45568 end*/
        update vendorAccountObj;
        Account storageLoc = EP_TestDataUtility.getStorageLocationPositiveScenario();
        Company__c company = [SELECT Id  FROM Company__c WHERE Id =: sellToAccountObj.EP_Puma_Company__c];
        EP_Payment_Term__c paytermObj = [SELECT ID FROM EP_Payment_Term__c LIMIT 1];
        Contract contractRec = EP_TestDataUtility.createContract(storageLoc.Id,company.id,paytermObj.Id,vendorAccountObj);
        Contract contractRec2 = EP_TestDataUtility.createContract(storageLoc.Id,company.id,paytermObj.Id,vendorAccountObj);
        contractRec.Status = EP_Common_Constant.ORDER_DRAFT_STATUS;
        contractRec2.Status = EP_Common_Constant.ORDER_DRAFT_STATUS;
        insert contractRec;
        insert contractRec2;
        contractRec.Status = EP_Common_Constant.EP_CONTRACT_STATUS_ACTIVE;
        contractRec2.Status = EP_Common_Constant.EP_CONTRACT_STATUS_ACTIVE;
        update contractRec;
        update contractRec2;
        List<Account> lSuppliers = new List<Account>{vendorAccountObj};
        Test.startTest();
        Map<Id,List<Contract>> result = EP_PortalOrderUtil.fetchContracts(lSuppliers);
        Test.stopTest();    
        System.assertEquals(true, result != null);
    }
    
    static testMethod void relatedBulkOrderMap_test() {
        csord__Order__c orderObj = EP_TestDataUtility.getConsumptionOrderPositiveScenario();
        String strAccountId = String.valueOf(orderObj.AccountId__c);
        Test.startTest();
        Map<String,csord__Order__c> result = EP_PortalOrderUtil.relatedBulkOrderMap(strAccountId);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    static testMethod void orgPckgdOrderMap_test() {
        String strAccountId = EP_TestDataUtility.getConsumptionOrderPositiveScenario().AccountId__c;
        Test.startTest();
        Map<String,csord__Order__c> result = EP_PortalOrderUtil.orgPckgdOrderMap(strAccountId);
        Test.stopTest();
        System.assertEquals(true, result != null);
    }
    static testMethod void setCustomerPoNumber_FromShipTo_test() {
        csord__Order__c newOrderRecord =EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        Account shipToAccountDetail = [SELECT id,EP_Customer_PO_Number__c,EP_Valid_From_date__c,EP_Valid_To_date__c FROM Account WHERE id =: newOrderRecord.EP_ShipTo__c LIMIT 1];
        Account sellToAccountDetail = [SELECT id,EP_Customer_PO_Number__c,EP_Valid_From_date__c,EP_Valid_To_date__c FROM Account WHERE id =: newOrderRecord.AccountId__c LIMIT 1];
        shipToAccountDetail.EP_Customer_PO_Number__c = CUST_PO_NUMBER;
        shipToAccountDetail.EP_Valid_From_date__c = System.today()-1;
        shipToAccountDetail.EP_Valid_To_date__c = System.today()+5;
        update shipTOAccountDetail;
        Test.startTest();
        EP_PortalOrderUtil.setCustomerPoNumber(newOrderRecord);
        Test.stopTest();
        System.AssertEquals(shipToAccountDetail.EP_Customer_PO_Number__c,newOrderRecord.EP_Customer_PO_Number__c);
    }
    
    static testMethod void setCustomerPoNumber_FromSellTo_test() {
        csord__Order__c newOrderRecord =EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        Account shipToAccountDetail = [SELECT id,EP_Customer_PO_Number__c,EP_Valid_From_date__c,EP_Valid_To_date__c FROM Account WHERE id =: newOrderRecord.EP_ShipTo__c LIMIT 1];
        Account sellToAccountDetail = [SELECT id,EP_Customer_PO_Number__c,EP_Valid_From_date__c,EP_Valid_To_date__c FROM Account WHERE id =: newOrderRecord.AccountId__c LIMIT 1];
        sellToAccountDetail.EP_Customer_PO_Number__c = CUST_PO_NUMBER;
        sellToAccountDetail.EP_Valid_From_date__c = System.today()-1;
        sellToAccountDetail.EP_Valid_To_date__c = System.today()+5;
        update sellToAccountDetail;
        Test.startTest();
        EP_PortalOrderUtil.setCustomerPoNumber(newOrderRecord);
        Test.stopTest();
        System.AssertEquals(sellToAccountDetail.EP_Customer_PO_Number__c,newOrderRecord.EP_Customer_PO_Number__c);
    }
    
    static testMethod void setCustomerPoNumber_ExRack_test() {
        csord__Order__c newOrderRecord =EP_TestDataUtility.getExRackOrder();
        //Account shipToAccountDetail = [SELECT id,EP_Customer_PO_Number__c,EP_Valid_From_date__c,EP_Valid_To_date__c FROM Account WHERE id =: newOrderRecord.EP_ShipTo__c LIMIT 1];
        Account sellToAccountDetail = [SELECT id,EP_Customer_PO_Number__c,EP_Valid_From_date__c,EP_Valid_To_date__c FROM Account WHERE id =: newOrderRecord.AccountId__c LIMIT 1];
        sellToAccountDetail.EP_Customer_PO_Number__c = CUST_PO_NUMBER;
        sellToAccountDetail.EP_Valid_From_date__c = System.today()-1;
        sellToAccountDetail.EP_Valid_To_date__c = System.today()+5;
        update sellToAccountDetail;
        Test.startTest();
        EP_PortalOrderUtil.setCustomerPoNumber(newOrderRecord);
        Test.stopTest();
        System.AssertEquals(sellToAccountDetail.EP_Customer_PO_Number__c,newOrderRecord.EP_Customer_PO_Number__c);
    }
    
    static testMethod void checkPrepayForPoNumber_PositiveScenariotest() {
        csord__Order__c newOrderRecord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        newOrderRecord.EP_Customer_PO_Number__c = '';
        Account orderAccount = [SELECT Id, EP_Is_Customer_Reference_Mandatory__c FROM Account WHERE Id =:newOrderRecord.AccountId__c];
        orderAccount.EP_Is_Customer_Reference_Mandatory__c = true;
        Boolean blnIsPrepaymentCustomer = true;
        Boolean blnIsConsignmentOrder = false;
        Boolean blnIsConsumptionOrder = true;
        Boolean isDummy = false;    
        Test.startTest();
        Boolean result = EP_PortalOrderUtil.checkPrepayForPoNumber(newOrderRecord,blnIsPrepaymentCustomer,blnIsConsignmentOrder,blnIsConsumptionOrder,isDummy,orderAccount);
        Test.stopTest();
        System.AssertEquals(true,result);
    }              
         
    static testMethod void checkPrepayForPoNumber_NegativeScenariotest() {
        csord__Order__c newOrderRecord = EP_TestDataUtility.getNonConsignmentOrderPositiveScenario();
        Boolean blnIsPrepaymentCustomer = false;
        Boolean blnIsConsignmentOrder = false;
        Boolean blnIsConsumptionOrder = false;
        Boolean isDummy = false;
        Account orderAccount = newOrderRecord.csord__Account__r;
        Test.startTest();
        Boolean result = EP_PortalOrderUtil.checkPrepayForPoNumber(newOrderRecord,blnIsPrepaymentCustomer,blnIsConsignmentOrder,blnIsConsumptionOrder,isDummy,orderAccount);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void getDeliveryStartDate_test() {
        Account stockLocAcc = EP_TestDataUtility.getSellToPositiveScenario();
        EP_Stock_Holding_Location__c stockLoc = [SELECT Id,Stock_Holding_Location__r.EP_Scheduling_Hours__c ,EP_Stock_Holding_Location__c.EP_Trip_Duration__c 
                                                    FROM EP_Stock_Holding_Location__c WHERE EP_Sell_To__c =: stockLocAcc.Id LIMIT 1];
        Test.startTest();
        String result = EP_PortalOrderUtil.getDeliveryStartDate(stockLoc);
        Test.stopTest();
        System.assertEquals(true, result!= null);
    }
    static testMethod void getAvailableProductQuantitiesMap_test() {
        EP_Order_Configuration__c orderConfiguration = EP_TestDataUtility.getOrderConfiguration();        
        orderConfiguration.RecordTypeId = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.ORDER_CONFIG_OBJ,EP_Common_Constant.RT_NON_MIXING_VALUE);
        update orderConfiguration;
        
        EP_Order_Configuration__c orderConfiguration2 = EP_TestDataUtility.getOrderConfiguration(); 
        orderConfiguration2.RecordTypeId = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.ORDER_CONFIG_OBJ,EP_Common_Constant.RT_NON_MIXING_VALUE);
        update orderConfiguration2;
        
        List<EP_Order_Configuration__c> orderConfigRecords = [SELECT Id, RecordType.DeveloperName,EP_Product__c,EP_Volume_UOM__c,EP_Min_Quantity__c FROM EP_Order_Configuration__c];        
        //EP_Order_Configuration__c orderConfigRecord2 = [SELECT Id, RecordType.DeveloperName,EP_Product__c,EP_Volume_UOM__c,EP_Min_Quantity__c FROM EP_Order_Configuration__c WHERE RecordType.DeveloperName =: EP_Common_Constant.RT_MIXING_RANGE LIMIT 1];        
        
        //List<EP_Order_Configuration__c> listOrderConfigurations = new List<EP_Order_Configuration__c>{orderConfigRecord,orderConfigRecord2 };
        Test.startTest();
        Map<String,List<String>> result = EP_PortalOrderUtil.getAvailableProductQuantitiesMap(orderConfigRecords);
        Test.stopTest();
        System.assert(result.size() > 0);
    }
    
     static testMethod void getProductRangeMap_test() {
        EP_TestDataUtility.getOrderConfiguration();
         Test.startTest();
        csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
         EP_Country__c country = [SELECT Id,EP_Mixing_Allowed__c FROM EP_Country__c LIMIT 1];      
         country.EP_Mixing_Allowed__c = true;
         update country;
        List<EP_Order_Configuration__c> orderConfigRecords = [SELECT Id, RecordType.DeveloperName,EP_Product__c,EP_Volume_UOM__c,EP_Min_Quantity__c,EP_Max_Quantity__c FROM EP_Order_Configuration__c];        
       
        Map<String,String> result = EP_PortalOrderUtil.getProductRangeMap(orderConfigRecords);
        Test.stopTest();
        System.assert(result != NULL);
    }
    
    public static testMethod void getProductRangeMap_MixingRange_test() {
       csord__Order__c ord = EP_TestDataUtility.getSalesOrder();
        EP_Country__c country = [SELECT Id,EP_Mixing_Allowed__c FROM EP_Country__c LIMIT 1];      
        country.EP_Mixing_Allowed__c = true;
        update country;
        
        Test.startTest();
        EP_Order_Configuration__c orderConfiguration = EP_TestDataUtility.getOrderConfiguration();  
        orderConfiguration.RecordTypeId = EP_Common_Util.getRecordTypeIdForGivenSObjectAndName(EP_Common_Constant.ORDER_CONFIG_OBJ,EP_Common_Constant.RT_MIXING_RANGE);
        update orderConfiguration;
        
        List<EP_Order_Configuration__c> orderConfigRecords = [SELECT Id, RecordType.DeveloperName,EP_Product__c,EP_Volume_UOM__c,EP_Min_Quantity__c,EP_Max_Quantity__c FROM EP_Order_Configuration__c];        
        
        Map<String,String> result = EP_PortalOrderUtil.getProductRangeMap(orderConfigRecords);
        Test.stopTest();
        System.assert(result != NULL);
    }
    
    static testMethod void constructProductQuantityLimitKey_test() {
        String strProductID = String.valueOf(EP_TestDataUtility.createProduct2(true).id);
        Test.startTest();
        String result = EP_PortalOrderUtil.constructProductQuantityLimitKey(strProductID,UOM);
        Test.stopTest();
        System.assertEquals(strProductID + EP_Common_Constant.UNDERSCORE_STRING + UOM , result);
    }
    
    static testMethod void getOperationalTanks_test() {        
        Account billToAcc = EP_AccountTestDataUtility.createBillToAccountWithProspectStatus(1, true)[0];
        Account shipToAcc = EP_TestDataUtility.createShipToAccount(billToAcc.Id, null);
        INSERT shipToAcc;
        
        EP_Tank__c tank = EP_TestDataUtility.createTestEP_Tank(shipToAcc);
        tank.EP_Tank_Status__c = EP_Common_Constant.TANK_OPERATIONAL_STATUS;
        
        Test.startTest();
            Map<Id,EP_Tank__c> result = EP_PortalOrderUtil.getOperationalTanks(new List<EP_Tank__c>{tank});
        Test.stopTest();
        
        System.assert(result.size() > 0);
    }
     
    static testMethod void getCountryRegionGroupMapping_test() {
        Test.startTest();
        List<EP_Country_Region_Group_Mapping__mdt> result = EP_PortalOrderUtil.getCountryRegionGroupMapping();
        Test.stopTest();
        System.assert(result.size() > 0);
    }

}