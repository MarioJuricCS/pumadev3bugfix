/*  
   @Author <Latika Parmar>
   @name <EP_Create_ShipTo_Extension>
   @CreateDate <22/09/2016>
   @Description <This class is a extension of page EP_Create_ShipTo which is lanched from Create Ship to button on Sell to>  
   @Version <1.0>
   */
   public without sharing class EP_CreateShipToExtension {
    
    public string selectedRecordTypeId{get;set;}
    public account acc=new account();

    /*private static final string IDPARAM  = 'id';
    private static final string ACC_EDIT_URL = '/001/e?retURL=';
    private static final string REC_TYPE_PARAM = '&RecordType=';
    private static final string ACCNAME_FIELDID  = '&acc3=';
    private static final string ACCNAME_HIDDEN_FIELDID = '&acc3_lkid=';
    private static final string EXTRA_URL = '&ent=Account';
    
    private static final string GET_RECORD_TYPE_MTHD = 'getRecordTypes';
    private static final string SELECT_RECORD_TYPE_MTHD = 'selectRecordType';*/
    private static final string CLASSNAME = 'EP_Create_ShipTo_Extension';
    private static final list<String> listOfShipToRecordTypeNames = new list<String>{
        EP_Common_Constant.NON_VMI_SHIP_TO_DEV_NAME,
        EP_Common_Constant.VMI_SHIP_TO_DEV_NAME
    };
    
    private EP_AccountMapper accountMapper =  new EP_AccountMapper();                                                               
    /*
    @Author: Latika Parmar
    @Method: EP_CreateShipToExtension
    @Description: Constructor Definition
    @CreateDate: 22/09/2016
    @nameParams ApexPages.StandardController controller
    @Return Type: NA
    */                                                         
    public EP_CreateShipToExtension(ApexPages.StandardController controller){      
      initializeData();
    }
  
    /*
    @Author: Latika Parmar
    @Method: initializeData
    @Description: This method query sell to data on load
    @CreateDate: 22/09/2016
    @nameParams: None
    @Return Type: void
    */
    @Testvisible
    private void initializeData(){
        EP_GeneralUtility.Log('Private','EP_CreateShipToExtension','initializeData');
        selectedRecordTypeId = EP_Common_Constant.BLANK;
        string AccountId= ApexPages.currentPage().getParameters().get(EP_AccountConstant.IDPARAM);
        if(string.isNotBlank(accountId)){
            /*QUERY THE SELL TO ACCOUNT BASED ON THE ID IN URL*/
            List<Account> listOfAccounts = accountMapper.getRecordsByIds(new set<id>{accountId});
            if(!listOfAccounts.isEmpty()){
                acc = listOfAccounts[0];
            }
        }
    }
    
    /*
    @Author: Latika Parmar
    @Method: getRecordTypes
    @Description: This method is used to create picklist of record type based on user profile and parent account record type
    @CreateDate: 22/09/2016
    @nameParams: None
    @Return Type: List<SelectOption>
    */
    public List<SelectOption> getRecordTypes(){
        EP_GeneralUtility.Log('Public','EP_CreateShipToExtension','getRecordTypes');
        
        List<SelectOption> options = new list<SelectOption>();   
        
        try{
            /*QUERY PROFILE ID OF STOCK ACCOUNTANT PROFILE*/
            //Defect 57595 START
            List<Profile> listOfProfiles  = queryProfiles(new list<String>{EP_Common_Constant.EP_Stock_Controller_R1});
            //Defect 57595 END
            if(!listOfProfiles.isEmpty() != null && listOfProfiles[0].id == UserInfo.getProfileId()){
                options.addAll(getSelectOptions(new list<String>{EP_Common_Constant.STORAGE_SHIP_TO_DEV_NAME}));    
            }
            else{
                options.addAll(getSelectOptions(listOfShipToRecordTypeNames));
            }
            }Catch(Exception handledException){
                
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR,system.label.EP_Internal_Error_Message);
                Apexpages.addmessage(msg);
                EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, EP_AccountConstant.GET_RECORD_TYPE_MTHD, CLASSNAME, ApexPages.Severity.ERROR);
                
            }
            return Options;
        }
        
    /*
    @Author Latika Parmar
    @Method: selectRecordType
    @Description: This method is used to launch selected record type page 
    @CreateDate: 22/09/2016
    @nameParams: None
    @Return Type: PageReference
    */
    
    public PageReference selectRecordType(){
        EP_GeneralUtility.Log('Public','EP_CreateShipToExtension','selectRecordType');
        pagereference page ;
        try {
            page = new pagereference(EP_AccountConstant.ACC_EDIT_URL+acc.Id+EP_AccountConstant.REC_TYPE_PARAM+selectedRecordTypeId+EP_AccountConstant.ACCNAME_FIELDID+acc.Name+EP_AccountConstant.ACCNAME_HIDDEN_FIELDID+acc.Id+EP_AccountConstant.EXTRA_URL);
            }catch(Exception handledException){
                apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.ERROR,system.label.EP_Internal_Error_Message);
                Apexpages.addmessage(msg);
                EP_LoggingService.logHandledException(handledException, EP_Common_Constant.EPUMA, EP_AccountConstant.SELECT_RECORD_TYPE_MTHD, CLASSNAME, ApexPages.Severity.ERROR);
            }
            return page;
        }
        
    /*
    @Author Latika Parmar
    @Method: queryProfiles
    @Description: This method query the profiles basesd on profile name to compare current user profile id with stock accountant profile id
    @CreateDate: 22/09/2016
    @nameParams: List<String>listOfProfileNames
    @Return Type: List<Profile>
    */
    @Testvisible
    private List<Profile> queryProfiles(List<String>listOfProfileNames){
        EP_GeneralUtility.Log('Private','EP_CreateShipToExtension','queryProfiles');
        EP_ProfileMapper profileMapper = new EP_ProfileMapper();
        /*QUERY PROFILE BASED ON PROFILE NAME*/
        return profileMapper.getProfilesByName(listOfProfileNames);
    }
    
    /*
    @Author Latika Parmar
    @Method: queryRecordTypes
    @Description: This method query the record types base on record type developer name to display record type in picklist based on current user's profile
    @CreateDate: 22/09/2016
    @nameParams: List<String>listOfRecordTypeDeveloperNames
    @Return Type: List<RecordType>
    */
    @Testvisible
    private List<RecordType> queryRecordTypes(List<String>listOfRecordTypeDeveloperNames){
        EP_GeneralUtility.Log('Private','EP_CreateShipToExtension','queryRecordTypes');
        /*QUERY RECORDTYPE VISIBLE TO CURRENT USER*/
        return accountMapper.getAccountRecordTypes(listOfRecordTypeDeveloperNames);
    }
    
    @Testvisible
    private List<SelectOption> getSelectOptions(list<string> listOfRecordTypeNames){
        EP_GeneralUtility.Log('Private','EP_CreateShipToExtension','getSelectOptions');        
        List<SelectOption> options = new list<SelectOption>();
        for(RecordType rts:queryRecordTypes(listOfRecordTypeNames)){
            options.add(EP_Common_Util.createOption(rts.ID, rts.name));// craeteSelectOption(rts.ID, rts.name));
        }
        return options;        
    }
    
}