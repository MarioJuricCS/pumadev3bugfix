@isTest
public class EP_RouteMapper_UT
{   
    static testMethod void getRecordsByStorageLocation_test() {
    Test.startTest();
        EP_RouteMapper localObj = new EP_RouteMapper();
        Set<String> Str = new Set<String>();
        list<EP_Route__c> listRouteLocation = [SELECT Id,EP_Storage_Location__c FROM EP_Route__c];
        for(EP_Route__c routeMapperObj : listRouteLocation){
            Str.add(routeMapperObj.id);
        }
        Integer limitVal = 1;
        
        list<EP_Route__c> result = localObj.getRecordsByStorageLocation(Str,limitVal);
        Test.stopTest();
        System.AssertEquals(Str.size(),result.size());
    }
    
    static testMethod void getRecordsByStorageLocation_test2() {
     Test.startTest();
        EP_RouteMapper localObj = new EP_RouteMapper();
        Set<Id> idSet= new Set<Id>();
        list<EP_Route__c> listRouteLocation = [SELECT Id,EP_Storage_Location__c FROM EP_Route__c];
        for(EP_Route__c routeMapperObj : listRouteLocation){
            idSet.add(routeMapperObj.id);
        }
       
        list<EP_Route__c> result = localObj.getRecordsByStorageLocation(idSet);
        Test.stopTest();
        System.AssertEquals(idSet.size(),result.size());
    }
   
    /* Petar
    public static testMethod void getRoutesWithOrdersForNameCodeChange_test(){
        
        EP_RouteMapper mapper = new EP_RouteMapper();
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        for(EP_Route__c route : routeRecords.values()){
            routeList.add(route);
        }
        Test.startTest();
        csord__Order__c submittedOrderObj = EP_TestDataUtility.getOrderStateSubmittedDomainObjectPositiveScenario().getOrder();
        submittedOrderObj.EP_Route__c = routeList[0].id;
        update submittedOrderObj;
        
        csord__Order__c planningOrderObj = EP_TestDataUtility.getConsignmentOrderPlanningStatus();
        planningOrderObj.EP_Route__c = routeList[0].id;
        update planningOrderObj;
        Map<Id,List<Order>> routeWithOrder =  mapper.getRoutesWithOrdersForNameCodeChange(routeRecords.keySet());
        Test.stopTest();
        System.assert(routeWithOrder != NULL);
    }
    */
    
    static testMethod void getAssociatedOrdersWithRoute_test(){
        EP_OrderDomainObject ordDomObj = EP_TestDataUtility.getOrderStatePlannedDomainObject();
        EP_AccountMapper accMapper = new EP_AccountMapper();
        csord__Order__c ord = ordDomObj.getOrder();
        Account acc = accMapper.getAccountRecord(ord.EP_ShipTo__r.Id);     
        Company__c company = acc.EP_Puma_Company__r;        
           EP_Route__c route = new EP_Route__c();
           route.EP_Route_Name__c = 'route';
           route.EP_Route_Code__c = 'code';
          route.EP_Status__c = 'Active';
           route.EP_Company__r = company;               
           insert route;
          
         List<EP_Run__c> runRecords = new List<EP_Run__c>();
         for(Integer i =0; i < 2; i++){
            EP_Run__c runRecToInsert = new EP_Run__c ();            
            runRecToInsert.EP_Run_Start_Date__c =  system.today();
            runRecToInsert.EP_Run_End_Date__c =  system.today()+5;
            runRecToInsert.EP_Route__c = route.id;
           runRecords.add(runRecToInsert);
         }      
           insert runRecords;

        EP_RouteMapper mapper = new EP_RouteMapper();
        Set<Id> routeSet = new Set<Id>();
        for(EP_Run__c run : runRecords){
            routeSet.add(run.EP_Route__c);
        }
        Test.startTest();
        
        ord.EP_Route__c = runRecords[0].EP_Route__c;
        ord.EP_Run__c = runRecords[0].id;
        update ord;
        
        Map<Id,List<Order>> routeWithOrder =  mapper.getAssociatedOrdersWithRoute(routeSet);
        Test.stopTest();
        System.assert(routeWithOrder != NULL);
    }
    
    
    static testMethod void getRouteRecordsById_test(){
        EP_RouteMapper mapper = new EP_RouteMapper();
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        for(EP_Route__c route : routeRecords.values()){
            routeList.add(route);
        }
        Test.startTest();
        Map<Id,EP_Route__c> routeMap =  mapper.getRouteRecordsById(routeRecords.keySet());
        Test.stopTest();
        System.assert(routeMap != NULL);
    }
    
    static testMethod void getRoutesWithAccountForStatusChange_test(){
        EP_RouteMapper mapper = new EP_RouteMapper();
        Map<Id,EP_Route__c> routeRecords = EP_TestDataUtility.getRouteObjects(1);
        List<EP_Route__c> routeList = new List<EP_Route__c>();
        for(EP_Route__c route : routeRecords.values()){
            routeList.add(route);
        }
         Test.startTest();
        csord__Order__c ord = [SELECT Id,EP_ShipTo__r.Id FROM csord__Order__c LIMIT : EP_Common_Constant.ONE];
        EP_AccountMapper accMapper = new EP_AccountMapper();
        Account acc = accMapper.getAccountRecord(ord.EP_ShipTo__r.Id);  
        EP_Route__c route = routeList[0];
        route.EP_Company__c = acc.EP_Puma_Company__c;
        update route;
        acc.EP_Default_Route__c = routeList[0].id;
        update acc;    
       
        Set<Id> routeSet =  mapper.getRoutesWithAccountForStatusChange(routeRecords.keySet());
        Test.stopTest();
        System.assert(routeSet != NULL);
    }
}
