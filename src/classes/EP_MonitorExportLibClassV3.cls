/* 
   @Author Spiros Markantonatos
   @name <EP_MonitorExportLibClassV3>
   @CreateDate <23/12/2016>
   @Description <This class is used by the APIs returning log records to the ePuma monitoring system to deliver common methods.
   				This version of the lib class is used to return the date from/to dates>
   @Version <1.0>
*/
public with sharing class EP_MonitorExportLibClassV3 {
    // Const
    private static final Integer QUERY_LIMIT = 1000;
    private static final Integer MAX_QUERY_LIMIT = 5000;
    private static final String RETURN_PAY_LOAD = 'returnPayloadBody';
	private static final String CLASS_NAME = 'EP_MonitorExportLibClass';
    
    // Methods
    /*
    Returns payload body
    */
    public static String returnPayloadBody(List<sObject> lRecords, String strSuccessHeader, String strFailHeader) {
    	String strResponse = EP_Common_Constant.BLANK;
    	
    	try {
	    	if (!lRecords.isEmpty()) {
	    		JSONGenerator gen = JSON.createGenerator(TRUE);
	        	// Convert list of records into JSON format
	        	strResponse = JSON.serialize(lRecords);
	        	
	            // Add data to the response string
				strResponse = strSuccessHeader + strResponse + EP_Common_Constant.COMMA_STRING;
	           	strResponse += EP_Common_Constant.RESULTS + lRecords.size() + EP_Common_Constant.RIGHT_CURLY_BRACE;
	        } else {
	        	// Add 'No Exception log data' to the response string
	            strResponse = strFailHeader + EP_Common_Constant.COMMA_STRING;
	            strResponse += EP_Common_Constant.RESULTS + lRecords.size() + EP_Common_Constant.RIGHT_CURLY_BRACE;
	        }
        } catch (exception ex) {
        		strResponse = EP_Common_Constant.BLANK;
        		EP_LoggingService.logServiceException(ex, 
        											UserInfo.getOrganizationId(), 
        												EP_Common_Constant.EPUMA, 
        													RETURN_PAY_LOAD, 
        														CLASS_NAME, 
        															EP_Common_Constant.ERROR, 
        																UserInfo.getUserId(), 
        																	EP_Common_Constant.TARGET_SF, 
        																		EP_Common_Constant.BLANK, 
        																			EP_Common_Constant.BLANK);
    	}
        
        return strResponse;
    }
    
    /*
    Return the limit to be used in the query
    */
    public static Integer returnQueryLimitFromRequest(String strParamName) {
    	
    	Integer intLimit = QUERY_LIMIT;
    	
    	String strLimit = returParameterFromRequest(strParamName);
    	
    	if (String.isNotBlank(strLimit))
    	{
    		if (strLimit.isNumeric())
    		{
    			intLimit = Integer.valueOf(strLimit);
    			
    			if (intLimit > MAX_QUERY_LIMIT)
    			{
    				intLimit = MAX_QUERY_LIMIT;	
    			}
    		}
    	}
    	
    	return intLimit;
    }
    
    /*
    Return the date filter to be used in the query
    */
    public static DateTime returnQueryDateFilterFromRequest(String strParamName) {
    	
    	DateTime dtSearchDateTime = NULL;
    	
    	String strDateTime = returParameterFromRequest(strParamName);
    	
    	if (String.isNotBlank(strDateTime))
    	{
	    	try {
	    		dtSearchDateTime = (DateTime)JSON.deserialize(
													EP_Common_Constant.DOUBLE_QUOTES_STRING + 
														strDateTime + 
 															EP_Common_Constant.DOUBLE_QUOTES_STRING, 
 																DateTime.class);
	    	} catch(exception e) {
	    		dtSearchDateTime = NULL;
	    	}
    	}
    	
    	return dtSearchDateTime;
    }
    
    /*
    Return the param value from the REST context
    */
    private static String returParameterFromRequest(String strParamName) {
    	String strParam = NULL;
    	
    	if (RestContext.request.params.containsKey(strParamName))
    	{
    		strParam = RestContext.request.params.get(strParamName);
    	}
    	
    	return strParam;
    }
}