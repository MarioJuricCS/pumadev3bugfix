/*
 *  @Author <Accenture>
 *  @Name <EP_ASTSellToActiveToBlocked>
 *  @CreateDate <16/02/2017>
 *  @Description <Handles Account status change from Active to Blocked>
 *  @Version <1.0>
 */
 public class EP_ASTSellToActiveToBlocked extends EP_AccountStateTransition {
    public EP_ASTSellToActiveToBlocked() {
        finalstate = EP_AccountConstant.BLOCKED;
    }

    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }
    
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }

    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTSellToActiveToBlocked','isGuardCondition');
        //No guard conditions for Active to Blocked
        return true;
    }
}