<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Holds the result of external callouts</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>csbb__Error_Code__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Error Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csbb__Error_Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Error Description</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>csbb__Error_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Error Type</label>
        <picklist>
            <picklistValues>
                <fullName>Internal</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Service</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No Error</fullName>
                <default>true</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>csbb__JSON_Request_Message__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>JSON Request Message</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>csbb__Method_Shortcut__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Method Shortcut</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csbb__Method__c</fullName>
        <deprecated>false</deprecated>
        <description>Name of the method invoked</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of the method invoked</inlineHelpText>
        <label>Method</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csbb__Request_Initiated__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Request Initiated</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>csbb__Request_Time__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>csbb__Response_Received__c - csbb__Request_Initiated__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Request Time</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csbb__Requested_By__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>User how requested this callout</description>
        <externalId>false</externalId>
        <inlineHelpText>User how requested this callout</inlineHelpText>
        <label>Requested By</label>
        <referenceTo>User</referenceTo>
        <relationshipName>CalloutResults</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>csbb__Response_Received__c</fullName>
        <deprecated>false</deprecated>
        <description>Time the response was received</description>
        <externalId>false</externalId>
        <inlineHelpText>Time the response was received</inlineHelpText>
        <label>Response Received</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>csbb__Retry__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Retry call in case of Fail</description>
        <externalId>false</externalId>
        <label>Retry</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>csbb__SOAP_Request_Message__c</fullName>
        <deprecated>false</deprecated>
        <description>Raw Request Message in SOAP format</description>
        <externalId>false</externalId>
        <inlineHelpText>Raw Request Message in SOAP format</inlineHelpText>
        <label>SOAP Request Message</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>csbb__Service_Method_Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>csbb__Service__c &amp; &apos;_&apos; &amp;  csbb__Method__c</formula>
        <label>Service Method Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csbb__Service__c</fullName>
        <deprecated>false</deprecated>
        <description>Name of the service</description>
        <externalId>false</externalId>
        <inlineHelpText>Name of the service</inlineHelpText>
        <label>Service</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csbb__Status_Code__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status Code</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>csbb__Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <picklist>
            <picklistValues>
                <fullName>Queued</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Success</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Fail</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Callout Result</label>
    <listViews>
        <fullName>csbb__All</fullName>
        <columns>NAME</columns>
        <columns>csbb__Status__c</columns>
        <columns>OBJECT_ID</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>csbb__All_Callouts</fullName>
        <columns>NAME</columns>
        <columns>csbb__Method__c</columns>
        <columns>csbb__Service__c</columns>
        <columns>csbb__Status__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All Callouts</label>
    </listViews>
    <nameField>
        <displayFormat>CR-{00000000}</displayFormat>
        <label>Callout Result Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Callout Results</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
