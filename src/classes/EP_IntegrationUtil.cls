/*   
     @Author <Sanchit Dua>
     @name <EP_IntegrationsUtil.cls>   
     @CreateDate <19/11/2015>   
     @Description <This class will include the common methods to be used for integration record maintenance.>   
     @Version <1.1> 
     Revision updates:
     Added 3 methods for ok by jyotsna yadav
     Added 3 methods for error handling by <<Sanchit Dua>>
     
     Added 2 methods for retry <Jai Singh>
     */
public without sharing class EP_IntegrationUtil {

    private static final String UNDERSCORE = '_'; // EP_Common_Constant.UNDERSCORE_STRING;
    public static Boolean updateHYPHEN_STRINGle = false;
    private static final String INITIAL_VAL = '';
    private static final String SF = 'SF';
    //private static final Integer LMT = Limits.getLimitQueries();
    static final String MSG_STAMP_FORMAT = 'ddMMYYYY\'-\'HH:mm:ss';
    /**NOVASUITE CHANGES BY ASHOK**-START**/
    private static final String COUNTRY_FIELD = 'EP_Country__c';
    private static final String SHIP_TO = 'EP_ShipTo__c';
    private static final String SENT = 'SENT';
    private static final String ERROR_SYNC_SENT_STATUS = 'Error';
    //Added by Sandeep
    private static final String SHIP_TOONTANK = 'EP_Ship_To__c';
    private static final String TANK_ID = 'EP_Tank__c';
    private static final String NAME = 'Name';
    private static final String ACCOUNT_ID = 'csord__Account__c';
    private static final String ORDER_NO = 'OrderNumber__c';
    private static final String ORDER_ID = 'csord__Order__c';
    private static final String ORDER_ITEM_NO = 'Order_Line_Number__c';
    //L4_45352_START
    public static boolean ISERRORSYNC = false;
    //L4_45352_END    
    private static final String QUERY_ORDER_ITEM_NO = 'Select Order_Line_Number__c,csord__Order__c from ';
    private static final String QUERY_ACC_ID_FRM_ORDER = 'Select csord__Account__c from csord__Order__c ';
    private static final String QUERY_ORDER_NO = 'Select OrderNumber__c, EP_ShipTo__c,csord__Account__c from ';
    private static final String QUERY_TANK = 'Select Name,EP_Tank__c from ';
    private static final String QUERY_CNTRY_FRM_ACC = 'Select Name,EP_Country__c from Account ';
    private static final String QUERY_COUNTRY = 'Select Name,EP_Country__c from ';
    private static final String QUERY_SHIP_TO = 'Select Name, EP_Ship_To__c from ';
    private static final String QUERY_NAME = 'Select Name from ';
    private static final String WHERE_ID = ' Where Id=';
    private static final String ESCAPE_SEQ = '\'';
    /**NOVASUITE CHANGES BY ASHOK**-START**/
    static final String HYPHEN_STRING = '-';

    public static Boolean isCallout = false;
    public static Boolean isManualRetry = true;
    public static Boolean isCstmrBlockUnblock = false;
    private static final string CLASS_NAME = 'EP_IntegrationUtil';

    private static final string GETMESSAGEID_MTHD = 'getMessageId';
    private static final string GET_TXN_ID_MTHD = 'getTransactionID';
    private static final string SEND_OK_MTHD = 'sendOk';
    private static final string SEND_BULK_OK_MTHD = 'sendBulkOk';
    private static final string SEND_BULK_ERROR_MTHD = 'sendBulkError';
    static List < EP_IntegrationRecord__c > lPreviousIntgRec = new List < EP_IntegrationRecord__c > ();

    @testVisible static map < String, String > objId_UniqueSeqIdMap = new map < String, String > ();
    private static Map < String, EP_Integration_Status_Update__c > integrationCS = EP_Integration_Status_Update__c.getAll();
	private static final string CREATE_INTEG_REC_INBOUND_MTHD = 'createIntegrationRecordInbound';
	private static final String CSORDERITEMS = 'csord__Order_Line_Item__c';
    private static final String CSORDERS = 'Orders';
    private static final String CSORDERSAPINAME = 'csord__Order__c';
    /*
     * Description: Method to create a unique message id for an integration record.
     * @author: Sanchit Dua
     */
    public static String getMessageId(String interfaceId) {
        EP_GeneralUtility.Log('Public', 'EP_IntegrationUtil', 'getMessageId');
        String msgId = INITIAL_VAL;
        msgId = interfaceId + underscore + DateTime.now();
        return msgId;
    }

    /*
     * Description: Method to create a unique transaction id for an integration record.
     * @Author: Sanchit Dua
     */
    public static String getTransactionID(String interfaceId, String transactionType) {
        EP_GeneralUtility.Log('Public', 'EP_IntegrationUtil', 'getTransactionID');
        String transactionId = INITIAL_VAL;
        transactionId = transactionType + underscore + interfaceId + underscore + DateTime.now();
        return transactionId;
    }
    
    /*
     * this method returns txn Type from custom setting based on TxnID
     */
    @testVisible
    private static String getTransactionType(String transactionId) {
        EP_GeneralUtility.Log('Private', 'EP_IntegrationUtil', 'getTransactionType');
        String returnVal = EP_Common_Constant.BLANK;
        // Find all the transaction types in the custom setting
        for (EP_IntegrationTransactionSettings__c transactionType: EP_IntegrationTransactionSettings__c.getAll().values()) {
            if (transactionType.Name.contains(transactionId)) {
                returnVal = transactionType.Name;
                break;
            }
        }
        return returnVal;
    }
    
    /*
     * returns message ID 
     */
    public static String getMessageId(String source, String local, String processName, DateTime timeStamp, String uniqueId) {
        String messageId = EP_Common_Constant.BLANK;
        EP_GeneralUtility.Log('Public', 'EP_IntegrationUtil', 'getMessageId');
        if (String.isNotBlank(uniqueId) && uniqueId.length() > 6) {
            uniqueId = uniqueId.right(6);
        }
        messageId = source +HYPHEN_STRING +local + HYPHEN_STRING + processName + HYPHEN_STRING + timeStamp.format(MSG_STAMP_FORMAT) +HYPHEN_STRING +uniqueId;
        return messageId;
    }

    /**
      Method to create multiple integration record with status as 'Sent' if multiple records are sent to external system.
      **/
    public static List < EP_IntegrationRecord__c > sendBulkOk(String transactionId, String msgId, List < Id > objIdList, List < String > objTypeList, String company, DateTime dtSent, String target, map < String, String > mChildParent, Boolean doDML) {
        EP_GeneralUtility.Log('Public', 'EP_IntegrationUtil', 'sendBulkOk');
        updateIntegrationRecordLatestFlag(transactionId, objIdList, target, doDML);
        EP_IntegrationRecord__c intRec;
        List < EP_IntegrationRecord__c > intRecToInsertList = new List < EP_IntegrationRecord__c > ();
         
        // Start Code Added Bulkification
        Map < String, String > intgStatusByObjAPIName = EP_CustomSettingsUtil.createIntgStatusByObjAPIName();
        Map<String,Set<Id>> mapObjTypeIds = new Map<String,Set<Id>>();
        for (Id objId : objIdList) {
            String objName = objId.getSObjectType().getDescribe().getName().toUpperCase();
            if (mapObjTypeIds.containsKey(objName)) {

                mapObjTypeIds.get(objName).add(objId);
            }
            else {
                mapObjTypeIds.put(objName,new Set<Id>{objId});
            }
        }
        system.debug(mapObjTypeIds.keySet()+'set--size:--'+mapObjTypeIds.size()+'***'+mapObjTypeIds.containskey(ACCOUNT_OBJNAME));
        try{
            
            for (String objNameKey : mapObjTypeIds.keySet()) {
              intRecToInsertList.addAll(EP_IntegrationUtilsBulk.createIntegrationRecordOutbound(mapObjTypeIds.get(objNameKey),transactionId,intgStatusByObjAPIName.get(objNameKey),msgId,company,dtSent,
                                                                        EP_Common_Constant.SENT_STATUS,target,INITIAL_VAL,mChildParent));
            }
            // End Code Added Bulkification

            /*
        try {
            for (integer i = 0; i < objIdList.size(); i++) {
                intRec = createIntegrationRecordOutbound(objIdList[i], objTypeList[i], transactionId, msgId, company, dtSent, EP_Common_Constant.SENT_STATUS,target, INITIAL_VAL);
                intRec.EP_ParentNode__c = mChildParent.containsKey(objIdList[i]) ? mChildParent.get(objIdList[i]) :EP_Common_Constant.BLANK;
                intRec.EP_SeqId__c = reCreateSeqId(msgId, objIdList[i]);
                intRecToInsertList.add(intRec);
            }*/

        } catch (Exception ex) {
            EP_loggingService.loghandledException(ex, EP_Common_Constant.EPUMA, SEND_BULK_OK_MTHD,CLASS_NAME, apexPages.severity.ERROR);
        }
        return intRecToInsertList;

    }
    
    /*
     * Description: To re create seqId from SF RecordId to <SFID-DATETIME-MESSAGEAUTONUMBER>
     * @Author: Jai Singh
     * @param messageId: generated mesageId
     * @param existingSeqId: SF RecordId
     */
    public static String reCreateSeqId(String mesageId, String existingSeqId) {
        EP_GeneralUtility.Log('Public', 'EP_IntegrationUtil', 'reCreateSeqId');
        String newSeqId = existingSeqId;
        try {
            if (String.isNotBlank(mesageId) && mesageId.length() == 36 && String.isNotBlank(existingSeqId) && existingSeqId.length() == 18) { /**NOVASUITE CHANGES BY ASHOK-START**/
                newSeqId = mesageId.substring(12);
                newSeqId = newSeqId.remove(EP_COMMON_CONSTANT.TIME_SEPARATOR_SIGN);
                newSeqId = newSeqId.replaceFirst(EP_COMMON_CONSTANT.STRING_HYPHEN, EP_COMMON_CONSTANT.BLANK);
                newSeqId = existingSeqId + EP_COMMON_CONSTANT.STRING_HYPHEN + newSeqId;
                /**NOVASUITE CHANGES BY ASHOK-END**/
            }
            if (String.isNotBlank(existingSeqId) && existingSeqId.length() >= 18) {
                String existingSeqIdTemp = existingSeqId.substring(0, 18);
                objId_UniqueSeqIdMap.put(existingSeqIdTemp, newSeqId);
            }
        } catch (Exception ex) {
            EP_loggingService.loghandledException(ex, EP_Common_Constant.EPUMA, SEND_OK_MTHD,CLASS_NAME, apexPages.severity.ERROR);
        }
        return newSeqId;
    }
    
    /**NOVASUITE CHANGES BY ASHOK - START**/
    //Start Added for E2E 006 Error Management
    /**
     Generic method to create an integration record.
     **/
    public static EP_IntegrationRecord__c createIntegrationRecordOutbound(Id ObjId, String ObjType, String transacionId, String msgId, String company, DateTime dtSent, String status, String target, String errorDescription) {
        EP_GeneralUtility.Log('Public', 'EP_IntegrationUtil', 'createIntegrationRecordOutbound');
        List < SObject > recordName;
        EP_IntegrationRecord__c intRecord = new EP_IntegrationRecord__c();
        Integer nRows = EP_Common_Util.getQueryLimit();
        try {
            intRecord.EP_Object_ID__c = ObjId;
            intRecord.EP_Object_Type__c = ObjType;
            intRecord.EP_Transaction_ID__c = transacionId;
            intRecord.EP_Message_ID__c = msgId;
            intRecord.EP_Company__c = company;
            intRecord.EP_DT_SENT__c = dtSent;
            intRecord.EP_Status__c = status.toUpperCase();
            intRecord.EP_Target__c = target;
            //change for unique seqId
            intRecord.EP_Error_Description__c = String.isNOtBlank(errorDescription) && errorDescription.length() >= 255 ? errorDescription.substring(0, 254) : errorDescription;
            intRecord.EP_isLatest__c = TRUE;
            intRecord.EP_Source__c = SF;

            //Added by Pooja

            if (intRecord.EP_Object_Type__c != NULL) {
                // Smark
                // 02/09/2016 - Added some extra catches to ensure that the integration record contains the object type
                if (integrationCS != NULL) {
                    if (integrationCS.containsKey(intRecord.EP_Object_Type__c)) {
                        Schema.SObjectType objName = Schema.getGlobalDescribe().get(integrationCS.get(intRecord.EP_Object_Type__c).EP_API_Name__c);

                        if (intRecord.EP_Object_Type__c.equals(CSORDERITEMS)) {
                            recordName = Database.query(QUERY_ORDER_ITEM_NO + objName + WHERE_ID + ESCAPE_SEQ + ObjId + ESCAPE_SEQ);
                            intRecord.EP_Object_Record_Name__c = String.ValueOf(recordName[0].get(ORDER_ITEM_NO));
                            String orderId = String.ValueOf(recordName[0].get(ORDER_ID));
                            List < csord__Order__c > ordObj = Database.query(QUERY_ACC_ID_FRM_ORDER + WHERE_ID + ESCAPE_SEQ + orderId + ESCAPE_SEQ);
                            List < Account > recordCountry = Database.query(QUERY_CNTRY_FRM_ACC + WHERE_ID + ESCAPE_SEQ + ordObj[0].csord__Account__c + ESCAPE_SEQ);
                            intRecord.EP_Country__c = String.ValueOf(recordCountry[0].get(COUNTRY_FIELD));
                        } else if (intRecord.EP_Object_Type__c.equals(CSORDERS) || intRecord.EP_Object_Type__c.equals(CSORDERSAPINAME) ) {
                            recordName = Database.query(QUERY_ORDER_NO + CSORDERSAPINAME + WHERE_ID + ESCAPE_SEQ + ObjId + ESCAPE_SEQ);
                            intRecord.EP_Object_Record_Name__c = String.ValueOf(recordName[0].get(ORDER_NO));
                            String sellTo = String.ValueOf(recordName[0].get(ACCOUNT_ID));
                            List < Account > recordCountry = Database.query(QUERY_CNTRY_FRM_ACC + WHERE_ID + ESCAPE_SEQ + sellTo + ESCAPE_SEQ);
                            intRecord.EP_Country__c = String.ValueOf(recordCountry[0].get(COUNTRY_FIELD));
                        } else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.ACCOUNTS)) {
                            recordName = Database.query(QUERY_COUNTRY + objName + WHERE_ID + ESCAPE_SEQ + ObjId + ESCAPE_SEQ);
                            intRecord.EP_Object_Record_Name__c = String.ValueOf(recordName[0].get(NAME));
                            intRecord.EP_Country__c = String.ValueOf(recordName[0].get(COUNTRY_FIELD));
                        } else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.DIPS)) {
                            recordName = Database.query(QUERY_TANK + objName + WHERE_ID + ESCAPE_SEQ + ObjId + ESCAPE_SEQ);
                            intRecord.EP_Object_Record_Name__c = String.ValueOf(recordName[0].get(NAME));
                            system.debug('--tank--' + recordName[0].get(TANK_ID));
                            String tankId = String.ValueOf(recordName[0].get(TANK_ID));
                            /**NOASUITE CHANGES BY ASHOK - START**/
                            List < EP_Tank__c > tankObj = [Select EP_Ship_To__c from EP_Tank__c where Id =: tankId Limit: nRows];
                            /**NOASUITE CHANGES BY ASHOK - END**/
                            //system.debug('--tankObj-'+tankObj);
                            String shipToId = String.ValueOf(tankObj[0].EP_Ship_To__c);
                            List < Account > recordCountry = Database.query(QUERY_CNTRY_FRM_ACC + WHERE_ID + ESCAPE_SEQ + shipToId + ESCAPE_SEQ);
                            intRecord.EP_Country__c = String.ValueOf(recordCountry[0].EP_Country__c);
                            //system.debug('--intRecord.EP_Country__c--'+intRecord.EP_Country__c);
                        } else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.STOCKLOCATIONS)) {
                            recordName = Database.query(QUERY_SHIP_TO + objName + WHERE_ID + ESCAPE_SEQ + ObjId + ESCAPE_SEQ);
                            intRecord.EP_Object_Record_Name__c = String.ValueOf(recordName[0].get(NAME));
                            String shipToId = String.ValueOf(recordName[0].get(SHIP_TOONTANK));
                            List < Account > recordCountry = Database.query(QUERY_CNTRY_FRM_ACC + WHERE_ID + ESCAPE_SEQ + shipToId + ESCAPE_SEQ);
                            intRecord.EP_Country__c = String.ValueOf(recordCountry[0].get(COUNTRY_FIELD));
                        } else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.BANKACCOUNTS)) {
                            recordName = Database.query(QUERY_COUNTRY + objName + WHERE_ID + ESCAPE_SEQ + ObjId + ESCAPE_SEQ);
                            intRecord.EP_Object_Record_Name__c = String.ValueOf(recordName[0].get(NAME));
                            intRecord.EP_Country__c = String.ValueOf(recordName[0].get(COUNTRY_FIELD));
                        } else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.CREDITEXCEPTIONS)) {
                            recordName = Database.query(QUERY_NAME + objName + WHERE_ID + ESCAPE_SEQ + ObjId + ESCAPE_SEQ);
                            intRecord.EP_Object_Record_Name__c = String.ValueOf(recordName[0].get(NAME));
                        } else if (intRecord.EP_Object_Type__c.equals(EP_Common_Constant.TANKS_INT)) {
                            recordName = Database.query(QUERY_SHIP_TO + objName + WHERE_ID + ESCAPE_SEQ + ObjId + ESCAPE_SEQ);
                            intRecord.EP_Object_Record_Name__c = String.ValueOf(recordName[0].get(NAME));
                            String shipToId = String.ValueOf(recordName[0].get(SHIP_TOONTANK));
                            List < Account > recordCountry = Database.query(QUERY_CNTRY_FRM_ACC + WHERE_ID + ESCAPE_SEQ + shipToId + ESCAPE_SEQ);
                            intRecord.EP_Country__c = String.ValueOf(recordCountry[0].get(COUNTRY_FIELD));
                        }
                    }

                    if (status.equalsIgnoreCase(EP_Common_Constant.ERROR_SENT_STATUS)) {
                        intRecord.EP_Integration_Error_Type__c = EP_Common_Constant.MIDDLEWARE_TRANS_ERROR;
                    } else if (status.equalsIgnoreCase(EP_Common_Constant.ERROR_SYNC_STATUS)) {
                        intRecord.EP_Integration_Error_Type__c = EP_Common_Constant.TARGET_TRANS_ERROR;
                    } else if ((status.equalsIgnoreCase(EP_Common_Constant.ERROR_RECEIVED_STATUS)) || (status.equalsIgnoreCase(EP_Common_Constant.ERROR_ACKNOWLEDGED_STATUS))) {
                        intRecord.EP_Integration_Error_Type__c = EP_Common_Constant.FUCTIONAL_ERROR;
                    }
                }

            }

            //change for unique seqId
            if (objId_UniqueSeqIdMap.containsKey(ObjId)) {
                intRecord.EP_SeqId__c = objId_UniqueSeqIdMap.get(ObjId);
            }
        } catch (Exception ex) {
            EP_loggingService.loghandledException(ex, EP_Common_Constant.EPUMA, SEND_OK_MTHD, CLASS_NAME, apexPages.severity.ERROR);
        }
        return intRecord;
    }
    /*
     * Description: To update the previous records with EP_Status__c = FALSE,Overloaded Method
     * @Author:Ashok Arora
     * @param objectId: To query the existing EP_IntegrationRecord__c to be compared with EP_Object_ID__c
     */
    private static void updateIntegrationRecordLatestFlag(String transactionID, list < Id > lobjectId, String target, Boolean doDML) {
        EP_GeneralUtility.Log('Private', 'EP_IntegrationUtil', 'updateIntegrationRecordLatestFlag');
        if (transactionID != NULL) {
            String transactionType = EP_Common_Constant.PERCENT + getTransactionType(transactionID);
            Integer nRows = Limits.getLimitQueryRows() - Limits.getQueryRows();
            lPreviousIntgRec.addAll([SELECT EP_Status__c FROM EP_IntegrationRecord__c WHERE EP_Object_ID__c IN: lobjectId AND EP_isLatest__c = TRUE AND EP_Target__c =: target  AND EP_Transaction_ID__c like: transactionType LIMIT: nRows ]);
            if (doDML && !lPreviousIntgRec.isEmpty()) {
                for (EP_IntegrationRecord__c ir: lPreviousIntgRec) {
                    ir.EP_IsLatest__c = FALSE;
                }
                Database.update(lPreviousIntgRec, FALSE);
            }
        }
    }
    /**
     Method to create multiple integration record with status as 'Sent' if multiple records are sent to external system.
     **/
    private static final string ACCOUNT_OBJNAME = 'ACCOUNT'; 
    public static void sendBulkOk(List < Id > objIdList, List < String > objTypeList, String transactionId, String msgId, String company, DateTime dtSent, String target, map < String, String > mChildParent) {
        EP_GeneralUtility.Log('Public', 'EP_IntegrationUtil', 'sendBulkOk');
        updateIntegrationRecordLatestFlag(transactionId, objIdList, target, true);
        EP_IntegrationRecord__c intRec;
        List < EP_IntegrationRecord__c > intRecToInsertList = new List < EP_IntegrationRecord__c > ();
        
        // Start Code Added Bulkification
        Map < String, String > intgStatusByObjAPIName = EP_CustomSettingsUtil.createIntgStatusByObjAPIName();
        Map<String,Set<Id>> mapObjTypeIds = new Map<String,Set<Id>>();
        for (Id objId : objIdList) {
            String objName = objId.getSObjectType().getDescribe().getName().toUpperCase();
            if (mapObjTypeIds.containsKey(objName)) {

                mapObjTypeIds.get(objName).add(objId);
            }
            else {
                mapObjTypeIds.put(objName,new Set<Id>{objId});
            }
        }
        system.debug(mapObjTypeIds.keySet()+'set--size:--'+mapObjTypeIds.size()+'***'+mapObjTypeIds.containskey(ACCOUNT_OBJNAME));
        try{
            
            for (String objNameKey : mapObjTypeIds.keySet()) {
               intRecToInsertList.addAll(EP_IntegrationUtilsBulk.createIntegrationRecordOutbound(mapObjTypeIds.get(objNameKey),transactionId,intgStatusByObjAPIName.get(objNameKey),msgId,company,dtSent,
                                                                        EP_Common_Constant.SENT_STATUS,target,INITIAL_VAL,mChildParent));
            }
            // End Code Added Bulkification

            /*
	        for (integer i = 0; i < objIdList.size(); i++) {
	            intRec = createIntegrationRecordOutbound(objIdList[i], objTypeList[i], transactionId, msgId, company, dtSent, EP_Common_Constant.SENT_STATUS, target, INITIAL_VAL);
	            intRec.EP_ParentNode__c = mChildParent.containsKey(objIdList[i]) ? mChildParent.get(objIdList[i]) : EP_Common_Constant.BLANK;
	            intRecToInsertList.add(intRec);
	        }
            */
            if (!intRecToInsertList.isEmpty()) {
                database.insert(intRecToInsertList);
            }

        } catch (Exception ex) {
            EP_loggingService.loghandledException(ex, EP_Common_Constant.EPUMA, SEND_BULK_OK_MTHD, CLASS_NAME, apexPages.severity.ERROR);
        }

    }

    /*
     *
     * @Author Jai Singh
     * Description: Creates multiple integration records with status = ERROR-SENT
     * @param objectIds: To target multiple records of objects
     * @param objectTypes: Multiple values corresponding to the Static picklist values like Dips, Accounts and Orders
     * @param transactionId: To maintain the transaction id
     * @param messageID: To maintain the message id
     * @param target: To target a specific record for the object
     * @param errorDescription: description of errors if any
     * @param dtSent: The date value to correspond to the DT_SENT field on Integration Record object 
     * @param URL: endpoint url of errors if any
     * @param XML_Message: XML Body of the callout
     */
    public static List < EP_IntegrationRecord__c > sendBulkError(List < ID > objectIds,List < String > objectTypes,String transactionId,String messageId, String target,String company,String errorDescription, Datetime dtSent, String URL, String XML_Message) {
        EP_GeneralUtility.Log('Public', 'EP_IntegrationUtil', 'sendBulkError');
        EP_IntegrationRecord__c intRec = new EP_IntegrationRecord__c();
        updateIntegrationRecordLatestFlag(transactionId, objectIds, target, true);
        List < EP_IntegrationRecord__c > intRecToInsertList = new List < EP_IntegrationRecord__c > ();
        for (integer i = 0; i < objectIds.size(); i++) {
            intRec = createIntegrationRecordOutbound(objectIds[i], objectTypes[i], transactionId, messageId, company, dtSent, ERROR_SYNC_SENT_STATUS + HYPHEN_STRING + SENT, target, errorDescription);
            intRec.EP_Endpoint_URL__c = URL;
            intRec.EP_XML_Message__c = XML_Message;
            intRec.EP_Resend__c = TRUE;
            intRecToInsertList.add(intRec);
        }
        try {
            if (!intRecToInsertList.isEmpty()) {
                database.insert(intRecToInsertList);
            }
        } catch (DMLException ex) {
            EP_loggingService.loghandledException(ex, EP_Common_Constant.EPUMA, SEND_BULK_ERROR_MTHD,CLASS_NAME, apexPages.severity.ERROR);
        }
        return intRecToInsertList;
    }
}