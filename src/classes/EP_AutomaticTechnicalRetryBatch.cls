/*
   @Author          Accenture
   @Name            EP_AutomaticTechnicalRetry - #59186
   @CreateDate      02/07/2017
   @Description     This batch class is used to retry the outbound call which failed due to some technical reasons
   @Version         1.0
*/
global class EP_AutomaticTechnicalRetryBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{ 
     
    private static final String QUERY_STRING = 'Select id,EP_Message_Type__c,EP_XML_Message__c,EP_Endpoint_URL__c,CreatedDate,EP_Message_ID__c,EP_Error_Code__c,EP_Queuing_Retry__c from EP_IntegrationRecord__c where EP_Status__c =\'ERROR-SENT\' AND EP_Next_Run__c <=: dtCurrentTime AND EP_Expiry_Date__c >=: dtCurrentTime AND EP_ATR_Threshold_Reached__c = false AND EP_ParentNode__c= null AND EP_Queuing_Retry__c= true order by EP_Next_Run__c';
    
    /**
    * @Author       Accenture
    * @Name         start
    * @Date         02/07/2017
    * @Description  This methods gets all the records which needs to be processed
    * @Param        Database.BatchableContext
    * @return       Database.QueryLocator
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
        DateTime dtCurrentTime = System.now();
        system.debug('**QUERY_STRING**'+ QUERY_STRING);
        return Database.getQueryLocator(QUERY_STRING);
    }
    
	/**
    * @Author       Accenture
    * @Name         execute
    * @Date         02/07/2017
    * @Description  This methods gets all the records which needs to be processed
    * @Param        Database.BatchableContext
    * @return       Database.QueryLocator
    */	
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    	//Do not initate the retry if ATR setting is disable
        if(!isATRDisabled()) {
            for(sobject sObj : scope){
            	EP_IntegrationRecord__c intRecord = (EP_IntegrationRecord__c)sObj;
            	EP_CS_OutboundMessageSetting__c msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(intRecord.EP_Message_Type__c);
            	if(!EP_OutboundMessageService.isCommunicationDisabled(msgSetting)){
            		sendOutboundMessage((EP_IntegrationRecord__c)sObj);
            	}
            }
        }
    }

	/**
    * @Author       Accenture
    * @Name         finish
    * @Date         02/07/2017
    * @Description  This methods deletes the completed scheduled jobs and schedules next job
    * @Param        Database.BatchableContext
    * @return       NA
    */
	global void finish(Database.BatchableContext BC){
		Set<string> cronStates = new Set<string>{EP_Common_Constant.COMPLETE_STRING,EP_Common_Constant.DELETED_STRING};
        if(!isATRDisabled()) {
    		deleteCronJobs(cronStates);
        	scheduleNextJob();
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         isATRDisabled
    * @Date         02/07/2017
    * @Description  This method checks if ATR is disabled
    * @Param        NA
    * @return       boolean
    */
    @TestVisible
    private boolean isATRDisabled(){
    	EP_CS_Communication_Settings__c communicationSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_ATR);
        return communicationSettings.Disable__c;
    }
    
    /**
    * @Author       Accenture
    * @Name         sendOutboundMessage
    * @Date         02/07/2017
    * @Description  This method sends the outbound message
    * @Param        EP_IntegrationRecord__c
    * @return       NA
    */
    public static void sendOutboundMessage(EP_IntegrationRecord__c intRecord){
    	set<id> integrationRecords = new set<id>();
        try {
            integrationRecords.add(intRecord.id);
            EP_CS_OutboundMessageSetting__c  msgSetting = EP_CustomSettingsUtil.getOutboundMessageSetting(string.isBlank(intRecord.EP_Message_Type__c) ? EP_Common_Constant.BLANK : intRecord.EP_Message_Type__c);
            string endPoint = msgSetting.End_Point__c; 
            string xmlMessage = intRecord.EP_XML_Message__c;
            EP_OutboundIntegrationService intService = new EP_OutboundIntegrationService(endPoint, EP_Common_Constant.POST, xmlMessage, '');
            
            EP_IntegrationServiceResult result = new EP_IntegrationServiceResult();
        	result = intService.invokeRequest();
            intService.processResult(result,integrationRecords,null, msgSetting.Name);
        } catch (exception exp){ 
           EP_loggingService.loghandledException(exp, EP_Common_Constant.EPUMA, 'sendOutboundMessage', 'EP_OutboundMessageService',apexPages.severity.ERROR);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         scheduleNextJob
    * @Date         02/07/2017
    * @Description  This method is used to schedule next job
    * @Param        NA
    * @return       NA
    */
    @TestVisible
    private void scheduleNextJob(){
    	EP_CS_Communication_Settings__c communicationSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.SCHEDULE_JOB_INTERVAL);
        DateTime dtNextSchedule = system.now().addMinutes( Integer.valueOf(communicationSettings.value__c));
        String day = string.valueOf(dtNextSchedule.day());
        String month = string.valueOf(dtNextSchedule.month());
        String hour = string.valueOf(dtNextSchedule.hour());
        String minute = string.valueOf(dtNextSchedule.minute());
        String second = EP_Common_Constant.ZERO_SECONDS;
        String year = string.valueOf(dtNextSchedule.year());
       	
        String strJobName =  EP_Common_Constant.ATR_STRING + EP_Common_Constant.STRING_HYPHEN + second + EP_Common_Constant.UNDERSCORE_STRING + minute + EP_Common_Constant.UNDERSCORE_STRING + hour + EP_Common_Constant.UNDERSCORE_STRING + day + EP_Common_Constant.UNDERSCORE_STRING + month + EP_Common_Constant.UNDERSCORE_STRING + year;
        String strSchedule = EP_Common_Constant.ZERO_STRING + EP_Common_Constant.SPACE + minute + EP_Common_Constant.SPACE + hour + EP_Common_Constant.SPACE + day + EP_Common_Constant.SPACE + month + EP_Common_Constant.QUESTION_MARK + EP_Common_Constant.SPACE + year;
    
        if (!isCronJobScheduled(strJobName)) {
            System.schedule(strJobName, strSchedule, new EP_ScheduleAutomaticRetry());
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         deleteCompletedJobs
    * @Date         02/07/2017
    * @Description  This method is used to delete the completed jobs
    * @Param        NA
    * @return       NA
    */
    public static void deleteCronJobs(Set<string> cronStates){
		for(CronTrigger cron : [select Id,CronJobDetail.Name from CronTrigger where CronJobDetail.Name like :EP_Common_Constant.ATR_SEARCH_STRING AND State IN :cronStates]) {
			system.abortJob(cron.id);
		}
    }

    /**
    * @Author       Accenture
    * @Name         isCronJobScheduled
    * @Date         06/16/2017
    * @Description 
    * @Param        NA
    * @return       boolean
    */
    public static boolean isCronJobScheduled(String strJobName) {
        boolean isCronJobSchedule = false;
        Set<string> cronStates = new Set<string>{'WAITING','ACQUIRED'};
        for(CronTrigger cron : [select Id,CronJobDetail.Name from CronTrigger where CronJobDetail.Name like :strJobName AND State IN :cronStates]) {
            isCronJobSchedule = true;
        }
        return isCronJobSchedule;
    }
}