/* 
@Author <Spiros Markantonatos>
@name <EP_AccountSharingHandler>
@CreateDate <09/08/2016>
@Description <This is handler class is used to create manual sharing rules to allow Sell-To portal users to access Ship-To accounts>
@Version <1.1>
- Updated the test class to use portal roles rather than sharing directly with users to allow easier portal account access management
*/
public without sharing class EP_AccountSharingHandler {
	
	// Notes
	// Smark
	// 09/08/2016
	// As part of R2 this handler must be extended in the future to cover scenarios where Bill-To users will be given access to the portal
	// to ensure that these users have access to the Sell-To and the associated Ship-Tos 
	// This scope if beyond R1
	
	private static final String ACCOUNT_EDIT_ACCESS_LEVEL = 'Edit';
	private static final String ACCOUNT_READ_ACCESS_LEVEL = 'Read';
	private static final String OPP_ACCESS_LEVEL = 'None';
	private static final String CASE_EDIT_ACCESS_LEVEL = 'Edit';
	private static final String CASE_NO_ACCESS_LEVEL = 'None';
	private static final String CONTACT_ACCESS_LEVEL = 'Read';
	private static final String MANUAL_ROW_CAUSE = 'Manual';
	
	private static final String CLASS_NAME = 'EP_AccountSharingHandler';
	private static final String ON_USER_CREATION_METHOD = 'manageSellToUserSharingRulesOnUserCreation';
	private static final String ON_SHIP_TO_CREATION_METHOD = 'manageSellToUserSharingRulesOnShipToCreation';
	private static final String PROCESS_RULE_METHOD = 'processPortalContactSharingRules';
	private static final String MANAGE_PORTAL_ROLE_ON_ACCOUNT_METHOD = 'managePortalRoleSharingRulesOnAccountCreation';
	private static final String INSERT_MANUAL_SHARING_RECORDS_METHOD = 'insertManualSharingRecords';
	private static final String MANAGE_PORTAL_USER_ON_ACCOUNT_METHOD = 'managePortalUserAccountSharingRulesOnUserCreation';
	
	/**
	* @author <Spiros Markantonatos>
	* @date <09/08/2016>
	* @methodname <addAccountPortalRoleManualSharingRecords>
	* @description <This method will generate the sharing record for the account/portal role combination>
	* @param String,String,Boolean,Boolean
	* @return AccountShare
	*/  
	private static AccountShare addAccountPortalRoleManualSharingRecords(String shipToID, 
		String portalRoleGroupID, 
		Boolean blnEditAccount, 
		Boolean blnEditCase) {
		EP_GeneralUtility.Log('Public','EP_AccountSharingHandler','addAccountPortalRoleManualSharingRecords');
		String strAccountAccess = ACCOUNT_READ_ACCESS_LEVEL;
		String strCaseAccess = CASE_NO_ACCESS_LEVEL;
		
		if (blnEditAccount) strAccountAccess = ACCOUNT_EDIT_ACCESS_LEVEL;
		if (blnEditCase) strCaseAccess = CASE_EDIT_ACCESS_LEVEL;
		
		AccountShare newAccountShareRecord = 	new AccountShare(AccountId = shipToID,
			UserOrGroupId = portalRoleGroupID,
			AccountAccessLevel = strAccountAccess,
			OpportunityAccessLevel = OPP_ACCESS_LEVEL,
			CaseAccessLevel = strCaseAccess,
			ContactAccessLevel = CONTACT_ACCESS_LEVEL,
			RowCause = MANUAL_ROW_CAUSE);
		return newAccountShareRecord;
	}
	
	/**
	* @author <Spiros Markantonatos>
	* @date <09/08/2016>
	* @methodname <managePortalUserAccountSharingRulesOnUserCreation>
	* @description <This method will trigger the insert of sharing rules for portal roles against Sell-To and Ship-To once a portal user has been created>
	* @param List<User>
	* @return void
	*/  
	public static void managePortalUserAccountSharingRulesOnUserCreation(List<User> lUsers) {
		EP_GeneralUtility.Log('Public','EP_AccountSharingHandler','managePortalUserAccountSharingRulesOnUserCreation');
		Integer nRows = EP_Common_Util.getQueryLimit();
		Set<ID> setContactIDs = new Set<ID>();
		Account acc;
		try{
			for (User u : lUsers){
				if (u.ContactId != NULL)
				setContactIDs.add(u.ContactId);
			}
			
			// Retrieve account details from the user record
			List<Account> lAccounts = new List<Account>();
			
			//List<Contact> lContacts = [SELECT AccountId, Account.ParentId FROM Contact WHERE Id IN :setContactIDs Limit :nRows];
			
			for (Contact c : [SELECT AccountId, Account.ParentId FROM Contact WHERE Id IN :setContactIDs Limit :nRows]){
				acc= new account(Id = c.AccountId, ParentId = c.Account.ParentId);
				lAccounts.add(acc);
			}
			
			managePortalRoleSharingRulesOnAccountCreation(lAccounts);
			}catch(Exception e){
				EP_LoggingService.logHandledException (e, EP_Common_Constant.EPUMA, MANAGE_PORTAL_USER_ON_ACCOUNT_METHOD , CLASS_NAME, ApexPages.Severity.ERROR);
			}
		}

	/**
	* @author <Spiros Markantonatos>
	* @date <09/08/2016>
	* @methodname <managePortalRoleSharingRulesOnAccountCreation>
	* @description <This method will trigger the insert of sharing rules for portal roles against Sell-To and Ship-To once an account has been created>
	* @param List<Account>
	* @return void
	*/  
	public static void managePortalRoleSharingRulesOnAccountCreation(List<Account> lAccounts) {
		EP_GeneralUtility.Log('Public','EP_AccountSharingHandler','managePortalRoleSharingRulesOnAccountCreation');
		Integer nRows;

		if (!lAccounts.isEmpty()){   
			nRows = EP_Common_Util.getQueryLimit();
			Set<ID> setParentAccountIDs = new Set<ID>();
			Set<ID> setAccountIDs = new Set<ID>();
			Set<ID> setChildAccountIDs = new Set<ID>();
			Map<String, Account> mapAccountsForProcessing = new Map<String, Account>(); // Map is used to ensure that accounts are added to the master list only once
			
			Map<String, String> mapGroupPortalUserRoleRecords = new Map<String, String>();
			Map<String, String> mapPortalUserRoleToGroupRecords = new Map<String, String>();
			
			for (Account a : lAccounts){
				setAccountIDs.add(a.Id);
				if (a.ParentId != NULL){
					setParentAccountIDs.add(a.ParentId);
				}
				// Add to the map the accounts that initiated the sharing rule change
				mapAccountsForProcessing.put(a.Id, a);
			}
			
			// Retrieve all child accounts based on the account list (e.g. retrieve all Ship-Tos for given Sell-Tos)
			
			// Add the child accounts (if any) into the overall account list
			// So that they are processed by the handler
			for (Account a : (List<Account>)([SELECT Id, ParentId 
				FROM Account 
				WHERE ParentId IN :setAccountIDs LIMIT :nRows])){
				setAccountIDs.add(a.Id);
				setChildAccountIDs.add(a.Id);
				// Add to the map the child accounts of the ones that initiated the sharing rule change
				mapAccountsForProcessing.put(a.Id, a);
			}
			
			// Retrieve all parent accounts based on the account list (e.g. retrieve all Sell-Tos for given Ship-Tos)
			if (!setParentAccountIDs.isEmpty()){
				// Add the child accounts (if any) into the overall account list
				// So that they are processed by the handler
				nRows = EP_Common_Util.getQueryLimit();
				for (Account a : (List<Account>)([SELECT Id, ParentId 
					FROM Account 
					WHERE ID IN :setParentAccountIDs LIMIT :nRows])){
					setAccountIDs.add(a.Id);
					setChildAccountIDs.add(a.Id);
					// Add to the map the parent accounts of the ones that initiated the sharing rule change
					mapAccountsForProcessing.put(a.Id, a);
				}
			}
			
			nRows = EP_Common_Util.getQueryLimit();
			// Retrieve all portal roles for the accounts and their parents (if any)
			List<UserRole> lAccountUserRoles = [SELECT Id, PortalAccountId FROM UserRole 
			WHERE PortalAccountId IN :setAccountIDs Limit:nRows];
			
			/* List<Group> lPortalUserRoleGroups = [SELECT Id, RelatedId 
													FROM Group 
													WHERE RelatedId IN :lAccountUserRoles Limit:nRows];*/

			// Build a map linking portal user roles to groups
			for (Group g : [SELECT Id, RelatedId FROM Group WHERE RelatedId IN :lAccountUserRoles Limit:nRows]){
				mapGroupPortalUserRoleRecords.put(g.RelatedId, g.Id); // User Role ID / Group ID
				mapPortalUserRoleToGroupRecords.put(g.Id, g.RelatedId);  // Group ID / User Role ID
			}
			
			// Build the master account list based on the map variable
			lAccounts = mapAccountsForProcessing.values();
			
			// Generate sharing rules if required (the method will check against the map)
			try {
				processPortalRoleSharingRules(lAccounts, lAccountUserRoles, mapGroupPortalUserRoleRecords);
				
				} catch(Exception ex){
					EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 
						MANAGE_PORTAL_ROLE_ON_ACCOUNT_METHOD, 
						CLASS_NAME, 
						ApexPages.Severity.ERROR);            }

				}
			}

	/**
	* @author <Spiros Markantonatos>
	* @date <09/08/2016>
	* @methodname <processPortalRoleSharingRules>
	* @description <This method will process the different cases of sharing rules, depending of this is triggered at child/parent level>
	* @param List<Account>,	List<UserRole>,	Map<String, String>
	* @return none
	*/
	public static void processPortalRoleSharingRules(List<Account> lAccounts, 
		List<UserRole> lAccountPortalRoles,
		Map<String, String> mapGroupPortalUserRoleRecords) {
		EP_GeneralUtility.Log('Public','EP_AccountSharingHandler','processPortalRoleSharingRules');
		if (!lAccounts.isEmpty()){
			
			List<String> lAccountIDs = new List<String>();
			List<String> lPortalRoleGroupIDs = new List<String>();
			List<Boolean> lEditAccountValues = new List<Boolean>();
			List<Boolean> lEditCaseValues = new List<Boolean>();
			
			for (Account a : lAccounts){
				// Add the a sharing rule for the actual account record
				// Check if the account has already sharing rules
				for (UserRole uRole : lAccountPortalRoles){
					// Ensure that the user role has a corresponding group
					if (mapGroupPortalUserRoleRecords.containsKey(uRole.Id)){
						// 1. This covers the actual account sharing and read-only sharing of the parent account to the child one
						// Check that the role is related to the account
						if (a.Id == uRole.PortalAccountId){
							lAccountIDs.add(a.Id);
							lPortalRoleGroupIDs.add(mapGroupPortalUserRoleRecords.get(uRole.Id));
							lEditAccountValues.add(TRUE);
							lEditCaseValues.add(TRUE);
							
							// 2. If the account is a child of another account, then the system must provide the child account roles, 
							// read-only access to the parent account
							// Note: This has been remove for the time being as parent access is not required by the current portal setup
							if (a.ParentId != NULL){
								// This logic might be added in future
								//  lAccountIDs.add(a.ParentId);
								//  lPortalRoleGroupIDs.add(mapGroupPortalUserRoleRecords.get(uRole.Id));
								//  lEditAccountValues.add(FALSE);
								//  lEditCaseValues.add(FALSE);
							}
						}
						
						// 3. This covers the sharing of the account children to the users of the parent account
						// Check that the role is related to the account
						if (a.ParentId != NULL){
							if (a.ParentId == uRole.PortalAccountId){
								lAccountIDs.add(a.Id);
								lPortalRoleGroupIDs.add(mapGroupPortalUserRoleRecords.get(uRole.Id));
								lEditAccountValues.add(TRUE);
								lEditCaseValues.add(TRUE);
							}
						}
					}
				}
			}
			
			// Ensure that the system is not performing an @future insert, 
			// when method being triggered by a batch job or another @future callout
			if(System.IsBatch() == FALSE && System.isFuture() == FALSE)	{
				insertManualSharingRecords(lAccountIDs, 
					lPortalRoleGroupIDs, 
					lEditAccountValues, 
					lEditCaseValues);
			}
		}
		
	}
	
	/**
	* @author <Spiros Markantonatos>
	* @date <09/08/2016>
	* @methodname <insertManualSharingRecords>
	* @description <This method will perform the actual insert @future>
	* @param List<String>,List<String>,List<Boolean>,List<Boolean>
	* @return none
	*/
	@future
	public static void insertManualSharingRecords(List<String> lAccountIDs, 
		List<String> lPortalRoleGroupIDs, 
		List<Boolean> lEditAccountValues, 
		List<Boolean> lEditCaseValues) {
		
		EP_GeneralUtility.Log('Public','EP_AccountSharingHandler','insertManualSharingRecords');

		if (!lAccountIDs.isEmpty() && 
			!lPortalRoleGroupIDs.isEmpty() && 
			!lEditAccountValues.isEmpty() && 
			!lEditCaseValues.isEmpty()){
			if (lAccountIDs.size() == lPortalRoleGroupIDs.size() &&
				lAccountIDs.size() == lPortalRoleGroupIDs.size() && 
				lAccountIDs.size() == lEditAccountValues.size() && 
				lAccountIDs.size() == lEditCaseValues.size()){
				
				List<AccountShare> lAccountShareRecordsForInsert = new List<AccountShare>();
				
				for (Integer i = 0; i < lAccountIDs.size(); i++){
					lAccountShareRecordsForInsert.add(
						addAccountPortalRoleManualSharingRecords(lAccountIDs[i], lPortalRoleGroupIDs[i], lEditAccountValues[i], lEditCaseValues[i]));
				}
				system.debug('lAccountShareRecordsForInsert**' + lAccountShareRecordsForInsert);
				if (!lAccountShareRecordsForInsert.isEmpty()){
					Savepoint sp = Database.setSavepoint();
					system.debug('Inserting Share records**');
					try {
						Database.insert(lAccountShareRecordsForInsert);
						} catch(Exception ex){
							Database.rollBack(sp);
							EP_LoggingService.logHandledException (ex, EP_Common_Constant.EPUMA, INSERT_MANUAL_SHARING_RECORDS_METHOD , CLASS_NAME, ApexPages.Severity.ERROR);                    
						}
					}
				}
			}
		}
	}