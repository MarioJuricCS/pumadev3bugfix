@isTest
private class CustomLookupShipToAccount_UT {

    @testSetup
    private static void setupTestData() {
           
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
    }
    
    @isTest
	private static void doLookupSearch() {

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];

        CustomLookupShipToAccount obj = new CustomLookupShipToAccount();

        Map<String, String> searchFields = new Map<String, String>();
        
        searchFields.put('AccountId', acc.Id);
        searchFields.put('Product Type', 'Bulk');
        searchFields.put('searchValue', acc.Name);
        searchFields.put('Order Epoch', 'Current');

		Test.startTest();

        Object[] retval = obj.doLookupSearch(searchFields, null, null, 1, 5);

		Test.stopTest();

        System.assertNotEquals(null, retval, 'Invalid data');
	}

    @isTest
	private static void doLookupSearchCurrent() {

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];

        CustomLookupShipToAccount obj = new CustomLookupShipToAccount();

        Map<String, String> searchFields = new Map<String, String>();
        
        searchFields.put('AccountId', acc.Id);
        searchFields.put('Product Type', 'Bulk');
        searchFields.put('searchValue', null);
        searchFields.put('Order Epoch', 'Current');

		Test.startTest();

        Object[] retval = obj.doLookupSearch(searchFields, null, null, 1, 5);

		Test.stopTest();

        System.assertNotEquals(null, retval, 'Invalid data');
	}
	
    @isTest
	private static void doLookupSearchRetrospective() {

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        
        CustomLookupShipToAccount obj = new CustomLookupShipToAccount();

        Map<String, String> searchFields = new Map<String, String>();
        
        searchFields.put('AccountId', acc.Id);
        searchFields.put('Product Type', 'Bulk');
        searchFields.put('searchValue', acc.Name);
        searchFields.put('Order Epoch', 'Retrospective');

		Test.startTest();

        Object[] retval = obj.doLookupSearch(searchFields, null, null, 1, 5);

		Test.stopTest();

        System.assertNotEquals(null, retval, 'Invalid data');
	}
	
    @isTest
	private static void doLookupSearchRetrospectiveNoName() {

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        
        CustomLookupShipToAccount obj = new CustomLookupShipToAccount();

        Map<String, String> searchFields = new Map<String, String>();
        
        searchFields.put('AccountId', acc.Id);
        searchFields.put('Product Type', 'Bulk');
        searchFields.put('searchValue', null);
        searchFields.put('Order Epoch', 'Retrospective');

		Test.startTest();

        Object[] retval = obj.doLookupSearch(searchFields, null, null, 1, 5);

		Test.stopTest();

        System.assertNotEquals(null, retval, 'Invalid data');
	}
	
    @isTest
	private static void doLookupSearchEmpty() {

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        
        CustomLookupShipToAccount obj = new CustomLookupShipToAccount();

        Map<String, String> searchFields = new Map<String, String>();
        
        searchFields.put('AccountId', acc.Id);
        searchFields.put('Product Type', '');
        searchFields.put('searchValue', acc.Name);
        searchFields.put('Order Epoch', 'Retrospective');

		Test.startTest();

        Object[] retval = obj.doLookupSearch(searchFields, null, null, 1, 5);

		Test.stopTest();

        System.assertNotEquals(null, retval, 'Invalid data');
	}
	
    @isTest
	private static void doLookupSearchNoName() {

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        
        CustomLookupShipToAccount obj = new CustomLookupShipToAccount();

        Map<String, String> searchFields = new Map<String, String>();
        
        searchFields.put('AccountId', acc.Id);
        searchFields.put('Product Type', '');
        searchFields.put('searchValue', null);
        searchFields.put('Order Epoch', 'Retrospective');

		Test.startTest();

        Object[] retval = obj.doLookupSearch(searchFields, null, null, 1, 5);

		Test.stopTest();

        System.assertNotEquals(null, retval, 'Invalid data');
	}
	
    @isTest
	private static void doLookupEmptyList() {

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];
        
        CustomLookupShipToAccount obj = new CustomLookupShipToAccount();

        Map<String, String> searchFields = new Map<String, String>();
        
        searchFields.put('AccountId', null);
        searchFields.put('Product Type', null);
        searchFields.put('searchValue', null);
        searchFields.put('Order Epoch', null);

		Test.startTest();

        Object[] retval = obj.doLookupSearch(searchFields, null, null, 1, 5);

		Test.stopTest();

        System.assertEquals(null, null, 'Invalid data');
	}
	
    @isTest
	private static void doDynamicLookupSearch() {

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];

        CustomLookupShipToAccount obj = new CustomLookupShipToAccount();

        Map<String, String> searchFields = new Map<String, String>();
        
        searchFields.put('AccountId', acc.Id);
        searchFields.put('Product Type', 'test');

		Test.startTest();
        
        Object[] retval = obj.doDynamicLookupSearch(searchFields, null);

		Test.stopTest();

        System.assertNotEquals(null, retval, 'Invalid data');
	}
	
    @isTest
	private static void doDynamicLookupBulkSearch() {

        Account acc = [SELECT Id, Name FROM Account LIMIT 1];

        CustomLookupShipToAccount obj = new CustomLookupShipToAccount();

        Map<String, String> searchFields = new Map<String, String>();
        
        searchFields.put('AccountId', acc.Id);
        searchFields.put('Product Type', 'Bulk');

		Test.startTest();
        
        Object[] retval = obj.doDynamicLookupSearch(searchFields, null);

		Test.stopTest();

        System.assertNotEquals(null, retval, 'Invalid data');
	}
	
	@isTest
	private static void getRequiredAttributes() {
	    
	    CustomLookupShipToAccount obj = new CustomLookupShipToAccount();
	    
		Test.startTest();
        
        String retval = obj.getRequiredAttributes();

		Test.stopTest();
		
		System.assertEquals('["AccountId","Product Type", "Order Epoch"]', retval, 'Invalid data');
	}
}