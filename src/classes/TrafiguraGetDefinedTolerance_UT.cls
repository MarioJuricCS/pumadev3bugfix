@IsTest
public class TrafiguraGetDefinedTolerance_UT {

    @testSetup
    private static void testSetup() {
     
        Company__c company = new Company__c(Name = 'Test', EP_Company_Code__c = 'Is',
                                                EP_Combined_Invoicing_Default__c = 'Create and Post',
                                                EP_Combined_Invoicing__c  = 'Delegate to Customer',
                                                EP_Window_Start_Hours__c = 8,
                                                EP_Window_End_Hours__c = 18,
                                                KYC_limit_in_KUSD__c = '10',
                                                EP_Company_Id__c = 01);
        insert company;
       
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        acc.EP_Puma_Company__c = company.Id;
        insert acc;
     
    }
    
    @IsTest
    public static void executeTest() {
        
        Account obj = [SELECT Id FROM Account LIMIT 1];
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', obj.Id);
        
        Test.startTest();
        
        TrafiguraGetDefinedTolerance t = new TrafiguraGetDefinedTolerance();
        Map<String, Object> companyInfo = t.execute(inputMap);
        
        Test.stopTest();
        
        System.assert(companyInfo != null, 'Invalid data');
        System.assert(companyInfo.size() > 0, 'Invalid data');
    }
    
}