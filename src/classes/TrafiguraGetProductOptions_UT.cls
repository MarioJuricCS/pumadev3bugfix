@isTest
public class TrafiguraGetProductOptions_UT {

    @isTest
    static void execute_case1_test(){
       
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        System.Assert(acc.Id != null);
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', acc.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypeBulk);
        inputMap.put('OrderType', EP_OrderConstant.salesOrderType);
        inputMap.put('LocationId', EP_OrderConstant.supplyLocationId);
        inputMap.put('orderEpoch', EP_OrderConstant.Order_Epoch_Retro);
        inputMap.put('isCommunity', false);
        inputMap.put('ShipToAccNumber', acc.Id);
        
        Test.startTest();
        TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
        getOptions.execute(inputMap);
        System.AssertEquals((String) inputMap.get('ProductType'),'Bulk');
        Test.stopTest();
    }
    
    
    @isTest
    static void execute_case2_test(){
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        System.Assert(acc.Id != null);
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', acc.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypeBulk);
        inputMap.put('OrderType', 'Test');
        inputMap.put('LocationId', EP_OrderConstant.supplyLocationId);
        inputMap.put('orderEpoch', EP_OrderConstant.Order_Epoch_Retro);
        inputMap.put('isCommunity', false);
        inputMap.put('ShipToAccNumber', acc.Id);
        
        Test.startTest();
        TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
        getOptions.execute(inputMap);
        System.AssertEquals((String) inputMap.get('orderEpoch'),'Retrospective');
        Test.stopTest();
    }
    
    
    @isTest
    static void execute_case3_test(){
       
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        System.Assert(acc.Id != null);
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', acc.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypeService);
        inputMap.put('OrderType', 'Test');
        inputMap.put('LocationId', EP_OrderConstant.supplyLocationId);
        inputMap.put('orderEpoch', EP_OrderConstant.Order_Epoch_Retro);
        inputMap.put('isCommunity', false);
        inputMap.put('ShipToAccNumber', acc.Id);
        
        Test.startTest();
        TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
        getOptions.execute(inputMap);
        System.AssertEquals((String) inputMap.get('ProductType'),'Service');
        Test.stopTest();
    }
    
    @isTest
    static void execute_case4_test(){
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        System.Assert(acc.Id != null);
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', acc.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypePackaged);
        inputMap.put('OrderType', EP_OrderConstant.salesOrderType);
        inputMap.put('LocationId', EP_OrderConstant.supplyLocationId);
        inputMap.put('orderEpoch', EP_OrderConstant.Order_Epoch_Retro);
        inputMap.put('isCommunity', false);
        inputMap.put('ShipToAccNumber', acc.Id);
        
        Test.startTest();
        TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
        getOptions.execute(inputMap);
        System.AssertEquals((String) inputMap.get('ProductType'),'Packaged');
        Test.stopTest();
    }
    
    
    @isTest
    static void execute_case5_test(){
        
        Account acc = new Account();
        acc.EP_VendorType__c = '3rd Party Stock Supplier';
        acc.EP_Vendor_Type__c = '3rd Party Stock Supplier';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        acc.Name='testEPAccount';
        acc.EP_Status__c = '06-Blocked';
        insert acc;
        System.Assert(acc.Id != null);
        
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('AccountId', acc.Id);
        inputMap.put('ProductType', EP_OrderConstant.productTypePackaged);
        inputMap.put('OrderType', 'Test');
        inputMap.put('LocationId', EP_OrderConstant.supplyLocationId);
        inputMap.put('orderEpoch', EP_OrderConstant.Order_Epoch_Retro);
        inputMap.put('isCommunity', false);
        inputMap.put('ShipToAccNumber', acc.Id);
        
        Test.startTest();
        TrafiguraGetProductOptions getOptions = new TrafiguraGetProductOptions();
        getOptions.execute(inputMap);
        System.AssertEquals((String) inputMap.get('OrderType'),'Test');
        Test.stopTest();
    }
}