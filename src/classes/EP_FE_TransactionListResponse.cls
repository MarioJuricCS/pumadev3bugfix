/* 
   @Author <Alper Genc>
   @name <EP_FE_TransactionListResponse >
   @CreateDate <30/05/2016>
   @Description <This class >  
   @Version <1.0>
*/
global with sharing class EP_FE_TransactionListResponse extends EP_FE_Response {
    public EP_WrapperAccountStatement customerAccountStatement {get; set;}
}