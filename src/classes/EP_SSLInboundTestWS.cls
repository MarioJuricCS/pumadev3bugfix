/* 
  @Author <Jai Singh>
   @name <EP_SSLInboundTestWS >
   @CreateDate <22/12/2016>
   @Description <This is apex RESTful WebService for testing SSL with IPAAS> 
   @Version <1.0>
*/
@RestResource(urlMapping='/v1/SSLInboundTest/*')
global with sharing class EP_SSLInboundTestWS {
    
    /*
        This is the method that handles HttpPost request for product sync
    */
    @HttpPost
    global static void vendorSync(){
        RestRequest request = RestContext.request;
        String requestBody = request.requestBody.toString();
        String resultstr = '{"data":"Your sent information this information: <'+requestBody+'>. Thanks for contacting"}';
        RestContext.response.responseBody = Blob.valueOf(resultstr); 
        RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
    }      
}