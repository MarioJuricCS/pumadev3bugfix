/*
 *  @Author <Accenture>
 *  @Name <EP_NonVMIShipToASProspect>
 *  @CreateDate <>
 *  @Description <VMI Ship To Account State for 01-Prospect Status>
 *  @Version <1.0>
 */
 public with sharing class EP_NonVMIShipToASProspect extends EP_AccountState{
     
    public EP_NonVMIShipToASProspect() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASProspect','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASProspect','doOnEntry');
        
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASProspect','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASProspect','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASProspect','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}