/*
 *  @Author <Accenture>
 *  @Name <EP_NonVMIShipToASInactive>
 *  @CreateDate <>
 *  @Description <VMI Ship To Account State for 07-Inactive Status>
 *  @Version <1.0>
 */
 public with sharing class EP_NonVMIShipToASInactive extends EP_AccountState{
     
    public EP_NonVMIShipToASInactive() {
        
    }

    public override void setAccountDomainObject(EP_AccountDomainObject currentAccount){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASInactive','setAccountDomainObject');
        super.setAccountDomainObject(currentAccount);
    }

    public override void doOnEntry(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASInactive','doOnEntry');
        EP_AccountService service = new EP_AccountService(this.account);
        service.doActionSendUpdateRequestToNavAndLomo();
    }  

    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASInactive','doOnExit');
        
    }
    
    public override boolean doTransition(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASInactive','doTransition');
        return super.doTransition();
    }

    public override boolean isInboundTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_NonVMIShipToASInactive','isInboundTransitionPossible');
        //Override this with all possible guard conditions that permits the state transition possible
        //For e.g.) Criteria based state "coming from", "Event" that's triggering this change 
        return super.isInboundTransitionPossible();

    }

}