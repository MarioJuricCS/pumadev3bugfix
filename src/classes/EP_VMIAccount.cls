/*
 *  @Author <Accenture>
 *  @Name <EP_VMIAccount>
 *  @CreateDate <21/12/2016>
 *  @Description <This class executes logic for VMI Ship To >
 *  @Version <1.0>
 */
 public class EP_VMIAccount extends EP_Account_VendorManagement {  
  
    
    /**  @Description:This method updates UTC offset on VMI ShipTo
      *  @date      :03/01/2017
      *  @name      :updateVMIShipToUTCTimeZoneOffset
      *  @param     :Account
      *  @return    :NA
      *  @throws    :NA
      */
      public override void updateVMIShipToUTCTimeZoneOffset(Account account) {
        EP_GeneralUtility.Log('Public','EP_VMIAccount','updateVMIShipToUTCTimeZoneOffset');
        if (account.EP_Ship_To_UTC_Timezone__c != NULL && account.EP_Ship_To_UTC_Timezone__c != EP_Common_Constant.BLANK) {
          account.EP_Ship_To_UTC_Offset__c = EP_PortalLibClass_R1.calcualteUTCOffsetFromTimezone(account.EP_Ship_To_UTC_Timezone__c);
        } 
      }


  	/**  @Description: Method to create Tank dip placeholder for ship to account if status is changed to active
      *  @date      :03/01/2017
      *  @name      :insertPlaceHolderTankDips
      *  @param     :Account
      *  @return    :NA
      *  @throws    :NA
      */
      public override void insertPlaceHolderTankDips(Account account){
        EP_GeneralUtility.Log('Public','EP_VMIAccount','insertPlaceHolderTankDips');
        if(account.EP_Is_Ship_To_Open_Today__c){
          EP_TankDipsDomainObject tankDipsDomainObj =  new EP_TankDipsDomainObject();
          tankDipsDomainObj.insertPlaceHolderTankDips(account);   
        }
      }


  	/**  @Description: Method to delete Tank dip placeholder for ship to account if status is changed to Inactive
      *  @date      :03/01/2017
      *  @name      :deletePlaceHolderTankDips
      *  @param     :Account
      *  @return    :NA
      *  @throws    :NA
      */
      public override void deletePlaceHolderTankDips(Account account){
        EP_GeneralUtility.Log('Public','EP_VMIAccount','deletePlaceHolderTankDips');
        if(account.EP_Status__c == EP_Common_Constant.INACTIVE_SHIP_TO_STATUS || !account.EP_Is_Ship_To_Open_Today__c){
          EP_TankDipsDomainObject tankDipsDomainObj =  new EP_TankDipsDomainObject();
          tankDipsDomainObj.deletePlaceHolderTankDips(account);   
        }
      }
 }