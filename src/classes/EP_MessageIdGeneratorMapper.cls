/**
   @Author          CR Team
   @name            EP_MessageIdGeneratorMapper
   @CreateDate      12/20/2016
   @Description     This class contains all SOQLs related to EP_Message_Id_Generator__c Object
   @Version         1.0
   @reference       NA
*/
/*  Version 1: Modification History  
    *************************************************************************************************************
    MODIFIED DATE       MODIFIED BY             REASON
    10/01/2017          Swapnil Gorane          Added Method -> getNameOfMesgIdGenerator(Id)
    -------------------------------------------------------------------------------------------------------------
    
    *************************************************************************************************************
*/
public with sharing class EP_MessageIdGeneratorMapper {
    
    /**
    *  Constructor. 
    *  @name            EP_MessageIdGeneratorMapper
    *  @param           NA
    *  @return          NA
    *  @throws          NA
    */   
    public EP_MessageIdGeneratorMapper() {
        
    }

    /** This method returns Message Id Generator records by Ids
    *  @name            getRecordsByIds
    *  @param           list<id>, Integer
    *  @return          list<EP_Message_Id_Generator__c>
    *  @throws          NA
    */
    public list<EP_Message_Id_Generator__c> getRecordsByIds(list<id> idList) {
        list<EP_Message_Id_Generator__c> messIdGenObjList = new list<EP_Message_Id_Generator__c>();
        for(list<EP_Message_Id_Generator__c> messIdGenList : [SELECT name, id 
                                                            FROM EP_Message_Id_Generator__c 
                                                            WHERE Id IN :idList 
                                                            //LIMIT :limitVals
                                                        ]){
            messIdGenObjList.addAll(messIdGenList);                                             
        }
        return messIdGenObjList ;
    }
    
    /** This method is used to get message Id generator Name
    *  @author          Swapnil Gorane
    *  @date            10/01/2017
    *  @name            getNameOfMesgIdGenerator
    *  @param           Id 
    *  @return          String
    *  @throws          NA
    */
    public String getNameOfMesgIdGenerator(Id mesIdGenId) {
        String msgIdGenName = [Select Name from EP_Message_Id_Generator__c where id=:mesIdGenId].Name;
        return msgIdGenName ;
    }
    
}