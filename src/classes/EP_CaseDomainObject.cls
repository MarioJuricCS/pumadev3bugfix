/* 
   @Author      Accenture
   @name        EP_CaseDomainObject
   @CreateDate  07/05/2018
   @Description This is the domain class for Case Object
   @Version     1.1
*/
 public class EP_CaseDomainObject{
    
    public list<case> caseTriggerNew;
    public map<Id,case> CaseTriggerOldMap;
    EP_CaseService CaseService;
    
     /**
    * @author     Accenture
    * @name      EP_ProductOptionDomain
    * @date      15/11/2017
    * @description   Constructor for setting new Product Option objects list and old map which will be passed by the trigger
    * @param     list of new version Product Option and old map records
    * @return     NA
    */  
    public EP_CaseDomainObject(list<case> caseTriggerNew,map<Id,case> OldMap){
        this.caseTriggerNew = caseTriggerNew;
        this.CaseTriggerOldMap = OldMap;
        this.CaseService = new EP_CaseService(this);
    }
    
    /**
    * @author       Accenture
    * @name         doActionBeforeUpdate
    * @date         07/05/2018
    * @description  This method is to be called thorugh trigger to execute all before update event logic/actions
    * @param        NA
    * @return       NA
    */
    public void doActionBeforeUpdate(){
        try{
            EP_GeneralUtility.Log('Public','EP_CaseDomainObject','doActionBeforeUpdate');
             CaseService.doBeforeUpdateHandle();     
        }catch(Exception ex){
            EP_LoggingService.logHandledException(ex, EP_Common_Constant.EPUMA, 'doActionBeforeUpdate' , 'EP_CaseDomainObject', ApexPages.Severity.ERROR);
        }
    }
 }