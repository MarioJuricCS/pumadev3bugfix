/* 
@Author      CloudSense
@name        EP_CsOrderTrigger
@CreateDate  15/01/2018
@Description CS Order Trigger
@Version     1.0
*/ 
trigger EP_CsOrderTrigger on csord__Order__c (before Insert, before Update, after update) {

    if(trigger.isUpdate && trigger.isAfter){
        EP_CsOrderTriggerHandler.doAfterUpdate(trigger.newmap);
        EP_CsOrderTriggerHandler.orchestratorUpdate(trigger.newmap);
    }


    if(trigger.isBefore){
        EP_CsOrderTriggerHandler.doBeforeInsertandUpdate(trigger.new);        
    } 

    if(trigger.isBefore && trigger.isUpdate){
    	EP_CsOrderTriggerHandler.doBeforeUpdateLoadingCode(trigger.newmap, trigger.oldMap);
    }
}