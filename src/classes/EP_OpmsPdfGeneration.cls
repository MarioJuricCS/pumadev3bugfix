/**
 * 	@Author <Kamal Garg>
 * 	@Name <EP_OpmsPdfGeneration>
 * 	@CreateDate <31/05/2016>
 * 	@Description <This is the apex RESTful WebService to generate PDF>
 * 	@Version <1.0>
 */
@RestResource(urlMapping='/v1/OPMSPDFGeneration/*')
global without sharing class EP_OpmsPdfGeneration {
	
	/**
	 * @author <Kamal Garg>
	 * @name <generatePDF>
	 * @date <31/05/2016>
	 * @description <This method is used to handle incoming HttpPost requests coming from OPMS System for PDF Generation>
	 * @version <1.0>
	 * @param none
	 * @return void
	 */
	@HttpPost
	global static void generatePDF() {
		RestRequest request = RestContext.request;
		String requestBody = request.requestBody.toString();
		if(String.isNotBlank(requestBody)){
			String resultString = EP_ManageDocument.manageDocuments(requestBody);
			RestContext.response.addHeader(EP_Common_Constant.CONTENT_TYPE, EP_Common_Constant.APPLICATION_SLASH_JSON);
			if(String.isNotBlank(resultString)) {
				RestContext.response.responseBody = Blob.valueOf(resultString);
			}
		}
	}
	
}