/*
   @Author          Accenture
   @Name            EP_ATRController - #59186
   @CreateDate      06/16/2017
   @Description     This controller class is used to set ATR enabled and disabled with custom setting and schedule a batch class
   @Version         1.0
*/
public with sharing class EP_ATRController {
    public EP_CS_Communication_Settings__c ATRSettings{get;set;}
    private Set<string> cronStates = new Set<string>{'WAITING','ACQUIRED'};

    /**
    * @Author       Accenture
    * @Name         EP_ATRController
    * @Date         06/16/2017
    * @Description  
    * @Param        NA
    * @return       NA
    */
    public EP_ATRController(){
        ATRSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_ATR);
    }
    
    /**
    * @Author       Accenture
    * @Name         enableATR
    * @Date         06/16/2017
    * @Description  
    * @Param        NA
    * @return       NA
    */
    public void enableATR(){
        EP_CS_Communication_Settings__c communicationSettings = EP_CS_Communication_Settings__c.getValues(EP_Common_Constant.DISABLE_OUTBOUND_COMMUNICATIONS);        
        if(communicationSettings.Disable__c) {
            addPageMessgage('ATR Can not be Enabled Since OUTBOUND COMMUNICATIONS is disabled for Org. Please uncheck "Disable Outbound Communications" in EP_CS_Communication_Settings__c custom setting');
        } else {
            updateATRSetting(false);
            runAutomaticTechnicalRetryBatch();
            addPageMessgage('ATR is Enabled');
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         disableATR
    * @Date         06/16/2017
    * @Description  
    * @Param        NA
    * @return       NA
    */
    public void disableATR(){
        updateATRSetting(true);
        EP_AutomaticTechnicalRetryBatch.deleteCronJobs(cronStates);
        addPageMessgage('ATR is Disabled');
    }
    
    /**
    * @Author       Accenture
    * @Name         execute
    * @Date         06/16/2017
    * @Description  This methods gets all the records which needs to be processed
    * @Param        SchedulableContext
    * @return       NA
    */
    @TestVisible
    private void updateATRSetting(boolean value) {
        ATRSettings.Disable__c = value;
        update ATRSettings;
    }
    
    /**
    * @Author       Accenture
    * @Name         runAutomaticTechnicalRetryBatch
    * @Date         06/16/2017
    * @Description  
    * @Param        NA
    * @return       NA
    */
    @TestVisible
    private void runAutomaticTechnicalRetryBatch() {
        if(!EP_AutomaticTechnicalRetryBatch.isCronJobScheduled(EP_Common_Constant.ATR_SEARCH_STRING)) {
            EP_AutomaticTechnicalRetryBatch  atr = new EP_AutomaticTechnicalRetryBatch();
            //Processing one record at a time because, if we have multiple records processing, then 
            //we cannot make DML as it won't allow to make further callouts after DML.
            Database.executeBatch(atr,1);
        }
    }
    
    /**
    * @Author       Accenture
    * @Name         addPageMessgage
    * @Date         06/16/2017
    * @Description  
    * @Param        string
    * @return       NA
    */
    @TestVisible
    private void addPageMessgage(string message) {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,message);
        ApexPages.addMessage(myMsg);
    }
}