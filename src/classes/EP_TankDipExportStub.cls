/* 
   @Author 			Accenture
   @name 			EP_TankDipExportStub
   @CreateDate 		03/10/2017
   @Description		Stub class for tank dip inbound interface which is used to send data to external system
   @Version 		1.0
*/
public class EP_TankDipExportStub {
	
	public MSG MSG;
    public EP_TankDipExportStub(){
		MSG = new MSG();
	}
	public class MSG {
		public EP_MessageHeader HeaderCommon;
		public payload Payload;
		public String StatusPayload;	
		public MSG(){
			HeaderCommon = new EP_MessageHeader();
			payload = new payload();
		}
	}
	public class payload {
		public any0 any0;
		public payload(){
			any0 = new any0();
		}
	}
	public class any0{
		public tankDips tankDips;
		public any0(){
			tankDips = new tankDips();
		}
	}
	public class tankDips{
		public List<tankDip> tankDip;
		public tankDips(){
			tankDip = new List<tankDip>();
		}
	}
	public class tankDip{
		public Integer itemId;
        public String uom;
        public Integer qty;
        //public DateTime readingTimesStmpLcl;
        public String readingTimesStmpLcl;
        //public DateTime modifyOn;
        public String modifyOn;
        public String modifyBy;
        public String seqId;
        public String clientId;
        public identifier identifier;  
        public tankDip(){
        	identifier = new identifier();
        }
	}
	public class identifier{
         public string custId;
         public string shiptoId;
         public string tankNr;  
    }
    
}