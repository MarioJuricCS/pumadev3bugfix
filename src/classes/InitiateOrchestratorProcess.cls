/*
   @Author          CloudSense
   @Name            InitiateOrchestratorProcess
   @CreateDate      02/07/2017
   @Description     This class is responsible for creating the Orchestrator process for Order after Submit
   @Version         1.1
 
*/
Public class InitiateOrchestratorProcess {
   
    // fetch Custom setting
    public static CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getOrgDefaults();
    /**
    * @Author       CloudSense
    * @Name         execute
    * @Date         02/09/2017
    * @Description  Method to Create Orchestrator process for the Order
    * @Param        list<OrderIds>
    * @return       NA
    */   
    
    @InvocableMethod
    public static void execute(List<Id> OrderIds){

        system.debug('OrderId List is :' +OrderIds);
        List<CSPOFA__Orchestration_Process__c> orderCreationProcessList = new List<CSPOFA__Orchestration_Process__c> ();
        List<CSPOFA__Orchestration_Process__c> newProcessList = new List<CSPOFA__Orchestration_Process__c> ();
        List<CSPOFA__Orchestration_Process__c> processesList;
        
        try{
            
                List<csord__Order__c> orders =[SELECT 
                                                    ID, csord__Order_Number__c,csord__Status2__c,OrderStatus__c,Is_Order_Indicative__c,Is_Indicative_on_Modify__c, EP_Is_Credit_Exception_Cancelled_Sync__c, Cancellation_Check_Done__c,EP_Sync_with_NAV__c,Sync_with_WINDMS__c,
                                                    (SELECT ID,EP_CS_Order__c,EP_Status__c FROM Credit_Exception_Requests2__r)
                                                FROM
                                                    csord__Order__c
                                                WHERE
                                                    ID in :orderIds
                                              ];
                                              
            //fetch Custom setting
            List<csord__Order__c> ordersList = new List<csord__Order__c>();
            CS_ORDER_SETTINGS__c csOrderSetting = CS_ORDER_SETTINGS__c.getInstance();    
            set<String> OrchestrationProcessTemplateId = new set<String>{
                csOrderSetting.Orchestrator_Process_TemplateId__c,
                csOrderSetting.Orchestrator_Modify_Template__c,
                csOrderSetting.Orchestrator_IsIndicativeModify_Template__c
            }; 
                                      
            system.debug('Orders list is :' +orders);
            //Process List with Create and Modify Template
            processesList = [SELECT Id, Order__c FROM CSPOFA__Orchestration_Process__c WHERE Order__c =: orders[0].Id and CSPOFA__Orchestration_Process_Template__c in :OrchestrationProcessTemplateId];
            
            //Process List with Cancellation Template
            List<CSPOFA__Orchestration_Process__c> processesListCancel = [SELECT Id, Order__c FROM CSPOFA__Orchestration_Process__c WHERE Order__c =: orders[0].Id and CSPOFA__Orchestration_Process_Template__c =:csOrderSetting.Orchestrator_Cancel_Template__c]; 
            
             if(!orders.isEmpty() && 
                    processesListCancel.isEmpty() && 
                    (processesList.size() == 0 || orders[0].Cancellation_Check_Done__c)){
                orderCreationProcessList = createNewOrderProcesses(orders);
                newProcessList.addall(orderCreationProcessList);
            }
	    // Handle for 94084.
            if(!orders.isEmpty()){
                for(csord__Order__c ord :orders)
                {
                    if(ord.OrderStatus__c == 'Draft'){
                       ord.Is_Indicative_on_Modify__c = 'No';
                       ordersList.add(ord);
                       system.debug('ordersList~~'+ordersList);
                      
                    }
                }
                update ordersList;
                system.debug('ordersLis>>>~'+ordersList);
            }
            system.debug('newProcessList list is :' +newProcessList);
            if(!newProcessList.isEmpty()) {
                upsert newProcessList;
            }

            if (!orders.isEmpty() && orders[0].Cancellation_Check_Done__c && !processesList.isEmpty()) {
                delete processesList;
            }
            System.Debug('-->'+processesList);
            
            //}
        }Catch(Exception exp){
                    system.debug('pep ' + exp);
             EP_loggingService.loghandledException(exp, EP_Common_Constant.EPUMA, 'execute', 'InitiateOrchestratorProcess',apexPages.severity.ERROR);
    }
}
    
    /**
    * @Author       CloudSense
    * @Name         createNewOrderProcesses
    * @Date         02/09/2017
    * @Description  Method to Set the Orchestrator Process fields
    * @Param        list<Order>
    * @return       NA
    */ 
  
    @TestVisible
    private  static List<CSPOFA__Orchestration_Process__c> createNewOrderProcesses(List<csord__Order__c> orders) {
        String processTemplateId;
        Boolean CEFlag = false;
        Boolean CEIndicativeOnModifyFlag = false;
        for(csord__Order__c order : orders){
                 
                 if(order.Credit_Exception_Requests2__r != null && !order.Credit_Exception_Requests2__r.isEmpty()){
                   for(EP_Credit_Exception_Request__c creditExcep : order.Credit_Exception_Requests2__r){
                         if(creditExcep.EP_Status__c == EP_Common_Constant.CREDIT_EXC_STATUS_PENDING_APPROVAL || creditExcep.EP_Status__c == EP_Common_Constant.CREDIT_EXC_STATUS_APPROVED ||creditExcep.EP_Status__c == EP_Common_Constant.BLANK){
                             CEFlag = true;
                         }
                         // Handle for 94084.
                         if(creditExcep.EP_Status__c == EP_Common_Constant.CREDIT_EXC_STATUS_PENDING_APPROVAL || creditExcep.EP_Status__c == EP_Common_Constant.CREDIT_EXC_STATUS_APPROVED ||creditExcep.EP_Status__c == EP_Common_Constant.BLANK ||creditExcep.EP_Status__c == EP_Common_Constant.CANCELLED_STATUS || creditExcep.EP_Status__c == EP_Common_Constant.NULL_VALUE){
                             CEIndicativeOnModifyFlag = true;
                         }
                      }
                  }  
            if((CEFlag == true || order.EP_Sync_with_NAV__c == true || order.Sync_with_WINDMS__c == true) && order.Cancellation_Check_Done__c == false && order.Is_Indicative_on_Modify__c != 'Yes'){
                system.debug('Inside If');
                processTemplateId = csOrderSetting.Orchestrator_Modify_Template__c;
                 
            } else if (order.Cancellation_Check_Done__c == true){
            system.debug('Inside cancel');
                processTemplateId = csOrderSetting.Orchestrator_Cancel_Template__c;
            }
            
            else if (order.Is_Indicative_on_Modify__c == 'Yes' && (order.EP_Sync_with_NAV__c == true || order.Sync_with_WINDMS__c == true || CEIndicativeOnModifyFlag == true)){
            system.debug('Inside IndicativeonModify');
                processTemplateId = csOrderSetting.Orchestrator_IsIndicativeModify_Template__c;
            }  
           
            else if (order.Is_Indicative_on_Modify__c != 'Yes'){
                system.debug('Inside create');
                processTemplateId = csOrderSetting.Orchestrator_Process_TemplateId__c;
            }
        }
        system.debug('processTemplateId is :' +processTemplateId);
        List<CSPOFA__Orchestration_Process__c> processes = new List<CSPOFA__Orchestration_Process__c>();   
        for (csord__Order__c order : orders) {
            CSPOFA__Orchestration_Process__c process = new CSPOFA__Orchestration_Process__c();
            process.Order__c = order.Id;
            process.Name ='Orchestration_Process_Order_Num:' + order.csord__Order_Number__c;
            process.CSPOFA__Orchestration_Process_Template__c = processTemplateId;
            system.debug('process is :' +process);
            processes.add(process);
        }                                                                
        return processes;
    }
}
