public with sharing class EP_CRM_LeadConvertController {

    public Boolean showForm { get; set; }
    
    public EP_CRM_LeadConvertBean bean { get; set; } 

    public EP_CRM_LeadConvertController(ApexPages.StandardController stdController) {
        // Set default value to form as true
        showForm = true;
                
        // If no Convert Lead permission is assigned to User then Show an error
        if(EP_CRM_SystemPermissionUtility.userHasPermission('PermissionsConvertLeads') == false) {
            PrintError('You are not authorize to convert this Lead. Please connect with System Administrator.');
            showForm = false;
            return;         
        }
        
        // Initialize Bean
        bean = new EP_CRM_LeadConvertBean(stdController.getId());
    }
    
    // This method is called when the user clicks the Convert button on the VF Page
    public PageReference mergeContact() {
        // If the Contact is not set, then show an error
        if (bean.selectedContactId == 'NONE') {
            PrintError('Please select a Contact.');
            return null;    
        }
    
        // To get instance of Database.LeadConvert
        Database.LeadConvert leadConvert = prepareLeadConvert();
        
        // Set Contact Id If selectedContactId is not NEW
        if (bean.selectedContactId != 'NEW') {
            leadConvert.setContactId(bean.selectedContactId);
        }
        
        // Overwrite Lead Source 
        leadConvert.setOverWriteLeadSource(bean.overwriteLeadSource);
        
        PageReference pref = convertLeadResults(leadConvert);  
        
        return pref;
    } 
    
    // This method is called when the user clicks the Convert button on the VF Page
    public PageReference convertLead() {
    
        // if no activity is logged then show an error  
        if (isActivityLogged() == false) {
            PrintError('To convert Lead, you must have at least one activity logged against this Lead.');
            return null;    
        }
    
        // if Lead Status is not entered show an error  
        if (bean.leadToConvert.Status == 'NONE') {    
            PrintError('Please select a Lead Status.');
            return null;    
        }
        
        // If the Account is not set, then show an error
        if (bean.selectedAccountId == 'NONE') {
            PrintError('Please select an Account.');
            return null;    
        } // otherwise set the account id
        else if (bean.selectedAccountId != 'NEW') {
            // Redirect to Duplicate Contact Page to avoid creating duplicate contacts
            PageReference pref = new PageReference('/apex/mergeContactPage');
            pref.setRedirect(false);
            return pref;         
        }   
        
        // To get instance of Database.LeadConvert
        Database.LeadConvert leadConvert = prepareLeadConvert();
        
        PageReference pref = convertLeadResults(leadConvert);  
        
        return pref;
    } 
    
    private Database.LeadConvert prepareLeadConvert() {
        
        // This is the lead convert object that will convert the lead 
        Database.LeadConvert leadConvert = new Database.LeadConvert();
        
        // set lead ID
        leadConvert.setLeadId(bean.leadToConvert.Id);
    
        // Set Account Id If selectedAccountId is not NEW
        if (bean.selectedAccountId != 'NEW') {
            leadConvert.setAccountId(bean.selectedAccountId);
        }        
    
        // Set the lead convert status
        leadConvert.setConvertedStatus(bean.leadToConvert.Status);
    
        // Set the variable to create or not create an opportunity
        leadConvert.setDoNotCreateOpportunity(bean.doNotCreateOppty);
    
        // Set the Opportunity name
        leadConvert.setOpportunityName(
            ((bean.doNotCreateOppty) ? null : bean.opportunityObj.Name)
        );
    
        // Set the owner id
        leadConvert.setOwnerId(bean.contactObj.ownerID);
    
        // Set whether to have a notification email
        leadConvert.setSendNotificationEmail(bean.sendOwnerEmail);
    
        system.debug('leadConvert --> ' + leadConvert);
        
        return leadConvert;
    }
    
    public PageReference convertLeadResults(Database.LeadConvert leadConvert) {
        // If the lead converting was a success then redirect it Account Page
        PageReference pageRef = null;            
        try {
            // Convert the lead
            Database.LeadConvertResult leadConvertResult = Database.convertLead(leadConvert);
        
            if (leadConvertResult.success) {            
                // Redirect the user to the newly created Account
                pageRef = new PageReference('/' + leadConvertResult.getAccountId());    
                pageRef.setRedirect(true);                
            } else {    
                // If converting was unsucessful, print the errors to the pageMessages and return null
                System.Debug(leadConvertResult.errors);    
                PrintErrors(leadConvertResult.errors);    
            }    
        } catch(Exception ex) {
            // If converting was unsucessful, print the errors to the pageMessages and return null
            System.Debug(ex.getMessage());    
            PrintError(ex.getMessage());    
        }                   
        return pageRef;
    }
    
    // This method will return Boolean whether activity is logged or not
    public Boolean isActivityLogged() {
        Boolean activityLogged = true;
        for(Lead leadObj : [SELECT Id, (SELECT Id FROM Tasks WHERE Status != 'Deferred'), (SELECT Id FROM Events) FROM Lead WHERE Id = :bean.leadToConvert.Id]){
            if(leadObj.Tasks.size() == 0 && leadObj.Events.size()== 0){
                activityLogged = false;
            }
        }
        return activityLogged;
    }  
    
    // This method will take database errors and print them to teh PageMessages 
    public void PrintErrors(Database.Error[] errors) {
        for (Database.Error error: errors) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error.message);
            ApexPages.addMessage(msg);
        }
    }
    
    // This method will put an error into the PageMessages on the page
    public void PrintError(string error) {
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error);
        ApexPages.addMessage(msg);
    }
}