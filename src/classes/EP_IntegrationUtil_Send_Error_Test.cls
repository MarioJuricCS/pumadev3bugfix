/*   
     @Author <Sanchit Dua>
     @name <EP_IntegrationUtil_Test.cls>   
     @CreateDate <19/11/2015>   
     @Description <This class is used to cover the class EP_IntegrationUtil.>   
     @Version <1.1> 
     Revision updates:
     Added 3 methods for ok by jyotsna yadav
     Added 3 methods for error handling by <<Sanchit Dua>>
*/  
@isTest
public class EP_IntegrationUtil_Send_Error_Test {
    //static member variable to be used as test data
    private static Account acc;
    private static String txId;
    private static String msgId;
    private static String company;
    private static DateTime dt_sent; 
    private static final String TESTREC = 'test';
    private static final String SENTERROR = 'ERROR-SENT';
    private static final String SYNCERROR = 'ERROR-SYNC';
    private static final String INITIAL_VAL = '';
    
    private static final Integer LMT = Limits.getLimitQueries();
    
    /*
     * Description: creating the sample test data
     * @author: sanchit dua
    */
    private static void createSampleData(){
        EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
        insert country;
        acc = new Account(name=TESTREC, EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
        BillingCountry='in',
        ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
        ShippingCountry='in');
        insert acc; 
        txId = EP_IntegrationUtil.getTransactionID(TESTREC, TESTREC);    
        msgId = EP_IntegrationUtil.getMessageId(TESTREC); 
        company = TESTREC;   
        dt_sent = DateTime.now();  
    }

     
    /**
       Method to test the functionality of unique message Id.
      **/     
     static testMethod void getMessageId_Test(){
         System.runAs(EP_TestDataUtility.createRunAsUser()) {
             String interfaceId = 'Customersync';
             test.startTest();
             msgId = EP_IntegrationUtil.getMessageId(interfaceId);
             test.stopTest();
             String expectedId = 'Customersync_'+DateTime.now();
             
         }
     }
     
    /**
       Method to test the functionality of unique message Id.
      **/     
     static testMethod void getMessageId2_Test(){
         System.runAs(EP_TestDataUtility.createRunAsUser()) {
             String interfaceId = 'Customersync';
             test.startTest();
             msgId = EP_IntegrationUtil.getMessageId('source'
                                        ,'local'
                                        ,'processName'
                                        ,DateTime.now()
                                        ,'uniqueId');
             test.stopTest();

         }
     }
    
    /**
       Method to test the functionality of unique transaction Id.
      **/         
    static testMethod void getTransactionId_Test(){
        System.runAs(EP_TestDataUtility.createRunAsUser()) {
            String interfaceId = 'Customersync';
            String transactionType = 'Create';
            test.startTest();
            txId = EP_IntegrationUtil.getTransactionID(interfaceId,transactionType);
            test.stopTest();
            String expectedId = 'Create_Customersync_'+DateTime.now();
           
        }
     }
 }