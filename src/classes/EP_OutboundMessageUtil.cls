/* 
   @Author :Accenture
   @name : EP_OutboundMessageUtil
   @CreateDate : 02/07/2017
   @Description : This is Utility class for Outbound Messages
   @Version : 1.0
*/
public with sharing class EP_OutboundMessageUtil {
    /**
	* @Author : Accenture
	* @Name : isAuthorized
	* @Date : 02/07/2017
	* @Description :This method to check if user is Authorized to access.
	* @Param :
	* @return :boolean
	*/ 
    public Static boolean isAuthorized(string secretCode) {
        boolean isAuthorized = true;
        if(string.isEmpty(secretCode)) {
            isAuthorized = false;
        }
        return isAuthorized;
    }
     
   /**
	* @Author : Accenture
	* @Name : getSObjectIdSet
	* @Date : 02/07/2017
	* @Description :This method is used to create a set of Ids by a List of sObject Object
	* @Param : list<SObject>
	* @return :Set<Id>
	*/   
    public Static Set<Id> getSObjectIdSet(list<SObject> sObjectList){
        set<Id> objIdSet = new set<id>();
        for(SObject sObj : sObjectList) {
            objIdSet.add(sObj.id);
        }
        return objIdSet;
    }

   /**
	* @Author : Accenture
	* @Name : setMessageHeader
	* @Date : 02/07/2017
	* @Description :This method will be used to setup the values for Header in XML message from the Custom setting
	* @Param : EP_CS_OutboundMessageSetting__c,  string messageId, string companyName
	* @return :EP_OutboundMessageHeader
	*/ 
    public Static EP_OutboundMessageHeader setMessageHeader(EP_CS_OutboundMessageSetting__c msgSetting, string messageId, string companyName) {
        EP_OutboundMessageHeader headerObj = new EP_OutboundMessageHeader();
        string sourceRespAdd = msgSetting.Response_End_Point__c;
        headerObj.MsgID = messageId;
        headerObj.InterfaceType = msgSetting.Interface_Type__c;
        headerObj.SourceCompany = companyName;
        headerObj.SourceUpdateStatusAddress = sourceRespAdd;
        headerObj.SourceResponseAddress = sourceRespAdd;
        headerObj.ObjectName = msgSetting.ObjectName__c;
        
        headerObj.ContinueOnError = msgSetting.EP_ContinueOnError__c;
        headerObj.UpdateSourceOnReceive = msgSetting.EP_UpdateSourceOnReceive__c;
        headerObj.UpdateSourceOnDelivery = msgSetting.EP_UpdateSourceOnDelivery__c;
        headerObj.UpdateSourceAfterProcessing = msgSetting.EP_UpdateSourceAfterProcessing__c;
        return headerObj;
    }
    
    /**
	* @Author : Accenture
	* @Name : getPayloadXML
	* @Date : 02/07/2017
	* @Description :This method will be use to load the payload page and setup the values for Payload in XML message
	* @Param : EP_CS_OutboundMessageSetting__c, Id, string messageId, string companyName
	* @return :string
	*/ 
    public static string getPayloadXML(EP_CS_OutboundMessageSetting__c msgSetting, Id recordId , string messageId,  string companyCode) {
        string encodedPayload = EP_XMLMessageGenerator.generateXML(recordId , messageId, msgSetting.Name , msgSetting.Payload_VF_Page_Name__c, companyCode);
        if(msgSetting.Payload_Encoded__c) {
            encodedPayload = EncodingUtil.base64Encode( Blob.valueOf(encodedPayload));
        }
        return encodedPayload;
    }
    
    /**
	* @Author : Accenture
	* @Name : redirectToErrorPage
	* @Date : 02/07/2017
	* @Description :This method will helps to redirect to error page if user is not Autherized.
	* @Param : 
	* @return :PageReference
	*/
    public static PageReference redirectToErrorPage() {
        PageReference pageRef = Page.EP_UnauthorizedAccess;
        pageRef.setRedirect(true);
        return pageRef;
    }
}