/*   
     @Author <Accenture>
     @name <EP_FE_TankDipSubmissionEndpointTest.cls>     
     @Description <This class is the test class for EP_FE_TankDipSubmissionEndpoint>   
     @Version <1.1> 
*/
@isTest
Private class EP_FE_TankDipSubmissionEndpointTest{

    private static User sysAdmUser = EP_FE_TestDataUtility.getRunAsUser();

    /*********************************************************************************************
        *@Description : This is the Test method to create Tank Dip Record from Rest Service .                  
        *@Params      :                    
        *@Return      : Void                                                                             
    *********************************************************************************************/ 
    static testMethod void testSubmitTankDip(){
      
        EP_FE_TankDipSubmissionRequest request = new EP_FE_TankDipSubmissionRequest(); 

         
                   
            //Account Creation
            Account acc = EP_TestDataUtility.createBillToAccount(); 
            Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(EP_FE_Constants.SHIPTO_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
            EP_Freight_Matrix__c freightMatrix =EP_TestDataUtility.createFreightMatrix();      
            Account sellTo = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
            Database.insert(sellTo);
            
            sellTo.EP_Status__c = '02-Basic Data Setup';
            Database.update(sellTo); 
            sellTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update (sellTo);
            
            Account shipTo = EP_TestDataUtility.createShipToAccount(sellTo.id, recTypeId);
            Database.insert(shipTo) ;
            
            shipTo.EP_Status__c = '02-Basic Data Setup';
            Database.update(shipTo); 
            shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(shipTo) ;
                
            EP_Tank__c tank = EP_TestDataUtility.createTestEP_Tank(shipTo );
            EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem  r1= new EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem();
             
            r1.tankId = tank.id ;
            r1.dipLevel = 100;
            r1.measurementTime = system.today();
            r1.unitOfMeasurement = 'test';
            
            
                
            System.runAs(sysAdmUser){
            Test.startTest();
                List<EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem> lstWrapp = new List<EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem>();
                    
                lstWrapp.add(r1); 
                request.tankDips = lstWrapp;
            
                RestRequest req = new RestRequest(); 
                
                req.httpMethod = 'POST';
                req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
                req.requestURI = '/services/apexrest/FE/V1/tank/submitdip/' ;      
                RestContext.request = req;
            
                
                EP_FE_TankDipSubmissionEndpoint.submitTankDip(request);  
            Test.stopTest();   
        }
    }
/*********************************************************************************************
    *@Description : This is the Test method to create Tank Dip Record from Rest Service .                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    static testMethod void testSubmitTankNotDip(){
      
        EP_FE_TankDipSubmissionRequest request = new EP_FE_TankDipSubmissionRequest();         
        EP_FE_TankDipSubmissionEndpoint obj = new EP_FE_TankDipSubmissionEndpoint();        
        EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem  r1= new EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem();
        
        System.runAs(sysAdmUser){
            
            Account acc = EP_TestDataUtility.createBillToAccount(); 
            Id recTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(EP_FE_Constants.SHIPTO_ACCOUNT_RECORD_TYPE_NAME).getRecordTypeId();
            EP_Freight_Matrix__c freightMatrix =EP_TestDataUtility.createFreightMatrix();      
            Account sellTo = EP_TestDataUtility.createSellToAccount(null,freightMatrix.id);
            Database.insert(sellTo);
            
            sellTo.EP_Status__c = '02-Basic Data Setup';
            Database.update(sellTo); 
         
            sellTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(sellTo);
            
            Account shipTo = EP_TestDataUtility.createShipToAccount(sellTo.id, recTypeId);
            Database.insert(shipTo) ;
            
            shipTo.EP_Status__c = '02-Basic Data Setup';
            Database.update(shipTo); 
            shipTo.EP_Status__c = EP_Common_Constant.STATUS_ACTIVE;
            Database.update(shipTo) ;
                
            EP_Tank__c tank = EP_TestDataUtility.createTestEP_Tank(shipTo );
             
            r1.tankId = tank.id ;
            r1.measurementTime = system.today();
            
            Test.startTest();
                List<EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem> lstWrapp = new List<EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem>();
                
                lstWrapp.add(r1); 
                request.tankDips = lstWrapp;
                
                RestRequest req = new RestRequest(); 
                
                req.httpMethod = 'POST';
                req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
                req.requestURI = '/services/apexrest/FE/V1/tank/submitdip/' ;      
                RestContext.request = req;
                
                EP_FE_TankDipSubmissionEndpoint.submitTankDip(request); 
            Test.stopTest();    
        }
                              
    }      
     /*********************************************************************************************
    *@Description : This is the Test method to create Tank Dip Record from Rest Service .                  
    *@Params      :                    
    *@Return      : Void                                                                             
    *********************************************************************************************/
    static testMethod void testSubmitTankDipNull(){
        
        EP_FE_TankDipSubmissionRequest request = new EP_FE_TankDipSubmissionRequest();  
        EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem  r1= new EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem();
            
            List<EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem> lstWrapp = new List<EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem>();
            
            lstWrapp.add(r1); 
            request.tankDips = null;
            
            System.runAs(sysAdmUser){
            Test.startTest();
            RestRequest req = new RestRequest(); 
            
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/tank/submitdip/' ;      
            RestContext.request = req;
            
            EP_FE_TankDipSubmissionEndpoint.submitTankDip(request);   
            Test.stopTest(); 
        }  
                        
    }       
        /*********************************************************************************************
        *@Description : This is the Test method to create Tank Dip Record from Rest Service .                  
        *@Params      :                    
        *@Return      : Void                                                                             
        *********************************************************************************************/
    static testMethod void testSubmitTankDipRequestNull(){
        
        EP_FE_TankDipSubmissionRequest request = new EP_FE_TankDipSubmissionRequest();  
        EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem  r1= new EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem();        
        
        List<EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem> lstWrapp = new List<EP_FE_TankDipSubmissionRequest.EP_FE_TankDipSubmissionItem>();
        
        lstWrapp.add(r1); 
        request.tankDips = null;
        request = null;
        System.runAs(sysAdmUser){
        
        Test.startTest();
            RestRequest req = new RestRequest(); 
            
            req.httpMethod = 'POST';
            req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated 
            req.requestURI = '/services/apexrest/FE/V1/tank/submitdip/' ;      
            RestContext.request = req;
            
            EP_FE_TankDipSubmissionEndpoint.submitTankDip(request);     
        Test.stopTest();   
        }         
    }              
}