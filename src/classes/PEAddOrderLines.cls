public with sharing class PEAddOrderLines {

 public List<WrapperpOrderLineList> waAccList {get;set;}
 public Integer rowToRemove {get;set;}
 public String selectedProduct {get;set;}

 public PEAddOrderLines(){
  waAccList = new List<WrapperpOrderLineList>();
}

 public void removeRowFromAccList(){
  List<csord__Order_Line_Item__c> orderLinesToDelete = new List<csord__Order_Line_Item__c>();
  for (Integer i = waAccList.size() - 1; i >= 0; i--){
    WrapperpOrderLineList wrapper = waAccList.get(i);
    system.debug('pep ' + wrapper.isSelected);
    system.debug('pep ' + waAccList);
    if(wrapper.isSelected){
    waAccList.remove(i);
    orderLinesToDelete.add(wrapper.orderLine);
    system.debug('pep ' + orderLinesToDelete);
    }
  }
  delete orderLinesToDelete;

 }

 public void addNewRowToAccList(){
  csord__Order_Line_Item__c orderLine = new csord__Order_Line_Item__c();
  waAccList.add(new WrapperpOrderLineList(orderLine, false));

 }
    
    

  public class WrapperpOrderLineList{
    public Boolean isSelected {get;set;}
    public csord__Order_Line_Item__c orderLine {get;set;}

    public WrapperpOrderLineList(csord__Order_Line_Item__c orderLine, Boolean isSelected){
      this.orderLine = orderLine;
      this.isSelected = isSelected;
    }
  } 
}