@isTest
public class EP_ConsumptionOrder_UT {
    public static final string PRICING_REQUEST_SENT='PRICING REQUEST SENT';
    
    @testSetup static void init() {
        EP_TestDataUtility.createDebugLog();
    }

    static testMethod void hasCreditIssue_positivetest() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getConsumptionOrderDomainObjectNegativeScenario();
        obj.localorder.EP_Bill_To__r = null;
        localObj.setOrderDomain(obj);
        Test.startTest();
        Boolean result = localObj.hasCreditIssue();
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    static testMethod void hasCreditIssue_negativetest() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject obj = EP_TestDataUtility.getConsumptionOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(obj);
        Test.startTest();
        Boolean result = localObj.hasCreditIssue();
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
   static testMethod void getShipTos_test() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObject();
        localObj.setOrderDomain(orderDomainObj);
        Id accountId =  orderDomainObj.getOrder().AccountId__c;
        Test.startTest();
        localObj.loadStrategies(new EP_OrderDomainObject(orderDomainObj.getOrder()));
        List<Account> result = localObj.getShipTos(accountId);
        Test.stopTest();
        System.AssertEquals(true, result.size()>0);
    }   
   
    static testMethod void findRecordType_test() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObject();
        localObj.setOrderDomain(orderDomainObj);
        Test.startTest();
        Id result = localObj.findRecordType();
        Test.stopTest(); 
        System.AssertEquals(true, result != null );
    }
    
    static testMethod void getApplicablePaymentTerms_test() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObject();
        localObj.setOrderDomain(orderDomainObj);
        csord__Order__c ord= orderDomainObj.getOrder();
        EP_AccountDomainObject accountDomain = new EP_AccountDomainObject(ord.AccountId__c);
        Account accountObject = accountDomain.getAccount();
        Test.startTest();
        List<String> result = localObj.getApplicablePaymentTerms(accountObject);
        Test.stopTest();
        System.AssertEquals(true,result!=null);
    } 
    
    static testMethod void getApplicableDeliveryTypes_test() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObject();
        localObj.setOrderDomain(orderDomainObj);
        Test.startTest();
        List<String> result = localObj.getApplicableDeliveryTypes();
        Test.stopTest();
        System.AssertEquals(true,result.size()>0);
    } 
    
    
    static testMethod void findPriceBookEntries_test() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObject();
        localObj.setOrderDomain(orderDomainObj);
        csord__Order__c orderObject = orderDomainObj.getOrder();
        Id pricebookId = orderObject.PriceBook2Id__c;
        Test.startTest();
        localObj.loadStrategies(new EP_OrderDomainObject(orderObject));
        List<PriceBookEntry> result = localObj.findPriceBookEntries(pricebookId,orderObject);
        Test.stopTest();
        System.AssertEquals(true,result.size()>0);
    }
    
    
    static testMethod void isValidOrderDate_positivetest() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObjectPositiveScenario();        
        localObj.setOrderDomain(orderDomainObj);
        csord__Order__c ord = orderDomainObj.getOrder();
        Test.startTest();
        localObj.loadStrategies(new EP_OrderDomainObject(ord));
        Boolean result = localObj.isValidOrderDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
   
    static testMethod void isValidOrderDate_negativetest() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObjectNegativeScenario();       
        localObj.setOrderDomain(orderDomainObj);
        csord__Order__c ord = orderDomainObj.getOrder();
        ord.EP_Order_Date__c = system.today().adddays(-1);
        update ord; 
        Test.startTest();
        localObj.loadStrategies(new EP_OrderDomainObject(ord));
        Boolean result = localObj.isValidOrderDate(ord);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    
    
    static testMethod void calculatePrice_test() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObject();
        localObj.setOrderDomain(orderDomainObj);
        List<EP_CS_OutboundMessageSetting__c> lOutboundCustomSetting = Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData'); 
        List<EP_Integration_Status_Update__c> lIntegrationStatusCustomSetting = Test.loadData(EP_Integration_Status_Update__c.sObjectType, 'EP_Integration_StatusUpdateTestData'); 
        EP_INTEGRATION_CUSTOM_SETTING__c integration_Setting = new EP_INTEGRATION_CUSTOM_SETTING__c(Name = 'Request TimeOut',EP_Value__c = '120000');
        insert integration_Setting;
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator()); 
        Set<ID> ordId = new Set<Id>{orderDomainObj.getOrder().id};
            String seqId = orderDomainObj.getOrder().id;
        Test.startTest();
        localObj.calculatePrice();
        Test.stopTest();
        EP_IntegrationRecord__c intRec = [SELECT Id,EP_Transaction_ID__c FROM EP_IntegrationRecord__c WHERE EP_Object_ID__c =: orderDomainObj.getOrder().Id];
        System.AssertNotEquals(NULL,intRec);
    }
    
    static testMethod void getDeliveryTypes_test() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObject();
        localObj.setOrderDomain(orderDomainObj);
        Test.startTest();
        List<String> result = localObj.getDeliveryTypes();
        Test.stopTest();
        System.AssertEquals(1,result.size());
        System.AssertEquals(result[0], EP_Common_Constant.CONSUMPTION);
    } 
    
   
    static testMethod void isOrderEntryValid_positivetest() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObjectPositiveScenario();
        localObj.setOrderDomain(orderDomainObj);
        Integer numOfTransportAccount = EP_Common_Constant.ONE;
        Test.startTest();
        localObj.loadStrategies(new EP_OrderDomainObject(orderDomainObj.getOrder()));
        Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
        Test.stopTest();
        System.AssertEquals(true,result);
    }
    
    
    static testMethod void isOrderEntryValid_negativetest() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObjectNegativeScenario();
        orderDomainObj.localorder.EP_Order_Date__c = null;
        localObj.setOrderDomain(orderDomainObj);
        Integer numOfTransportAccount = 0;
        Test.startTest();
        Boolean result = localObj.isOrderEntryValid(numOfTransportAccount);
        Test.stopTest();
        System.AssertEquals(false,result);
    }
    static testMethod void setConsumptionOrderDetails_test() {
        EP_ConsumptionOrder localObj = new EP_ConsumptionOrder();
        EP_OrderDomainObject orderDomainObj = EP_TestDataUtility.getConsumptionOrderDomainObject();
        localObj.setOrderDomain(orderDomainObj);
        Test.startTest();
        localObj.setConsumptionOrderDetails();
        Test.stopTest();
        System.AssertEquals(true,orderDomainObj.getOrder().RecordTypeId != null);    
    }
}