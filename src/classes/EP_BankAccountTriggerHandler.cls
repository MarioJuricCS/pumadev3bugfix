/* 
  @Author <Ashok Arora>
   @name <EP_BankAccountTriggerHandler>
   @CreateDate <28/10/2015>
   @Description <This class handles request from BankAccountTrigger> 
   @Version <1.0>
 
*/
public with sharing class EP_BankAccountTriggerHandler {
    public static Boolean isExecuteBeforeUpdate = false;
    public static Boolean isExecuteAfterUpdate = False;//TO AVOID MUTILPLE EXECUTION OF AFTER UPDATE TRIGGER IN SAME TRANSACTION
    public static boolean isExecutedByIntegrationUser = false;
    public static boolean skipValidationForCR = false;
    public static boolean executeBankAccountTrigger = true;
    /*
        This method handles before insert requests from Bank Account trigger
    */
    public static void doBeforeInsert(List<EP_Bank_Account__c>lNewAccounts){
        //VALIDATION - IF BILL TO IS SAME AS SELL TO
        //EP_BankAccountTriggerHelper.validateBillTo(lNewAccounts);
        //VALIDATE ONLY ONE ACTIVE BANK ACCOUNT PER CUSTOMER
        //EP_BankAccountTriggerHelper.validateActiveBankAccs(lNewAccounts);
        //VALIDATE IBAN
        if(executeBankAccountTrigger){
	        EP_BankAccountTriggerHelper.validateIBAN(lNewAccounts);
	        if(!isExecutedByIntegrationUser && !skipValidationForCR){
	            //restrict user to allow create and edit bank Account
	            EP_BankAccountTriggerHelper.restrictUserTOUPSERTBankAccount(lNewAccounts);
	        }
        }
    }
    
    /*
        This method handles before update requests from Bank Account trigger
    */
    public static void doBeforeUpdate(List<EP_Bank_Account__c>lOldAccounts
                                      ,List<EP_Bank_Account__c>lNewAccounts
                                      ,map<Id,EP_Bank_Account__c>mOldAccounts
                                      ,map<Id,EP_Bank_Account__c>mNewAccounts){
		if(executeBankAccountTrigger){                                      	
	        //SET TO TRUE TO AVOID RE-EXECUTION
	        isExecuteBeforeUpdate = True;
	        
	        //VALIDATE ONLY ONE ACTIVE BANK ACCOUNT PER CUSTOMER
	        //EP_BankAccountTriggerHelper.validateActiveBankAccs(lNewAccounts);
	        //VALIDATE IBAN
	        EP_BankAccountTriggerHelper.validateIBAN(lNewAccounts);
	        //VALIDATION - IF BILL TO IS SAME AS SELL TO
	        //EP_BankAccountTriggerHelper.validateBillTo(lNewAccounts);
	        //ON SYNC UPDATE BANK ACCOUNT AS ACTIVE
	        //EP_BankAccountTriggerHelper.doActiveSyncBankAcc(mOldAccounts,mNewAccounts);
	        /**DEFECT 49029 FIX START**/
	        if(!isExecutedByIntegrationUser && !skipValidationForCR){
	            //restrict user to allow create and edit bank Account
	            EP_BankAccountTriggerHelper.restrictUserTOUPSERTBankAccount(lNewAccounts);
	        }
	        /**DEFECT 49029 FIX END**/
	        
	        //Allow Modification On Bank Account 
	        //EP_BankAccountTriggerHelper.allowModificationOnBankAccount(lNewAccounts,mOldAccounts);
	        //EP_BankAccountTriggerHelper.doActiveSyncBankAcc(mOldAccounts,mNewAccounts);  
		}       
   }
    
    
    /*
        This method handles after insert update requests from Bank Account trigger
    */
    /*
   public static void doAfterInsertUpdate(List<EP_Bank_Account__c>lOldAccounts
                                          ,List<EP_Bank_Account__c>lNewAccounts
                                          ,map<Id,EP_Bank_Account__c>mOldAccounts
                                          ,map<Id,EP_Bank_Account__c>mNewAccounts){
        /**
        //SET TO TRUE TO AVOID RE-EXECUTION
        set<Id> sAccId = new set<Id>();
        //isExecuteAfterUpdate = True;
        if(mOldAccounts != null && !mOldAccounts.isEmpty()){
            for(Id accid : mNewAccounts.keySet()){
                if(mNewAccounts.get(accId).EP_Integration_Status__c == mOldAccounts.get(accId).EP_Integration_Status__c)    
                {
                    sAccId.add(accId);
                }
                
            }
        }
        else{
            sAccId.addAll(mNewAccounts.keySet());
        }
        
        EP_BankAccountTriggerHelper.bankAccountSyncSfdcNavFuture(sAccId);
        
   } **/
   
   /*
     This method handles after inster requests from Bank Account trigger
    */
   public static void doAfterInsert(List<EP_Bank_Account__c>lNewBankAccounts){
   		if(executeBankAccountTrigger){
	        if(!isExecutedByIntegrationUser){
	            EP_BankAccountTriggerHelper.populateNAVBankAccountCodeField(lNewBankAccounts) ;  
	        }
   		}
   }

   /*
   public static void doAfterUpdate(List<EP_Bank_Account__c>lNewBankAccounts
                                          ,List<EP_Bank_Account__c>lNewBankAccounts
                                          ,map<Id,EP_Bank_Account__c>mOldBankAccounts
                                          ,map<Id,EP_Bank_Account__c>mNewBankAccounts){
    isExecuteAfterUpdate = true;
    
  }*/
  
}