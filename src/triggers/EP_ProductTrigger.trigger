/* 
  @Author <Amit Singh>
  @name <EP_ProductTrigger>
  @CreateDate <09/06/2016>
  @Description <This is Product Trigger. This creates standard price records for all available currencies when new product inserts>
  @Version <1.0>
*/
trigger EP_ProductTrigger on Product2 (after insert) {
    if(trigger.isInsert){
        if(trigger.isAfter){
            //actions whcih needs to executes after insertion event-
            //1.Create standard price records for all available currencies
            EP_ProductTriggerHandler.doAfterInsert(trigger.New);
        }
    }
}