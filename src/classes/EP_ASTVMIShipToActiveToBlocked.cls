/*
*  @Author <Accenture>
*  @Name <EP_ASTVMIShipToActiveToBlocked>
*  @CreateDate <24/02/2017>
*  @Description <Handles VMI Ship To Account status change from 05-Active to 06-Blocked>
*  @NovaSuite Fix -- comments added
*  @Version <1.0>
*/
public class EP_ASTVMIShipToActiveToBlocked extends EP_AccountStateTransition {
    /**
    * @author <Accenture>
    * @name EP_ASTVMIShipToActiveToBlocked 
    */
    public EP_ASTVMIShipToActiveToBlocked() {
        finalState = EP_AccountConstant.BLOCKED;
    }
      /**
    * @author <Accenture>
    * @name isTransitionPossible
    * @Return Boolean
    */
    public override boolean isTransitionPossible(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToActiveToBlocked','isTransitionPossible');
        return super.isTransitionPossible();
    }
     /**
    * @author <Accenture>
    * @name isRegisteredForEvent
    * @Return Boolean
    */
    public override boolean isRegisteredForEvent(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToActiveToBlocked','isRegisteredForEvent');
        return super.isRegisteredForEvent();        
    }
      /**
    * @author <Accenture>
    * @name isGuardCondition 
    * @Return Boolean
    */
    public override boolean isGuardCondition(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToActiveToBlocked','isGuardCondition');
        //Code changes for L4 #45362 Start
        //Check if any inflight orders are exits for the ShipTo, Don't allow to Blocked the shipTo if inflight order exists
        EP_AccountService service = new EP_AccountService(this.account);
        if(service.hasInfligtOrders()) {
        	accountEvent.isError = true;
            accountEvent.setEventMessage(System.label.EP_Close_In_flight_Orders_Error_Msg);
            return false;
        }
        //Code changes for L4 #45362 End
        return true;
    }
      /**
    * @author <Accenture>
    * @name doOnExit
    */
    public override void doOnExit(){
        EP_GeneralUtility.Log('Public','EP_ASTVMIShipToActiveToBlocked','doOnExit');

    }
}