@isTest
public class EP_OrderItemMapper_UT
{
    static testMethod void doCSDelete_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper();
        Id OrderId = EP_TestDataUtility.getSalesOrder().Id;
        List<csord__Order_Line_Item__c> listRecordDelete = [SELECT Id,OrderId__c FROM csord__Order_Line_Item__c Where OrderId__c =: orderId];
        Test.startTest();
        localObj.doCSDelete(listRecordDelete);    
        Test.stopTest();
        List<csord__Order_Line_Item__c> listOrderItem = [SELECT Id,OrderId__c FROM csord__Order_Line_Item__c Where OrderId__c =: orderId];
        System.AssertEquals(0,listOrderItem.size());
    }
    static testMethod void getCSNoParentRecsByOrderIds_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper(); 
        Set<Id> setOrderId = new Set<Id>{EP_TestDataUtility.getSalesOrder().Id};
        Test.startTest();
        list<csord__Order_Line_Item__c> result = localObj.getCSNoParentRecsByOrderIds(setOrderId);    
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    static testMethod void getCSOrderById_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper(); 
        Set<Id> setOrderId = new Set<Id>{EP_TestDataUtility.getSalesOrder().Id};
        Test.startTest();
        list<csord__Order_Line_Item__c> result = localObj.getCSOrderById(setOrderId);    
        Test.stopTest();
        System.AssertEquals(1,result.size());
    }
    static testMethod void getCSInvoiceItemsByOrderNumber_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper(); 
        List<csord__Order__c> orderObjList = [SELECT Id,OrderNumber__c FROM csord__Order__c WHERE Id=:EP_TestDataUtility.getSalesOrderNegativeScenario().Id];
        if(orderObjList.size()>0){
            csord__Order__c orderObj = orderObjList[0];
            Set<String> setOrderNumber = new Set<String>{orderObj.OrderNumber__c};
            Test.startTest();
            list<csord__Order_Line_Item__c> result = localObj.getCSInvoiceItemsByOrderNumber(setOrderNumber); 
            Test.stopTest();
            System.Assert(result.size() == 0);
        }
    }
    static testMethod void getCsOrderLineWithInvoiceItem_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper(); 
        Set<Id> setOrderid = new Set<Id>{EP_TestDataUtility.getSalesOrder().Id};
        Test.startTest();
        list<csord__Order_Line_Item__c> result = localObj.getCsOrderLineWithInvoiceItem(setOrderid);  
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
    static testMethod void getCSOrderItemsByOrderNumber_test() {      
        EP_OrderItemMapper localObj = new EP_OrderItemMapper(); 
        List<csord__Order__c> orderObjList = [SELECT Id,OrderNumber__c FROM csord__Order__c WHERE Id=:EP_TestDataUtility.getSalesOrder().Id];
        if(orderObjList.size()>0){
            csord__Order__c orderObj = orderObjList[0];
            String orderNumber = orderObj.OrderNumber__c;
            Test.startTest();
            list<csord__Order_Line_Item__c> result = localObj.getCSOrderItemsByOrderNumber(orderNumber);  
            Test.stopTest();
            System.Assert(result.size() > 0);
        }
    }
    static testMethod void getCSOrderLineItemsforOrder_test() {
        EP_OrderItemMapper localObj = new EP_OrderItemMapper();
        Test.startTest();
        list<csord__Order_Line_Item__c> result = localObj.getCSOrderLineItemsforOrder(EP_TestDataUtility.getSalesOrder().Id); 
        Test.stopTest();
        System.Assert(result.size() > 0);
    }
}