/****************************************************************
* @author       Accenture                                       *
* @name         EP_LookUp_Context                               *
* @Created Date 26/12/2017                                      *
* @description  Class to declare variables                      *
****************************************************************/
public with sharing class EP_LookUp_Context {
    public List<Sobject> SobjectList{get;set;}
    public String objectVariable{get;set;}
    public String IdVariable{get;set;}
    public String targetId{get;set;}
    public string folderName;
    public EP_Look_Up__c queryData{get;set;}
    public String inputSearch{get;set;}
    public string inputHiddenId{get;set;}
    public string searchText{get;set;}
    public boolean clearSearchButton{get;set;}
    

/****************************************************************
* @author       Accenture                                       *
* @name         EP_LookUp_Context                               *
* @description  Contructor of class                             *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public EP_LookUp_Context(SObject objVariable) {
        init();
    }

/****************************************************************
* @author       Accenture                                       *
* @name         init                                            *
* @description  method to initialize variable                   *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    private void init(){
        SobjectList = new List<Sobject>();
        clearSearchButton = false;
    }
}