/* 
@Author      Accenture
@name        EP_DateTimeUtility
@CreateDate  29/06/2017
@Description Unit test class for EP_DateTimeUtility class
@Version     1.0
*/
@isTest
private class EP_DateTimeUtility_UT {

	@testSetup static void setupTestdata(){
		EP_CS_Communication_Settings__c dateFormat = new EP_CS_Communication_Settings__c();
		dateFormat.Value__c = 'yyyy-MM-dd';
		dateFormat.name = 'DateFormat';
		insert dateFormat;

		EP_CS_Communication_Settings__c datetimeFormat = new EP_CS_Communication_Settings__c();
		datetimeFormat.Value__c = 'yyyy-MM-dd\'T\'HH:mm:ss';
		datetimeFormat.name = 'DatetimeFormat';
		insert datetimeFormat;
	}
	
	@isTest static void formatDateAsString_testDateFormat() {
		Date newdate = Date.newInstance(2017, 12, 25);		
		string result = EP_DateTimeUtility.formatDateAsString(newDate);
		system.assertEquals('2017-12-25',result);
	}
	
	@isTest static void formatDateAsString_testDateTimeFormat() {
		Datetime newdatetime = Datetime.newInstanceGMT(2017, 12, 25,07,50,00);		
		string result = EP_DateTimeUtility.formatDateAsString(newdatetime);
		system.assertEquals('2017-12-25T07:50:00',result);
	}

	@isTest static void formatDateAsString_testStringFormat() {
		string newdatetimestr = '2017-12-25 07:50:00';		
		string result = EP_DateTimeUtility.formatDateAsString(newdatetimestr);
		system.assertEquals('2017-12-25T07:50:00',result);
	}
	
	@isTest static void convertStringToDate_test() {
		string newStrDate = '2017-12-25';
		Date newdate = Date.newInstance(2017, 12, 25);		
		Date result = EP_DateTimeUtility.convertStringToDate(newStrDate);
		system.assertEquals(newdate,result);
	}
}