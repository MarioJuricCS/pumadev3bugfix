<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>EP_Run_Name_Update</fullName>
        <description>used to update run name</description>
        <field>EP_Run_Name__c</field>
        <formula>EP_RunName_Sys__c</formula>
        <name>Run Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>EP_RunName_Update</fullName>
        <actions>
            <name>EP_Run_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( EP_RunName_Sys__c ) ||  ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
