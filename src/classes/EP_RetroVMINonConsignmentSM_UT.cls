@isTest
public class EP_RetroVMINonConsignmentSM_UT
{
	static testMethod void getOrderState_test() {
		List<EP_State_Transitions__c> lStateTran = Test.loadData(EP_State_Transitions__c.sObjectType, 'EP_State_Transitions_TestData');
    	List<EP_Order_State_Mapping__c>  lOrdStateMapping = Test.loadData(EP_Order_State_Mapping__c.sObjectType,'EP_Order_State_Mapping');
    	EP_RetroVMINonConsignmentSM localObj = new EP_RetroVMINonConsignmentSM();
    	EP_OrderDomainObject obj = EP_TestDataUtility.getVmiNonConsignmentOrderStateDraftDomainObjectPositiveScenario();
		localObj.setOrderDomainObject(obj);
		EP_OrderEvent currentEvent =new EP_OrderEvent(EP_OrderConstant.USER_SUBMIT);
		Test.startTest();
		EP_OrderState result = localObj.getOrderState(currentEvent);
		Test.stopTest();
		System.AssertNotEquals(null,result);
	}
}