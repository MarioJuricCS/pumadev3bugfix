/****************************************************************
* @author       Accenture                                       *
* @name         EP_Account_Notification_Settings_Ctrl           *
* @Created Date 21/12/2017                                      *
* @description  Class to show Notifications Data                *
****************************************************************/
public with sharing class EP_Account_Notification_Settings_Ctrl {
    private Account accObj;
    public EP_Account_Notification_Settings_Context ctx{get;set;}
    private EP_Account_Notification_Settings_Hlpr helperObj;

/****************************************************************
* @author       Accenture                                       *
* @name         EP_Account_Notification_Settings_Ctrl           *
* @description  Contructor                                      *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public EP_Account_Notification_Settings_Ctrl(ApexPages.StandardController controller) {
        try{
            accObj = (Account) controller.getRecord();
            ctx = new EP_Account_Notification_Settings_Context(accObj);
            helperObj = new EP_Account_Notification_Settings_Hlpr(ctx);
        }Catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage())); 
        }
    }

/****************************************************************
* @author       Accenture                                       *
* @name         getDefaultAccountNotificationSetting            *
* @description  method to get all credit customer               *
* @param        NA                                              *
* @return       void                                            *
****************************************************************/
    public pageReference Save(){
        EP_GeneralUtility.Log('Public','EP_Account_Notification_Settings_Ctrl ','Save');
        try{
            return(helperObj.save());
        }Catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage())); 
            
        }
        return null;
    }

}