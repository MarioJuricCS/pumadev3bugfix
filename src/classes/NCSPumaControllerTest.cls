@isTest
private class NCSPumaControllerTest {

	@testSetup
	private static void setupTestData() {

        cscfga__Product_Definition__c parent = new cscfga__Product_Definition__c();
        parent.Name = 'Parent Puma Energy Order Line';
        parent.cscfga__Description__c = 'Parent Test Product Definition';
        insert parent;

        cscfga__Product_Definition__c productDefinition = new cscfga__Product_Definition__c();
        productDefinition.Name = 'Puma Energy Order Line';
        productDefinition.cscfga__Description__c = 'Test Product Definition';
        insert productDefinition;
        
        cscfga__Product_Basket__c basketInsert = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId()
        );
        insert basketInsert;
        
        cscfga__Product_Configuration__c testProductConfiguration = new cscfga__Product_Configuration__c();
        testProductConfiguration.name = 'Test';
        testProductConfiguration.cscfga__Product_Basket__c = basketInsert.id;
        testProductConfiguration.cscfga__Product_Definition__c = productDefinition.id;
        insert testProductConfiguration;
        
	}

	private static void setTestPage() {

        cscfga__Product_Definition__c defParent = [SELECT Id, Name FROM cscfga__Product_Definition__c WHERE Name = 'Parent Puma Energy Order Line' LIMIT 1];
        cscfga__Product_Definition__c defMain = [SELECT Id, Name FROM cscfga__Product_Definition__c WHERE Name = 'Puma Energy Order Line' LIMIT 1];
        cscfga__Product_Configuration__c config = [SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE Name = 'Test' LIMIT 1];
        
		NCSPumaController ctrl = new NCSPumaController();

        // productDefinitionId = 'schemaId'
		// parentDefinitionId = 'defId'
		
        Test.setCurrentPageReference(new PageReference('NCSPuma.component'));
        System.currentPageReference().getParameters().put('configId', config.Id);
        System.currentPageReference().getParameters().put('schemaId', defMain.Id);
        System.currentPageReference().getParameters().put('defId', defParent.Id);
        System.currentPageReference().getParameters().put('defName', defMain.Name);
	}


	@IsTest
	public static void configurationObject() {

        NCSPumaControllerTest.setTestPage();
        
        cscfga__Product_Configuration__c config = [SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE Name = 'Test' LIMIT 1];

		test.startTest();

        NCSPumaController ctrl = new NCSPumaController();

		test.stopTest();

		System.assertNotEquals(null, ctrl.configurationObject, 'Invalid data');
	}

	@IsTest
	public static void configId() {

        NCSPumaControllerTest.setTestPage();
        
        cscfga__Product_Configuration__c config = [SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE Name = 'Test' LIMIT 1];

		test.startTest();

        NCSPumaController ctrl = new NCSPumaController();

		test.stopTest();

		System.assertEquals(ctrl.configId, config.Id, 'Invalid data');
	}

	@IsTest
	public static void definitionName() {

        NCSPumaControllerTest.setTestPage();
        
        cscfga__Product_Definition__c defMain = [SELECT Id, Name FROM cscfga__Product_Definition__c WHERE Name = 'Puma Energy Order Line' LIMIT 1];

		test.startTest();

        NCSPumaController ctrl = new NCSPumaController();

		test.stopTest();

		System.assertEquals(ctrl.definitionName, defMain.Name, 'Invalid data');
	}

	@IsTest
	public static void definitionId() {

        NCSPumaControllerTest.setTestPage();
        
        cscfga__Product_Definition__c defMain = [SELECT Id, Name FROM cscfga__Product_Definition__c WHERE Name = 'Puma Energy Order Line' LIMIT 1];

		test.startTest();

        NCSPumaController ctrl = new NCSPumaController();

		test.stopTest();

		System.assertEquals(ctrl.definitionId, defMain.Id, 'Invalid data');
	}

	@IsTest
	public static void schema() {

        NCSPumaControllerTest.setTestPage();

		test.startTest();

        NCSPumaController ctrl = new NCSPumaController();

		test.stopTest();

		System.assertNotEquals(null, ctrl.schema, 'Invalid data');
	}

	@IsTest
	public static void schemaId() {

        NCSPumaControllerTest.setTestPage();
        
		test.startTest();

        NCSPumaController ctrl = new NCSPumaController();

		test.stopTest();

		System.assertEquals(ctrl.schemaId, '', 'Invalid data');
	}

	@IsTest
	public static void renderType() {

        NCSPumaControllerTest.setTestPage();
        
		test.startTest();

        NCSPumaController ctrl = new NCSPumaController();

		test.stopTest();

		System.assertEquals(ctrl.renderType, '', 'Invalid data');
	}

	@IsTest
	private static void getJSONAttachment() {

		test.startTest();
		
		NCSPumaController ctrl = new NCSPumaController();
		
		ctrl.getJSONAttachment();

		test.stopTest();
	}

	@IsTest
	private static void getJsonConfigurationField() {

		test.startTest();
		
		NCSPumaController ctrl = new NCSPumaController();
		
		ctrl.getJsonConfigurationField(null, null);

		test.stopTest();
	}
	
	@IsTest
	private static void getMLEData() {

        NCSPumaControllerTest.setTestPage();

		test.startTest();
		
		NCSPumaController ctrl = new NCSPumaController();

		Map<String, String> mleMap = NCSPumaController.getMLEData(ctrl.definitionName, ctrl.productDefinitionId, ctrl.definitionId, ctrl.configId);

		test.stopTest();
		
		System.assertNotEquals(null, mleMap, 'Invalid data');
	}
	
	@IsTest
	private static void saveJSONData() {
	    
        NCSPumaControllerTest.setTestPage();

        cscfga__Product_Configuration__c config = [SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE Name = 'Test' LIMIT 1];

		test.startTest();
		
		NCSPumaController ctrl = new NCSPumaController();

		String retVal = NCSPumaController.saveJSONData('{}', config.Id, 'Test');

		test.stopTest();
		
		System.assertNotEquals(null, retVal, 'Invalid data');
	}
}