/* 
  @Author <Ram Rai>
   @name <EP_FE_CancelOrderInfoEndpoint>
   @CreateDate <20/05/2016>
   @Description <This class Cancel Order >  
   @Version <1.0>
*/
@RestResource(urlMapping = '/FE/V1/order/cancel/*')
global with sharing class EP_FE_CancelOrderInfoEndpoint {

    // Parameters keys
    global final static String PARAM_KEY_ORDER_ID = 'orderId';
    global final static String PARAM_KEY_REASON = 'reason';
    global final static String PARAM_KEY_REASONDETAIL = 'reasonDetail';

    global final static String OTHER = 'Other';
    global static final String CANCEL_ORDER= 'Order has been canceled at ';
    
    global static final String PARAM_KEY_ACTION = 'action';
    
    global static final String strCancel = 'c';
    global static final String strdelete = 'd';
    
   /** API Call */
    @HttpPost
    webservice static EP_FE_CancelOrderInfoResponse cancelOrder() {
        EP_FE_CancelOrderInfoResponse response = new EP_FE_CancelOrderInfoResponse();
        System.debug('Test Cancel');
        // Get the parameters
         
        Map<String, Object> params = (Map<String, Object>) JSON.deserializeUntyped(RestContext.request.requestBody.toString());
           
        String orderIdStr = params.containsKey(PARAM_KEY_ORDER_ID) 
            ? String.valueOf(params.get(PARAM_KEY_ORDER_ID)) : null;
        String reasonStr = params.containsKey(PARAM_KEY_REASON) 
            ? String.valueOf(params.get(PARAM_KEY_REASON)) : null;
        String reasonDetailStr = params.containsKey(PARAM_KEY_REASONDETAIL) 
            ? String.valueOf(params.get(PARAM_KEY_REASONDETAIL)) : null;
        String actionStr = params.containsKey(PARAM_KEY_ACTION) 
            ? String.valueOf(params.get(PARAM_KEY_ACTION)) : null;
        System.debug('Test Cancel11'+params);
        // Check Parameter
        //if(string.isNotBlank(orderIdStr) && (string.isNotBlank(reasonStr) || (reasonStr == OTHER && string.isNotBlank(reasonDetailStr))) && (string.isBlank(actionStr) || actionStr.equals(strCancel))) {
        if(string.isNotBlank(orderIdStr) && (string.isBlank(actionStr) || actionStr.equals(strCancel))) {
        
            /*if (reasonStr == OTHER && string.isBlank(reasonDetailStr)) {
                
                response.status = EP_FE_CancelOrderInfoResponse.ERROR_MISSING_REASONDETAIL;
                return response;
            }*/

            System.debug('Cancel Ord');
            cancelOrder(response, Id.valueOf(orderIdStr), EP_FE_Utils.getInputValue(reasonStr), EP_FE_Utils.getInputValue(reasonDetailStr));

        } else if(string.isNotBlank(orderIdStr) && (string.isNotBlank(actionStr) && actionStr.equals(strdelete))){
            deleteOrder(response, Id.valueOf(orderIdStr)); 
        } else {
            response.status = EP_FE_CancelOrderInfoResponse.ERROR_MISSING_ORDERID; 
        }

        return response; 
    }    

   /*********************************************************************************************
    *@Description : This is the method to return Cancel Order.                  
    *@Params      : Response, OrderId                 
    *@Return      : Void                                                                             
    *********************************************************************************************/ 
    private static void cancelOrder(EP_FE_CancelOrderInfoResponse response, Id orderId, String reason, String reasonDetail) {
    System.debug('response:'+response+'orderId:'+orderId+'reason:'+reason+'reasonDetail:'+reasonDetail);
        // Get the order
        csord__Order__c order = [SELECT csord__Status2__c, CreatedById,EP_Delivery_Type__c, EP_Order_Delivery_Type__c,EP_Requested_Delivery_Date__c,
            Stock_Holding_Location__r.Stock_Holding_Location__r.EP_Scheduling_Hours__c, Type__c, EP_Is_Pre_Pay__c,
            Stock_Holding_Location__r.EP_Trip_Duration__c, EP_Is_Portal_Order_Active__c, EP_Sell_To__c, EP_Bill_To__c,
            EP_Order_Locked__c, Cancellation_Check_Done__c, EP_Order_Epoch__c, csord__Account__r.EP_Status__c,
            EP_ShipTo__r.RecordTypeName__c                     
            FROM csord__Order__c where id = :orderId LIMIT 1 ];
system.debug('==EP_FE_OrderUtils.evaluateCancellableEditableOrder(order)=='+ EP_FE_OrderUtils.evaluateCancellableEditableOrder(order));
        // check if it's cancellable 
       if(EP_FE_OrderUtils.evaluateCancellableEditableOrder(order) /*&& order.createdById == userinfo.getUserId()*/) {
            try {
               /* order.Status = EP_Common_Constant.ORDER_CANCELLED_STATUS;
                order.EP_Order_Customer_Status__c =  EP_Common_Constant.ORDER_CANCELLED_STATUS;
                order.EP_Order_Status_Caption__c = CANCEL_ORDER + DateTime.now();
                order.EP_Reason_For_Change__c = reason;
                if (reasonDetail != null) {
    
                    order.EP_Other_Reason__c = reasonDetail;
                }
                Database.upsert(order);  
                response.status = EP_FE_Response.STATUS_SUCCESS;  
                response.order = order;*/
                
                response.responseCancelOrder=EP_PortalCancelOrderPageController.cancelOrder(orderId);
                System.debug('--------------'+response.responseCancelOrder);
                //EP_FE_Utils.raiseExceptionIfTestIsRunning();
            } catch (exception ex) { 
            System.debug('--------------'+ex);                     
                response.status = EP_FE_CancelOrderInfoResponse.ERROR_UPDATE_FAILED;
                response.addError(ex.getMessage());
            }
        } else {
            response.status = EP_FE_CancelOrderInfoResponse.ERROR_NOT_CANCELLABLE_ORDER;
        }
    }

/*********************************************************************************************
    *@Description : This is the method to return Cancel Order.                  
    *@Params      : Response, OrderId                 
    *@Return      : Void                                                                             
    *********************************************************************************************/ 
    private static void deleteOrder(EP_FE_CancelOrderInfoResponse response, Id orderId) {
        // Get the order
       /* Order order = [SELECT Status, CreatedById, EP_Order_Delivery_Type__c,EP_Requested_Delivery_Date__c,EP_Customer_Reference_Number__c,
            Stock_Holding_Location__r.Stock_Holding_Location__r.EP_Scheduling_Hours__c, Type, EP_Is_Pre_Pay__c,
            Stock_Holding_Location__r.EP_Trip_Duration__c, EP_Is_Portal_Order_Active__c, EP_Sell_To__c, EP_Bill_To__c
            FROM Order where id = :orderId LIMIT 1 ];

        if(order != null && order.Status == EP_Common_Constant.ORDER_DRAFT_STATUS) {
            system.debug('>>>>> STATUS BEFORE DELETE :' + order.Status);
            try {
                order.Status = EP_FE_Constants.ORDER_DELETED_STATUS;
                system.debug('>>>>> STATUS AFTER DELETE :' + order.Status);
                Database.upsert(order);  
                response.status = EP_FE_Response.STATUS_SUCCESS;  
                response.order = order;

                //EP_FE_Utils.raiseExceptionIfTestIsRunning();
            } catch (exception ex) {                      
                response.status = EP_FE_CancelOrderInfoResponse.ERROR_UPDATE_FAILED;
                system.debug('>>>>> STATUS IN CATCH :' + response.Status);
                response.addError(ex.getMessage());
            }
        } else {
            response.status = EP_FE_CancelOrderInfoResponse.ERROR_ORDER_NOT_AVAILABLE_FOR_DELETING;
        }*/
    }
}