/* 
   @Author Spiros Markantonatos
   @name <EP_MonitorExportLibClass>
   @CreateDate <04/11/2016>
   @Description <This class is used by the APIs returning log records to the ePuma monitoring system to deliver common methods>
   @Version <1.0>
 
*/
public with sharing class EP_MonitorExportLibClass {
    // Const
    private static final Integer QUERY_LIMIT = 1000;
    private static final Integer MAX_QUERY_LIMIT = 5000;
    private static final String RETURN_PAY_LOAD = 'returnPayloadBody';
    private static final String CLASS_NAME = 'EP_MonitorExportLibClass';
    
    // Methods
    /*
    Returns payload body
    */
    public static String returnPayloadBody(List<sObject> lRecords, String strSuccessHeader, String strFailHeader) {
        String strResponse = EP_Common_Constant.BLANK;
        
        if (!lRecords.isEmpty()) {
            JSONGenerator gen = JSON.createGenerator(TRUE);
            // Convert list of records into JSON format
            try {
                strResponse = JSON.serialize(lRecords);
            } catch (exception ex) {
                EP_LoggingService.logServiceException(ex,UserInfo.getOrganizationId(),EP_Common_Constant.EPUMA,RETURN_PAY_LOAD,CLASS_NAME,EP_Common_Constant.ERROR,UserInfo.getUserId(),EP_Common_Constant.TARGET_SF,EP_Common_Constant.BLANK,EP_Common_Constant.BLANK);
            }
            
            // Add data to the response string
            strResponse = strSuccessHeader + strResponse + EP_Common_Constant.COMMA_STRING;
            strResponse += EP_Common_Constant.RESULTS + lRecords.size() + EP_Common_Constant.RIGHT_CURLY_BRACE;
        } else {
            // Add 'No Exception log data' to the response string
            strResponse = strFailHeader + EP_Common_Constant.COMMA_STRING;
            strResponse += EP_Common_Constant.RESULTS + lRecords.size() + EP_Common_Constant.RIGHT_CURLY_BRACE;
        }
        
        return strResponse;
    }
    
    /*
    Return the limit to be used in the query
    */
    public static Integer returnQueryLimitFromRequest(String strParamNam) {
        
        Integer intLimit = QUERY_LIMIT;
        
        String strLimit = returParameterFromRequest(strParamNam);
        
        if (String.isNotBlank(strLimit))
        {
            if (strLimit.isNumeric())
            {
                intLimit = Integer.valueOf(strLimit);
                
                if (intLimit > MAX_QUERY_LIMIT)
                {
                    intLimit = MAX_QUERY_LIMIT; 
                }
            }
        }
        return intLimit;
    }
    
    /*
    Return the date filter to be used in the query
    */
    public static Date returnQueryDateFilterFromRequest(String strParamNam) {
        
        Date dSearchDate = NULL;
        
        String strDate = returParameterFromRequest(strParamNam);
        
        if (String.isNotBlank(strDate))
        {
            String[] lDates = strDate.split(EP_Common_Constant.hyphen);
            
            // Convert date format to YYYYMMDD (Expecting date format from URL should be MMDDYYYY e.g. 11-19-2015)
            if (lDates.size() == 3)
            {
                try {
                    Integer intYear = Integer.valueOf(lDates[2]);
                    Integer intMonth = Integer.valueOf(lDates[0]);
                    Integer intDay = Integer.valueOf(lDates[1]);
                    if (intMonth > 0 && intMonth <= 12 && intDay > 0 && intDay < 32)
                        dSearchDate = Date.newInstance(
                                                    intYear,
                                                        intMonth,
                                                            intDay);
                } catch (exception ex) {
                    dSearchDate = NULL;
                }
            }
        }
        return dSearchDate;
    }
    
    /*
    Return the param value from the REST context
    */
    private static String returParameterFromRequest(String strParamName) {
        String strParam = NULL;
        
        if (RestContext.request.params.containsKey(strParamName))
        {
            strParam = RestContext.request.params.get(strParamName);
        }
        
        return strParam;
    }
    
}