@isTest
public class TrialCustomerPortalHomePController_Test {
	public static testMethod void SortTest(){
        
        EP_Country__c country = new EP_Country__c(EP_Country_Code__c = '101', EP_Region__c = 'India');
        insert country;
        Account acc = new Account(name='TESTREC', EP_Country__c = country.Id, BillingStreet='street', BillingCity='city', BillingState='state', BillingPostalCode='123',
        BillingCountry='in',
        ShippingStreet='street', ShippingCity='city', ShippingState='state', ShippingPostalCode='123',
        ShippingCountry='in');
        insert acc; 
        
        Asset newAsset = new Asset(
        	CurrencyIsoCode = 'GBP',
            Name = 'Test Asset',
            AccountId = acc.Id
        );
        insert newAsset;
        
        Case newCase = new Case(
        	Status = 'New Application',
            Origin = 'Email',
            CurrencyIsoCode = 'GBP'
        );
        insert newCase;
        
        Solution newSolution = new Solution(
        	CurrencyIsoCode = 'GBP',
            IsPublished = false,
            SolutionNote = 'Test Solution',
            SolutionName = 'Test Solution',
            Status = 'Draft'
        );
        insert newSolution;
        list<Community> communityList = [Select c.Name, c.IsActive, c.Id From Community c Limit 1];
        
        if( !communityList.isEmpty() )
        {
            Idea newIdea = new Idea(
                Body = 'Test Idea',
                CurrencyIsoCode = 'GBP',
                Status = 'Test',
                Title = 'Test Idea',
                CommunityId = communityList[0].Id
            );
            insert newIdea;
        }
    	TrialCustomerPortalHomePageController TCPHPC = new TrialCustomerPortalHomePageController();
        TCPHPC.sortField1 = 'Name';
        TCPHPC.PrevioussortField1 = 'Name';
        TCPHPC.SortProducts();
        TCPHPC.sortField2 = 'CaseNumber';
        TCPHPC.PrevioussortField2 = 'CaseNumber';
        TCPHPC.SortCases();
        TCPHPC.sortField3 = 'SolutionName';
        TCPHPC.PrevioussortField3 = 'SolutionName';
        TCPHPC.SortSolutions();
       
        if( !communityList.isEmpty() )
        {
            TCPHPC.sortField4 = 'Title';
        	TCPHPC.PrevioussortField4 = 'Title';
        	TCPHPC.SortIdeas();
        }
		
    
    }
}