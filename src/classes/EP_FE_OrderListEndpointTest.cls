/*   
@Author <Accenture>
@name <EP_FE_OrderListEndpointTest.cls>     
@Description <This class is used as parent response controller class>   
@Version <1.1> 
*/
@isTest
public class EP_FE_OrderListEndpointTest {
    
    private static csord__Order__c od;
    private static User usrd;
    public static Contact cntct;
    
    @testSetup public static void setUp(){
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        UserRole ur = new UserRole(Name = 'CEO');
        insert ur;
        User tuser = new User(  firstname = 'Manisha',
                              lastName = 'chaudhary',
                              email = 'test@gmail.com',
                              Username = 'mChaudhary@gmail.com',
                              EmailEncodingKey = 'ISO-8859-1',
                              Alias = uniqueName.substring(18, 23),
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              ProfileId = pf.id,
                              UserRoleId = ur.id
                             );
        insert tuser;
        System.runAs(tuser){
            Id RecordTypeIdSellToAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Sell To').getRecordTypeId();        
            Id RecordTypeIdShipToAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non-VMI Ship To').getRecordTypeId();
            
            Account acc=new Account();
            acc.name='test sell to';
            acc.BillingPostalCode = '122002';
            acc.BillingCountry = 'India';
            acc.BillingState = 'Haryana';
            acc.BillingCity = 'Gurugram';
            acc.BillingStreet = '1st floor Building No. 8, Infinity Tower, DLF Cyber City, DLF Phase 2, Sector 24';
            acc.recordtypeid = RecordTypeIdSellToAccount; 
            insert acc;
            
            Account accShip = new Account ();
            accShip.name = 'ShipTo Account';
            accShip.recordtypeid = RecordTypeIdShipToAccount;
            accShip.ShippingCity = 'shipCity';
            accShip.ParentId = acc.id;
            
            Insert accShip;
            
            Contact c =new Contact(FirstName='Manisha',LastName='Chaudhary',AccountID=acc.Id);
            insert c;
            
            EP_FE_PortalUserAccountAccess__c a = new EP_FE_PortalUserAccountAccess__c();
            a.EP_FE_Customer_Name__c = c.Id;
            a.EP_FE_Sellto_Name__c = acc.Id;
            a.EP_FE_Shipto_Name__c = accShip.Id;
            Insert a;
        }
        contact PortaCon = [Select id from Contact limit 1];
        Profile pf1 = [SELECT Id FROM Profile WHERE name LIKE '%Portal User%' limit 1];
        
        User PUser = new User(  firstname = 'Manisha1',
                              lastName = 'chaudhary1',
                              email = 'test@gmail1.com',
                              Username = 'mChaudhary@gmail1.com',
                              EmailEncodingKey = 'ISO-8859-1',
                              Alias = uniqueName.substring(18, 23),
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              ProfileId = pf1.id,
                              contactid=PortaCon.Id);
        insert PUser;
        
        Account acc1 = [Select id from Account where name = 'test sell to' limit 1];
        
    }
    
    /*********************************************************************************************
*@Description : This is the Test method Create Order.                  
*@Params      :                    
*@Return      : Void                                                                             
*********************************************************************************************/
    @testSetup static void testCreateOrder(){
        
        od = EP_FE_TestDataUtility.createNonVMIOrder();    
    }       
    /*********************************************************************************************
*@Description : This is the Test method to return Order of Past Type.                  
*@Params      :                    
*@Return      : Void                                                                             
*********************************************************************************************/   
    static testMethod void testFetchOrderPast() {
        Profile pf1 = [SELECT Id FROM Profile WHERE name LIKE '%Portal User%' limit 1];
        User rUser=[SELECT ID FROM USER WHERE ProfileId =:pf1.Id];
        String selectedSellToId;
        Set<id> sellToIds = new Set<id>();
        Map<String,String> orderStatusLangTransMap = new Map<String,String>();
        sellToIds.add(selectedSellToId);
        orderStatusLangTransMap.put('Delivered','Entregado');
        
  Account acc1 = [Select id from Account where name = 'test sell to' limit 1];
         system.runas(rUser){ 
                
            
        csord__Order__c odd = [select  id, AccountId__c, EP_Sell_To__c, EP_Bill_To__c,EP_Is_Portal_Order_Active__c, EP_ShipTo__c, EP_Is_Pre_Pay__c,csord__Account__c,  
                               EP_Order_Status_Caption__c,RecordType.DeveloperName, EP_Expected_Delivery_Date__c,OrderNumber__c, EffectiveDate__c, csord__Status2__c,
                               EP_Delivery_Type__c,Stock_Holding_Location__c from csord__Order__c limit 1];
        EP_FE_OrderUtils.evaluatePriceVisibility(odd);
        EP_FE_Utils.returnTerminalInfo();
        Test.startTest();
        RestRequest req = new RestRequest();   
        req.httpMethod = 'Get';  
        req.requestURI = '/services/apexrest/FE/V1/order/list/';  
        RestContext.request = req;
        
        req.params.put('offset', '0');
        req.params.put('limit', '10');          
        req.params.put('type', 'past');
        req.params.put('selectedSellToId',acc1.Id);
            
            //EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
            EP_FE_OrderListResponse response = new EP_FE_OrderListResponse();
          EP_FE_OrderListEndpoint.getAccountSummaryQuery(2,2,true,sellToIds);
            EP_FE_OrderListEndpoint.getQueryFields(true);
            EP_FE_OrderListEndpoint.getQueryFields(false);
            EP_FE_OrderListEndpoint.getAlertQueryBody(sellToIds);
            EP_FE_OrderListEndpoint.fetchOrders(response,'Test',2,2,sellToIds);
            EP_FE_OrderListEndpoint.currentUserCanSeePrices(response,'Sales',true,2,2,sellToIds);
            EP_FE_OrderListEndpoint.orderStatusLangConverter('Test1','Test2',orderStatusLangTransMap);
            // System.assertEquals(odd.id,results.orderList[0].id);
            Test.stopTest();
        }
    }   
    /*********************************************************************************************
*@Description : This is the Test mehod to return Order of Past Type.                  
*@Params      :                    
*@Return      : Void                                                                             
*********************************************************************************************/   
    
    static testMethod void testFetchOrderActive() {
        Profile pf1 = [SELECT Id FROM Profile WHERE name LIKE '%Portal User%' limit 1];
        User PUser=[SELECT ID FROM USER WHERE ProfileId =:pf1.Id];
        
        
        Account acc1 = [Select id from Account where name = 'test sell to' limit 1];
        csord__Order__c odd = [select id from csord__Order__c limit 1];   
        
        Test.startTest();
        
        RestRequest req = new RestRequest();   
        req.httpMethod = 'Get';  
        req.requestURI = '/services/apexrest/FE/V1/order/list/';  
        RestContext.request = req;
        
        req.params.put('offset', '0');
        req.params.put('limit', '10');          
        req.params.put('type', 'active');
        req.params.put('selectedSellToId',acc1.Id);
        
        System.runAs(PUser){
            EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
            
            Test.stopTest();
            System.assertEquals(results.status ,-4);
        }
        
        
    }    
    
    /*********************************************************************************************
*@Description : This is the Test mehod to return Order of Active Type.                  
*@Params      :                    
*@Return      : Void                                                                             
*********************************************************************************************/    
    static testMethod void testFetechOrderActiveQuerLimit() { 
        
        Profile pf1 = [SELECT Id FROM Profile WHERE name LIKE '%Portal User%' limit 1];
        User PUser=[SELECT ID FROM USER WHERE ProfileId =:pf1.Id];
        system.runas(PUser){
            csord__Order__c odd = [select id from csord__Order__c limit 1];
            Account acc1 = [Select id from Account where name = 'test sell to' limit 1];
            Test.startTest();
            
            RestRequest req = new RestRequest();   
            req.httpMethod = 'Get';  
            req.requestURI = '/services/apexrest/FE/V1/order/list/';  
            RestContext.request = req;
            
            req.params.put('offset', '0');
            req.params.put('limit', '100000');          
            req.params.put('type', 'active');
            req.params.put('selectedSellToId',acc1.Id);
            
            
            EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList(); 
            Test.stopTest();
            System.assertEquals(results.status , -2);
            
        }
        
    }
    
    /*********************************************************************************************
*@Description : This is the Test mehod to return Order of Active Type.                  
*@Params      :                    
*@Return      : Void                                                                             
*********************************************************************************************/    
    static testMethod void testFetechOrderActiveNegative() { 
        
        Profile pf1 = [SELECT Id FROM Profile WHERE name LIKE '%Portal User%' limit 1];
        User PUser=[SELECT ID FROM USER WHERE ProfileId =:pf1.Id];
        system.runas(PUser){
            csord__Order__c odd = [select id from csord__Order__c limit 1];
            Account acc1 = [Select id from Account where name = 'test sell to' limit 1];
            Test.startTest();
            
            RestRequest req = new RestRequest();   
            req.httpMethod = 'Get';  
            req.requestURI = '/services/apexrest/FE/V1/order/list/';  
            RestContext.request = req;
            
            req.params.put('offset', '-1');
            req.params.put('limit', '100');          
            req.params.put('type', 'active');
            req.params.put('selectedSellToId',acc1.Id);
            
            EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
            Test.stopTest();
            System.assertEquals(results.status , -1);
            
        }       
    }     
    
    /*********************************************************************************************
*@Description : This is the Test mehod to return Order of Active Type.                  
*@Params      :                    
*@Return      : Void                                                                             
*********************************************************************************************/    
    static testMethod void testFetechOrderNotActive() { 
        
        Profile pf1 = [SELECT Id FROM Profile WHERE name LIKE '%Portal User%' limit 1];
        User PUser=[SELECT ID FROM USER WHERE ProfileId =:pf1.Id];
        system.runas(PUser){ 
            csord__Order__c odd = [select id from csord__Order__c limit 1];
            Account acc1 = [Select id from Account where name = 'test sell to' limit 1];      
            Test.startTest();
            
            RestRequest req = new RestRequest();   
            req.httpMethod = 'Get';  
            req.requestURI = '/services/apexrest/FE/V1/order/list/';  
            RestContext.request = req;
            
            req.params.put('offset', '1');
            req.params.put('limit', '100');          
            req.params.put('type', 'notactive');
            req.params.put('selectedSellToId',acc1.Id);
            
            EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
            Test.stopTest();
            System.assertEquals(results.status , -3);
            
        }
    } 
 /*********************************************************************************************
*@Description : This is the Test mehod to return Order of Active Type.                  
*@Params      :                    
*@Return      : Void                                                                             
*********************************************************************************************/   
    static testMethod void testFetchAccountSummary() { 
    Profile pf1 = [SELECT Id FROM Profile WHERE name LIKE '%Portal User%' limit 1];
        User PUser=[SELECT ID FROM USER WHERE ProfileId =:pf1.Id];
        Account shpTo = [Select id,ParentId from Account where name = 'ShipTo Account' limit 1];
         csord__Order__c csOrder = new csord__Order__c(Name='Test Order',csord__Identification__c='XXXXX',csord__Delivered_Date__c=Date.today(),EP_Payment_Term__c='PrePayment',
            EP_Order_Epoch__c='Current',EP_Loading_Date__c = Date.today(),
            EP_Sell_To__c= shpTo.ParentId ,EP_ShipTo__c = shpTo.Id,EP_Delivery_Type__c='Ex-Rack',
            EP_Expected_Delivery_Date__c = Date.today(),Status__c='Draft');


        insert csOrder;
        system.runas(PUser){ 
             
           


            csord__Order__c odd = [select id from csord__Order__c WHERE ID=:csOrder.id limit 1];
            Account acc1 = [Select id from Account where name = 'test sell to' limit 1];
            Test.startTest();
            
            RestRequest req = new RestRequest();   
            req.httpMethod = 'Get';  
            req.requestURI = '/services/apexrest/FE/V1/order/list/';  
            RestContext.request = req;
            
            req.params.put('offset', '1');
            req.params.put('limit', '100');          
            req.params.put('type', 'AccountSummary');
            req.params.put('selectedSellToId',acc1.Id);
            
            EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
            Test.stopTest();
            System.assertEquals(results.status ,-3); 
            
        }       
    }
    
    /*********************************************************************************************
*@Description : This is the Test mehod to return Order of Active Type.                  
*@Params      :                    
*@Return      : Void                                                                             
*********************************************************************************************/    
    static testMethod void testFetechOrderAlert() { 
        
        Profile pf1 = [SELECT Id FROM Profile WHERE name LIKE '%Portal User%' limit 1];
        User PUser=[SELECT ID FROM USER WHERE ProfileId =:pf1.Id];
        system.runas(PUser){ 
            csord__Order__c odd = [select id from csord__Order__c limit 1];
            Account acc1 = [Select id from Account where name = 'test sell to' limit 1];
            Test.startTest();
            
            RestRequest req = new RestRequest();   
            req.httpMethod = 'Get';  
            req.requestURI = '/services/apexrest/FE/V1/order/list/';  
            RestContext.request = req;
            
            req.params.put('offset', '1');
            req.params.put('limit', '100');          
            req.params.put('type', 'alert');
            req.params.put('selectedSellToId',acc1.Id);
            
            EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
            Test.stopTest();
            System.assertEquals(results.status ,-4); 
            
        }       
    } 
    /*********************************************************************************************
*@Description : This is the Test mehod to return Order of blanket Type.                  
*@Params      :                    
*@Return      : Void                                                                             
*********************************************************************************************/    
    static testMethod void testFetechOrderblanket() { 
        
        Profile pf1 = [SELECT Id FROM Profile WHERE name LIKE '%Portal User%' limit 1];
        User PUser=[SELECT ID FROM USER WHERE ProfileId =:pf1.Id];
       
        system.runas(PUser){
         Account acc1 = [Select id from Account where name = 'test sell to' limit 1];
            csord__Order__c odd = [select id from csord__Order__c limit 1];
            
            Test.startTest();
            
            RestRequest req = new RestRequest();   
            req.httpMethod = 'Get';  
            req.requestURI = '/services/apexrest/FE/V1/order/list/';  
            RestContext.request = req;
            
            req.params.put('offset', '1');
            req.params.put('limit', '100');          
            req.params.put('type', 'blanket');
            req.params.put('selectedSellToId',acc1.Id);
            
            EP_FE_OrderListResponse results = EP_FE_OrderListEndpoint.getOrderList();
            
            Test.stopTest();
            System.assertEquals(results.status , 0);   
        }
    }    
    
}