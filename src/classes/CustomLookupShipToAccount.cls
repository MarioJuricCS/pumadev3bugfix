global  without sharing class CustomLookupShipToAccount extends cscfga.ALookupSearch {

  public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
        String accountId = searchFields.get('AccountId');
        String pType = searchFields.get('Product Type');
        String accName = searchFields.get('searchValue');
		String userId = UserInfo.getUserId();
        String epoch = searchFields.get('Order Epoch');

        if(accName != null) {
            accName = '%'+ accName +'%';
        }
        List <Account> accList = new List<Account>();
        List<String> nonBulk = new List<String> {'Non-VMI Ship To','VMI Ship To'};
        List<Profile> ps = [SELECT Id, Name FROM Profile WHERE Name = 'EP_Portal_Basic_Ordering_Access_User'];
		String PBOAUserId = ps.get(0).Id;
		List<User> us = [SELECT Id, Name, ProfileId, Contact.Id FROM User WHERE Id =: userID];
		String userProfileId = us.get(0).ProfileId;
        List<Contact> cs = [SELECT Id, Name, AccountId FROM Contact WHERE Id =: us.get(0).Contact.Id];
      
        if(pType!=null) {

            if(pType.equalsIgnoreCase('Bulk') && epoch.equalsIgnoreCase('Current')) {

                if(accName != null) { 
                    accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId and recordtype.name ='Non-VMI Ship To' and EP_Status__c = '05-Active' and name like :accName];
                }
                else  {
                    accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId and recordtype.name ='Non-VMI Ship To' and EP_Status__c = '05-Active' ];
                }

            }

            else if(pType.equalsIgnoreCase('Bulk') && epoch.equalsIgnoreCase('Retrospective')) {
                if(accName != null) { 
                    accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId  and EP_Status__c = '05-Active' and name like :accName];
                }
                else  {
                    accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId  and EP_Status__c = '05-Active' ];
                }
            }

            else {
                if(accName != null) { 
                    accList= [SELECT id,name,RouteAllocationCount__c,ParentId,EP_Puma_Company__c,AccountNumber,ShippingPostalCode,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress 
                              FROM Account 
                              WHERE parentid =: accountId and recordtype.name in:nonBulk and EP_Status__c = '05-Active' and EP_Ship_To_Type__c != 'Consignment' and name like :accName];
                }
                else {
                    accList= [SELECT id,name,RouteAllocationCount__c,ParentId,EP_Puma_Company__c,AccountNumber,ShippingPostalCode,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c,ShippingStreet,ShippingAddress
                              FROM Account 
                              WHERE parentid =: accountId and recordtype.name in:nonBulk and EP_Status__c = '05-Active' and EP_Ship_To_Type__c != 'Consignment'];
                }

            }
        }
      
        if(PBOAUserId == userProfileId){
		    for(Integer i = 0; i < accList.size();i++)
              if(accList[i].Id != cs.get(0).AccountId){
                  accList.remove(i);
              } 
        }
      
        if(accList == null && !accList.isEmpty()){
            return null;
        }
      
      


        return accList;
    }  
    global override List<Object> doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionId) {
        String accountId = searchFields.get('AccountId');
        String pType = searchFields.get('Product Type');
        String userId = UserInfo.getUserId();
        system.debug('map'+accountId);
        system.debug('accountId'+searchFields);
        system.debug('pType'+pType);
        
     	List<Profile> ps = [SELECT Id, Name FROM Profile WHERE Name = 'EP_Portal_Basic_Ordering_Access_User'];
		String PBOAUserId = ps.get(0).Id;
		List<User> us = [SELECT Id, Name, ProfileId, Contact.Id FROM User WHERE Id =: userID];
		String userProfileId = us.get(0).ProfileId;
        List<Contact> cs = [SELECT Id, Name, AccountId FROM Contact WHERE Id =: us.get(0).Contact.Id];
        
        List <Account> accList = new List<Account>();
        List<String> nonBulk = new List<String> {'Non-VMI Ship To','VMI Ship To'};
        if(pType!=null) {
            if(pType.equalsIgnoreCase('Bulk')) {
                accList= [SELECT id,name,ShippingPostalCode,ParentId,EP_Puma_Company__c,AccountNumber,RouteAllocationCount__c,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c 
                          FROM Account 
                          WHERE parentid =: accountId and recordtype.name ='Non-VMI Ship To' and EP_Status__c = '05-Active'];
            }
            else {
                accList= [SELECT id,name,RouteAllocationCount__c,ParentId,EP_Puma_Company__c,AccountNumber,ShippingPostalCode,ShippingState,ShippingCity,EP_Ship_To_Type__c,EP_Status__c 
                          FROM Account 
                          WHERE parentid =: accountId and recordtype.name in:nonBulk and EP_Status__c = '05-Active' and EP_Ship_To_Type__c != 'Consignment'];
            }
        }
        
        if(PBOAUserId == userProfileId){
		    for(Integer i = 0; i < accList.size();i++)
              if(accList[i].Id != cs.get(0).AccountId){
                  accList.remove(i);
              } 
        }
        
        if(accList == null && !accList.isEmpty()){
            return null;
        }
        
        return accList;
    }
    global override String getRequiredAttributes() {

        return '["AccountId","Product Type", "Order Epoch", "UserId"]';
    }    
}