/*
    @Author          Accenture
    @Name            EP_OrderImportHelper
    @CreateDate     
    @Description     This is a helper class for EP_OrderImportController class
    @Version         1.0
    @Reference       NA
*/
public with sharing class EP_OrderImportHelper {
    //TODO - Move Messages in Custom Label and Replace variable with Constant
    @testVisible private EP_OrderImportContext context;
    @testVisible private string hexDigest;
    @testVisible private list<SObject> sObjectList;
    @testVisible private map<integer,string> indexFieldAPIMap;
    @testVisible private list<EP_Import_File_Column_Mapping__mdt> listImportFileColumns;
    @testVisible private map<String, Schema.SobjectField> mapObjectFields;
    @testVisible private EP_File__c newfileRecord;
    
    public EP_OrderImportHelper(EP_OrderImportContext ctx) {
    	EP_GeneralUtility.Log('public','EP_OrderImportHelper','EP_OrderImportHelper');
    	init(ctx);
    	setObjectFieldMap();
    	getCSVColumnSquence();
    }
    
    public boolean hasValidFile() {
    	EP_GeneralUtility.Log('public','EP_OrderImportHelper','hasValidFile');
    	if(context.blbCSVFile == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, system.Label.EP_CSV_IMPORT_ERROR_MSG));
            return false;
		}
		
    	hexDigest = ConvertToHex(context.blbCSVFile);
    	if(hasDuplicateFile(hexDigest)) {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,system.Label.EP_CSV_File_Exists_Error_MSG));
    		return false;
    	}
    	return true;
    }
    
    /*
    This method will be used to process the CSV data and validate the CSV rows and column
    */
    public void processCSVData() {
    	EP_GeneralUtility.Log('public','EP_OrderImportHelper','processCSVData');
        context.strErrorMessage = '';
        context.CSVParseSuccess = false;
        context.strCSVFileContents = parseCSVforCommaAndNewline(context.blbCSVFile.toString());
        list<string> lCSVFileLines = context.strCSVFileContents.split(EP_Common_Constant.NEW_LINE);
        
        if(hasValidCSVRows(lCSVFileLines.size())) {
            parseCSVRows(lCSVFileLines);
        } else {
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, system.label.EP_HEADER_FILE_ERROR_MSG));
        }
    }
    
    public void processImportRequest() {
    	EP_GeneralUtility.Log('public','EP_OrderImportHelper','processImportRequest');
    	//File Duplicate Check
    	Savepoint sp = Database.setSavepoint();
    	try {
	    	if(context.CSVParseSuccess) {
	    		createStagingRecords(createFileRecord(hexDigest, context.strCSVFileName));
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, system.Label.EP_CSV_Parse_Success_MSG));
	    	}
    	} catch( exception exp) {
    		Database.rollback(sp);
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, system.Label.EP_CSV_IMPORT_ERROR_MSG + EP_Common_Constant.COLON_WITH_SPACE + exp.getMessage()));
    		system.debug('**Exception** ' + exp.getMessage()+' ***' + exp.getStackTraceString());
    	}
    }
    
    public void initDataValidation() {
    	EP_GeneralUtility.Log('public','EP_OrderImportHelper','initDataValidation');
    	//Call Future method for Data Validation
    	EP_ROImportStagingFactory.ProcessStagingRecords(newfileRecord.Id,'EP_Third_Party_Order');
    }
    
    
    @testVisible 
    private void init(EP_OrderImportContext ctx){
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','init');
    	this.context = ctx;
    	sObjectList = new list<SObject>();
    	indexFieldAPIMap = new map<integer,string>();
    	mapObjectFields = new Map<String, Schema.SobjectField>();
    	listImportFileColumns = new EP_CustomSettingMapper().getImportFileSetting();
    }
    
    /*
     This method will be used to map Field API Name with SObjectType for metaData field mapping
    */
    @testVisible 
    private void setObjectFieldMap() {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','setObjectFieldMap');
        Map<String, Schema.SobjectField> oldmapObjectFields = Schema.SObjectType.EP_RO_Import_Staging__c.fields.getMap();
        mapObjectFields = new Map<String, Schema.SobjectField>();
        for (string fieldName: oldmapObjectFields.keySet()){
            mapObjectFields.put(string.valueof(oldmapObjectFields.get(fieldName)), oldmapObjectFields.get(fieldName));
        }  
    }
    
    
    /*
    This method will be used to validate the CSV rows and and its column
    */
    @testVisible 
    private void parseCSVRows(list<string> lCSVFileLines) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','parseCSVRows');
        list<string> CSVHeader = lCSVFileLines[0].split( EP_Common_Constant.COMMA );
        if(hasValidColumnSquence(CSVHeader)) {
            parseCSVColumn(lCSVFileLines);
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, system.label.EP_CSV_Header_Error_MSG));
        }
    }
    
    /*
    This method will be used to validate the CSV columns for each row and wraped the data/column value into the stanging object fields
    */
    @testVisible 
    private void parseCSVColumn(  list<string> CSVFileLines) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','parseCSVColumn');
        list<string> columnValues;
        SObject sObj;
        sObjectList = new list<SObject>();
        Schema.SObjectType schemaSObjType = Schema.EP_RO_Import_Staging__c.sObjectType;
        for(Integer i = 1; i < CSVFileLines.size(); i++) {
            columnValues = CSVFileLines[i].split( EP_Common_Constant.COMMA );
            if(hasValidCSVColumn(columnValues.size())){
                sObj = schemaSObjType.newSObject();
                sObj.put(EP_Common_Constant.SPREADSHEET_FLD_APINAME, i);
                processCSVColumn(columnValues, i, sObj);
                sObjectList.add(sObj);
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Each line of the CSV file must have exactly ' + listImportFileColumns.size()  + ' columns. Please review the import file and try again.'));
                break;
            }
        }
    }
    
    /*
    This method will be used to wraped the data/column value into the stanging object fields
    */
    @testVisible 
    private void processCSVColumn(  list<string> lColumnValues, integer rowNumber, SObject sObj) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','processCSVColumn');
        for(Integer index : indexFieldAPIMap.keySet()) {
            try {
                setFieldValue(indexFieldAPIMap.get(index), parseText(lColumnValues[index].trim()), sObj);
                //context.strErrorMessage += indexFieldAPIMap.get(index) + ' = ' + parseText(lColumnValues[index].trim()) + '\n\n,';
                context.CSVParseSuccess = true;
            } catch (exception exp) {
                context.strErrorMessage += exp.getMessage() + '. (Row No# '+(rowNumber+1) +' and Column# ' + (index+1) + ')';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,context.strErrorMessage));
                context.CSVParseSuccess = false;
                break;
            }
        }   
    }
    
    /*
     This method will be used to transform the CSV column datatype into the Object Field's Data Type
    */
    @testVisible 
    private void setFieldValue(string strFieldName, string fieldValue, SObject sObj) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','setFieldValue');
        if(mapObjectFields.containsKey(strFieldName)){
            if (mapObjectFields.get(strFieldName).getDescribe().getType() == Schema.DisplayType.Date){
                sObj.put(strFieldName, date.parse(fieldValue));
            } else if(mapObjectFields.get(strFieldName).getDescribe().getType() == Schema.DisplayType.Double) {
                sObj.put(strFieldName, Double.valueOf(fieldValue));
            }else if(mapObjectFields.get(strFieldName).getDescribe().getType() == Schema.DisplayType.Integer) {
                sObj.put(strFieldName, Integer.valueOf(fieldValue));
            } else {
                sObj.put(strFieldName, fieldValue);
            }
        }
    }
    
    /*
    * @ Description: This method is to parse CSV for COMMA or NEW LINE character in data
    	This method will remove the COMMA and NEW LINE character if coming in any column value
    */
    @testVisible 
    private String parseCSVforCommaAndNewline( String strCSV ){
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','parseCSVforCommaAndNewline');
        integer foundStart = 0; 
        integer foundEnd = 0; 
        integer currentSpot = 0; 
        String strCSVtemp = strCSV;
        String strDoubleQuote = EP_Common_Constant.DOUBLE_QUOTES_STRING;
        do {    
            foundStart = strCSV.indexOf(strDoubleQuote, currentSpot); 
            if(foundStart >= 0){ 
                foundEnd = strCSV.indexOf(strDoubleQuote, foundStart + 1); 
                String Val = strCSV.substring(foundStart + 1, foundEnd ); 
                String ValProcessed = Val;
                if( String.isNotBlank( Val ) ){
                    ValProcessed = ValProcessed.remove(EP_Common_Constant.COMMA);
                    ValProcessed = ValProcessed.remove( EP_Common_Constant.NEW_LINE );
                    ValProcessed = ValProcessed.remove( EP_Common_Constant.CARRIAGE_RETURN );
                    strCSVtemp = strCSVtemp.replace(strDoubleQuote+Val+strDoubleQuote,ValProcessed);
                }
            }   
            currentSpot = foundEnd + 1;   
        } while (foundStart != -1); 
        return strCSVtemp;      
    }
    
    /*
    * @ Description: This method is to parse text in CSV coloumn
    */
    @testVisible 
    private String parseText(String strValue) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','parseText');
        if(EP_Common_Constant.NULL_VALUE.equalsIgnoreCase(strValue)){
                strValue = NULL;
        }
        return strValue;
    }
    
    @testVisible 
    private boolean hasValidCSVRows(integer numRows) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','hasValidCSVRows');
        if(numRows < 2) {
            return false;
        }
        return true;
    }
    
    @testVisible 
    private boolean hasValidCSVColumn(integer numCol) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','hasValidCSVColumn');
        if(numCol != listImportFileColumns.size()) {
            return false;            
        }
        return true;
    }
    
    @testVisible 
    private boolean hasValidColumnSquence( list<string> CSVHeader) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','hasValidColumnSquence');
        integer i = 0;
        boolean columnFound;
        for(string csvColumnHeader : CSVHeader){
        	columnFound = false;
            for(EP_Import_File_Column_Mapping__mdt columnName : listImportFileColumns) {
                if(columnName.CSV_Column_Name__c.trim().equalsignorecase(csvColumnHeader.trim())) {
                    indexFieldAPIMap.put(i,columnName.MasterLabel);
                	columnFound = true;
                }
             }
             if(!columnFound) {
             	return false;
             }
             i++;
         }
         return true;
    }
    
    @testVisible 
    private string ConvertToHex(Blob CSVData) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','ConvertToHex');
        Blob hash = Crypto.generateDigest(EP_Common_Constant.CRPTO_ALGO_SHA512,CSVData);
        return EncodingUtil.convertToHex(hash);
    } 
    
    @testVisible 
    private boolean hasDuplicateFile(string hexDigest) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','hasDuplicateFile');
		list<EP_File__c> existingFiles  = new EP_ROImportFileMapper().getRecordsByFileNameAndCheckSumKey(context.strCSVFileName, hexDigest);
		/*if(existingFiles.size() > 0) {
			return true;
		}*/
		return false;
    }
    
    @testVisible
    private EP_File__c createFileRecord(string hexDigest, string fileName) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','createFileRecord');
    	newfileRecord = new EP_File__c();
    	newfileRecord.Name = fileName;
    	newfileRecord.EP_CheckSum_Key__c = hexDigest;
    	newfileRecord.EP_Company__c = context.ctxfileRecord.EP_Company__c;
    	newfileRecord.EP_Status__c = EP_Common_Constant.STATUS_UNPROCESSED;
    	database.insert(newfileRecord);
    	return newfileRecord;
    }
    
    @testVisible 
    private void createStagingRecords(EP_File__c fileRecord) {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','createFileRecord');
		//Create Staging Records
		EP_File__c fRecord = new EP_ROImportFileMapper().getFileRecordById(fileRecord.Id);
		Id recordTypeID = new EP_ROImportStagingMapper().getRecordTypeIdByName(EP_Common_Constant.THIRD_PARTY_ORDER_RT_NAME);
		for(SObject sobj: sObjectList) {
			sobj.put(EP_Common_Constant.FILE_NAME_FLD_APINAME,fileRecord.id);
			sobj.put(EP_Common_Constant.COMPANY_CODE_FLD_APINAME,fRecord.EP_Company_Code__c);
			sobj.put(EP_Common_Constant.REC_TYPE_ID,recordTypeID);
		}
		database.insert(sObjectList);
    	
    }
    
	/*
    	This method will be used to display the Column sequence at VF Page in message box 
    */
    @testVisible 
    private void getCSVColumnSquence() {
    	EP_GeneralUtility.Log('private','EP_OrderImportHelper','getCSVColumnSquence');
        context.strCSVColumnSquence = system.label.EP_CSV_Column_Sequence_MSG;
        for(EP_Import_File_Column_Mapping__mdt columnName : listImportFileColumns) {
            context.strCSVColumnSquence += columnName.CSV_Column_Name__c+ EP_Common_Constant.COMMA;
        }
    }
    
}