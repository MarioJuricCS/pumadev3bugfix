@IsTest
public class TrafiguraConfigurationControllerNewTest {



    @testSetup
    static void prepareData() {

        Account testBasketAccount = new Account();
        testBasketAccount.Name = 'testAccount';
        testBasketAccount.EP_CRM_Geo1__c='NA';
        testBasketAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        INSERT testBasketAccount;

        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c();
        testBasket.csordtelcoa__Account__c = testBasketAccount.Id;
        INSERT testBasket;

        cscfga__Product_Configuration__c testProductConfiguration = new cscfga__Product_Configuration__c();
        testProductConfiguration.Name = 'Puma Energy Order Line';
        testProductConfiguration.cscfga__Product_Basket__c = testBasket.Id;
        INSERT testProductConfiguration;

        cscfga__Attribute__c testAttribute = new cscfga__Attribute__c();
        testAttribute.Name = 'inventoryCheck';
        testAttribute.cscfga__Product_Configuration__c = testProductConfiguration.Id;
        INSERT testAttribute;

        cscfga__Product_Basket__c testBasket1 = new cscfga__Product_Basket__c();
        INSERT testBasket1;

        cscfga__Product_Configuration__c testProductConfiguration1 = new cscfga__Product_Configuration__c();
        testProductConfiguration1.Name = 'Puma Energy Order Line';
        testProductConfiguration1.cscfga__Product_Basket__c = testBasket1.Id;
        INSERT testProductConfiguration1;

        cscfga__Attribute__c testAttribute1 = new cscfga__Attribute__c();
        testAttribute1.Name = 'inventoryCheck';
        testAttribute1.cscfga__Value__c = 'Test';
        testAttribute1.cscfga__Product_Configuration__c = testProductConfiguration1.Id;
        INSERT testAttribute1;

        // BusinessHours bhrs = [Select Name from BusinessHours where IsActive =true AND IsDefault = true limit 1];

        Company__c testCompany = new Company__c();
        testCompany.Name = 'testCompany';
        testCompany.EP_Combined_Invoicing__c = 'Delegate to Customer';
        testCompany.EP_Company_Id__c = 1234;
        testCompany.EP_Company_Code__c = 'Test code';
        testCompany.EP_Window_Start_Hours__c = 8;
        testCompany.KYC_limit_in_KUSD__c = '0';
        INSERT testCompany;

        Account testAccount = new Account();
        testAccount.Name = 'testAccount';
        testAccount.EP_Puma_Company__c = testCompany.Id;
        testAccount.EP_CRM_Geo1__c='NA';
        testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        INSERT testAccount;

        Id recordTypeVendor = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();

        Account testTransporterAccount = new Account();
        testTransporterAccount.Name = 'testTransporterAccount';
        testTransporterAccount.RecordTypeId = recordTypeVendor;
        testTransporterAccount.EP_Status__c = '05-Active';
        testTransporterAccount.EP_VendorType__c = 'Transporter';
        
        INSERT testTransporterAccount;

        EP_Stock_Holding_Location__c testHoldingLocation = new EP_Stock_Holding_Location__c();
        testHoldingLocation.EP_Transporter__c = testTransporterAccount.Id;
        INSERT testHoldingLocation;

        csord__Order__c testOrder = new csord__Order__c();
        testOrder.csord__Identification__c = 'TestIdentification';
        testOrder.EP_Delivery_Type__c = 'Delivery';
        testOrder.Delivery_From_Date__c = System.Today();
        testOrder.Delivery_To_Date__c = System.Today();
        testOrder.csord__Account__c = testAccount.Id;
        testOrder.Stock_Holding_Location__c = testHoldingLocation.Id;
        INSERT testOrder;
    }

    @isTest
    private static void testCloseWinBasket() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        TrafiguraConfigurationControllerNew.closeWinBasket(testBasket[0].Id);
    }
    
     

    @isTest
    private static void testSaveBasket() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        cscfga__Product_Configuration__c testProductConfiguration = getPC();
        TrafiguraConfigurationControllerNew.saveBasket(testBasket[0].Id, testProductConfiguration.Id);
    }

    @isTest
    private static void testCheckInventory() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        TrafiguraConfigurationControllerNew.checkInventory(testBasket[0].Id);
        TrafiguraConfigurationControllerNew.checkInventory(testBasket[1].Id);
    }

    @isTest
    private static void testProcessCutOffMatrixCheckDelivery() {
        csord__Order__c testOrder = getOrder();
        testOrder.EP_Requested_Delivery_Date__c = Date.today() + 1;
        update testOrder;

        Test.startTest();
        TrafiguraConfigurationControllerNew.processCutOffMatrixCheck(testOrder.Id, '', '', '');
        Test.stopTest();
    }

    @isTest
    private static void testCheckCutOffMatrix() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        EP_Stock_Holding_Location__c testSiteLocation = getSiteLocation();
        Company__c testCompany = getCompany();
        TrafiguraConfigurationControllerNew.checkCutOffMatrix(testBasket[0].Id, '', '', '', '', 'Test,Test-Test', testSiteLocation.Id, testCompany.Id, '');
    }

    @isTest
    private static void testgetAllConfiguratorDetails() {
        cscfga__Product_Configuration__c pc = getPC();
        csord__Order__c testOrder = getOrder();
        testOrder.csordtelcoa__product_configuration__c = pc.Id;
        update testOrder;
        String configId = String.valueOf(pc.Id);
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c();
        String proddefin= String.valueOf(prodDef.Id);

        
        Test.startTest();
       //TrafiguraConfigurationControllerNew.getAllConfiguratorDetails(configId,proddefin);
        Test.stopTest();
    }
    
    @isTest
    static void testcloneOrderWebservice() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        csord__Order__c testOrder = getOrder();
        testOrder.csord__Identification__c = testBasket[0].Id;
        update testOrder;
        system.debug('testOrder'+testOrder);

        cscfga__Product_Configuration__c pc = getPC();
        pc.cscfga__product_basket__c = testBasket[0].Id;
        update pc;

        Attachment att = new Attachment();
        att.ParentId = pc.Id;
        att.Name = 'Puma Energy Order Line_schema.json';
        att.body = Blob.valueOf('Test');
        insert att;
        
        Test.startTest();
        String result = TrafiguraConfigurationControllerNew.cloneOrderWebservice(testOrder.Id);
        Test.stopTest();

        //System.assertEquals(true, result, 'Result was not as expected.');
    }
    
    @isTest
    static void testUpdateCancellationCheckOnOrder() {
        String testString = 'Test';

        Test.startTest();
        Boolean result = TrafiguraConfigurationControllerNew.updateCancellationCheckOnOrder(testString);
        Test.stopTest();

        System.assertEquals(true, result, 'Result was not as expected.');
    }

    @isTest
    static void testgetPaymentTerms() {
         
        csord__Order__c testOrder = getOrder();
        update testOrder;
        String accountid=String.valueOf(testOrder.csord__Account__c);
        
        Test.startTest();
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testBasket[0]);
        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew(stdCtrl);
        controller.getPaymentTerms(accountid);
        Test.stopTest();
    }
    //===========================
    
    @isTest
    static void testGetOrderStatus() {
        cscfga__Product_Configuration__c pc = getPC();
        csord__Order__c testOrder = getOrder();
        testOrder.csordtelcoa__product_configuration__c = pc.Id;
        update testOrder;

        Test.startTest();
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testBasket[0]);
        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew(stdCtrl);
        controller.getOrderStatus(pc.Id);
        Test.stopTest();
    }

    @isTest
    static void testGetOrderStatusNegativeCase() {
        cscfga__Product_Configuration__c pc = getPC();

        Test.startTest();
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(testBasket[0]);
        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew(stdCtrl);
        controller.getOrderStatus(pc.Id);
        Test.stopTest();
    }
    
    @isTest
    static void testGetOrderStatusNoConstructorParameters() {
        cscfga__Product_Configuration__c pc = getPC();
        csord__Order__c testOrder = getOrder();
        testOrder.csordtelcoa__product_configuration__c = pc.Id;
        update testOrder;
        
        PageReference portalPage = Page.EP_FE_Portal;
        Test.setCurrentPage(portalPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('configId', pc.Id);
        
        Test.startTest();
        
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        
        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew();
        controller.getOrderStatus(pc.Id);
        
        Test.stopTest();
    }
    
    @isTest
    static void testGetOrderStatusUISupportConstructorParameters() {
        cscfga__Product_Configuration__c pc = getPC();
        csord__Order__c testOrder = getOrder();
        testOrder.csordtelcoa__product_configuration__c = pc.Id;
        update testOrder;
        
        PageReference portalPage = Page.EP_FE_Portal;
        Test.setCurrentPage(portalPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('configId', pc.Id);

        cscfga.UISupport uiController = new cscfga.UISupport();
        
        Test.startTest();
        
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        
        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew(uiController);
        controller.getOrderStatus(pc.Id);
        
        Test.stopTest();
    }

    @isTest
    static void testGetOrderStatusInitFromParameters() {
        
        cscfga__Product_Configuration__c pc = getPC();
        csord__Order__c testOrder = getOrder();
        testOrder.csordtelcoa__product_configuration__c = pc.Id;
        update testOrder;
        
        PageReference portalPage = Page.EP_FE_Portal;
        Test.setCurrentPage(portalPage);
        
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('configId', pc.Id);

        Test.startTest();
        
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        
        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew();
        controller.initFromParameters();
        
        Test.stopTest();
    }

    @isTest
    static void testIsEditModeNegative() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();

        Test.startTest();
        Boolean result = TrafiguraConfigurationControllerNew.IsEditMode(testBasket[0].Id);
        Test.stopTest();

        System.assertEquals(false, result, 'Result not as expected.');
    }

    @isTest
    static void testCancelOrder() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();

        Test.startTest();
        String result = TrafiguraConfigurationControllerNew.cancelOrder(testBasket[0].Id);
        Test.stopTest();

        System.assertEquals('nok,NULL', result, 'Result not as expected.');
    }

    @isTest
    static void testCancelOrderNegative() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        csord__Order__c testOrder = getOrder();
        testOrder.csord__Identification__c = testBasket[0].Id;
        update testOrder;

        Test.startTest();
        String result = TrafiguraConfigurationControllerNew.cancelOrder(testBasket[0].Id);
        Test.stopTest();

        System.assertEquals('ok,' + testOrder.Id, result, 'Result not as expected.');
    }

    @isTest
    static void testGetPrimarySupplyLocationNegative() {
        csord__Order__c testOrder = getOrder();
        String shipTo = String.valueOf(testOrder.csord__Account__c);

        Test.startTest();
        String result = TrafiguraConfigurationControllerNew.getPrimarySupplyLocation(shipTo);
        Test.stopTest();

        System.assertEquals('none', result, 'Result not as expected.');
    }

    @isTest
    static void testSendWarningEmail() {
        String msg = 'Test';

        Test.startTest();
        String result = TrafiguraConfigurationControllerNew.sendWarningEmail(msg);
        Test.stopTest();

        System.assertEquals('200', result, 'Result not as expected.');
    }

    @isTest
    static void testUpdateCreditHold() {
        csord__Order__c testOrder = getOrder();
         system.debug('testOrder'+testOrder);
        Test.startTest();
        //String result = TrafiguraConfigurationControllerNew.updateCreditHold(testOrder.csord__Account__c);
        Test.stopTest();

        //System.assertEquals('null,null', result, 'Result not as expected.');
    }

    @isTest
    static void testCloneBasket() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();

        Test.startTest();
        TrafiguraConfigurationControllerNew.cloneBasket(testBasket[0].Id);
        Test.stopTest();
    }

    @isTest
    static void testNullifyAttributesOrder() {
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c();
        prodDef.Name = 'Puma Energy Order';
        prodDef.cscfga__Description__c = 'Test';
        insert prodDef;

        List<cscfga__Product_Basket__c> testBasket = getBasket();
        cscfga__Product_Configuration__c pc = getPC();
        pc.cscfga__product_basket__c = testBasket[0].Id;
        pc.cscfga__product_definition__c = prodDef.Id;
        update pc;

        Test.startTest();
        TrafiguraConfigurationControllerNew.nullifyAttributes(testBasket[0].Id, true);
        Test.stopTest();
    }

    @isTest
    static void testNullifyAttributesLineItem() {
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c();
        prodDef.Name = 'Puma Energy Order Line';
        prodDef.cscfga__Description__c = 'Test';
        insert prodDef;

        List<cscfga__Product_Basket__c> testBasket = getBasket();
        cscfga__Product_Configuration__c pc = getPC();
        pc.cscfga__product_basket__c = testBasket[0].Id;
        pc.cscfga__product_definition__c = prodDef.Id;
        update pc;

        Test.startTest();
        TrafiguraConfigurationControllerNew.nullifyAttributes(testBasket[0].Id, true);
        Test.stopTest();
    }

    @isTest
    private static void testCloneOrder() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        csord__Order__c testOrder = getOrder();
        testOrder.csord__Identification__c = testBasket[0].Id;
        update testOrder;
        system.debug('testOrder'+testOrder);

        cscfga__Product_Configuration__c pc = getPC();
        pc.cscfga__product_basket__c = testBasket[0].Id;
        update pc;

        Attachment att = new Attachment();
        att.ParentId = pc.Id;
        att.Name = 'Puma Energy Order Line_schema.json';
        att.body = Blob.valueOf('Test');
        insert att;

        Test.startTest();
        TrafiguraConfigurationControllerNew.cloneOrder(testOrder.Id);
        Test.stopTest();
    }

    @isTest
    private static void testCloneLastOrder() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        csord__Order__c testOrder = getOrder();
        testOrder.csord__Identification__c = testBasket[0].Id;
        update testOrder;

        cscfga__Product_Configuration__c pc = getPC();
        pc.cscfga__product_basket__c = testBasket[0].Id;
        update pc;

        Attachment att = new Attachment();
        att.ParentId = pc.Id;
        att.Name = 'Puma Energy Order Line_schema.json';
        att.body = Blob.valueOf('Test');
        insert att;

        Test.startTest();
        TrafiguraConfigurationControllerNew.cloneLastOrder(testOrder.ownerId, testOrder.csord__Account__c);
        Test.stopTest();
    }


    @isTest
    static void testFetchOrderLineItems() {
        List<cscfga__Product_Basket__c> testBasket = getBasket();

        Test.startTest();
        TrafiguraConfigurationControllerNew.fetchOrderLineItems(testBasket[0].Id);
        Test.stopTest();
    }

    @isTest
    static void testCreateInventoryAction() {
        csord__Order__c testOrder = getOrder();

        Test.startTest();
        TrafiguraConfigurationControllerNew.createInventoryAction(testOrder.Id, testOrder.csord__Account__c);
        Test.stopTest();
    }

    @isTest
    static void testUpdateOrder() {
        csord__Order__c testOrder = getOrder();
        cscfga__Product_Configuration__c pc = getPC();

        Test.startTest();
        TrafiguraConfigurationControllerNew.updateOrder(pc.Id, testOrder);
        Test.stopTest();
    }

    @isTest
    static void testCopyAttributeValuesToOrder() {
        csord__Order__c testOrder = getOrder();
        cscfga__Product_Configuration__c pc = getPC();
        Map<String, String> attributeNameValueMap = TrafiguraConfigurationControllerNew.getAttributes(pc.Id);

        Test.startTest();
        TrafiguraConfigurationControllerNew.copyAttributeValuesToOrder(testOrder, attributeNameValueMap);
        Test.stopTest();
    }
    
    @IsTest
    private static void generateQuote() {
        
        List<cscfga__Product_Basket__c> testBasket = getBasket();

        Test.startTest();
        
        String retval = TrafiguraConfigurationControllerNew.generateQuote(testBasket[0].Id);
        
        Test.stopTest();
        
        System.assertNotEquals(null, retval, 'Invalid data');
    }

    @IsTest
    private static void InventoryReview(){

        csord__Order__c testOrder = getOrder();
        
        List<csord__Order_Line_Item__c> csOrderLineItems = [SELECT Id, Name FROM csord__Order_Line_Item__c WHERE csord__Order__c = :testOrder.Id];

        Test.startTest();
        
        TrafiguraConfigurationControllerNew.InventoryReview(testOrder.Id, csOrderLineItems);
        
        Test.stopTest();
    }
    
    @IsTest
    private static void basketIdPrefixTest() {

        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew();

        Test.startTest();
        
        String basketIdPrefix = controller.basketIdPrefix;
        
        Test.stopTest();
        
        System.assertNotEquals(null, basketIdPrefix, 'Invalid data');
    }

    @IsTest
    private static void accountIdPrefixTest() {

        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew();

        Test.startTest();
        
        String accountIdPrefix = controller.accountIdPrefix;
        
        Test.stopTest();

        System.assertNotEquals(null, accountIdPrefix, 'Invalid data');
    }

    @IsTest
    private static void retUrlOverrideTest() {
        
        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew();

        Test.startTest();
        
        String retUrlOverride = controller.retUrlOverride;
        
        Test.stopTest();
        
        System.assertNotEquals(null, retUrlOverride, 'Invalid data');
    }
    
    @IsTest
    private static void sitePrefixTest() {
        
        TrafiguraConfigurationControllerNew controller = new TrafiguraConfigurationControllerNew();

        Test.startTest();
        
        String sitePrefix = controller.sitePrefix;
        
        Test.stopTest();
        
        System.assertNotEquals(null, sitePrefix, 'Invalid data');
    }

    /*
    @isTest
    private static void testGetAttributesCSOE() {
        
        List<cscfga__Product_Basket__c> testBasket = getBasket();
        csord__Order__c testOrder = getOrder();
        testOrder.csord__Identification__c = testBasket[0].Id;
        update testOrder;

        cscfga__Product_Configuration__c pc = getPC();
        pc.cscfga__product_basket__c = testBasket[0].Id;
        update pc;

        Attachment att = new Attachment();
        att.ParentId = pc.Id;
        att.Name = 'Puma Energy Order Line_schema.json';
        att.body = Blob.valueOf('{}');
        insert att;

        Test.startTest();
        
        TrafiguraConfigurationControllerNew.getAttributesCSOE(pc.Id);
        
        Test.stopTest();
        
        
    }*/
    
    private static String getproddefin() {
        String id;
        cscfga__Product_Definition__c testdef =
            [
                SELECT  Id
                FROM    cscfga__Product_Definition__c
                LIMIT   1
            ];
        id=String.valueOf(testdef.id);
        return id;
    }
    
    private static List<cscfga__Product_Basket__c> getBasket() {

        List<cscfga__Product_Basket__c> testBasketList =
            [
                SELECT  Id
                FROM    cscfga__Product_Basket__c
            ];

        return testBasketList;
    }
    
    private static String getPC1() {
         String PCID;
        cscfga__Product_Configuration__c testProductConfiguration =
            [
                SELECT  Id
                FROM    cscfga__Product_Configuration__c
                LIMIT   1
            ];
        PCID=String.valueOf(testProductConfiguration.id);
        return PCID;
    }

    private static cscfga__Product_Configuration__c getPC() {

        cscfga__Product_Configuration__c testProductConfiguration =
            [
                SELECT  Id
                FROM    cscfga__Product_Configuration__c
                LIMIT   1
            ];

        return testProductConfiguration;
    }

    private static csord__Order__c getOrder() {

        csord__Order__c testOrder =
            [
                SELECT  Id, ownerId, csord__Account__c, Delivery_From_Date__c, Delivery_To_Date__c
                FROM    csord__Order__c
                LIMIT   1
            ];

        return testOrder;
    }

    private static EP_Stock_Holding_Location__c getSiteLocation() {

        EP_Stock_Holding_Location__c testSiteLocation =
            [
                SELECT  Id
                FROM    EP_Stock_Holding_Location__c
                LIMIT   1
            ];

        return testSiteLocation;
    }

    private static Company__c getCompany() {

        Company__c testCompany =
            [
                SELECT  Id
                FROM    Company__c
                LIMIT   1
            ];

        return testCompany;
    }
}