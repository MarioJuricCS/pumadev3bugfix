/*   
     @Author <Accenture>
     @name <EP_FE_AccountStatementEndpointTest .cls>     
     @Description <This class is the Test Class for EP_FE_AccountStatementEndpoint>   
     @Version <1.1> 
*/
@isTest
private class EP_FE_AccountStatementEndpointTest {
	
	private static User sysAdmUser = EP_FE_TestDataUtility.getRunAsUser();

/*********************************************************************************************
    *@Description : This is the Test method for list of custom                   
    *@Params      :                    
    *@Return      : Void                                                                             
**********************************************************************************************/   
    static testMethod void testlistCustomAccountStatement3() {
        
        EP_FE_AccountStatementEndpoint obj = new EP_FE_AccountStatementEndpoint();
        EP_FE_AccountStatementRequest obj1 = new EP_FE_AccountStatementRequest();
        //Account bilTo =  EP_TestDataUtility.createBillToAccount(); 
       
        obj1.dateFrom = system.today();
        obj1.dateTo = system.today() + 7;
        obj1.type = null;
        obj1.billToId = 'test'; 
         
        Test.startTest();
        System.RunAs(sysAdmUser){
            EP_FE_AccountStatementEndpoint.listCustomAccountStatement(obj1);
        }
        Test.stopTest();    
    }

/*********************************************************************************************
    *@Description : This is the Test method for list of custom                   
    *@Params      :                    
    *@Return      : Void                                                                             
*********************************************************************************************/   
    static testMethod void testlistCustomAccountStatement() {
        
        EP_FE_AccountStatementEndpoint obj = new EP_FE_AccountStatementEndpoint();
        EP_FE_AccountStatementRequest obj1 = new EP_FE_AccountStatementRequest();
        Account bilTo =  EP_TestDataUtility.createBillToAccount(); 
       
        obj1.dateFrom = system.today();
        obj1.dateTo = system.today() + 10;
        obj1.type = null;
        obj1.billToId = bilTo.Id; 
         
        Test.startTest();
        System.RunAs(sysAdmUser){
            EP_FE_AccountStatementEndpoint.listCustomAccountStatement(obj1);
        }
        Test.stopTest();    
    }
/*********************************************************************************************
    *@Description : This is the Test method for list of custom                   
    *@Params      :                    
    *@Return      : Void                                                                             
*********************************************************************************************/   
    static testMethod void testlistCustomAccountStatementBlank() {
        
        EP_FE_AccountStatementEndpoint obj = new EP_FE_AccountStatementEndpoint();
        EP_FE_AccountStatementRequest obj1 = new EP_FE_AccountStatementRequest();
        obj1 = null;
        
        Test.startTest();
        System.RunAs(sysAdmUser){
            EP_FE_AccountStatementEndpoint.listCustomAccountStatement(obj1);
        }
        Test.stopTest();    
    }

/*********************************************************************************************
    *@Description : This is the Test method for list of custom                   
    *@Params      :                    
    *@Return      : Void                                                                             
**********************************************************************************************/   
    static testMethod void testlistCustomAccountStatementBlank1() {
        
        EP_FE_AccountStatementEndpoint obj = new EP_FE_AccountStatementEndpoint();
        EP_FE_AccountStatementRequest obj1 = new EP_FE_AccountStatementRequest();
        
        obj1.dateFrom =null;
        obj1.dateTo = null;
        obj1.type = 'test';
        obj1.billToId = 'test';
        
        Test.startTest();
        System.RunAs(sysAdmUser){
            EP_FE_AccountStatementEndpoint.listCustomAccountStatement(obj1);
        }
        Test.stopTest();        
    }
    
/*********************************************************************************************
    *@Description : This is the Test method for list of custom                   
    *@Params      :                    
    *@Return      : Void                                                                             
*********************************************************************************************/   
    static testMethod void testlistCustomAccountStatement2() {
        
        EP_FE_AccountStatementEndpoint obj = new EP_FE_AccountStatementEndpoint();
        EP_FE_AccountStatementRequest obj1 = new EP_FE_AccountStatementRequest();
        //Account bilTo =  EP_TestDataUtility.createBillToAccount(); 
        
        obj1.dateFrom = system.today();
        obj1.dateTo = system.today()+ 180;
        obj1.type = 'test';
        obj1.billToId = 'test';
        
        Test.startTest();
        System.RunAs(sysAdmUser){
            EP_FE_AccountStatementEndpoint.listCustomAccountStatement(obj1);
        }
        Test.stopTest();    
    } 

/*********************************************************************************************
    *@Description : This is the Test method for list of custom                   
    *@Params      :                    
    *@Return      : Void                                                                             
*********************************************************************************************/   
    static testMethod void testlistCustomAccountStatement4() {
        
        EP_FE_AccountStatementEndpoint obj = new EP_FE_AccountStatementEndpoint();
        EP_FE_AccountStatementRequest obj1 = new EP_FE_AccountStatementRequest();
        Account billTo =  EP_TestDataUtility.createBillToAccount(); 
        Database.insert(billto);
        
        obj1.dateFrom = system.today();
        obj1.dateTo = system.today();
        obj1.type = null;
        obj1.billToId = billTo.Id;
        
        Test.startTest();
        System.RunAs(sysAdmUser){
            EP_FE_AccountStatementEndpoint.listCustomAccountStatement(obj1);
        }
        Test.stopTest();    
    } 

/*********************************************************************************************
    *@Description : This is the Test method for list of custom                   
    *@Params      :                    
    *@Return      : Void                                                                             
*********************************************************************************************/   
    static testMethod void testlistCustomAccountStatement5() {
        
        EP_FE_AccountStatementEndpoint obj = new EP_FE_AccountStatementEndpoint();
        EP_FE_AccountStatementRequest obj1 = new EP_FE_AccountStatementRequest();
        Account billTo =  EP_TestDataUtility.createBillToAccount(); 
        Database.insert(billto);
        
        obj1.dateFrom = system.today();
        obj1.dateTo = system.today();
        obj1.type = null;
        obj1.billToId = 'test';
        
        Test.startTest();
        System.RunAs(sysAdmUser){
            EP_FE_AccountStatementEndpoint.listCustomAccountStatement(obj1);
        }
        Test.stopTest();    
    } 
}