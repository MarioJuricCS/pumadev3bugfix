@isTest
private class EP_OrderSyncWithNAVXML_UT {
    
    private static final string MESSAGE_TYPE = 'SFDC_TO_NAV_ORDER_SYNC'; 
    @testSetup
    public static void init(){
        Test.loadData(EP_CS_OutboundMessageSetting__c.sObjectType, 'EP_CS_OutboundMessageSettingTestData');
    	Test.loadData(EP_CS_Communication_Settings__c.sObjectType, 'EP_CS_CommunicationSettingTestData');
    }
     
   static testMethod void createXML_Test() {
       EP_OrderSyncWithNAVXML localObj = new EP_OrderSyncWithNAVXML();
       localObj.OrderObj = EP_TestDataUtility.getTransferOrder();
       localObj.recordid = localObj.OrderObj.id;
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       String result = localObj.createXML();  
       Test.stopTest();
       System.assert(!String.isBlank(result));
   }

  static testMethod void createPayload_Test() {
       EP_OrderSyncWithNAVXML localObj = new EP_OrderSyncWithNAVXML();
       //localObj.orderObj = EP_TestDataUtility.getTransferOrderWithInvoiceLineItem(); //Accenture CCT, 8/28/2018
       
       EP_OrderDomainObject odo = EP_TestDataUtility.getNonVMINonConsignmentOrderPositiveScenario();  //Accenture CCT, 8/28/2018
       localObj.orderObj = [SELECT Id FROM csord__Order__c LIMIT 1];//Accenture CCT, 8/28/2018
       localObj.recordid = localObj.OrderObj.id;
       localObj.orderObj.EP_OnOff_Run__c = EP_OrderConstant.ON_STR; //Accenture CCT, 8/28/2018
       update localObj.orderObj;//Accenture CCT, 8/28/2018
       List<csord__Order_Line_Item__c> oliList = new  List<csord__Order_Line_Item__c>();//Accenture CCT, 8/30/2018
       //Accenture CCT, 8/30/2018
       Integer num = 12345;
       for(csord__Order_Line_Item__c oli : [SELECT Id, EP_WinDMS_Line_Item_Reference_Number__c FROM csord__Order_Line_Item__c]) {
       oli.EP_WinDMS_Line_Item_Reference_Number__c = String.valueOf(num + 1);
       oliList.add(oli);
       }
       update oliList;
       localObj.setOrderLineItems();
       
       
       //localObj.orderLineItemsWrapper[0].orderLineItem = [SELECT Id, EP_WinDMS_Line_Item_Reference_Number__c FROM csord__Order_Line_Item__c LIMIT 1];
       
       localObj.messageId = EP_Common_Constant.TEMPSTRINGWITHHYPHEN;
       localObj.messageType = MESSAGE_TYPE;
       Test.startTest();
       localObj.init();
       localObj.createPayload();  
       Test.stopTest();
       //no assert needed. Value are only being set
   }   
}