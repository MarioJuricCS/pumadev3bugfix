/* 
   @Author 			Accenture
   @name 			EP_CustomerOtherAdjustmentMapper_UT
   @CreateDate 		05/05/2017
   @Description		Test class for EP_CustomerOtherAdjustmentMapper
   @Version 		1.0
*/
@isTest
public class EP_CustomerOtherAdjustmentMapper_UT {
	
	/**
	* @author 			Accenture
	* @name				getCustomerOtherAdjustmentRecordsByUniqueId_test
	* @date 			02/08/2017
	* @description 		Test method used to get the customer adjustment records by unique Id
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void getCustomerOtherAdjustmentRecordsByUniqueId_test() {
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		EP_Customer_Other_Adjustment__c custAdjust = EP_TestDataUtility.createCustomerOtherAdjustment(sellTo.Id,paymentTerm.Id,sellTo.EP_Puma_Company_Code__c);
		Database.insert(custAdjust);
		
		EP_Customer_Other_Adjustment__c custAdj =[select id,Name,EP_Client_ID__c,EP_Bill_To__r.AccountNumber,EP_Entry_Number__c,EP_Adjustment_Unique_Key__c from EP_Customer_Other_Adjustment__c limit 1];
		system.debug('**custAdj**' +custAdj);
		Set<string> setOfCustomerAdjustments = new Set<string>();
		setOfCustomerAdjustments.add((custAdj.EP_Client_ID__c+ EP_Common_Constant.UNDERSCORE_STRING + custAdj.EP_Bill_To__r.AccountNumber + EP_Common_Constant.UNDERSCORE_STRING + custAdj.Name + EP_Common_Constant.UNDERSCORE_STRING + custAdj.EP_Entry_Number__c).toUpperCase());
		system.debug('**setOfCustomerAdjustments**' +setOfCustomerAdjustments);
		EP_CustomerOtherAdjustmentMapper localObj = new EP_CustomerOtherAdjustmentMapper();
		Test.startTest();
		List<EP_Customer_Other_Adjustment__c> result = localObj.getCustomerOtherAdjustmentRecordsByUniqueId(setOfCustomerAdjustments);
		Test.stopTest();
		System.AssertEquals(1, result.size());
	}
	
	/**
	* @author 			Accenture
	* @name				getCustomerOtherAdjustmentMapByUniqueId_test
	* @date 			02/08/2017
	* @description 		Test method used to get the customer adjustment map by unique Id
	* @param 			NA
	* @return 			NA
	*/
	static testMethod void getCustomerOtherAdjustmentMapByUniqueId_test() {
		Account sellTo = EP_TestDataUtility.getSellToPositiveScenario();
		EP_Payment_Term__c paymentTerm = [select Id from EP_Payment_Term__c limit 1];
		EP_Customer_Other_Adjustment__c custAdjust = EP_TestDataUtility.createCustomerOtherAdjustment(sellTo.Id,paymentTerm.Id,sellTo.EP_Puma_Company_Code__c);
		Database.insert(custAdjust);
		
		EP_Customer_Other_Adjustment__c custAdj =[select id,Name,EP_Client_ID__c, EP_Bill_To__r.AccountNumber,EP_Entry_Number__c,EP_Adjustment_Unique_Key__c from EP_Customer_Other_Adjustment__c limit 1];
		system.debug('**custAdj**' +custAdj);
		Set<string> setOfCustomerAdjustments = new Set<string>();
		setOfCustomerAdjustments.add((custAdj.EP_Client_ID__c+ EP_Common_Constant.UNDERSCORE_STRING +custAdj.EP_Bill_To__r.AccountNumber + EP_Common_Constant.UNDERSCORE_STRING + custAdj.Name+ EP_Common_Constant.UNDERSCORE_STRING + custAdj.EP_Entry_Number__c).toUpperCase());
		system.debug('**setOfCustomerAdjustments**' +setOfCustomerAdjustments);
		EP_CustomerOtherAdjustmentMapper localObj = new EP_CustomerOtherAdjustmentMapper();
		Test.startTest();
		Map<String,EP_Customer_Other_Adjustment__c> result = localObj.getCustomerOtherAdjustmentMapByUniqueId(setOfCustomerAdjustments);
		Test.stopTest();
		System.AssertEquals(1, result.size());
	}
    
}